/*
 This file is generated and updated by Sencha Cmd. You can edit this file as
 needed for your application, but these edits will have to be merged by
 Sencha Cmd when it performs code generation tasks such as generating new
 models, controllers or views and when running "sencha app upgrade".

 Ideally changes to this file would be limited and most work would be done
 in other places (such as Controllers). If Sencha Cmd cannot merge your
 changes and its generated code, it will produce a "merge conflict" that you
 will need to resolve manually.
 */


Ext.Loader.setPath({
  'Ext.ux.M1': './ux/M1',
  'M1CRM.model': 'app/model/',
  'M1CRM': 'app'
});


Ext.application({
  name: 'M1CRM',
  config: {
    SessionObj: '',
    AddEditQuote: null
  },
  requires: [
    'Ext.MessageBox',
    'Ext.form.FieldSet',
    'Ext.ux.TabMenuButton',
    'M1CRM.model.crmSession',
    'M1CRM.model.crmServiceAccessModel',
    'M1CRM.util.crmCustomerControls',
    'M1CRM.util.crmViews'
  ],
  models: ['crmLogin', 'crmPriority'],
  store: [],
  controllers: [
    'crmLogin',
    'crmHome',

    'crmMyFollowups',
    'crmAllFollowups',
    'crmAllOpenFollowups',
    'crmCloseFollowups',
    'crmFollowupAddEdit',

    'crmMyOpenCalls',
    'crmAllOpenCalls',
    'crmWaitingCalls',
    'crmPendingCalls',
    'crmCloseCalls',
    'crmAddEditCall',
    'crmCallLines',
    'crmCallLineAddEdit',

    'crmContacts',


    'crmCustomers',
    'crmCustomerAddEdit',
    'crmCustomerContacts',
    'crmCustomerLocations',
    'crmCustomerOpenCalls',
    'crmCustomerFollowups',
    'crmAllQuotesOnContact',
    'crmCustomerContactAddEdit',
    'crmCustomerLocationAddEdit',

    'crmMyOpenQuotes',
    'crmAllOpenQuotes',
    'crmClosedQuotes',
    'crmAddNewQuote',
    'crmQuoteParts',
    'crmQuotePartAddEdit',

    'crmMyOpenLeads',
    'crmOpenLeads',
    'crmLeadsCloseNextMonth',
    'crmLeadsCloseThisMonth',
    'crmLeadsMarkedForFollowup',
    'crmAddEditLead',
    'crmCustQuoteIQSLocations',
    'crmLeadLines',
    'crmLeadLineAddEdit',

    'crmParts',
    'crmPartDetail',
    'crmPartRevisions'
  ],
  views: [
    'crmLogin',
    'crmHome',
    'crmMyFollowups',
    'crmAllFollowups',
    'crmAllOpenFollowups',
    'crmCloseFollowups',
    'crmFollowupAddEdit',
    'crmMyOpenCalls',
    'crmAllOpenCalls',
    'crmWaitingCalls',
    'crmPendingCalls',
    'crmCloseCalls',
    'crmAddEditCall',
    'crmCallLines',
    'crmCallLineAddEdit',
    'crmContacts',
    'crmCustomers',
    'crmCustomerAddEdit',
    'crmCustomerContacts',
    'crmCustomerLocations',
    'crmCustomerOpenCalls',
    'crmCustomerFollowups',
    'crmAllQuotesOnContact',
    'crmCustomerContactAddEdit',
    'crmCustomerLocationAddEdit',
    'crmMyOpenQuotes',
    'crmAllOpenQuotes',
    'crmClosedQuotes',
    'crmAddNewQuote',
    'crmQuoteParts',
    'crmQuotePartAddEdit',
    'crmMyOpenLeads',
    'crmOpenLeads',
    'crmLeadsCloseNextMonth',
    'crmLeadsCloseThisMonth',
    'crmLeadsMarkedForFollowup',
    'crmAddEditLead',
    'crmCustQuoteIQSLocations',
    'crmLeadLines',
    'crmLeadLineAddEdit',
    'crmParts',
    'crmPartDetail',
    'crmPartRevisions'
  ],
  launch: function () {
    Ext.require("Ext.ux.M1.M1TextField");
    this.loadOverLoads();
    Ext.Loader.setConfig({disableCaching: false});
    SessionObj = Ext.create('M1CRM.model.crmSession');
    SessionObj.constructor();
    crmViews = Ext.create('M1CRM.util.crmViews');
    crmViews.constructor();
    Ext.Viewport.add(crmViews.self.getHome());
    Ext.Ajax.setTimeout(240000);  // 4 min
  },
  loadOverLoads: function () {
    Ext.override(Ext.MessageBox, {
      hide: function () {
        if (this.activeAnimation && this.activeAnimation._onEnd) {
          this.activeAnimation._onEnd();
        }
        return this.callParent(arguments);
      }
    });
  }
});
