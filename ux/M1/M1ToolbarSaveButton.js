/*
 * @class Ext.ux.M1ToolbarSaveButton
 */


Ext.define('Ext.ux.M1.M1ToolbarSaveButton', {
  extend: 'Ext.Button',
  xtype: 'm1toolbarsavebutton',
  config: {
    docked: 'right',
    iconMask: true,
    iconCls: 'save',
    align: 'right',
    layout: {pack: 'center'},
    cls: 'm1-title-button',
    listeners: {
      tap: function (field) {
        var obj = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id);
        var contname = obj.__proto__.$className.split('.')[2];
        if (Ext.isDefined(M1CRM.app.getController(contname).btnSaveTap)) {
          M1CRM.app.getController(contname).btnSaveTap();
        } else {
          Ext.Msg.alert('Warning', 'Method not defined');
        }
      }
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-title-button');
    this.setIconCls('save');
    this.setIconMask(true);
  }
});



