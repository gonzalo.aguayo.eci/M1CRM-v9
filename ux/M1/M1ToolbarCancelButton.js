/*
 * @class Ext.ux.M1ToolbarCancelButton
 */


Ext.define('Ext.ux.M1.M1ToolbarCancelButton', {
  extend: 'Ext.Button',
  xtype: 'm1toolbarcancelbutton',
  config: {
    docked: 'right',
    iconCls: 'reply',
    defaults: {
      iconMask: true,
      ui: 'plain'
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-title-button');
  }
});



