/*
 * @class Ext.ux.M1.M1UrlField
 */

/*
 Ext.define('Ext.ux.M1.M1UrlField', {
 extend: 'Ext.field.Url',
 xtype:'m1urlfield',
 config :{
 flex: 1,
 isDirty : false,
 touppercase : false,
 listeners : {
 scope: this,
 focus : function(field){
 if (field.getReadOnly() && field.getValue().length > 0 ){
 window.open('http://' + field.getValue() + '/');
 }
 },
 change : function(field){
 field.isDirty = true;
 }
 }
 },
 initialize : function(){
 this.callParent();
 this.setCls('m1-field-crm');
 this.setLabelCls('m1-label-crm');
 this.setInputCls('m1-textinput-crm');
 this.isDirty=false;
 }
 });

 */

Ext.define('Ext.ux.M1.M1UrlField', {
  extend: 'Ext.Container',
  xtype: 'm1urlfield',
  requires: [
    'Ext.field.Url',
    'Ext.Button',
    'Ext.ux.M1.M1InvisibleLookupButton'
  ],
  config: {
    layout: 'hbox',
    urlfieldid: null,
    label: '',
    readOnly: false,
    hasbrowsebtn: false,
    touppercase: null,
    defaults: {
      iconMask: true,
      ui: 'plain'
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-custcontrolcontainer');
    this.setUrlField();

    if (Ext.os.deviceType == 'Phone') {
      Ext.getCmp(this.getUrlfieldid()).setLabelAlign('top');
      Ext.getCmp(this.getUrlfieldid()).setLabelCls('m1-label-phone-crm');
    } else {
      Ext.getCmp(this.getUrlfieldid()).setLabelCls('m1-label-crm');
    }

    this.isDirty = false;
  },
  setUrlField: function () {
    var itemsArry = [];

    itemsArry.push({
      xtype: 'urlfield',
      readOnly: this.getReadOnly(),
      id: this.getUrlfieldid(),
      itemId: this.getUrlfieldid(),
      flex: 4,
      label: this.getLabel(),
      isDirty: false,
      cls: 'm1-field-crm',
      labelCls: 'm1-label-crm',
      inputCls: 'm1-textinput-crm',
      listeners: {
        scope: this,
        keyup: function (field, e) {
          if (this.getTouppercase() != null && this.getTouppercase() == true) {
            field.setValue(field.getValue().toUpperCase());
          } else if (this.getTouppercase() != null && !this.getTouppercase()) {
            field.setValue(field.getValue().toLowerCase());
          }
        },
        change: function (field) {
          if (!field.getReadOnly()) {
            field.isDirty = true;
          }
        }
      }
    });

    if (this.getHasbrowsebtn()) {
      itemsArry.push({
        xtype: 'button',
        id: this.getUrlfieldid() + '_browse',
        itemId: this.getUrlfieldid() + '_browse',
        ui: 'plain',
        iconMask: true,
        iconCls: 'search',
        defaults: {
          iconMask: true,
          ui: 'plain'
        },
        cls: 'm1-advancefieldbutton',
        disabledCls: 'm1-advancefieldbutton-disable',
        scope: this,
        hidden: !this.getReadOnly(),
        listeners: {
          scope: this,
          tap: function (field) {
            if (this.getUrlfieldid() && this.getUrlfieldid().length > 0) {
              window.open('http://' + Ext.getCmp(this.getUrlfieldid()).getValue() + '/');
            }
          }
        }
      });
    } else {
      itemsArry.push({
        xtype: 'm1invisiblelookupbutton'
      });
    }
    this.setItems(itemsArry);
  }

});    


