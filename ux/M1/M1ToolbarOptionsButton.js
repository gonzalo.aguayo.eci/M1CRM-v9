/*
 * @class Ext.ux.M1ToolbarOptionsButton
 */


Ext.define('Ext.ux.M1.M1ToolbarOptionsButton', {
  extend: 'Ext.Button',
  xtype: 'm1toolbaroptionsbutton',
  config: {
    docked: 'right',
    iconCls: 'options2',
    iconMask: true
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-title-button');
  }
});



