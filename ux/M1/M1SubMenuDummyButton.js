/*
 * @class Ext.ux.M1.M1SubMenuDummyButton
 */

Ext.define('Ext.ux.M1.M1SubMenuDummyButton', {
  extend: 'Ext.Button',
  xtype: 'm1submenudummybutton',
  config: {
    flex: 1,
    height: 40,
    margin: 2,
    layout: {pack: 'center'},
    disable: true
  },
  initialize: function () {
    this.setCls('m1-submenuemptybutton-crm');
  }
});    
