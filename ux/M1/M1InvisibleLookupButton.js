/*
 * @class Ext.ux.M1.M1InvisibleLookupButton
 */

Ext.define('Ext.ux.M1.M1InvisibleLookupButton', {
  extend: 'Ext.Button',
  xtype: 'm1invisiblelookupbutton',
  config: {
    iconAlign: 'center',
    iconMask: true,
    height: 40,
    margin: 2,
    layout: {pack: 'center'}
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-advancefieldinvisiblebutton-crm');
    this.setIconCls('searchempty');

    this.disable();
  }
});



