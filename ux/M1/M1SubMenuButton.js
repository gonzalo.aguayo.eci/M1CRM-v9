/*
 * @class Ext.ux.M1.M1SubMenuButton
 */

Ext.define('Ext.ux.M1.M1SubMenuButton', {
  extend: 'Ext.Button',
  xtype: 'm1submenubutton',
  config: {
    iconMask: true,
    flex: 1,
    height: 40,
    margin: 2,
    layout: {pack: 'center'},
    showAnimation: "fadeIn",
    hideAnimation: "fadeOut"
  },
  initialize: function () {
    this.callParent();
    //this.setCls('m1-submenuactionbutton-crm');
    this.setLabelCls('m1-submenuactionbutton-label-crm');
    this.setBadgeCls('m1-button-badge-crm');
    this.setDisabledCls('m1-mainmenuactionbuttondisable-crm');
    this.enable();
  }
});
