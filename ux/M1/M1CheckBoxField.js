/*
 * @class Ext.ux.M1.M1CheckBoxField
 */

Ext.define('Ext.ux.M1.Checkbox', {
  extend: 'Ext.field.Checkbox',
  xtype: 'm1checkboxfield',
  config: {
    flex: 1,
    isDirty: false,
    listeners: {
      check: function (field) {
        field.isDirty = true;
      },
      uncheck: function (field) {
        field.isDirty = true;
      }
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-textinput-crm');
    //this.setLabelCls('m1-label-crm');
    if (Ext.os.deviceType == 'Phone') {
      this.setLabelAlign('top');
      this.setLabelCls('m1-label-phone-crm');
    } else {
      this.setLabelCls('m1-label-crm');
    }
    this.setDisabledCls('m1-label-crm');
    //this.isDirty=false;
  }
});    

