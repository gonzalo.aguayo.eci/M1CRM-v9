/*
 * @class Ext.ux.M1.M1AdvTextArea
 */

/*
 Ext.define('Ext.ux.M1.M1TextArea',{
 extend: 'textareafield',
 xtype : 'm1textareafield'
 config:{

 }
 });
 */
Ext.define('Ext.ux.M1.M1AdvTextArea', {
  extend: 'Ext.Container',
  xtype: 'm1advtextarea',
  requires: [
    'Ext.field.TextArea',
    'Ext.Button'
  ],
  config: {
    layout: 'hbox',
    defaults: {
      iconMask: true,
      ui: 'plain'
    },
    textareaid: null,
    textarealabel: '',
    textareaflex: 1,
    detaillabel: ''
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-field-crm');
    this.setAdvTextField();

    if (Ext.os.deviceType == 'Phone') {
      Ext.getCmp(this.getTextareaid()).setLabelAlign('top');
      Ext.getCmp(this.getTextareaid()).setLabelCls('m1-label-phone-crm');
    } else {
      Ext.getCmp(this.getTextareaid()).setLabelCls('m1-label-crm');
    }

  },
  setAdvTextField: function () {
    var id = this.getTextareaid();
    var itemsArry = [
      {
        xtype: 'textareafield',
        label: this.getTextarealabel(),
        id: id,
        clearIcon: false,
        readOnly: true,
        cls: 'm1-field-crm',
        labelCls: 'm1-label-crm',
        inputCls: 'm1-textinput-crm',
        isDirty: false,
        flex: this.getTextareaflex(),
        maxRows: 4,
        parentviewid: null,
        listeners: {
          scope: this,
          change: function (field) {
            field.isDirty = true;
          }

        }
      },

      {
        xtype: 'button',
        iconAlign: 'center',
        id: id + '_info',
        iconMask: true,
        ui: 'plain',
        iconCls: 'zoomin', //'navigate_right',
        scope: this,
        cls: "m1-advancefieldbutton",
        pressedCls: 'm1-mainmenuactionbutton-pressed',
        listeners: {
          scope: this,
          tap: function (field) {
            var parentid = field.getId().split('_')[0];
            this.parentviewid = parentid;
            var titlebarcls = 'm1-crmhomescreen-title';
            if (Ext.getCmp(parentid).config._mode != 'view') {
              moreDesReadOnly = false;
              titlebarcls = 'm1-crmhomescreen-edit';
              btnoptions = [
                {
                  xtype: 'button',
                  docked: 'right',
                  id: id + '_cancelbtn',
                  iconCls: 'delete',
                  iconMask: true,
                  ui: 'plain',
                  width: 50,
                  height: 50,
                  cls: 'm1-title-button-edit',
                  align: 'right',
                  listeners: {
                    scope: this,
                    tap: function (field) {
                      var pnl = Ext.getCmp(this.overlayer.id);
                      pnl.hide({type: 'slide', direction: 'right'});
                      //pnl.hide();
                    }
                  }
                },
                {
                  xtype: 'button',
                  docked: 'right',
                  id: id + '_deletebtn',
                  iconCls: 'trash',
                  iconMask: true,
                  ui: 'plain',
                  width: 50,
                  height: 50,
                  cls: 'm1-title-button-edit',
                  align: 'right',
                  listeners: {
                    scope: this,
                    tap: function (field) {
                      Ext.getCmp(field.getId().split('_')[0] + '_' + field.getId().split('_')[1] + '_more').setValue('');
                      Ext.getCmp(field.getId().split('_')[0] + '_' + field.getId().split('_')[1]).setValue('');
                      Ext.getCmp(field.getId().split('_')[0] + '_' + field.getId().split('_')[1]).isDirty = true;
                      var pnl = Ext.getCmp(this.overlayer.id);
                      pnl.hide({type: 'slide', direction: 'right'});
                      //pnl.hide();
                    }
                  }
                },
                {
                  xtype: 'button',
                  docked: 'right',
                  id: id + '_savebtn',
                  iconCls: 'check',
                  iconMask: true,
                  ui: 'plain',
                  width: 50,
                  height: 50,
                  cls: 'm1-title-button-edit',
                  align: 'right',
                  listeners: {
                    scope: this,
                    tap: function (field) {
                      Ext.getCmp(field.getId().split('_')[0] + '_' + field.getId().split('_')[1]).setValue(
                        Ext.getCmp(field.getId().split('_')[0] + '_' + field.getId().split('_')[1] + '_more').getValue()
                      );
                      Ext.getCmp(field.getId().split('_')[0] + '_' + field.getId().split('_')[1]).isDirty = true;
                      var pnl = Ext.getCmp(this.overlayer.id);
                      pnl.hide({type: 'slide', direction: 'right'});
                      //pnl.hide();
                    }
                  }
                }


              ];
            } else {
              moreDesReadOnly = true;
              btnoptions = [
                {
                  xtype: 'button',
                  docked: 'right',
                  id: id + '_cancelbtn',
                  iconCls: 'delete',
                  iconMask: true,
                  ui: 'plain',
                  width: 50,
                  height: 50,
                  cls: 'm1-title-button',
                  align: 'right',
                  listeners: {
                    scope: this,
                    tap: function (field) {
                      var pnl = Ext.getCmp(this.overlayer.id);
                      pnl.hide({type: 'slide', direction: 'right'});
                      //Ext.getCmp(this.parentviewid).show({type: 'slide', direction: 'up'});
                    }
                  }
                }
              ];
            }


            var ldesDisplayArr = [
              {
                xtype: 'titlebar',
                docked: 'top',
                title: this.getDetaillabel(),
                cls: titlebarcls,
                defaults: {
                  iconMask: true,
                  ui: 'plain'
                },
                items: btnoptions

              },
              {
                xtype: 'textareafield',
                id: id + '_more',
                value: Ext.getCmp(id).getValue(),
                readOnly: moreDesReadOnly,
                clearIcon: false,
                cls: 'm1-emptycontainer',
                labelCls: 'm1-label-crm',
                inputCls: 'm1-textinput-crm',
                isDirty: false,
                fullscreen: true,
                width: '100%',
                height: '100%',
                maxRows: Ext.getCmp(id).getValue().split("\n").length < 20 ? 20 : Ext.getCmp(id).getValue().split("\n").length + 10,
                listeners: {
                  change: function (field) {
                    field.isDirty = true;
                  }
                }
              }

            ];

            this.overlayer =

              Ext.Viewport.add({
                xtype: 'panel',
                cls: 'm1-popuppanel-crm',
                modal: true,
                fullscreen: true,
                hideOnMaskTap: true,
                showAnnimation: {
                  type: 'popIn',
                  duration: 250,
                  easing: 'ease-out'
                },
                hideAnimation: {
                  type: 'popOut',
                  duration: 250,
                  easing: 'ease-out'
                },
                centered: true,
                width: '100%',
                height: '100%',
                items: ldesDisplayArr,
                scrollable: true
              });
            // Ext.getCmp(this.parentviewid).hide({type: 'slide', direction: 'down'});
            this.overlayer.show({type: 'slide', direction: 'left'});
          }
        }
      }
    ];

    this.setItems(itemsArry);

  }

});    