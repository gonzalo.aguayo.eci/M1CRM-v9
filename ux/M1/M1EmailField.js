/*
 * @class Ext.ux.M1.M1EmailField
 */

Ext.define('Ext.ux.M1.M1EmailField', {
  extend: 'Ext.field.Email',
  xtype: 'm1emailfield',
  config: {
    flex: 1,
    isDirty: false,
    touppercase: false,
    listeners: {
      scope: this,
      focus: function (field) {
        if (field.getReadOnly() && field.getValue().length > 0) {
          window.open('mailto:' + field.getValue());
        }
      },
      change: function (field) {
        field.isDirty = true;
      }
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-field-crm');
    //this.setLabelCls('m1-label-crm');
    if (Ext.os.deviceType == 'Phone') {
      this.setLabelAlign('top');
      this.setLabelCls('m1-label-phone-crm');
    } else {
      this.setLabelCls('m1-label-crm');
    }
    this.setInputCls('m1-textinput-crm');
    this.isDirty = false;
  }
});    