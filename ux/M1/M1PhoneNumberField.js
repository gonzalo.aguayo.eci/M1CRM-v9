/*
 * @class Ext.ux.M1.M1PhoneNumberField
 */


Ext.define('Ext.ux.M1.M1PhoneNumberField', {
  extend: 'Ext.ux.M1.M1TextField',
  xtype: 'm1phonenumberfield',
  config: {
    flex: 1,
    component: {type: 'tel'},
    isDirty: false,
    touppercase: true,
    dialontap: true,
    listeners: {
      scope: this,
      focus: function (field) {
        if (Ext.os.deviceType == 'Phone' && field.getReadOnly() && field.getValue().length > 0 && field.getDialontap()) {
          window.open('tel:' + field.getValue());
        }
      }
    }
  },
  initialize: function () {
    this.callParent();
    //this.setLabelCls('m1-label-crm');
    if (Ext.os.deviceType == 'Phone') {
      this.setLabelAlign('top');
      this.setLabelCls('m1-label-phone-crm');
    } else {
      this.setLabelCls('m1-label-crm');
    }
    this.setInputCls('m1-textinput-crm');
    this.isDirty = false;
  }
});


