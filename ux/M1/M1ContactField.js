/*
 * @class Ext.ux.M1.M1ContactField
 */

Ext.define('Ext.ux.M1.M1ContactField', {
  extend: 'Ext.Container',
  xtype: 'm1contactfield',
  requires: [
    'Ext.ux.M1.M1TextField',
    'Ext.Button',
    'Ext.Toolbar',
    'Ext.tab.Panel'
  ],
  config: {
    layout: 'hbox',
    contactfieldid: null,
    contactfieldlabel: '',
    contactfielreadonly: false
  },

  initialize: function () {
    this.callParent();
    this.setCls('m1-custcontrolcontainer');
    this.setContactField();
  },

  setContactField: function () {
    var itemsArry = [];
    var id = this.getContactfieldid();
    itemsArry.push(this.getContactTextField(id));
    itemsArry.push(this.getDialButton(id));
    itemsArry.push(this.getLookupButton(id));
    this.setItems(itemsArry);
  },

  getContactTextField: function (id) {
    return new Ext.create('Ext.ux.M1.M1TextField', {
      readOnly: this.getContactfielreadonly(),
      id: id,
      flex: 4,
      label: this.getContactfieldlabel(),
      labelCls: 'm1-label-crm',
      inputCls: 'm1-textinput-crm',
      isDirty: false
    });
  },

  getLookupButton: function (id) {
    return new Ext.create('Ext.Button', {
      iconAlign: 'center',
      id: id + '_lookupbtn',
      iconMask: true,
      hidden: this.getContactfielreadonly(),
      iconCls: 'search',
      //cls: "m1-lookupbutton-crm",
      cls: 'm1-advancefieldbutton',
      listeners: {
        scope: this,
        tap: function (field) {
          var parentid = field.getId().split('_')[0];
          var custContacts = Ext.create('M1CRM.model.crmCustomerContacts', {
            OrganizationID: Ext.getCmp(parentid + '_custid').getValue(),
            LocationID: Ext.getCmp(parentid + '_locationid').getValue() != null ? Ext.getCmp(parentid + '_locationid').getValue() : '',
            ContactID: null
          });
          var view = M1CRM.util.crmViews.getCustomerContacts();
          view.config._parentformid = parentid;
          M1CRM.util.crmViews.slideLeft(view);
        }
      }
    });
  },

  getPhoneNumberButton: function (phonenumber, phonelabel) {
    var idval = phonenumber.replace(/[^0-9]/g, '');
    var display = phonenumber.replace(/[^0-9]/g, '').replace(/(\d{3})(\d{3})(\d{4})/, "($1) $2-$3");
    return new Ext.create('Ext.Button', {
      itemId: idval,
      id: idval,
      text: Ext.String.format('<i>Call {0} </i>:<br><b>{1}</b>', phonelabel, display),
      cls: 'm1-contactnumberoptionbutton-crm',
      labelCls: 'm1-contactnumberoption-label-crm',
      listeners: {
        scope: this,
        tap: function (field) {
          field.getParent().hide();
          window.open('tel:' + field.getId());
        }
      }
    });
  },

  getPhoneNumberDisplayPanel: function (dialNumberArr) {
    return new Ext.create('Ext.Panel', {
      cls: 'm1-popuppanel-crm',
      modal: true,
      hideOnMaskTap: true,
      showAnnimation: {
        type: 'popIn',
        duration: 250,
        easing: 'ease-out'
      },
      hideAnimation: {
        type: 'popOut',
        duration: 250,
        easing: 'ease-out'
      },
      centered: true,
      width: Ext.os.deviceType == 'Phone' ? 260 : 300,
      height: Ext.os.deviceType == 'Phone' ? 220 : 240,
      items: dialNumberArr,
      scrollable: false
    })
  },

  getDialButton: function (id) {
    return new Ext.create('Ext.Button', {
      id: id + '_dial',
      iconMask: true,
      iconCls: 'phone',
      hidden: !this.getContactfielreadonly(),
      //cls: "m1-dialnobutton-crm",
      cls: 'm1-advancefieldbutton',
      listeners: {
        scope: this,
        tap: function (field) {
          var parentid = field.getId().split('_')[0];
          if (parentid != null && Ext.getCmp(parentid + '_contactid').getValue() != null && Ext.getCmp(parentid + '_contactid').getValue() != '') {
            var custContacts = Ext.create('M1CRM.model.crmCustomerContactAddEdit', {
              OrganizationID: Ext.getCmp(parentid + '_custid').getValue(),
              LocationID: Ext.getCmp(parentid + '_locationid').getValue() != null ? Ext.getCmp(parentid + '_locationid').getValue() : '',
              ContactID: Ext.getCmp(parentid + '_contactid').getValue()
            });
            custContacts.getIt({
              RequestID: 'data',
              scope: this,
              onReturn: function (result) {
                var count = 0;
                var dialNumberArr = [];
                dialNumberArr.push({
                    docked: 'top',
                    xtype: 'toolbar',
                    title: 'Select Number To Call',
                    cls: "m1-crmhomescreen-title"
                  }
                );
                var dialNumber = '';
                var rec = result.ResultObject;
                if (rec.Phone != null && rec.Phone != '') {
                  count++;
                  dialNumberArr.push(this.getPhoneNumberButton(rec.Phone, 'Phone'));
                }
                if (rec.Phone2 != null && rec.Phone2 != '') {
                  count++;
                  dialNumberArr.push(this.getPhoneNumberButton(rec.Phone2, 'Alt Phone'));
                }
                if (rec.Mobile != null && rec.Mobile != '') {
                  count++;
                  dialNumberArr.push(this.getPhoneNumberButton(rec.Mobile, 'Mobile'));
                }

                if (count > 0) {
                  if (count < 2) {
                    dialNumber = dialNumberArr[1].id;
                    dialNumberArr = null;
                    window.open('tel:' + dialNumber);
                  } else {
                    this.overlayer = Ext.Viewport.add(this.getPhoneNumberDisplayPanel(dialNumberArr));
                    this.overlayer.show();
                  }
                }
              }
            });
          }
        }
      }
    });
  }


});    