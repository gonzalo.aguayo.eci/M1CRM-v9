/*
 * @class Ext.ux.M1.M1DatePicker
 */

Ext.define('Ext.ux.M1.M1DatePicker', {
  extend: 'Ext.field.DatePicker',
  xtype: 'm1datepicker',
  config: {
    flex: 1,
    isDirty: false,
    picker: {
      slotOrder: ['day', 'month', 'year'],
      yearFrom: new Date().getFullYear(),
      yearTo: new Date().getFullYear() + 3
    },
    listeners: {
      change: function (field) {
        if (!field.getReadOnly()) {
          field.isDirty = true;
        }
      }
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-selectfield-crm');
    //this.setLabelCls('m1-label-crm');
    if (Ext.os.deviceType == 'Phone') {
      this.setLabelAlign('top');
      this.setLabelCls('m1-label-phone-crm');
    } else {
      this.setLabelCls('m1-label-crm');
    }
    this.setInputCls('m1-textinput-crm');
    this.setPlaceHolder(SessionObj.statics.DateFormat);
    this.isDirty = false;
  }
});    