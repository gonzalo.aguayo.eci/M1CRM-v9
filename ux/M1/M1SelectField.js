/*
 * @class Ext.ux.M1.M1SelectField
 */

Ext.define('Ext.ux.M1.M1SelectField', {
  extend: 'Ext.field.Select',
  xtype: 'm1selectfield',
  config: {
    flex: 1,
    isDirty: false,
    listeners: {
      scope: this,
      change: function (field) {
        if (!field.getReadOnly()) {
          field.isDirty = true;
        }
      }
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-selectfield-crm');
    //this.setLabelCls('m1-label-crm');
    if (Ext.os.deviceType == 'Phone') {
      this.setLabelAlign('top');
      this.setLabelCls('m1-label-phone-crm');
    } else {
      this.setLabelCls('m1-label-crm');
    }
    if (this.getRequired() == true) {
      this.setLabel(this.getLabel() + '<span style="font-weight:normal;font-size:15px;"> * </span>');
      this.setRequiredCls('m1-required-crm2');
    }

    this.setInputCls('m1-textinput-crm');
    this.isDirty = false;
  }
});    