/*
 * @class Ext.ux.M1.M1MainMenuButton
 */

Ext.define('Ext.ux.M1.M1MainMenuButton', {
  extend: 'Ext.Button',
  xtype: 'mainmenubutton',
  config: {
    iconAlign: 'left',
    iconMask: true,
    height: 80,
    width: 70,
    margin: 2,
    flex: 4,
    layout: {pack: 'left'},
    showAnimation: "fadeIn",
    hideAnimation: "fadeOut",
    rowid: 1
  },
  initialize: function () {
    this.callParent();

    if (Ext.browser.is.IE == true) {
      this.setIconCls(null);
    }

    this.setCls('m1-mainmenuactionbutton-crm' + this.getRowid());
    this.setLabelCls('m1-mainmenubutton-label-crm');
    this.setBadgeCls('m1-button-badge-crm');
    this.setDisabledCls('m1-mainmenuactionbuttondisable-crm');
    this.enable();
  },
  setClassBaseOnID: function () {
    var name = '';
    if (this.id.indexOf('followups') >= 0)
      name = '-followups';
    else if (this.id.indexOf('calls') >= 0)
      name = '-calls';
    else if (this.id.indexOf('contacts') >= 0)
      name = '-contacts';
    else if (this.id.indexOf('org') >= 0)
      name = '-organisation';
    else if (this.id.indexOf('quotes') >= 0)
      name = '-quotes';
    else if (this.id.indexOf('leads') >= 0)
      name = '-leads';
    else if (this.id.indexOf('parts') >= 0)
      name = '-parts';
    this.setCls('m1-mainmenuactionbutton-crm' + name);
  }

});




