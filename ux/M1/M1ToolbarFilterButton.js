/*
 * @class Ext.ux.M1ToolbarFilterButton
 */


Ext.define('Ext.ux.M1.M1ToolbarFilterButton', {
  extend: 'Ext.Button',
  xtype: 'm1toolbarfilterbutton',
  config: {
    docked: 'right',
    iconCls: 'find',
    defaults: {
      iconMask: true,
      ui: 'plain'
    },
    listeners: {
      tap: function (field) {
        var tparent = field.id.split('_')[0];
        var tid = '#' + tparent + ' searchfield';
        var temp = Ext.ComponentQuery.query(tid)[0].id;
        var parent = Ext.getCmp(temp).getParent().id;
        if (Ext.getCmp(parent).isHidden()) {
          Ext.getCmp(parent).show();
          Ext.getCmp(temp).focus();
        } else {
          Ext.getCmp(parent).hide();
        }
      }
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-title-button');
  }
});



