Ext.define('Ext.ux.M1.M1FormButton', {
  extend: 'Ext.Button',
  xtype: 'formbutton',
  config: {
    iconAlign: 'top',
    iconMask: true,
    flex: 1,
    height: 50,
    width: '100%',
    margin: 2,
    layout: {pack: 'center'},
    showAnimation: "fadeIn",
    hideAnimation: "fadeOut"
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-imagebutton-crm');
    this.setDisabledCls('m1-imagebutton-empty-crm');
    this.enable();
  }
});



