/*
 * @class Ext.ux.M1ToolbarEntryForm
 */


Ext.define('Ext.ux.M1.M1ToolbarEntryForm', {
  extend: 'Ext.Toolbar',
  //alternateClassName:
  xtype: 'm1toolbarentryform',
  config: {
    docked: 'top',
    iconMask: true,
    layout: {
      pack: 'center'
    },
    defaults: {
      iconMask: true,
      ui: 'plain'
    },
    includeSave: true,
    includeAddNew: true,
    includeOptions: false,
    includeSubTitle: true
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-crmhomescreen-title');
    this.setToolbarItems(this.getClsByuId());
  },
  getClsByuId: function () {
    var tid = this.id;

    if (tid.indexOf('followup') >= 0) {
      return 'm1-title-button-followups';
    } else if (tid.indexOf('customerlocation') >= 0) {
      return 'm1-title-button-locations';
    } else if (tid.indexOf('callline') >= 0) {
      return 'm1-title-button-calllines';
    } else if (tid.indexOf('call') >= 0) {
      return 'm1-title-button-calls';
    } else if (tid.indexOf('allquotesoncontact') >= 0) {
      return 'm1-title-button-quotes';
    } else if (tid.indexOf('contact') >= 0) {
      return 'm1-title-button-contacts';
    } else if (tid.indexOf('customer') >= 0 || tid.indexOf('cust') >= 0) {
      return 'm1-title-button-organisation';
    } else if (tid.indexOf('quote') >= 0) {
      return 'm1-title-button-quotes';
    } else if (tid.indexOf('leadline') >= 0) {
      return 'm1-title-button-leadlines';
    } else if (tid.indexOf('lead') >= 0) {
      return 'm1-title-button-leads';
    } else if (tid.indexOf('part') >= 0) {
      return 'm1-title-button-parts';
    } else {
      return 'm1-title-button';
    }
  },
  setToolbarItems: function (clsid) {
    var tid = this.getId().split('_toolbarid')[0];
    var itemsArry = [];

    itemsArry.push({
      xtype: 'button',
      itemId: tid + '_back',
      id: tid + '_back',
      docked: 'left',
      iconMask: true,
      ui: 'plain',
      width: 50,
      height: 50,
      iconCls: 'back',
      cls: clsid, //'m1-title-button',
      listeners: {
        tap: function (field) {
          var obj = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id);
          var contname = obj.__proto__.$className.split('.')[2];
          if (Ext.isDefined(M1CRM.app.getController(contname).processAutoSave)) {
            M1CRM.app.getController(contname).processAutoSave('back');
          } else {
            var viewid = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id).id;
            M1CRM.app.getController(contname).toBackTap(viewid);
          }
        }
      }

    });
    itemsArry.push({
      xtype: 'button',
      docked: 'left',
      id: tid + '_home',
      iconMask: true,
      ui: 'plain',
      width: 50,
      height: 50,
      iconCls: 'home',
      cls: clsid, //'m1-title-button',
      align: 'left',
      listeners: {
        tap: function (field) {
          var obj = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id);
          var contname = obj.__proto__.$className.split('.')[2];

          if (Ext.isDefined(Ext.getCmp(Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id).id).config._parentformid) &&
            Ext.getCmp(Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id).id).config._parentformid.indexOf('list') > 0) {
            var view = Ext.getCmp(Ext.getCmp(Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id).id).config._parentformid);
            if (Ext.isDefined(view.config._haslistchange)) {
              view.config._haslistchange = true;
            }
            view.config._pageid = 1;
            //Ext.getCmp(Ext.getCmp(Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id).id).config._parentformid).config._pageid = 1;
          }


          if (Ext.isDefined(M1CRM.app.getController(contname).processAutoSave)) {
            M1CRM.app.getController(contname).processAutoSave('home');
          } else {
            var viewid = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id).id;
            M1CRM.app.getController(contname).toHome();
            //M1CRM.app.getController(contname).toBackTap(viewid);
          }
        }
      }
    });

    if (this.getIncludeSave() || this.getIncludeAddNew()) {
      itemsArry.push({
        xtype: 'button',
        docked: 'right',
        id: tid + '_savebtn',
        iconMask: true,
        ui: 'plain',
        width: 50,
        height: 50,
        iconCls: 'save', //'check',
        cls: 'm1-title-button-edit',
        align: 'right',
        listeners: {
          tap: function (field) {
            var obj = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id);
            var contname = obj.__proto__.$className.split('.')[2];
            if (Ext.isDefined(M1CRM.app.getController(contname).btnSaveTap)) {
              M1CRM.app.getController(contname).btnSaveTap();
            } else {
              Ext.Msg.alert('Warning', 'Method not defined');
            }
          }
        }
      });
    }

    if (this.getIncludeOptions()) {
      itemsArry.push({
        xtype: 'button',
        docked: 'right',
        itemId: tid + '_options',
        id: tid + '_options',
        iconMask: true,
        ui: 'plain',
        width: 50,
        height: 50,
        iconCls: 'list',
        cls: clsid, //'m1-title-button',
        handler: function () {
          if (Ext.Viewport.getMenus().right.isHidden()) {
            Ext.Viewport.showMenu('right');
          } else {
            Ext.Viewport.hideMenu('right');
          }
        }

      });
    }

    if (this.getIncludeSave() || this.getIncludeAddNew()) {
      itemsArry.push({
        xtype: 'button',
        itemId: tid + '_cancelbtn',
        id: tid + '_cancelbtn',
        docked: 'right',
        iconMask: true,
        ui: 'plain',
        width: 50,
        height: 50,
        iconCls: 'undo',
        align: 'right',
        cls: 'm1-title-button-edit'
      });
    }

    if (this.getIncludeSave()) {
      itemsArry.push({
        xtype: 'button',
        docked: 'right',
        itemId: tid + '_editbtn',
        id: tid + '_editbtn',
        iconMask: true,
        ui: 'plain',
        width: 50,
        height: 50,
        iconCls: 'compose',
        cls: clsid, //'m1-title-button',
        align: 'right',
        listeners: {
          tap: function (field) {
            var obj = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id);
            var contname = obj.__proto__.$className.split('.')[2];
            if (Ext.isDefined(M1CRM.app.getController(contname).btnEditTap)) {
              M1CRM.app.getController(contname).config._mode = 'edit';
              M1CRM.app.getController(contname).setEntryFormMode('edit');
              M1CRM.app.getController(contname).btnEditTap();
            } else {
              Ext.Msg.alert('Warning', 'Method not defined');
            }
          }
        }
      });
    }

    if (this.getIncludeAddNew()) {
      itemsArry.push({
        xtype: 'button',
        docked: 'right',
        itemId: tid + '_addnew',
        id: tid + '_addnew',
        iconMask: true,
        ui: 'plain',
        width: 50,
        height: 50,
        iconCls: 'add',
        cls: clsid, //'m1-title-button',
        align: 'right'
      });
    }

    this.setItems(itemsArry);
  }
});

