/*
 * @class Ext.ux.M1.M1TextField
 */

Ext.define('Ext.ux.M1.M1TextField', {
  extend: 'Ext.field.Text',
  xtype: 'm1textfield',
  config: {
    flex: 1,
    isDirty: false,
    currecyField: false,
    touppercase: null,
    listeners: {
      scope: this,
      change: function (field) {
        if (!field.getReadOnly()) {
          field.isDirty = true;
        }
      },
      keyup: function (field, e) {
        if (field.getTouppercase() != null && field.getTouppercase() == true) {
          field.setValue(field.getValue().toUpperCase());
        } else if (field.getTouppercase() != null && !field.getTouppercase()) {
          field.setValue(field.getValue().toLowerCase());
        }
      }
    }
  },
  initialize: function () {
    this.callParent();
    if (Ext.os.deviceType == 'Phone') {
      this.setLabelAlign('top');
      this.setLabelCls('m1-label-phone-crm');
    } else {
      this.setLabelCls('m1-label-crm');
    }
    this.setInputCls('m1-textinput-crm');
    this.isDirty = false;
    if (this.getRequired() == true) {
      this.setLabel(this.getLabel() + '<span style="font-weight:normal;font-size:15px;"> * </span>');
      this.setRequiredCls('m1-required-crm2');
    }
  }
});    