/*
 * @class Ext.ux.M1.M1MainMenuButton2
 */


Ext.define('Ext.ux.M1.M1MainMenuButton2', {
  extend: 'Ext.Button',
  xtype: 'mainmenubutton2',
  config: {
    iconAlign: 'top',
    iconMask: true,
    height: 80,
    width: 40,
    margin: 1,
    flex: 2,
    // color : '',
    layout: {pack: 'center'},
    showAnimation: "fadeIn",
    hideAnimation: "fadeOut",
    rowid: 1
  },
  initialize: function () {
    this.callParent();
    if (Ext.browser.is.IE == true && this.id.indexOf('logout') < 0) {
      this.setIconCls('add');
      this.setLabelCls('m1-mainmenubutton-label-crm');
    } else if (Ext.browser.is.IE == true && this.id.indexOf('logout') >= 0) {
      this.setIconCls('logout');
      this.setLabelCls('m1-mainmenuexitbutton-label-crm');
      //this.setText('Exit');
    }
    this.setCls('m1-mainmenuactionbutton-crm' + this.getRowid());
    this.setDisabledCls('m1-mainmenuactionbuttondisable-crm');
    this.enable();
  }
});
