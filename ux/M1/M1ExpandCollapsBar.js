/*
 * @class Ext.ux.M1.M1ExpandCollapsBar
 */


Ext.define('Ext.ux.M1.M1ExpandCollapsBar', {
  extend: 'Ext.Button',
  xtype: 'm1expandcollapsbar',
  config: {
    flex: 1,
    margin: 2,
    ctrlfieldset: null,
    iconMask: true,
    padding: 0,
    showAnimation: "fadeIn",
    hideAnimation: "fadeOut",
    hideAnimation: {type: 'popIn', duration: 200},
    listeners: {
      scope: this,
      initialize: function (field) {
        field.setIconCls('arrow_down');
      },
      tap: function (field) {
        if (Ext.getCmp(field.getCtrlfieldset()).isHidden()) {
          Ext.getCmp(field.getCtrlfieldset()).show();
          field.setIconCls('arrow_up');
        } else {
          Ext.getCmp(field.getCtrlfieldset()).hide();
          field.setIconCls('arrow_down');
        }
      }
    }
  },
  initialize: function () {
    this.callParent();
    this.setIconCls('arrow_down');
    this.setCls('m1-titlebutton-crm');
    this.setLabelCls('m1-titlebutton-label-crm');
    this.enable();
  }
});



