/*
 * @class Ext.ux.M1ToolbarEditButton
 */


Ext.define('Ext.ux.M1.M1ToolbarEditButton', {
  extend: 'Ext.Button',
  xtype: 'm1toolbareditbutton',
  config: {
    docked: 'right',
    iconCls: 'edit',
    defaults: {
      iconMask: true,
      ui: 'plain'
    },
    listeners: {
      tap: function (field) {
        var obj = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id);
        var contname = obj.__proto__.$className.split('.')[2];
        if (Ext.isDefined(M1CRM.app.getController(contname).btnEditTap)) {
          M1CRM.app.getController(contname).config._mode = 'edit';
          M1CRM.app.getController(contname).setEntryFormMode('edit');
          M1CRM.app.getController(contname).btnEditTap();
        } else {
          Ext.Msg.alert('Warning', 'Method not defined');
        }
      }
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-title-button');
  }
});



