/*
 * @class Ext.ux.M1.M1NumberField
 */

Ext.define('Ext.ux.M1.M1NumberField', {
  extend: 'Ext.field.Number',
  xtype: 'm1numberfield',
  config: {
    flex: 1,
    allowDecimals: true,
    hidden: false,
    value: 0,
    decimalPrecision: 2,
    currecyField: false,
    isDirty: false,
    listeners: {
      scope: this,
      change: function (field) {
        if (!field.getReadOnly()) {
          field.isDirty = true;
        }
      },
      clearicontap: function (field) {
        Ext.getCmp(field.id).setValue(0);
        Ext.getCmp(field.id).focus();
      }
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-field-crm');
    //this.setLabelCls('m1-label-crm');
    if (Ext.os.deviceType == 'Phone') {
      this.setLabelAlign('top');
      this.setLabelCls('m1-label-phone-crm');
    } else {
      this.setLabelCls('m1-label-crm');
    }
    this.setInputCls('m1-textinput-crm');
    this.isDirty = false;
  },
  isHidden: function () {
    return this.config.hidden;
  }
});    