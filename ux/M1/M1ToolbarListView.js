/*
 * @class Ext.ux.M1ToolbarListView
 */


Ext.define('Ext.ux.M1.M1ToolbarListView', {
  extend: 'Ext.Toolbar',
  xtype: 'm1toolbarlistview',
  requires: [
    'Ext.ux.M1.M1ToolbarBackButton'
  ],
  config: {
    docked: 'top',
    layout: {
      pack: 'center'
    },
    defaults: {
      iconMask: true,
      ui: 'plain'
    },
    includeAdd: false,
    includeFilter: false,
    includeOptions: false
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-crmhomescreen-title');
    this.setToolbarItems(this.getClsByuId());
  },
  getClsByuId: function () {
    var tid = this.id;

    if (tid.indexOf('followup') >= 0) {
      return 'm1-title-button-followups';
    } else if (tid.indexOf('customerlocation') >= 0) {
      return 'm1-title-button-locations';
    } else if (tid.indexOf('calls') >= 0) {
      return 'm1-title-button-calls';
    } else if (tid.indexOf('callline') >= 0) {
      return 'm1-title-button-calllines';
    } else if (tid.indexOf('quotesoncontact') >= 0) {
      return 'm1-title-button-quotes';
    } else if (tid.indexOf('contact') >= 0) {
      return 'm1-title-button-contacts';
    } else if (tid.indexOf('customer') >= 0 || tid.indexOf('cust') >= 0) {
      return 'm1-title-button-organisation';
    } else if (tid.indexOf('quoteparts') >= 0) {
      return 'm1-title-button-quote-lines';
    } else if (tid.indexOf('quotes') >= 0) {
      return 'm1-title-button-quotes';
    } else if (tid.indexOf('leads') >= 0) {
      return 'm1-title-button-leads';
    } else if (tid.indexOf('leadlines') >= 0) {
      return 'm1-title-button-leadlines';
    } else if (tid.indexOf('parts') >= 0) {
      return 'm1-title-button-parts';
    } else {
      return 'm1-title-button';
    }
  },
  setToolbarItems: function (clsid) {
    var tid = this.getId().split('_toolbarid')[0];
    var itemsArry = [];

    itemsArry.push({
      xtype: 'button',
      itemId: tid + '_back',
      id: tid + '_back',
      docked: 'left',
      iconMask: true,
      ui: 'plain',
      width: 50,
      height: 50,
      iconCls: 'back',
      cls: clsid,
      listeners: {
        tap: function (field) {
          var obj = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id);
          var contname = obj.__proto__.$className.split('.')[2];
          if (Ext.isDefined(M1CRM.app.getController(contname).processAutoSave)) {
            M1CRM.app.getController(contname).processAutoSave('back');
          } else {
            var viewid = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id).id;
            M1CRM.app.getController(contname).toBackTap(viewid);
          }
        }
      }
    });

    itemsArry.push({
      xtype: 'button',
      docked: 'left',
      id: tid + '_home',
      iconCls: 'home',
      iconMask: true,
      ui: 'plain',
      width: 50,
      height: 50,
      cls: clsid,
      align: 'left',
      listeners: {
        scope: this,
        tap: function (field) {
          var obj = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id);
          var contname = obj.__proto__.$className.split('.')[2];
          if (Ext.isDefined(M1CRM.app.getController(contname).processAutoSave)) {
            M1CRM.app.getController(contname).processAutoSave('home');
          } else {
            var viewid = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id).id;
            if (Ext.getCmp(viewid).id.indexOf('list') > 0) {
              Ext.getCmp(viewid).config._pageid = 1;
              if (Ext.isDefined(Ext.getCmp(viewid).config._haslistchange)) {
                Ext.getCmp(viewid).config._haslistchange = true;
                Ext.getCmp(viewid).config._pageid = 1;
              }

            }
            M1CRM.app.getController(contname).toHome();
          }
        }
      }
    });

    if (this.getIncludeOptions()) {
      itemsArry.push({
        xtype: 'button',
        docked: 'right',
        itemId: tid + '_options',
        id: tid + '_options',
        iconCls: 'list',
        iconMask: true,
        ui: 'plain',
        width: 50,
        height: 50,
        cls: clsid,
        handler: function () {
          if (Ext.Viewport.getMenus().right.isHidden()) {
            Ext.Viewport.showMenu('right');
          } else {
            Ext.Viewport.hideMenu('right');
          }
        }

      });
    }

    if (this.getIncludeAdd()) {
      itemsArry.push({
        xtype: 'button',
        docked: 'right',
        itemId: tid + '_addnew',
        id: tid + '_addnew',
        iconCls: 'add',
        iconMask: true,
        ui: 'plain',
        width: 50,
        height: 50,
        cls: clsid,
        align: 'right',
        listeners: {
          tap: function (field) {
            var viewid = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id).id;
            if (Ext.getCmp(viewid).id.indexOf('list') > 0) {
              Ext.getCmp(viewid).config._pageid = 1;
              if (Ext.isDefined(Ext.getCmp(viewid).config._haslistchange)) {
                Ext.getCmp(viewid).config._haslistchange = true;
                Ext.getCmp(viewid).config._pageid = 1;
              }
            }
            if (Ext.isDefined(Ext.Viewport.getMenus()) && Ext.isDefined(Ext.Viewport.getMenus().right) && !Ext.Viewport.getMenus().right.isHidden()) {
              Ext.Viewport.hideMenu('right');
            }


          }
        }
      });
    }

    if (this.getIncludeFilter()) {
      itemsArry.push({
        xtype: 'button',
        docked: 'right',
        id: tid + '_showfilter',
        itemId: tid + '_showfilter',
        iconCls: 'search',
        iconMask: true,
        ui: 'plain',
        width: 50,
        height: 50,
        cls: clsid,
        listeners: {
          tap: function (field) {
            if (Ext.isDefined(Ext.Viewport.getMenus()) && Ext.isDefined(Ext.Viewport.getMenus().right) && !Ext.Viewport.getMenus().right.isHidden()) {
              Ext.Viewport.hideMenu('right');
            }
            var tparent = field.id.split('_')[0];
            var tid = '#' + tparent + ' searchfield';
            var temp = Ext.ComponentQuery.query(tid)[0].id;
            var parent = Ext.getCmp(temp).getParent().id;
            if (Ext.getCmp(parent).isHidden()) {
              Ext.getCmp(parent).show();
              Ext.getCmp(temp).focus();
            } else {
              Ext.getCmp(parent).hide();
            }
          }
        }
      });
    }


    this.setItems(itemsArry);
  }
});

