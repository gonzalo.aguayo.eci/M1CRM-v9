/*
 * @class Ext.ux.M1.M1InfoButton
 */

Ext.define('Ext.ux.M1.M1InfoButton', {
  extend: 'Ext.Button',
  xtype: 'm1infobutton',
  config: {
    iconAlign: 'center',
    iconMask: true,
    height: 40,
    margin: 1,
    layout: {pack: 'center'},
    parentformid: null,
    infoarray: null,
    infotype: null,
    listeners: {
      scope: this,
      tap: function (field) {
        if (field.getInfoarray() != null) {
          var infoarray = field.getInfoarray();
          M1CRM.util.crmViews.viewInfoWindow(
            field.getInfotype(),
            field.getParentformid(),
            infoarray[0] != null ? Ext.getCmp(infoarray[0]).getValue() : null,
            infoarray[1] != null ? Ext.getCmp(infoarray[1]).getValue() : null,
            infoarray[2] != null ? Ext.getCmp(infoarray[2]).getValue() : null,
            infoarray[3] != null ? Ext.getCmp(infoarray[3]).getValue() : null,
            infoarray[4] != null ? Ext.getCmp(infoarray[4]).getValue() : null,
            infoarray[5] != null ? Ext.getCmp(infoarray[5]).getValue() : null,
            infoarray[6] != null ? Ext.getCmp(infoarray[6]).getValue() : null,
            infoarray[7] != null ? Ext.getCmp(infoarray[7]).getValue() : null
          );
        }
      }
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-advancefieldbutton');
    this.setDisabledCls('m1-advancefieldbutton-disable');
    this.setIconCls('info');
    this.enable();
  }
});



