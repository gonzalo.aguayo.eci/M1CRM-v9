/*
 * @class Ext.ux.M1ToolbarAddNewButton
 */


Ext.define('Ext.ux.M1.M1ToolbarAddNewButton', {
  extend: 'Ext.Button',
  xtype: 'm1toolbaraddnewbutton',
  config: {
    docked: 'right',
    iconCls: 'add',
    defaults: {
      iconMask: true,
      ui: 'plain'
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-title-button');
  }
});



