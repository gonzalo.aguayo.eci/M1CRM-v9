/*
 * @class Ext.ux.M1.M1AdvTextField
 */

Ext.define('Ext.ux.M1.M1AdvTextField', {
  extend: 'Ext.Container',
  xtype: 'm1advtextfield',
  requires: [
    'Ext.field.Text',
    'Ext.Button',
    'Ext.ux.M1.M1InvisibleLookupButton'
  ],
  config: {
    layout: 'hbox',
    textfieldid: null,
    label: '',
    readOnly: false,
    hasinfobtn: false,
    infoarray: null,
    haslookupbtn: false,
    touppercase: null,
    required: false,
    defaults: {
      iconMask: true,
      ui: 'plain'
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-custcontrolcontainer');
    this.setAdvTextField(this.getRequired());

    if (Ext.os.deviceType == 'Phone') {
      Ext.getCmp(this.getTextfieldid()).setLabelAlign('top');
      Ext.getCmp(this.getTextfieldid()).setLabelCls('m1-label-phone-crm');
    } else {
      Ext.getCmp(this.getTextfieldid()).setLabelCls('m1-label-crm');
    }


    this.isDirty = false;
  },
  setAdvTextField: function (required) {
    var itemsArry = [];
    var label = required == true ? this.getLabel() + '<span style="font-weight:normal;font-size:15px;"> * </span>' : this.getLabel();

    itemsArry.push({
      xtype: 'textfield',
      readOnly: this.getReadOnly(),
      id: this.getTextfieldid(),
      itemId: this.getTextfieldid(),
      flex: 4,
      label: label,
      isDirty: false,
      cls: 'm1-field-crm',
      labelCls: 'm1-label-crm',
      inputCls: 'm1-textinput-crm',
      requiredCls: 'm1-required-crm2',
      listeners: {
        scope: this,
        keyup: function (field, e) {
          if (this.getTouppercase() != null && this.getTouppercase() == true) {
            field.setValue(field.getValue().toUpperCase());
          } else if (this.getTouppercase() != null && !this.getTouppercase()) {
            field.setValue(field.getValue().toLowerCase());
          }
        },
        change: function (field) {
          if (!field.getReadOnly()) {
            field.isDirty = true;
          }
        }
      }
    });

    if (this.getHaslookupbtn()) {
      itemsArry.push({
        xtype: 'button',
        id: this.getTextfieldid() + '_lookup',
        itemId: this.getTextfieldid() + '_lookup',
        ui: 'plain',
        iconMask: true,
        iconCls: 'search',
        defaults: {
          iconMask: true,
          ui: 'plain'
        },
        cls: 'm1-advancefieldbutton',
        disabledCls: 'm1-advancefieldbutton-disable',
        scope: this,
        hidden: this.getReadOnly()
      });
    } else {
      itemsArry.push({
        xtype: 'm1invisiblelookupbutton'
      });
    }

    if (this.getHasinfobtn() != null && this.getHasinfobtn() == true && this.getInfoarray() != null) {
      var infoarray = this.getInfoarray();
      itemsArry.push({
        xtype: 'button',
        id: this.getTextfieldid() + '_info',
        itemId: this.getTextfieldid() + '_info',
        iconAlign: 'center',
        iconCls: 'info',
        ui: 'plain',
        defaults: {
          iconMask: true,
          ui: 'plain'
        },
        scope: this,
        cls: 'm1-advancefieldbutton',
        disabledCls: 'm1-advancefieldbutton-disable',
        hidden: !this.getReadOnly(),
        listeners: {
          scope: this,
          tap: function (field) {
            M1CRM.util.crmViews.viewInfoWindow(
              infoarray[0],
              infoarray[1],
              infoarray[2] != null ? Ext.getCmp(infoarray[2]).getValue() : null,
              infoarray[3] != null ? Ext.getCmp(infoarray[3]).getValue() : null,
              infoarray[4] != null ? Ext.getCmp(infoarray[4]).getValue() : null,
              infoarray[5] != null ? Ext.getCmp(infoarray[5]).getValue() : null,
              infoarray[6] != null ? Ext.getCmp(infoarray[6]).getValue() : null,
              infoarray[7] != null ? Ext.getCmp(infoarray[7]).getValue() : null,
              infoarray[8] != null ? Ext.getCmp(infoarray[8]).getValue() : null,
              infoarray[9] != null ? Ext.getCmp(infoarray[9]).getValue() : null
            );
          }
        }
      });
    } else if (this.getHasinfobtn() != null && this.getHasinfobtn() == true && this.getInfoarray() == null) {
      itemsArry.push({
        xtype: 'button',
        iconAlign: 'center',
        iconCls: 'searchempty',
        ui: 'plain',
        defaults: {
          iconMask: true,
          ui: 'plain'
        },
        cls: 'm1-advancefieldinvisiblebutton-crm',
        disable: true
      });
    }

    this.setItems(itemsArry);
  }

});    