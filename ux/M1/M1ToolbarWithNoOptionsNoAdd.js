/*
 * @class Ext.ux.M1ToolbarWithNoOptionsNoAdd
 */


Ext.define('Ext.ux.M1.M1ToolbarWithNoOptionsNoAdd', {
  extend: 'Ext.Toolbar',
  xtype: 'm1toolbarwithnooptionsnoadd',

  requires: [
    'Ext.ux.M1.M1ToolbarBackButton',
    'Ext.ux.M1.M1ToolbarHomeButton',
    'Ext.ux.M1.M1ToolbarFilterButton',
    'Ext.ux.M1.M1ToolbarAddNewButton'
  ],
  config: {
    docked: 'top',
    layout: {
      pack: 'center'
    },
    defaults: {
      iconMask: true,
      ui: 'plain'
    },
    includeAddOption: false,
    includeFilter: false
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-crmhomescreen-title');
    this.setItemButtons();
  },

  setItemButtons: function () {
    var tid = this.getId().split('_tabbar')[0];
    var itemsArry = [];


    itemsArry.push(
      {
        xtype: 'm1toolbarbackbutton',
        itemId: tid + '_back',
        id: tid + '_back'
      }
    );
    itemsArry.push(
      {
        xtype: 'm1toolbarhomebutton',
        itemId: tid + '_tohome',
        id: tid + '_tohome'
      }
    );

    if (this.getIncludeAddOption()) {
      itemsArry.push(
        {
          xtype: 'm1toolbaraddnewbutton',
          itemId: tid + '_addnew',
          id: tid + '_addnew'
        }
      );
    }
    if (this.getIncludeFilter()) {
      itemsArry.push(
        {
          xtype: 'm1toolbarfilterbutton',
          id: tid + '_showfilter',
          itemId: tid + '_showfilter'
        }
      );
    }
    this.setItems(itemsArry);
  }
});

