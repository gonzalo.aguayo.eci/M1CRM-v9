/*
 * @class Ext.ux.M1.M1MenuButton
 */


Ext.define('Ext.ux.M1.M1MenuButton', {
  extend: 'Ext.Button',
  xtype: 'm1menubutton',
  config: {
    iconAlign: 'left',
    iconMask: false,
    ui: 'plain'
  },
  initialize: function () {
    this.callParent();
    this.setCls('mainmenu');
    this.setDisabledCls('m1-submenuemptybutton-crm');
    this.enable();
  }
});




