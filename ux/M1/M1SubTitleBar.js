/*
 * @class Ext.ux.M1.M1SubTitleBar
 */

Ext.define('Ext.ux.M1.M1SubTitleBar', {
  extend: 'Ext.TitleBar',
  xtype: 'm1subtitlebar',
  config: {
    docked: 'top',
    height: 15,
    titlelabelid: ''
  },
  initialize: function () {
    this.callParent();
    this.setHeight(15);
    //var titlecls = 'm1-subtitle-crm';
    var labelcls = 'm1-subtitlelabel-crm';
    if (this.getTitlelabelid().indexOf('list') > 0) {
      //titlecls =  this.getSubTitleClasBaseOnID(); //'m1-listsubtitle-crm';
      labelcls = 'm1-listsubtitlelabel-crm';
    }
    // this.setCls(titlecls);
    this.setItems([
      {
        xtype: 'label',
        id: this.getTitlelabelid(),
        cls: labelcls
      }
    ]);
  },
  getSubTitleClasBaseOnID: function () {
    if (this.getTitlelabelid().indexOf('followups') >= 0)
      return 'm1-listsubtitle-crm-followup';
    else if (this.getTitlelabelid().indexOf('customeropencallslist') >= 0)
      return 'm1-listsubtitle-crm-calls';
    else if (this.getTitlelabelid().indexOf('callslist') >= 0)
      return 'm1-listsubtitle-crm-calls';
    else if (this.getTitlelabelid().indexOf('customercontactlist') >= 0)
      return 'm1-listsubtitle-crm-contact';
    else if (this.getTitlelabelid().indexOf('cust') >= 0)
      return 'm1-listsubtitle-crm-organisation';
    else if (this.getTitlelabelid().indexOf('quoteparts') >= 0)
      return 'm1-listsubtitle-crm-quotes';
    else if (this.getTitlelabelid().indexOf('leads') >= 0)
      return 'm1-listsubtitle-crm-leads';
    else if (this.getTitlelabelid().indexOf('leadlines') >= 0)
      return 'm1-listsubtitle-crm-leadlines';
    else
      return 'm1-listsubtitle-crm';
  }

});    