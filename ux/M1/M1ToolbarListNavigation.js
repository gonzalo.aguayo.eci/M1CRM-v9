/*
 * @class Ext.ux.M1.M1ToolbarListNavigation
 */


Ext.define('Ext.ux.M1.M1ToolbarListNavigation', {
  extend: 'Ext.Toolbar',
  xtype: 'm1toolbarlistnavigation',
  requires: [
    'Ext.Button',
    'Ext.Label',
    'Ext.ux.M1.M1FormButton'
  ],
  config: {
    docked: 'bottom',
    scrollable: false, // 'horizontal',
    titleid: '',
    layout: {
      pack: 'center'
    },
    defaults: {
      iconMask: true,
      ui: 'plain'
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-screen-bar');
    this.setNavItems();
  },

  getNavButton: function (navid, labelid) {
    var iconclsname = '';
    if (labelid == '_ToFirstPage') {
      iconclsname = 'first';   //'rewind';
    } else if (labelid == '_ToPrevPage') {
      iconclsname = 'backward';  //'arrow_left';
    } else if (labelid == '_ToNextPage') {
      iconclsname = 'forward'; //'arrow_right';
    } else if (labelid == '_ToLastPage') {
      iconclsname = 'last'  //'forward';
    }

    return new Ext.create('Ext.Button', {
      id: navid + labelid,
      iconCls: iconclsname,
      iconMask: true,
      ui: 'plain',
      width: 50,
      height: 50,
      cls: 'm1-navigate-button', // 'm1-tab',
      listeners: {
        tap: function (field) {
          var obj = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id);
          var contname = obj.__proto__.$className.split('.')[2];
          var tempid = field.id.split(obj.id)[1];
          if (tempid == '_ToFirstPage') {
            M1CRM.app.getController(contname).toFirstPageTap();
          } else if (tempid == '_ToPrevPage') {
            M1CRM.app.getController(contname).toNextORPrevBtnTap(false);
          } else if (tempid == '_ToNextPage') {
            M1CRM.app.getController(contname).toNextORPrevBtnTap(true);
          } else if (tempid == '_ToLastPage') {
            M1CRM.app.getController(contname).toLastPageTap();
          }
        }
      }
    });
  },

  setNavItems: function () {
    var tid = this.getId().split('_tabbar')[0];
    var itemsArry = [];
    itemsArry.push(this.getNavButton(tid, '_ToFirstPage'));
    itemsArry.push(this.getNavButton(tid, '_ToPrevPage'));
    itemsArry.push({
      id: this.getTitleid(),
      xtype: 'label',
      cls: 'm1-pagesummarylabel-crm'
    });
    itemsArry.push(this.getNavButton(tid, '_ToNextPage'));
    itemsArry.push(this.getNavButton(tid, '_ToLastPage'));
    this.setItems(itemsArry);
  }
});

