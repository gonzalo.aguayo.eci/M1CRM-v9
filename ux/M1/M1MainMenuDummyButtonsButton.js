/*
 * @class Ext.ux.M1.M1MainMenuDummyButtonsButton
 */

Ext.define('Ext.ux.M1.M1MainMenuDummyButtonsButton', {
  extend: 'Ext.Button',
  xtype: 'mainmenudummybutton',
  config: {
    height: 80,
    width: 100,
    margin: 2,
    flex: 2,
    showAnimation: "fadeIn",
    hideAnimation: "fadeOut"
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-mainmenudummyactionbutton-crm');
  }
});    
