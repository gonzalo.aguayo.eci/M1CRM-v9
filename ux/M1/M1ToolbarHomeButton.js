/*
 * @class Ext.ux.M1ToolbarHomeButton
 */


Ext.define('Ext.ux.M1.M1ToolbarHomeButton', {
  extend: 'Ext.Button',
  xtype: 'm1toolbarhomebutton',
  config: {
    docked: 'left',
    iconCls: 'home1',
    iconMask: true,
    align: 'left',
    listeners: {
      tap: function (field) {
        var obj = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id);
        var contname = obj.__proto__.$className.split('.')[2];
        if (Ext.isDefined(M1CRM.app.getController(contname).processAutoSave)) {
          M1CRM.app.getController(contname).processAutoSave('home');
        } else {
          var viewid = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id).id;
          M1CRM.app.getController(contname).toBackTap(viewid);
        }
      }
    }
  },
  initialize: function () {
    this.callParent();
    // this.setCls('m1-title-button');
    this.setIconCls('home1');

  }
});



