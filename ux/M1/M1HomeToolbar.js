/*
 * @class Ext.ux.M1.M1HomeToolbar
 */


Ext.define('Ext.ux.M1.M1HomeToolbar', {
  extend: 'Ext.Toolbar',
  xtype: 'm1hometoolbar',
  config: {
    id: 'homeToolBar',
    docked: 'top',
    title: {
      title: 'The title text',
      style: {
        'text-align': 'left'
      }
    },
    cls: "m1-crmhomescreen-title",
    layout: {
      pack: 'left'
    },
    defaults: {
      iconMask: true,
      ui: 'plain'
    }

  },
  initialize: function () {
    this.callParent();
    //this.setToolbarItems();
  },
  setToolbarItems: function () {
    var tid = this.getId().split('_toolbarid')[0];
    var itemsArry = [];
    itemsArry.push({
      xtype: 'button',
      docked: 'right',
      //itemId: tid + '_exit',
      id: tid + '_exit',
      iconMask: true,
      ui: 'plain',
      width: 50,
      height: 50,
      iconCls: 'delete',
      cls: 'm1-title-button',
      align: 'right',
      listeners: {
        tap: function (field) {
          var contname = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id).__proto__.$className.split('.')[2];
          if (contname.indexOf('crm') >= 0) {
            M1CRM.app.getController(contname).logoutTap();
          }
        }
      }
    });

    this.setItems(itemsArry);
  }
});
   
  