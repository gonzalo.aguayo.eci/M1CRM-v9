/*
 * @class Ext.ux.M1ToolbarListSearch
 */


Ext.define('Ext.ux.M1.M1ToolbarListSearch', {
  extend: 'Ext.Toolbar',
  xtype: 'm1toolbarlistsearch',
  requires: [
    'Ext.field.Search'
  ],
  config: {
    docked: 'top',
    height: 25,
    // cls: 'm1-screen-title',
    defaults: {
      iconMask: true
    },
    searchfieldid: null,
    placeHolderText: ''
  },
  initialize: function () {
    this.callParent();
    //this.setSearchBackgroundClassByID();
    //this.setCls('m1-toolbar-search');
    this.setSearchItems();
  },
  setSearchBackgroundClassByID: function () {
    var tid = this.getId().split('_toolbarid')[0];
    if (tid.indexOf('followup') >= 0) {
      this.setCls('m1-toolbar-search-followup');
      // }else if (tid.indexOf('custlocations') >= 0){
      //	 this.setCls('m1-toolbar-search-locations');
    } else if (tid.indexOf('callline') >= 0) {
      this.setCls('m1-title-button-calllines');
    } else if (tid.indexOf('calls') >= 0 || tid.indexOf('opencall')) {
      this.setCls('m1-toolbar-search-calls');
    } else if (tid.indexOf('allquotesoncontact') >= 0) {
      return 'm1-toolbar-search-quotes';
    } else if (tid.indexOf('contact') >= 0) {
      this.setCls('m1-toolbar-search-contacts');
    } else if (tid.indexOf('customer') >= 0 || tid.indexOf('cust') >= 0) {
      this.setCls('m1-toolbar-search-organisation');
    } else if (tid.indexOf('quotes') >= 0) {
      this.setCls('m1-toolbar-search-quotes');
    } else if (tid.indexOf('quotes') >= 0) {
      this.setCls('m1-toolbar-search-quotes');
    } else if (tid.indexOf('leads') >= 0) {
      this.setCls('m1-toolbar-search-leads');
    } else if (tid.indexOf('leads') >= 0) {
      this.setCls('m1-toolbar-search-leads');
    } else if (tid.indexOf('leadlines') >= 0) {
      this.setCls('m1-toolbar-search-leadlines');
    } else if (tid.indexOf('parts') >= 0) {
      this.setCls('m1-toolbar-search-parts');
    } else {
      this.setCls('m1-toolbar-search');
    }
  },
  setSearchItems: function () {

    var tid = this.getId().split('_toolbarid')[0];
    var itemsArry = [];


    var itemsArry = [
      {
        xtype: 'searchfield',
        cls: 'm1-field-crm',
        inputCls: 'm1-textsearchfield-input-crm',   //'m1-textinput-crm',
        itemId: this.getSearchfieldid(),
        id: this.getSearchfieldid(),
        flex: 1,
        placeHolder: this.getPlaceHolderText(),
        listeners: {
          change: function (field) {
            var listid = Ext.getCmp(field.id).getParent().getParent().id;

            if (Ext.isDefined(Ext.getCmp(listid).config._haslistchange)) {
              Ext.getCmp(listid).config._haslistchange = true;
            }


            var obj = Ext.getCmp(listid);
            var contname = obj.__proto__.$className.split('.')[2];
            Ext.getCmp(listid).config._pageid = 1;
            M1CRM.app.getController(contname).loadData();
          },

          clearicontap: function (field) {
            var listid = Ext.getCmp(field.id).getParent().getParent().id;
            if (Ext.isDefined(Ext.getCmp(listid).config._haslistchange)) {
              Ext.getCmp(listid).config._haslistchange = true;
            }
            //Ext.getCmp(field.id).setValue('');
            var obj = Ext.getCmp(listid);
            var contname = obj.__proto__.$className.split('.')[2];
            M1CRM.app.getController(contname).onClearSearch(listid);
          }
        }
      }

    ];
    this.setItems(itemsArry);
  }
});

