/*
 * @class Ext.ux.M1.M1QuoteQtyAddClear
 */

Ext.define('Ext.ux.M1.M1QuoteQtyAddClear', {
  extend: 'Ext.Container',
  xtype: 'm1quoteqtyaddclear',
  requires: [
    'Ext.Button'
  ],
  config: {
    layout: 'hbox',
    id: null,
    fieldsetid: null
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-custcontrolcontainer');
    this.setQuoteQtyAddClearBtns();
  },
  setQuoteQtyAddClearBtns: function () {
    var itemsArry = [];
    //var id = this.getContainerid();
    itemsArry.push(this.getCancelQtyBtn(this.getId()));
    itemsArry.push(this.getAddQtyBtn(this.getId()));
    this.setItems(itemsArry);
  },

  getCancelQtyBtn: function (id) {
    return new Ext.create('Ext.Button', {
      iconAlign: 'left',
      id: id + '_clear',
      iconMask: true,
      iconCls: 'delete',
      text: ' Remove Quote Break' + parseInt(id.slice(-1)),
      labelCls: "m1-titlebutton-label-crm",
      cls: "m1-titlebutton-crm",
      listeners: {
        scope: this,
        tap: function () {
          Ext.getCmp(this.getFieldsetid()).hide();
          Ext.getCmp(id + '_clear').hide();
          Ext.getCmp(id + '_add').show();

          var curctrlid = parseInt(id.slice(-1));
          var curctrl = id.substring(0, id.length - 1);
          Ext.getCmp(curctrl + 'id' + curctrlid).setValue(0);
          Ext.getCmp('addeditquotepartqty_quoteqty' + curctrlid).setValue(0);
          Ext.getCmp('addeditquotepartqty_unitprice' + curctrlid).setValue(0);
          Ext.getCmp('addeditquotepartqty_discount' + curctrlid).setValue(0);
          Ext.getCmp('addeditquotepartqty_unitdiscount' + curctrlid).setValue(0);
          Ext.getCmp('addeditquotepart_revisedunitprice' + curctrlid).setValue(0);

          var nextctrlid = id.substring(0, id.length - 1) + curctrlid;
          if (curctrlid > 2) {
            curctrlid++;
            nextctrlid = id.substring(0, id.length - 1) + curctrlid;
            Ext.getCmp(nextctrlid).hide();
            Ext.getCmp(nextctrlid + '_add').hide();
            Ext.getCmp(nextctrlid + '_clear').show();
          } else {
            nextctrlid = id.substring(0, id.length - 1) + 2;
            Ext.getCmp(id.substring(0, id.length - 1) + curctrlid + '_add').show();
            for (curctrlid = 3; curctrlid <= 9; curctrlid++) {
              if (Ext.getCmp('addeditquotepart_qty' + curctrlid + 'fieldset').isHidden()) {
                if (!Ext.getCmp(id.substring(0, id.length - 1) + curctrlid + '_add').isHidden()) {
                  Ext.getCmp(id.substring(0, id.length - 1) + curctrlid + '_add').hide();
                }
              }
            }
          }
          Ext.getCmp(this.getFieldsetid()).hide();
        }
      }
    });
  },

  getAddQtyBtn: function (id) {
    return new Ext.create('Ext.Button', {
      iconAlign: 'left',
      id: id + '_add',
      iconMask: true,
      iconCls: 'add',
      cls: "m1-titlebutton-crm",
      text: ' Add Quote Break ' + parseInt(id.slice(-1)),
      textAlign: 'left',
      labelCls: "m1-titlebutton-label-crm",
      listeners: {
        tap: function () {
          Ext.getCmp(this.getParent().getFieldsetid()).show();
          Ext.getCmp(id + '_clear').show();
          Ext.getCmp(id + '_add').hide();
          var nextid = parseInt(id.slice(-1));

          if (nextid < 9) {
            nextid++;
            var nextctrlid = id.substring(0, id.length - 1) + nextid;
            Ext.getCmp(nextctrlid).show();
            Ext.getCmp(nextctrlid + '_add').show();
            Ext.getCmp(nextctrlid + '_clear').hide();
            var curctrl = id.substring(0, id.length - 1);
            Ext.getCmp('addeditquotepartqty_quoteqty' + parseInt(id.slice(-1))).focus();
          }

        }
      }
    });
  },

  destroy: function () {
    var me = this,
      items = me.listItems,
      ln = items.length,
      i;
    me.callParent(arguments);
    if (me.onIdleBound) {
      Ext.AnimationQueue.unIdle(me.onAnimationIdle, me);
    }
    for (i = 0; i < ln; i++) {
      items[i].destroy();
    }
    me.listItems = null;
  }

});    