/*
 * @class Ext.ux.M1.M1Logo
 */

Ext.define('Ext.ux.M1.M1Logo', {
  extend: 'Ext.Container',
  requires: [
    'Ext.Img'
  ],
  xtype: 'm1logo',
  config: {
    docked: 'bottom',
    height: 20,
    items: [
      {
        xtype: 'label',
        cls: 'm1-label',
        html: 'Version 3.0.44'
      },
      {
        xtype: 'image',
        docked: 'right',
        iconCls: 'logo',
        height: 20,
        width: 64,
        src: 'resources/images/logo.png'
      }

    ]
  },
  initialize: function () {
    this.callParent();
  }
});
