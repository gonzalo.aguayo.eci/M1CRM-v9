/*
 * @class Ext.ux.M1.M1BasicTextArea
 */

Ext.define('Ext.ux.M1.M1BasicTextArea', {
  extend: 'Ext.field.TextArea',
  xtype: 'm1basictextarea',
  config: {
    flex: 1,
    isDirty: false,
    listeners: {
      scope: this,
      change: function (field) {
        if (!field.getReadOnly()) {
          field.isDirty = true;
        }
      }
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-field-crm');
    //this.setLabelCls('m1-label-crm');
    if (Ext.os.deviceType == 'Phone') {
      this.setLabelAlign('top');
      this.setLabelCls('m1-label-phone-crm');
    } else {
      this.setLabelCls('m1-label-crm');
    }
    this.setInputCls('m1-textinput-crm');
    this.isDirty = false;
  }
});    