/*
 * @class Ext.ux.M1ToolbarBackButton
 */


Ext.define('Ext.ux.M1.M1ToolbarBackButton', {
  extend: 'Ext.Button',
  xtype: 'm1toolbarbackbutton',
  config: {
    docked: 'left',
    iconCls: 'back',
    iconMask: true,
    ui: 'plain',
    //cls: 'm1-title-button',
    listeners: {
      tap: function (field) {
        var obj = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id);
        var contname = obj.__proto__.$className.split('.')[2];
        if (Ext.isDefined(M1CRM.app.getController(contname).processAutoSave)) {
          M1CRM.app.getController(contname).processAutoSave('back');
        } else {
          var viewid = Ext.getCmp(Ext.getCmp(field.id).getParent().getParent().id).id;
          M1CRM.app.getController(contname).toBackTap(viewid);
        }
      }
    }
  },
  initialize: function () {
    this.callParent();
    this.setCls('m1-title-button');
  }
});



