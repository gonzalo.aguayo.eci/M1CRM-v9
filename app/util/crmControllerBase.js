Ext.define('M1CRM.util.crmControllerBase', {
  extend: 'Ext.app.Controller',
  requires: [
    'M1CRM.model.crmUserSecurity'
  ],
  config: {
    _nextPageId: null,
    _prevPageId: null,
    _navigateSummaryId: null,
    _dirty: false,
    _seckey: 0,
    _orgrec: null,
    _paramObj: null,
    _userkey: null
  },

  securityAccessLevel: {
    Default: 0,
    None: 1,
    View: 2,
    Edit: 4,
    Add: 8,
    Delete: 16,
    ChangeID: 32
  },

  checkAccessLevel: function (current) {
    var access = {
      Default: !!(current & this.securityAccessLevel.Default),
      None: !!(current & this.securityAccessLevel.None),
      View: !!(current & this.securityAccessLevel.View),
      Edit: !!(current & this.securityAccessLevel.Edit),
      Add: !!(current & this.securityAccessLevel.Add),
      Delete: !!(current & this.securityAccessLevel.Delete),
      ChangeID: !!(current & this.securityAccessLevel.ChangeID)
    };

    return access;
  },

  getOptionsMenu: function (controller) {
    return M1CRM.util.crmViews.getOptionsMenu(controller);

  },
  getParamObj: function (viewid) {
    return Ext.getCmp(viewid).config._paramObj;
  },
  restParamObj: function (viewid) {
    Ext.getCmp(viewid).config._paramObj = null;
  },

  optionsMenuTap: function (button) {
    this.getOptionsMenu(this).showBy(button);
  },

  hideOptionsMenu: function () {
    if (Ext.isDefined(Ext.Viewport.getMenus()) && !Ext.Viewport.getMenus().right.isHidden()) {
      Ext.Viewport.hideMenu('right');
    }
  },

  createMenu: function (items) {
    return Ext.create('Ext.Menu', {
      style: 'padding :0',
      cls: items[0].cls,  //'mainmenu',
      id: 'menu',
      width: 200,
      top: 0,
      left: 0,
      bottom: 0,
      zIndex: 0,
      width: 266,
      padding: '40 0 0 0',
      scrollable: 'vertical',
      defaultType: 'button',
      defaults: {
        textAlign: 'left'
      },
      items: items
    });
  },
  onClearSearch: function (listid) {
    Ext.getCmp(listid).getStore().clearFilter();
  },
  applyUserSecuritySettingsOnHome: function (key, control) {
    if (key == '12' || key == '28' || key == '30' || key == '60' || key == '62') {
      Ext.getCmp(control).enable();
    } else {
      Ext.getCmp(control).disable();
    }
  },
  applyUserSecurityOnControl: function (level, controls) {
    var that = this;
    var currentLevel = level === 60 ? 62 : level || 1;
    var grantedLevels = this.checkAccessLevel(currentLevel);

    _.each(controls, function(control, access) {
      var isGranted = grantedLevels[access];
      Ext.getCmp(control)[isGranted ? 'enable' : 'disable']();
    });
  },
  getParetViewId: function () {
    return Ext.getCmp(this.config._listid).config._parentformid;
  },
  applyUserSecuritySettings: function (viewid, key, skey, formbuttons) {
    if (key == 0 || key == 1) {
      var msgstr = viewid.match(/list/i) ? "list" : "form";
      Ext.Msg.alert('Warning', 'You don\'t have access to ' + msgstr, function (btn) {
        if (btn === 'ok') {
          var flag = false;
          this.toBackTap(viewid);
        }
      }, this);
      return false;
    } else {
      if (viewid.match(/list/i)) {
        if (Ext.getCmp(viewid + '_toolbarid') != null) {
          this.processEnableOrDisableToolbarButtons(viewid, key);
        }
        this.processHideOrShowMenuOptions(viewid, key, true);

      } else {
        if (key != this.config._seckey) {
          this.config._seckey = key;
        }
        if (Ext.getCmp(viewid + '_toolbarid') != null) {
          this.processEnableOrDisableToolbarButtons(viewid, key);
        }
        this.processHideOrShowMenuOptions(viewid, key, true);
      }
    }
    return true;
  },
  isFormButtonHiddenBySecurity: function (buttonid) {
    if (this.ismatchingButton(buttonid, this.config._seckey)) {
      return Ext.getCmp(buttonid).isHidden();
    } else
      return false;
  },
  processEnableOrDisableToolbarButtons: function (viewid, key) {
    var toolbar = Ext.getCmp(viewid + '_toolbarid');
    var harray = [];
    var sarray = [];
    var itemid = '';
    var totItms = Ext.getCmp(viewid + '_toolbarid').getItems().length;
    for (var i = 0; i < totItms; i++) {
      itemid = toolbar.getItems().items[i].getId();
      if (!itemid.match(/_options/i) && !itemid.match(/_tohome/i) && !itemid.match(/_back/i)) {
        if (this.ismatchingButton(itemid, key)) {
          harray.push(itemid);
        } else {
          sarray.push(itemid);
        }
      }
    }
    if (harray.length > 0) {
      this.hideControls(harray);
    }
    if (sarray.length > 0) {
      this.showControls(sarray);
    }

  },
  processHideOrShowMenuOptions: function (viewid, key, flag) {
    if (this.getOptionsMenu(this) != null && this.getOptionsMenu(this).items != null && this.getOptionsMenu(this).items.items != null) {
      var mitemarray = [];
      var totItms = this.getOptionsMenu(this).items.items.length;
      var itemid = '';
      for (var i = 0; i < totItms; i++) {
        itemid = this.getOptionsMenu(this).items.items[i].id;
        if (this.ismatchingButton(itemid, key)) {
          mitemarray.push(itemid);
        }
      }
      if (mitemarray.length > 0) {
        this.hideControls(mitemarray);
        this.optionsMenuHide(viewid, flag);
      }
    }
  },
  ismatchingButton: function (itemid, key) {
    if (key == 2) {
      if (itemid.match(/_addnewbtn/i) || itemid.match(/_addnew/i) || itemid.match(/_newbtn/i) || itemid.match(/_addbtn/i) || itemid.match(/_editbtn/i) || itemid.match(/_deletebtn/i) || itemid.match(/_processbtn/i) || itemid.match(/_savebtn/i) || itemid.match(/_completebtn/i))
        return true;
      else
        return false;
    } else if (key == 4) {
      if (itemid.match(/_addnewbtn/i) || itemid.match(/_addnew/i) || itemid.match(/_addbtn/i) || itemid.match(/_deletebtn/i) || itemid.match(/_processbtn/i) || itemid.match(/_completebtn/i))
        return true;
      else
        return false;
    } else if (key == 12) {
      if (itemid.match(/_deletebtn/i) || itemid.match(/_processbtn/i))
        return true;
      else
        return false;
    } else if (key == 20) {
      if (itemid.match(/_addnewbtn/i) || itemid.match(/_addbtn/i) || itemid.match(/_addnew/i) || itemid.match(/_processbtn/i) || itemid.match(/_completebtn/i))
        return true;
      else
        return false;
    }
    if (key == 28) {
      if (itemid.match(/_processbtn/i))
        return true;
      else
        return false;
    }
    else
      return false;
  },
  setComponentSecurity: function (formbuttons) {
    if (formbuttons != null && formbuttons.length > 0) {
      var itemarray = [];
      var itemid = '';
      //Call Management
      for (var i = 0; i < formbuttons.length; i++) {
        itemid = formbuttons[i];
        if (this.ismatchingCallManagementButtons(itemid)) {
          if (!Ext.getCmp(itemid).isDisabled())
            itemarray.push(itemid);
        }
      }
      if (itemarray.length > 0) {
        SessionObj.getCallManagement() == true ? this.enableControls(itemarray) : this.disableControls(itemarray);
      }


      //CRM Advance
      if (this.isCRMAdvanceAvailable() == true) {

        //Quote Management
        itemarray = [];
        for (var i = 0; i < formbuttons.length; i++) {
          itemid = formbuttons[i];
          if (this.ismatchingQuoteManagementButtons(itemid)) {
            if (!Ext.getCmp(itemid).isDisabled())
              itemarray.push(itemid);
          }
        }
        if (itemarray.length > 0) {
          SessionObj.getQuotationManagement() == true ? this.enableControls(itemarray) : this.disableControls(itemarray);
        }

        //Lead Management
        itemarray = [];
        for (var i = 0; i < formbuttons.length; i++) {
          itemid = formbuttons[i];
          if (this.ismatchingLeadManagementButtons(itemid)) {
            if (!Ext.getCmp(itemid).isDisabled())
              itemarray.push(itemid);
          }
        }
        if (itemarray.length > 0) {
          SessionObj.getLeadManagement() == true ? this.enableControls(itemarray) : this.disableControls(itemarray);
        }
      } else {
        itemarray = [];
        for (var i = 0; i < formbuttons.length; i++) {
          itemid = formbuttons[i];
          if (this.ismatchingCRMAdvanceButtons(itemid)) {
            if (!Ext.getCmp(itemid).isDisabled())
              itemarray.push(itemid);
          }
        }
        if (itemarray.length > 0) {
          this.disableControls(itemarray);
        }
      }
    }
  },

  ismatchingCallManagementButtons: function (itemid) {
    if (itemid.match(/calls/i) || itemid.match(/call/i))
      return true;
    return false;
  },
  ismatchingQuoteManagementButtons: function (itemid) {
    return (itemid.match(/quotes/i) || itemid.match(/quote/i) || itemid.match(/quotations/i)) ? true : false;
  },

  ismatchingLeadManagementButtons: function (itemid) {
    return (itemid.match(/leads/i) || itemid.match(/lead/i)) ? true : false;
  },

  ismatchingCRMAdvanceButtons: function (itemid) {
    if (itemid.match(/quotes/i) || itemid.match(/quote/i) || itemid.match(/quotations/i) ||
      itemid.match(/leads/i) || itemid.match(/lead/i))
      return true;
    return false;
  },

  processHideOrShowFormButtons: function (formbuttons, key) {
    if (formbuttons != null && formbuttons.length > 0) {
      var itemarray = [];
      var itemid = '';
      for (var i = 0; i < formbuttons.length; i++) {
        itemid = formbuttons[i];
        if (this.ismatchingButton(itemid, key)) {
          itemarray.push(itemid);
        }
      }
      if (itemarray.length > 0) {
        this.disableControls(itemarray);
      }
    }
  },
  optionsMenuHide: function (viewid, hide) {
    if (this.getOptionsMenu(this) != null && this.getOptionsMenu(this).items != null && this.getOptionsMenu(this).items.items != null) {
      var totItms = this.getOptionsMenu(this).items.items.length;
      itemcount = 0;
      for (var i = 0; i < totItms; i++) {
        if (Ext.getCmp(this.getOptionsMenu(this).items.items[i].id).isHidden())
          itemcount++;
      }
      if (itemcount == totItms) {
        var toolbarid = viewid + '_toolbarid';
        var array = Ext.getCmp(toolbarid).getItems().items;
        if (array.length > 0) {
          for (var i = 0; i < array.length; i++) {
            if (array[i].id == (viewid + '_options')) {
              if (hide) {
                Ext.getCmp(Ext.getCmp(toolbarid).getItems().items[i].id).hide();
                break;
              }
              else {
                if (Ext.getCmp(Ext.getCmp(toolbarid).getItems().items[i].id).isHidden()) {
                  Ext.getCmp(Ext.getCmp(toolbarid).getItems().items[i].id).show();
                  break;
                }
              }
            }
          }
        }
      }
    }
  },
  onSearchKeyUp: function (field, storeid, modelid) {
    if (field) {
      var value = field.getValue();
      var store = Ext.getCmp(storeid).getStore();
      store.clearFilter();
      var fieldArr = Ext.create('M1CRM.model.' + modelid, {}).getListSearchFields();
      var thisRegEx = new RegExp(value, "i");
      store.filterBy(function (record) {
        for (idx = 0; idx < fieldArr.length; idx++) {
          if (thisRegEx.test(record.get(fieldArr[idx])))
            return true;
        }
        return false;
      });
    }
  },
  slideLeft: function (view) {
    Ext.Viewport.animateActiveItem(view, {type: 'slide', direction: 'left'});
  },
  slideRight: function (view) {
    Ext.Viewport.animateActiveItem(view, {type: 'slide', direction: 'right'});
  },

  viewDetail: function (currentview, item) {
    var view = M1CRM.util.crmViews.getViewByParentFormId(Ext.getCmp(currentview).config._detailformid);
    view.setRecord(item);
    view.config._parentformid = currentview;
    this.slideLeft(view);
  },
  toBackTap: function (currentview, keeprecord) {
    if (Ext.getCmp(currentview).getRecord() != null && keeprecord == null || keeprecord == false) {
      Ext.getCmp(currentview).setRecord(null);
    }
    this.clearSearchField();
    var parentformid = Ext.getCmp(currentview).config._parentformid;
    if (Ext.getCmp(currentview).id.indexOf('list') > 0) {
      Ext.getCmp(currentview).config._pageid = 1;
      if (Ext.isDefined(Ext.getCmp(currentview).config._haslistchange))
        Ext.getCmp(currentview).config._haslistchange = true;
    }
    var view = M1CRM.util.crmViews.getHome();

    if (parentformid != null) {
      var view = M1CRM.util.crmViews.getViewByParentFormId(parentformid);
      if (view.id != 'crmhome' && Ext.isDefined(Ext.getCmp(view.id).config._haslistchange)) {
        Ext.getCmp(view.id).config._haslistchange = false;
      }
    }
    M1CRM.util.crmViews.slideRight(view);
  },
  toHome: function () {
    this.clearSearchField();
    var view = M1CRM.util.crmViews.getHome();
    M1CRM.util.crmViews.slideRight(view);
  },

  onNewContainerPop: function (objnevView, objview) {
    if (objview._store)
      Ext.data.StoreManager.unregister(objview.getStore());
    objview.destroy();
  },
  manageShowHideControl: function (control) {
    if (control.isHidden()) {
      control.show();
    } else {
      control.hide();
    }
  },
  displayValidationMessage: function (error) {
    var msg = '';
    for (var i = 0; i < error.items.length; i++) {
      msg += error.items[i].getMessage() + '<br>';
    }
    ;
    Ext.Msg.alert('Warning', msg);
  },
  displaySaveErrorMessage: function (process) {
    var msg = 'Error saving ' + process;
    Ext.Msg.alert('Error', msg);
  },
  getSelectFieldDisplayValue: function (control) {
    if (control != null)
      return control.getRecord() != null ? control.getRecord().get(control.getDisplayField()) : '';
    else
      return '';
  },
  viewInfo: function (type, parentform, custid, custname, locationid, contactid, partid, revisionid) {
    M1CRM.util.crmViews.viewInfoWindow(type, parentform, custid, custname, locationid, contactid, partid, revisionid);
  },
  createOptionsMenu: function (items) {
    M1CRM.util.crmViews.createOptionsMenu(this, items);
  },
  clearFields: function (fields) {
    Ext.each(fields, function (field) {
      field.setValue('');
    }, this);
  },
  clearFormFields: function () {
    var fields = this.getForm().getFieldsArray()
    Ext.each(fields, function (field) {
      if (field.getComponent().getType() == 'checkbox') {
        if (field.originalState)
          field.check();
        else
          field.uncheck();
      } else {
        if (field.getUi() != null && field.getUi() == 'select') {
          field.setValue('');
        } else {
          field.setValue(field.originalValue != null ? field.originalValue : '');
        }
      }
    }, this);
  },
  initilize: function () {
    this.resetPageId();
  },
  setEntryFormMode: function (mode) {
    if (mode.toLowerCase() == 'view') {
      this.setEntryFormAsView();
    } else if (mode.toLowerCase() == 'edit') {
      this.setEntryFormAsEdit();
    } else if (mode.toLowerCase() == 'addnew') {
      this.setEntryFormAsAddNew();
    }
  },
  setEntryFormAsView: function () {
    this.getSavebtn().hide()
    this.getCancelbtn().hide();
    if (this.isFormButtonHiddenBySecurity(this.getAddnewbtn().id)) {
      this.getAddnewbtn().hide();
    } else {
      this.getAddnewbtn().show();
    }
    if (this.isFormButtonHiddenBySecurity(this.getEditbtn().id)) {
      this.getEditbtn().hide();
    } else {
      this.getEditbtn().show();
    }
    var clsid = 'm1-crmhomescreen-title';
    this.getSavebtn().getParent().setCls(clsid);
    clsid = 'm1-subtitle-crm';
    if (this.getSubtitle() != null) {
      this.getSubtitle().getParent().setCls(clsid);
      this.getSubtitle().getParent().getParent().setCls(clsid);
      this.getSubtitle().getParent().getParent().show();
    }
    var arr = ["m1-title-button", "x-dock-item", "x-docked-left"];
    Ext.getCmp(this.getForm().id + '_back').setCls(arr);
    Ext.getCmp(this.getForm().id + '_home').setCls(arr);
  },
  setEntryFormAsEdit: function () {
    this.getSavebtn().show()
    this.getCancelbtn().show();

    this.getAddnewbtn().hide();
    this.getEditbtn().hide();
    var clsid = 'm1-crmhomescreen-edit';
    this.getSavebtn().getParent().setCls(clsid);
    clsid = 'm1-subtitle-crm-edit';
    if (this.getSubtitle() != null) {
      this.getSubtitle().getParent().setCls(clsid);
      this.getSubtitle().getParent().getParent().setCls(clsid);
      this.getSubtitle().getParent().getParent().show();
    }
    var arr = ["m1-title-button-edit", "x-dock-item", "x-docked-left"];
    Ext.getCmp(this.getForm().id + '_back').setCls(arr);
    Ext.getCmp(this.getForm().id + '_home').setCls(arr);
  },
  setEntryFormAsAddNew: function () {
    this.getSavebtn().show()
    this.getCancelbtn().show();
    this.getAddnewbtn().hide();
    this.getEditbtn().hide();
    var clsid = 'm1-crmhomescreen-edit';
    this.getSavebtn().getParent().setCls(clsid);
    clsid = 'm1-subtitle-crm-edit';
    if (this.getSubtitle() != null) {
      this.getSubtitle().getParent().setCls(clsid);
      this.getSubtitle().getParent().getParent().setCls(clsid);
      this.getSubtitle().getParent().getParent().hide();
    }
    var arr = ["m1-title-button-edit", "x-dock-item", "x-docked-left"];
    Ext.getCmp(this.getForm().id + '_back').setCls(arr);
    Ext.getCmp(this.getForm().id + '_home').setCls(arr);
  },
  /*
   search : function (modelid, dataObj, pagesize) {
   var listid = this.getListId();
   var prevbtnid = this.config._prevPageId;
   var nextbtnid = this.config._nextPageId;
   var summarylabelid = this.config._navigateSummaryId;
   if (dataObj == null ) {
   dataObj = Ext.create('M1CRM.model.' + modelid, {});
   }
   dataObj.data.filterValues = this.getSearchFieldValue();
   dataObj.data.filterFields = dataObj.getListSearchFields();
   dataObj.data.pageID = Ext.getCmp(listid).config._pageid;
   dataObj.data.maxLimit = SessionObj.statics.MaxRecordsPerPage;

   Ext.create('M1CRM.model.' + modelid, {}).searchData({
   jsondataObj : JSON.stringify(dataObj.data),
   onReturn : function (result) {
   if (result != null){
   var store = result;
   Ext.getCmp(listid).setStore(store);
   store.load();

   if ( store.getData().items.length > 0 ){
   Ext.getCmp(prevbtnid).show();
   Ext.getCmp(nextbtnid).show();
   Ext.getCmp(listid).config._totalRecords = store.getData().items[0].data.totalRec;
   if (Ext.getCmp(listid).config._pageid == 1){
   Ext.getCmp(prevbtnid).disable();
   Ext.getCmp( listid + '_ToFirstPage' ).disable();
   }else{
   Ext.getCmp(prevbtnid).enable();
   Ext.getCmp( listid + '_ToFirstPage' ).enable();
   }
   var totalRecords = Ext.getCmp(listid).config._totalRecords;
   var pgCount = Math.ceil(totalRecords/SessionObj.statics.MaxRecordsPerPage);
   if ( pgCount  ==  Ext.getCmp(listid).config._pageid ) {
   Ext.getCmp(nextbtnid).disable();
   Ext.getCmp( listid + '_ToLastPage' ).disable();
   }else{
   Ext.getCmp(nextbtnid).enable();
   Ext.getCmp( listid + '_ToLastPage' ).enable();
   }
   pgCount = pgCount == 0 ? 1 : pgCount;
   Ext.getCmp(summarylabelid).setHtml(Ext.getCmp(listid).config._pageid + ' of '  + pgCount  );
   }else{
   Ext.Msg.alert('Info', 'No Records Found');
   Ext.getCmp(prevbtnid).hide();
   Ext.getCmp(nextbtnid).hide();
   }
   }
   }
   });
   },
   searchData : function (modelid, dataObj, pagesize,  prevbtnid, nextbtnid, summarylabelid ) {
   var listid = this.getListId();
   var prevbtnid = this.config._prevPageId;
   var nextbtnid = this.config._nextPageId;
   var summarylabelid = this.config._navigateSummaryId;
   if (dataObj == null )
   {
   dataObj = Ext.create('M1CRM.model.' + modelid, {});
   }
   dataObj.data.filterValues = this.getSearchFieldValue();
   dataObj.data.filterFields = dataObj.getListSearchFields();
   dataObj.data.pageID = Ext.getCmp(listid).config._pageid;
   dataObj.data.maxLimit = SessionObj.statics.MaxRecordsPerPage;

   Ext.create('M1CRM.model.' + modelid, {}).searchData({
   jsondataObj : JSON.stringify(dataObj.data),
   onReturn : function (result) {
   if (result != null){
   var store = result;
   Ext.getCmp(listid).setStore(store);
   store.load();

   if ( store.getData().items.length > 0 ){
   Ext.getCmp(prevbtnid).show();
   Ext.getCmp(nextbtnid).show();
   Ext.getCmp(listid).config._totalRecords = store.getData().items[0].data.totalRec;
   if (Ext.getCmp(listid).config._pageid == 1){
   Ext.getCmp(prevbtnid).disable();
   Ext.getCmp( listid + '_ToFirstPage' ).disable();
   }else{
   Ext.getCmp(prevbtnid).enable();
   Ext.getCmp( listid + '_ToFirstPage' ).enable();
   }
   var totalRecords = Ext.getCmp(listid).config._totalRecords;
   var pgCount = Math.ceil(totalRecords/SessionObj.statics.MaxRecordsPerPage);
   if ( pgCount  ==  Ext.getCmp(listid).config._pageid ) {
   Ext.getCmp(nextbtnid).disable();
   Ext.getCmp( listid + '_ToLastPage' ).disable();
   }else{
   Ext.getCmp(nextbtnid).enable();
   Ext.getCmp( listid + '_ToLastPage' ).enable();
   }
   pgCount = pgCount == 0 ? 1 : pgCount;
   Ext.getCmp(summarylabelid).setHtml(Ext.getCmp(listid).config._pageid + ' of '  + pgCount  );


   }else{
   Ext.Msg.alert('Info', 'No Records Found');
   Ext.getCmp(prevbtnid).hide();
   Ext.getCmp(nextbtnid).hide();
   }
   }
   }
   });
   },
   */
  /*
   toFirstPageTap : function () {
   this.resetPageId();
   if (Ext.isDefined(Ext.getCmp(this.getListId()).config._haslistchange)){
   Ext.getCmp(this.getListId()).config._haslistchange = true;
   }
   this.loadData();
   },
   toNextORPrevBtnTap : function (isnext) {
   listid = this.getListId();
   if (Ext.isDefined(Ext.getCmp(listid).config._haslistchange)){
   Ext.getCmp(listid).config._haslistchange = true;
   }
   var pageid = Ext.getCmp(listid).config._pageid;
   Ext.getCmp(listid).config._pageid =  isnext == true ?  pageid +  1 :  pageid - 1;
   this.loadData();
   },
   toLastPageTap : function(){
   if (Ext.isDefined(Ext.getCmp(this.getListId()).config._haslistchange)){
   Ext.getCmp(this.getListId()).config._haslistchange = true;
   }
   var totalRecords = Ext.getCmp(this.getListId()).config._totalRecords;
   var pgCount = Math.ceil(totalRecords/SessionObj.statics.MaxRecordsPerPage);
   pgCount = pgCount == 0 ? 1 : pgCount;
   Ext.getCmp(this.getListId()).config._pageid = pgCount;
   this.loadData();
   },*/
  resetPageId: function () {
    Ext.getCmp(this.getListId()).config._pageid = 1;
  },
  getListId: function () {
    return this.config._listid;
  },
  getSearchFieldValue: function () {
    return Ext.getCmp(this.config._seardfieldid).getValue();
  },
  clearSearchField: function () {
    if (this.config._seardfieldid != null)
      Ext.getCmp(this.config._seardfieldid).setValue('');
  },
  enableControls: function (controls) {
    for (var i = 0; i < controls.length; i++) {
      if (Ext.getCmp(controls[i]).isHidden() != null && Ext.getCmp(controls[i]).isHidden()) {
        Ext.getCmp(controls[i]).show();
      }
      Ext.getCmp(controls[i]).enable();
    }
  },
  disableControls: function (controls) {
    for (var i = 0; i < controls.length; i++) {
      Ext.getCmp(controls[i]).disable();
    }
  },
  hideControls: function (controls) {
    for (var i = 0; i < controls.length; i++) {
      Ext.getCmp(controls[i]).hide();
    }
  },
  showControls: function (controls) {
    for (var i = 0; i < controls.length; i++) {
      Ext.getCmp(controls[i]).show();
    }
  },
  setControlsReadOnly: function (flag, controls) {
    for (var i = 0; i < controls.length; i++) {
      Ext.getCmp(controls[i]).setReadOnly(flag);
    }
  },

  loadContacts: function (control, organizationid, locationid, sellocid) {
    var custContacts = Ext.create('M1CRM.model.crmCustomerContacts', {});
    custContacts.loadContacts({
      organizationid: organizationid,
      locationid: locationid,
      onReturn: function (result) {
        control.setStore(custContacts.getContacts(result));
        if (sellocid != null)
          control.setValue(sellocid);
      }
    });
  },
  getDateFromJSonDate: function (jsonDate) {
    return new Date(new Date(jsonDate).toJSON().substring(0, 10).replace('T', ' '));
  },
  getFormatDateFromJSonDate: function (jsonDate, format) {
    if (format == null)
      return formatDate(new Date(new Date(jsonDate).toJSON().substring(0, 10).replace('T', ' ')), 'dd-MMM-yyyy');
    else
      return formatDate(new Date(new Date(jsonDate).toJSON().substring(0, 10).replace('T', ' ')), format);
  },
  roundWithDecimal: function (num, decimals) {
    return Math.round(num * Math.pow(10, decimals)) / Math.pow(10, decimals);
  },
  setDirty: function (flag) {
    this.config._dirty = flag;
  },
  isDirty: function () {
    return this.config._dirty;
  },
  isFormDirty: function (controls) {
    for (var i = 0; i < controls.length; i++) {
      if (Ext.getCmp(controls[i]).isDirty == true)
        return true;
    }
    return false;
  },
  resetEditableForms: function (controls) {
    for (var i = 0; i < controls.length; i++) {
      Ext.getCmp(controls[i]).isDirty = false;
    }
  },
  pairControlsShowAndHide: function (readonly, controlid) {
    if (readonly) {
      Ext.getCmp(controlid).show();
      Ext.getCmp(controlid + 'sel').hide();
    } else {
      Ext.getCmp(controlid).hide();
      Ext.getCmp(controlid + 'sel').show();
    }
  },
  get12HourTimeString: function (dateTime) {
    var hrsstr = this.getUTCHourAs12Hour(dateTime);
    hrsstr = hrsstr.toString().length == 1 ? '0' + hrsstr : hrsstr;
    return hrsstr + ':' + dateTime.getUTCMinutes() + ' ' + this.getAMPMStr(dateTime);
  },
  getUTCHourAs12Hour: function (dateTime) {
    return ((dateTime.getUTCHours() + 11) % 12 + 1);
  },
  getUTCMinutes: function (dateTime) {
    return dateTime.getUTCMinutes()
  },
  getAMPMStr: function (dateTime) {
    return dateTime.getUTCHours() > 12 ? 'PM' : 'AM';
  },
  getDateFromDateCustomString: function (date, month, year, hr, min, ampm) {
    var h = hr.length < 2 ? '0' + hr : hr;
    var m = min.length < 2 ? '0' + min : min;
    return new Date(startDate = month + '/' + date + '/' + year + ' ' + h + ':' + m + ':00 ' + ampm);
  },
  getRecordInCompleteMessage: function () {
    return "Record Incomplete. Discard changes?";
  },
  processAfterAutoSave: function (type, id) {
    Ext.getCmp(id).config._orgrec = null;
    if (type == 'home') {
      this.toHome();
    } else {
      this.toBackTap(id);
    }
  },
  scrollFormToTop: function () {
    this.getForm().getScrollable().getScroller().scrollTo(0, 0, true);
  },
  isCRMAdvanceAvailable: function () {
    return SessionObj.getCRMAdvace();
  },
  createViewPort: function (items) {
    return Ext.Viewport.add({
      xtype: 'actionsheet',
      align: 'center',
      style: {},
      items: items != null ? items : []
    });
  },
  changeDisplayDateToCorrectDateTime: function (dispDate) {
    dispDate = dispDate === null || dispDate === '' ? formatDate(new Date(), SessionObj.getSimpleDateFormat()) :
      isDate(dispDate) ? dispDate : formatDate(new Date(dispDate), SessionObj.getSimpleDateFormat());
      
    return new  Date(dispDate);
  },
  getFormatNumberWithDecimal: function (value) {
    var negative = '',
      value = value - 0;
    if (value < 0) {
      value = -value;
      negative = '-';
    }
    x = value.toString().split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '.00';
    value = parseFloat(x1 + x2).toFixed(2);
    return Ext.String.format("{0}{1}", negative, value);
  },
  getFormatNumberAsCurrency: function (value, thousandSeparator) {
    if (value == null) {
      value = 0.0;
    }
    var value = this.getFormatNumberWithDecimal(value);
    if (Ext.isDefined(thousandSeparator)) {
      x = value.toString().split('.');
      x1 = x[0];
      x2 = x.length > 1 ? '.' + x[1] : '';
      var rgx = /(\d+)(\d{3})/;

      while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
      }
      value = x1 + x2;
    }
    return Ext.String.format("{0}", value);
  },
  getFormatCurrencyAsNumber: function (value) {
    if (value == 0)
      value = '0.0';
    x = value.split('.');
    if (x.length > 0 && x[0].length > 0 && x[0].toString().indexOf(',') > 0) {
      x[0] = x[0].toString().replace(',', '');
    }
    return parseFloat(Ext.String.format('{0}.{1}', x[0], x[1].toString()));
  },
  setButtonBadgeText: function (field, total) {
    total > 0 ? Ext.getCmp(field).setBadgeText(total) : Ext.getCmp(field).setBadgeText(null);
  },
  setUserKey: function (viewid, pkey, skey) {
    this.config._userkey = SessionObj.getSession() + viewid + ',' + pkey.toString() + ',' + skey.toString();
  },
  getUserKey: function () {
    return this.config._userkey;
  },
  isValiedUserKey: function (viewid) {
    return this.config._userkey == null || this.config._userkey.split(',')[0] != SessionObj.getSession() + viewid ? false : true;
  },
  getCodesFromUserKey: function () {
    return this.config._userkey.split(',');
  },
  getUserPkey: function () {
    return this.config._userkey != null ? this.getCodesFromUserKey()[1] : 0;
  },
  getUserSkey: function () {
    return this.config._userkey != null ? this.config._userkey.split(',')[2] : 0;
  },
  getPKeyFromSessionObj: function (data) {
    return CryptoJS.AES.decrypt(data, SessionObj.getToken()).toString(CryptoJS.enc.Utf8)[0];
  },
  getSKeyFromSessionObj: function (data) {
    return CryptoJS.AES.decrypt(data, SessionObj.getToken()).toString(CryptoJS.enc.Utf8)[1];
  }


});
