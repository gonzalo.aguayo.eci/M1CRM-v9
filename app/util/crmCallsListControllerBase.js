Ext.define('M1CRM.util.crmCallsListControllerBase', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'Ext.ux.M1.M1MenuButton'
  ],
  config: {},
  getOptionMenuItems: function (scope) {
    var id = scope.config._listid;
    return [
      {
        xtype: 'm1menubutton',
        cls: 'calls-menu',
        itemId: id + 'op_myopencallsbtn',
        id: id + 'op_myopencallsbtn',
        text: 'My Open Calls',
        listeners: {
          scope: scope,
          tap: function (field) {
            if (scope.config._listid != 'myopencallslist') {
              this.myOpenCallsTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }

      },
      {
        xtype: 'm1menubutton',
        cls: 'calls-menu',
        itemId: id + 'opp_allopencallsbtn',
        id: id + 'opp_allopencallsbtn',
        text: 'All Open Calls',
        listeners: {
          scope: scope,
          tap: function (field) {
            if (scope.config._listid != 'allopencallslist') {
              this.allOpenCallsTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      },
      {
        xtype: 'm1menubutton',
        cls: 'calls-menu',
        itemId: id + 'op_waitingcallsbtn',
        id: id + 'op_waitingcallsbtn',
        text: 'Waiting Calls',
        listeners: {
          scope: scope,
          tap: function (field) {
            if (scope.config._listid != 'waitinglist') {
              this.waitingCallsTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      },
      {
        xtype: 'm1menubutton',
        cls: 'calls-menu',
        itemId: id + 'op_pendingcallsbtn',
        id: id + 'op_pendingcallsbtn',
        text: 'Pending Calls',
        listeners: {
          scope: scope,
          tap: function (field) {
            if (scope.config._listid != 'pendingcalllist') {
              this.pendingCallsTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      },
      {
        xtype: 'm1menubutton',
        cls: 'calls-menu',
        itemId: id + 'op_closecallsbtn',
        id: id + 'op_closecallsbtn',
        text: 'Closed Calls',
        listeners: {
          scope: scope,
          tap: function (field) {
            if (scope.config._listid != 'closecallslist') {
              this.closeCallsTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      }
    ];

  },
  myOpenCallsTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getMyOpenCalls();
    view.config._parentformid = this.config._listid;
    view.config._detailformid = 'addeditcall'; //'crmcalldateil';
    this.slideLeft(view);
  },
  allOpenCallsTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getAllOpenCalls();
    view.config._parentformid = this.config._listid;
    view.config._detailformid = 'addeditcall';
    this.slideLeft(view);
  },
  waitingCallsTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getWaitingCalls();
    view.config._parentformid = this.config._listid;
    view.config._detailformid = 'addeditcall';
    this.slideLeft(view);
  },
  pendingCallsTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getPendingCalls();
    view.config._parentformid = this.config._listid;
    view.config._detailformid = 'addeditcall';
    this.slideLeft(view);
  },
  closeCallsTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getCloseCalls();
    view.config._parentformid = this.config._listid;
    view.config._detailformid = 'addeditcall';
    this.slideLeft(view);
  },
  addNewCallTap: function () {
    var view = M1CRM.util.crmViews.getAddEditCall();
    view.config._parentformid = this.config._listid;
    Ext.getCmp(view.id).config._mode = 'addnew';
    M1CRM.util.crmViews.slideLeft(view);
  }


});
