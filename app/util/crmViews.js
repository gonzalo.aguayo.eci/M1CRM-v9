Ext.define('M1CRM.util.crmViews', {
  statics: {

    constructor: function () {
      this.statics();
    },

    //CRM Loging
    getCRMLogin: function () {
      Ext.create('M1CRM.model.crmUtility', {}).deleteCookies();
      SessionObj.constructor();
      return Ext.getCmp('loginform') || Ext.create('M1CRM.view.crmLogin');
    },

    //CRM Home
    getHome: function () {
      if (SessionObj.getSession()) {
        return Ext.getCmp('crmhome') || Ext.create('M1CRM.view.crmHome');
      }

      return this.getCRMLogin();
    },

    // Followups
    getAllFollowups: function () {
      return Ext.getCmp('allfollowuplist') || Ext.create('M1CRM.view.crmAllFollowups');
    },
    getAllOpenFollowups: function () {
      return Ext.getCmp('allopenfollowuplist') || Ext.create('M1CRM.view.crmAllOpenFollowups');
    },
    getAddNewFollowup: function () {
      return Ext.getCmp('addnewfollowup') || Ext.create('M1CRM.view.crmAddNewFollowup');
    },
    getFollowupAddedit: function () {
      return Ext.getCmp('followupaddedit') || Ext.create('M1CRM.view.crmFollowupAddEdit');
    },
    getMyFollowups: function () {
      return Ext.getCmp('myfollowuplist') || Ext.create('M1CRM.view.crmMyFollowups');
    },
    getCloseFollowups: function () {
      return Ext.getCmp('closefollowuplist') || Ext.create('M1CRM.view.crmCloseFollowups');
    },
    getCustomerFollowups: function () {
      return Ext.getCmp('customerfollowupslist') || Ext.create('M1CRM.view.crmCustomerFollowups');
    },

    //Customers
    getCustomerInfo: function () {
      return Ext.getCmp('crmcustomerinfo') || Ext.create('M1CRM.view.crmCustomerInfo');
    },
    getCustomers: function () {
      return Ext.getCmp('customerlist') || Ext.create('M1CRM.view.crmCustomers');
    },
    getCustomerAddEdit: function () {
      return Ext.getCmp('customeraddedit') || Ext.create('M1CRM.view.crmCustomerAddEdit');
    },
    getCustomerContactAddEdit: function () {
      return Ext.getCmp('customercontactaddedit') || Ext.create('M1CRM.view.crmCustomerContactAddEdit');
    },
    getCustomerCallAddEdit: function () {
      return Ext.getCmp('customercalladdedit') || Ext.create('M1CRM.view.crmCustomerCallAddEdit');
    },
    getCustomerFollowupAddEdit: function () {
      return Ext.getCmp('custfollowupaddedit') || Ext.create('M1CRM.view.crmCustomerFollowupAddEdit');
    },

    //Locations
    getCustomerLocationAddEdit: function () {
      return Ext.getCmp('customerlocationaddedit') || Ext.create('M1CRM.view.crmCustomerLocationAddEdit');
    },
    getCustomerLocations: function () {
      return Ext.getCmp('customerlocationlist') || Ext.create('M1CRM.view.crmCustomerLocations');
    },

    //Contacts
    getContacts: function () {
      return Ext.getCmp('contactlist') || Ext.create('M1CRM.view.crmContacts');
    },
    getCustomerContacts: function () {
      return Ext.getCmp('customercontactlist') || Ext.create('M1CRM.view.crmCustomerContacts');
    },
    getAddNewContact: function () {
      return Ext.getCmp('addnewcontact') || Ext.create('M1CRM.view.crmAddNewContact');
    },

    //Calls
    getAllOpenCalls: function () {
      return Ext.getCmp('allopencallslist') || Ext.create('M1CRM.view.crmAllOpenCalls');
    },
    getMyOpenCalls: function () {
      return Ext.getCmp('myopencallslist') || Ext.create('M1CRM.view.crmMyOpenCalls');
    },
    getCloseCalls: function () {
      return Ext.getCmp('closecallslist') || Ext.create('M1CRM.view.crmCloseCalls');
    },
    getCustomerOpenCalls: function () {
      return Ext.getCmp('customeropencallslist') || Ext.create('M1CRM.view.crmCustomerOpenCalls');
    },
    getWaitingCalls: function () {
      return Ext.getCmp('waitinglist') || Ext.create('M1CRM.view.crmWaitingCalls');
    },
    getPendingCalls: function () {
      return Ext.getCmp('pendingcalllist') || Ext.create('M1CRM.view.crmPendingCalls');
    },
    getCallLines: function () {
      return Ext.getCmp('calllineslist') || Ext.create('M1CRM.view.crmCallLines');
    },
    getAddEditCall: function () {
      return Ext.getCmp('addeditcall') || Ext.create('M1CRM.view.crmAddEditCall');
    },
    getCallLineAddEdit: function () {
      return Ext.getCmp('calllineaddedit') || Ext.create('M1CRM.view.crmCallLineAddEdit');
    },
    getAllCallsOnContact: function () {
      return Ext.getCmp('allcallsoncontactlist') || Ext.create('M1CRM.view.crmAllCallsOnContact')
    },

    //Parts
    getParts: function () {
      return Ext.getCmp('partslist') || Ext.create('M1CRM.view.crmParts');
    },
    getLinkedParts: function () {
      return Ext.getCmp('linkedpartslist') || Ext.create('M1CRM.view.crmLinkedParts');
    },
    getPartDetail: function () {
      return Ext.getCmp('crmpartdateil') || Ext.create('M1CRM.view.crmPartDetail');
    },
    getPartImage: function () {
      return Ext.getCmp('partimage') || Ext.create('M1CRM.view.crmPartImage');
    },
    getPartRevisions: function () {
      return Ext.getCmp('partrevisionlist') || Ext.create('M1CRM.view.crmPartRevisions');
    },

    //Quotes
    getMyOpenQuotes: function () {
      return Ext.getCmp('myopenquoteslist') || Ext.create('M1CRM.view.crmMyOpenQuotes');
    },
    getAllOpenQuotes: function () {
      return Ext.getCmp('allopenquoteslist') || Ext.create('M1CRM.view.crmAllOpenQuotes');
    },
    getClosedQuotes: function () {
      return Ext.getCmp('closedquoteslist') || Ext.create('M1CRM.view.crmClosedQuotes');
    },
    getAddNewQuote: function () {
      return Ext.getCmp('addnewquote') || Ext.create('M1CRM.view.crmAddNewQuote');
    },
    getQuoteParts: function () {
      return Ext.getCmp('quotepartslist') || Ext.create('M1CRM.view.crmQuoteParts');
    },
    getQuotePartAddEdit: function () {
      return Ext.getCmp('addeditquotepart') || Ext.create('M1CRM.view.crmQuotePartAddEdit');
    },
    getQuoteIQSLocations: function () {
      return Ext.getCmp('custquoteiqslocationslist') || Ext.create('M1CRM.view.crmCustQuoteIQSLocations');
    },
    getAllQuotesOnContact: function () {
      return Ext.getCmp('allquotesoncontactlist') || Ext.create('M1CRM.view.crmAllQuotesOnContact');
    },

    //Lead Management
    getMyOpenLeads: function () {
      return Ext.getCmp('myopenleadslist') || Ext.create('M1CRM.view.crmMyOpenLeads');
    },
    getOpenLeads: function () {
      return Ext.getCmp('openleadslist') || Ext.create('M1CRM.view.crmOpenLeads');
    },
    getLeadsMarkedForFollowup: function () {
      return Ext.getCmp('followupleadslist') || Ext.create('M1CRM.view.crmLeadsMarkedForFollowup');
    },
    getLeadsCloseThisMonth: function () {
      return Ext.getCmp('leadsclosethismonthlist') || Ext.create('M1CRM.view.crmLeadsCloseThisMonth');
    },
    getLeadsCloseNextMonth: function () {
      return Ext.getCmp('leadsclosenextmonthlist') || Ext.create('M1CRM.view.crmLeadsCloseNextMonth');
    },
    getAddEditLead: function () {
      return Ext.getCmp('addeditlead') || Ext.create('M1CRM.view.crmAddEditLead');
    },
    getLeadLines: function () {
      return Ext.getCmp('leadlineslist') || Ext.create('M1CRM.view.crmLeadLines');
    },
    getLeadLineAddEdit: function () {
      return Ext.getCmp('leadlineaddedit') || Ext.create('M1CRM.view.crmLeadLineAddEdit');
    },


    // Getting Info Views
    getViewInfoByType: function (type) {
      var viewByDefault = 'getCustomerContactAddEdit';
      var views = {
        'customer': 'getCustomerAddEdit',
        'location': 'getCustomerLocationAddEdit',
        'custcontactaddedit': 'getCustomerCallAddEdit',
        'part': 'getPartDetail'
      };
      
      var view = views[type] || viewByDefault;
      return this[view](); 
    },
    viewInfoWindow: function (type, parentformrefid, custid, custname, locationid, locationname, contactid, contactname, partid, revisionid, partdesc) {
      if (type != null || type != '') {
        var view = this.getViewInfoByType(type);
        view.config._parentformid = parentformrefid;
        Ext.getCmp(view.id).config._mode = 'view';
        if (type == 'customer') {
          if (custid != null && custid != '') {
            view.setRecord(Ext.create('M1CRM.model.crmCustomers', {
              OrganizationID: custid,
              OrganizationName: custname
            }));
          } else {
            Ext.Msg.alert('Warning', 'Select ' + SessionObj.statics.OrgLabel + ' to view details.');
          }
        } else if (type == 'location') {
          view.setRecord(Ext.create('M1CRM.model.crmCustomerLocations', {
            OrganizationID: custid,
            OrganizationName: custname,
            LocationID: locationid,
            LocationName: locationname
          }));
        } else if (type == 'part') {
          view.setRecord(Ext.create('M1CRM.model.crmParts', {
            PartID: partid,
            RevisionID: revisionid
          }));
        } else if (type == 'call') {

        } else {
          view.setRecord(Ext.create('M1CRM.model.crmCustomerContactAddEdit', {
            OrganizationID: custid,
            Organization: custname,
            LocationID: locationid,
            ContactID: contactid,
            LocationName: locationname
          }));
        }
        this.slideLeft(view);
      }
    },
    viewInfo: function (type, parentformrefid, primefieldid, fieldid1, fieldid2, custname) {
      if ((type != null || type != '') && (parentformrefid != null || parentformrefid != '')) {
        var view = this.getViewInfoByType(type);
        if (type == 'customer') {
          if (primefieldid != null && primefieldid != '') {
            view.setRecord(Ext.create('M1CRM.model.crmCustomers', {
              OrganizationID: primefieldid,
              OrganizationName: custname
            }));
            view.config.customerdetail_parentid = parentformrefid;
          } else {
            Ext.Msg.alert('Error', 'Select organisation to view details.');

          }

        } else if (type == 'location') {
          view.setRecord(Ext.create('M1CRM.model.crmCustomerLocations', {
            OrganizationID: fieldid1,
            OrganizationName: custname,
            LocationID: primefieldid
          }));
          view.config.customerlocationaddedit_parentid = parentformrefid;
        } else {
          view.setRecord(Ext.create('M1CRM.model.crmCustomerContactAddEdit', {
            OrganizationID: fieldid1,
            Organization: custname,
            LocationID: fieldid2,
            ContactID: primefieldid
          }));
          view.config._parentformid = parentformrefid;
        }

        Ext.Viewport.animateActiveItem(view, {type: 'slide', direction: 'left'});
      }
    },
    getViewByParentFormId: function (parentformid) {
      var view = null;
      switch (parentformid) {

        case 'crmhome' : {
          view = this.getHome();
          break;
        }
        case 'addnewmyfollowup' : {
          view = this.getAddNewFollowup();
          break;
        }
        case  'followupdetail' : {
          view = this.getFollowupAddedit();
          break;
        }
        case 'addeditcall': {
          view = this.getAddEditCall();
          break;
        }
        case 'calldetail' : {
          view = this.getCallDetail();
          break;
        }
        case 'custcontactaddedit' : {
          view = this.getCustomerContactAddEdit();
          break;
        }
        case 'custcalladdedit' : {
          view = this.getCustomerCallAddEdit();
          break;
        }

        case  'customercalladdedit' : {
          view = this.getCustomerCallAddEdit();
          break;
        }
        case 'myfollowuplist' : {
          view = this.getMyFollowups();
          break;
        }
        case 'allfollowuplist' : {
          view = this.getAllFollowups();
          break;
        }
        case 'allopenfollowuplist' : {
          view = this.getAllOpenFollowups();
          break;
        }
        case 'closefollowuplist' : {
          view = this.getCloseFollowups();
          break;
        }
        case 'customerfollowupslist' : {
          view = this.getCustomerFollowups();
          break;
        }
        case 'contactlist' : {
          view = this.getContacts();
          break;
        }
        case 'followupaddedit' : {
          view = this.getFollowupAddedit();
          break;
        }
        case 'customerlist' : {
          view = this.getCustomers();
          break;
        }
        case 'customerlocationaddedit' : {
          view = this.getCustomerLocationAddEdit();
          break;
        }
        case 'customerlocationlist' : {
          view = this.getCustomerLocations();
          break;
        }
        case 'customeropencallslist' : {
          view = this.getCustomerOpenCalls();
          break;
        }
        case 'crmcalldateil' : {
          view = this.getCallDetail();
          break;
        }
        case 'calllineslist' : {
          view = this.getCallLines();
          break;
        }
        case 'calllineaddedit' : {
          view = this.getCallLineAddEdit();
          break;
        }
        case 'customercontactlist' : {
          view = this.getCustomerContacts();
          break;
        }
        case 'custfollowupaddedit' : {
          view = this.getCustomerFollowupAddEdit();
          break;
        }
        case 'crmpartdateil' : {
          view = this.getPartDetail();
          break;
        }
        case 'partslist' : {
          view = this.getParts();
          break;
        }
        case 'linkedpartslist' : {
          view = this.getLinkedParts();
          break;
        }
        case 'myopencallslist' : {
          view = this.getMyOpenCalls();
          break;
        }
        case 'allopencallslist' : {
          view = this.getAllOpenCalls();
          break;
        }
        case 'myopenquoteslist' : {
          view = this.getMyOpenQuotes();
          break;
        }
        case 'allopenquoteslist' : {
          view = this.getAllOpenQuotes();
          break;
        }
        case 'closedquoteslist' : {
          view = this.getClosedQuotes();
          break;
        }
        case 'addnewquote' : {
          view = this.getAddNewQuote();
          break;
        }
        case 'addeditquotepart' : {
          view = this.getQuotePartAddEdit();
          break;
        }
        case 'quotepartslist' : {
          view = this.getQuoteParts();
          break;
        }
        case 'addnewcontact' : {
          view = this.getAddNewContact();
          break;
        }
        case 'waitinglist' : {
          view = this.getWaitingCalls();
          break;
        }
        case 'pendingcalllist' : {
          view = this.getPendingCalls();
          break;
        }
        case 'customercontactaddedit' : {
          view = this.getCustomerContactAddEdit();
          break;
        }
        case 'closecallslist' : {
          view = this.getCloseCalls();
          break;
        }
        case 'allcallsoncontactlist' : {
          view = this.getAllCallsOnContact();
          break;
        }
        case 'allquotesoncontactlist' : {
          view = this.getAllQuotesOnContact();
          break;
        }
        case  'customeraddedit'  : {
          view = this.getCustomerAddEdit();
          break;
        }
        case 'linkedcontactlist' : {
          view = this.getLinkedContacts();
          break;
        }
        case 'partrevisionlist' : {
          view = this.getPartRevisions();
          break;
        }
        case 'myopenleadslist' : {
          view = this.getMyOpenLeads();
          break;
        }
        case 'openleadslist' : {
          view = this.getOpenLeads();
          break;
        }
        case 'followupleadslist' : {
          view = this.getLeadsMarkedForFollowup();
          break;
        }
        case 'leadsclosethismonthlist' : {
          view = this.getLeadsCloseThisMonth();
          break;
        }
        case 'leadsclosenextmonthlist' : {
          view = this.getLeadsCloseNextMonth();
          break;
        }
        case 'addeditlead' : {
          view = this.getAddEditLead();
          break;
        }
        case 'leadlineslist' : {
          view = this.getLeadLines();
          break;
        }
        case 'leadlineaddedit' : {
          view = this.getLeadLineAddEdit();
          break;
        }
        default :
          view = this.getHome();
          break;
      }
      return view;
    },
    displayValidationMessage: function (error) {
      var msg = '';
      for (var i = 0; i < error.items.length; i++) {
        msg += error.items[i].getMessage() + '<br>';
      }
      ;
      Ext.Msg.alert('Warning', msg);
    },
    slideLeft: function (view) {
      Ext.Viewport.animateActiveItem(view, {type: 'slide', direction: 'left'});
    },
    slideRight: function (view) {
      Ext.Viewport.animateActiveItem(view, {type: 'slide', direction: 'right'});
    },
    slideDown: function (view) {
      Ext.Viewport.down(view);
    },

    getOptionsMenu: function (control) {
      return control.config._optionsMenu;
    },


    createOptionsMenu: function (control, items) {
      control.config._optionsMenu = new Ext.Panel({
        top: 0,
        width: 150,
        height: 50 * items.length,
        modal: true,
        hideOnMaskTap: true,
        layout: {
          type: 'vbox'
        },
        items: items
      });
    }
  }
});
