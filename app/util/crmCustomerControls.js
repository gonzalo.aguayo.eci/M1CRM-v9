Ext.define('M1CRM.util.crmCustomerControls', {
  statics: {

    displayAnimation: function (message) {
      Ext.Viewport.setMasked({
        xtype: 'loadmask',
        message: message
      });
    },

    hideAnimation: function () {
      Ext.Viewport.setMasked(false);
    },

    changeTextToUpperCase: function (field, e) {
      field.setValue(field.getValue().toUpperCase());
    },

    changeTextToLowerCase: function (field, e) {
      field.setValue(field.getValue().toLowerCase());
    },

    clearSearch: function (storeid) {
      Ext.getCmp(storeid).getStore().clearFilter();
    },

    onSearchKeyUp: function (field, storeid, modelid) {
      if (field) {
        var value = field.getValue();
        var store = Ext.getCmp(storeid).getStore();
        store.clearFilter();
        var fieldArr = Ext.create('M1CRM.model.' + modelid, {}).getListSearchFields();
        var thisRegEx = new RegExp(value, "i");
        store.filterBy(function (record) {
          for (idx = 0; idx < fieldArr.length; idx++) {
            if (thisRegEx.test(record.get(fieldArr[idx])))
              return true;
          }
          return false;
        });
      }
    },

    loadData: function (obj, modelid, searchid) {
      if (searchid != null)
        Ext.getCmp(searchid).setValue('');

      Ext.create('M1CRM.model.' + modelid, {}).loadData({
        onReturn: function (result) {
          var store = result;
          obj.setStore(store);
          store.load();
        }
      });
    },

    clearFields: function (fields) {
      Ext.each(fields, function (field) {
        field.setValue('');
      }, this);
    },

    hideFields: function (fields) {
      for (var i = 0; i < fields.length; i++) {
        Ext.getCmp(fields[i]).hide();
      }
    },

    showFields: function (fields) {
      for (var i = 0; i < fields.length; i++) {
        Ext.getCmp(fields[i]).show();
      }
    },

    setButtonBadgeText: function (field, total) {
      total > 0 ? Ext.getCmp(field).setBadgeText(total) : Ext.getCmp(field).setBadgeText(null);
    },


    manageShowHideControl: function (control) {
      if (control.isHidden()) {
        control.show();
      } else {
        control.hide();
      }
    },


    displayDBResponceError: function (message, settings) {
      if (message) {
        Ext.Msg.alert('Error', message, function (btn) {
          if (btn === 'ok') {
            var view = Ext.getCmp('loginform') || Ext.create('M1CRM.view.crmLogin');
            M1CRM.util.crmViews.slideRight(view);
          }
        }, this);

      } else {
        Ext.Msg.alert('Error', 'Server cannot be reached.', function (btn) {
          if (btn == 'ok') {
            var view = Ext.getCmp('loginform') == null ? Ext.create('M1CRM.view.crmLogin') : Ext.getCmp('loginform');
            M1CRM.util.crmViews.slideRight(view);
          }
        }, this);
      }
    }
  }
});

