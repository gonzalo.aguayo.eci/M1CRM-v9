Ext.define('M1CRM.util.crmQuotesListControllerBase', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'Ext.ux.M1.M1MenuButton'
  ],
  config: {},
  getAdvanceOptionMenuItems: function (scope) {
    var id = scope.config._listid;
    if (scope.config._listsort == null) {
      scope.config._listsort = 'duedate_asc';
    }
    var options = [];
    var basicitemsArr = this.getOptionMenuItems(scope);
    for (i = 0; i < basicitemsArr.length; i++) {
      options.push(basicitemsArr[i]);
    }
    options.push({
      xtype: 'component',
      cls: 'divider',
      html: 'Sort Options'
    });
    var sortBtnsArr = this.getSortButtons(scope);
    for (i = 0; i < sortBtnsArr.length; i++) {
      options.push(sortBtnsArr[i]);
    }
    options.push({
      xtype: 'component',
      cls: 'divider',
      html: 'Group Options'
    });
    var groupBtnsArr = this.getGroupButtons(scope);
    for (i = 0; i < groupBtnsArr.length; i++) {
      options.push(groupBtnsArr[i]);
    }

    return options;
  },
  getGroupButtons: function (scope) {
    var id = scope.config._listid;
    return [
      {
        xtype: 'm1menubutton',
        cls: 'quotes-menu',
        itemid: id + 'op_quotedategroup',
        id: id + 'op_quotedategroup',
        text: 'Quote Date'
      },
      {
        xtype: 'm1menubutton',
        cls: 'quotes-menu',
        itemid: id + 'op_quoteduedatgroup',
        id: id + 'op_quoteduedatgroup',
        text: 'Due Date'

      },
      {
        xtype: 'm1menubutton',
        cls: 'quotes-menu',
        itemid: id + 'op_quoteexpdatgroup',
        id: id + 'op_quoteexpdatgroup',
        text: 'Exp Date'
      }
    ];
  },
  getSortButtons: function (scope) {
    var id = scope.config._listid;
    return [

      {
        xtype: 'm1menubutton',
        cls: 'quotes-menu',
        itemid: id + 'op_quotedateascsort',
        id: id + 'op_quotedateascsort',
        text: 'Quote Date - ASC'
      },
      {
        xtype: 'm1menubutton',
        cls: 'quotes-menu',
        itemid: id + 'op_quotedatedescsort',
        id: id + 'op_quotedescsort',
        text: 'Quote Date - DESC'
      },

      {
        xtype: 'm1menubutton',
        cls: 'quotes-menu',
        itemid: id + 'op_quoteduedateascsort',
        id: id + 'op_quoteduedateascsort',
        text: 'Due Date - ASC'

      },
      {
        xtype: 'm1menubutton',
        cls: 'quotes-menu',
        itemid: id + 'op_quoteduedatedescsort',
        id: id + 'op_quoteduedatedescsort',
        text: 'Due Date - DESC'
      },

      {
        xtype: 'm1menubutton',
        itemid: id + 'op_expdateascsort',
        id: id + 'op_expdateascsort',
        text: 'Exp Date - ASC'
      }

    ]
  },
  getOptionMenuItems: function (scope) {
    var id = scope.config._listid;
    return [
      {
        xtype: 'm1menubutton',
        cls: 'quotes-menu',
        itemId: id + 'op_myopenquotesbtn',
        id: id + 'op_myopenquotesbtn',
        text: 'My Open Quotes',
        listeners: {
          scope: scope,
          tap: function (field) {
            if (scope.config._listid != 'myopenquoteslist') {
              this.myOpenQuotesTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      },

      {
        xtype: 'm1menubutton',
        cls: 'quotes-menu',
        itemId: id + 'op_allopenquotesbtn',
        id: id + 'op_allopenquotesbtn',
        text: 'All Open Quotes',
        listeners: {
          scope: scope,
          tap: function (field) {
            if (scope.config._listid != 'allopenquoteslist') {
              this.allOpenQuotesTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      },

      {
        xtype: 'm1menubutton',
        cls: 'quotes-menu',
        itemId: id + 'op_closedquotesbtn',
        id: id + 'op_closedquotesbtn',
        text: 'Closed Quotes',
        listeners: {
          scope: scope,
          tap: function (field) {
            if (scope.config._listid != 'closedquoteslist') {
              this.closedQuotesTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      }

    ];

  },
  myOpenQuotesTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getMyOpenQuotes();
    view.config._detailformid = 'addnewquote';
    view.config._parentformid = this.config._listid;
    M1CRM.util.crmViews.slideLeft(view);
  },
  allOpenQuotesTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getAllOpenQuotes();
    view.config._detailformid = 'addnewquote';
    view.config._parentformid = this.config._listid;
    M1CRM.util.crmViews.slideLeft(view);
  },
  closedQuotesTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getClosedQuotes();
    view.config._detailformid = 'addnewquote';
    view.config._parentformid = this.config._listid;
    M1CRM.util.crmViews.slideLeft(view);
  },
  addNewQuoteTap: function () {
    var view = M1CRM.util.crmViews.getAddNewQuote();
    view.config._parentformid = this.config._listid;

    Ext.getCmp('addnewquote').setRecord(null);
    Ext.getCmp('addnewquote').config._mode = 'addnew';
    M1CRM.util.crmViews.slideLeft(view);
  },

  optionsMenuTap: function (button) {
    var items = this.getOptionMenuItems(this);
    this.config._optionsMenu = new Ext.Panel({
      top: 0,
      width: 180,
      height: 50 * items.length,
      modal: true,
      hideOnMaskTap: true,
      layout: {
        type: 'vbox'
      },
      scrollable: {
        direction: 'vertical',
        directionLock: true
      },
      items: items
    });
    this.getOptionsMenu(this).showBy(button);
  }
});
