Ext.define('M1CRM.util.crmLeadsListControllerBase', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'Ext.ux.M1.M1MenuButton'
  ],
  config: {},
  getOptionMenuItems: function (scope) {
    var id = scope.config._listid;
    return [
      {
        xtype: 'm1menubutton',
        cls: 'leads-menu',
        itemId: id + 'op_myopenleads',
        id: id + 'op_myopenleads',
        text: 'My Open Leads',
        listeners: {
          scope: scope,
          tap: function (field) {
            if (scope.config._listid != 'myopenleadslist') {
              this.optionsMyOpenLeadsTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      },

      {
        xtype: 'm1menubutton',
        cls: 'leads-menu',
        itemId: id + 'op_openleads',
        id: id + 'op_openleads',
        text: 'All Open Leads',
        listeners: {
          scope: scope,
          tap: function (field) {
            if (scope.config._listid != 'openleadslist') {
              this.optionsOpenLeadsTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      },

      {
        xtype: 'm1menubutton',
        cls: 'leads-menu',
        itemId: id + 'op_leadsmarkedforfollowup',
        id: id + 'op_leadsmarkedforfollowup',
        text: 'Marked For Follow-up',
        listeners: {
          scope: scope,
          tap: function (field) {
            if (scope.config._listid != 'followupleadslist') {
              this.optionsMarkedForFollowupTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      },

      {
        xtype: 'm1menubutton',
        cls: 'leads-menu',
        itemId: id + 'op_leadclosethismonth',
        id: id + 'op_leadclosethismonth',
        text: 'Expected To Close This Month',
        listeners: {
          scope: scope,
          tap: function (field) {
            if (scope.config._listid != 'leadsclosethismonthlist') {
              this.optionsCloseThisMonthTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      },

      {
        xtype: 'm1menubutton',
        cls: 'leads-menu',
        itemId: id + 'op_leadclosenextmonth',
        id: id + 'op_leadclosenextmonth',
        text: 'Expected To Close Next Month',
        listeners: {
          scope: scope,
          tap: function (field) {
            if (scope.config._listid != 'leadsclosenextmonthlist') {
              this.optionsCloseNextMonthTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      }


    ];
  },
  optionsMyOpenLeadsTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getMyOpenLeads();
    view.config._parentformid = this.config._listid;
    view.config._detailformid = 'addeditlead';
    this.slideRight(view);
  },
  optionsOpenLeadsTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getOpenLeads();
    view.config._parentformid = this.config._listid;
    view.config._detailformid = 'addeditlead';
    this.slideRight(view);
  },
  optionsMarkedForFollowupTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getLeadsMarkedForFollowup();
    view.config._parentformid = this.config._listid;
    view.config._detailformid = 'addeditlead';
    this.slideRight(view);
  },
  optionsCloseThisMonthTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getLeadsCloseThisMonth();
    view.config._parentformid = this.config._listid;
    view.config._detailformid = 'addeditlead';
    this.slideRight(view);
  },
  optionsCloseNextMonthTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getLeadsCloseNextMonth();
    view.config._parentformid = this.config._listid;
    view.config._detailformid = 'addeditlead';
    this.slideRight(view);
  },
  addNewLeadsTap: function (button) {
    var view = M1CRM.util.crmViews.getAddEditLead();
    view.setRecord(null);
    view.config._parentformid = this.config._listid;
    Ext.getCmp('addeditlead').config._mode = 'addnew';
    this.slideLeft(view);
  },
  optionsMenuTap: function (button) {
    var items = this.getOptionMenuItems(this);
    this.config._optionsMenu = new Ext.Panel({
      top: 0,
      width: 180,
      height: 50 * items.length,
      modal: true,
      hideOnMaskTap: true,
      layout: {
        type: 'vbox'
      },
      scrollable: {
        direction: 'vertical',
        directionLock: true
      },
      items: items
    });
    this.getOptionsMenu(this).showBy(button);
  }

});
