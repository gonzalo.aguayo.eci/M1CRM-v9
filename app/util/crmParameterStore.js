Ext.define("M1CRM.util.crmParameterStore", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.util.crmParameterModel'],
  config: {
    model: 'M1CRM.util.crmParameterModel'
  }
});
 