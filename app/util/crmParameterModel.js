Ext.define('M1CRM.util.crmParameterModel', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'fieldName',
      'fieldValue'
    ]
  }
});
