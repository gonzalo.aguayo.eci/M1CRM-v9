Ext.define('M1CRM.util.crmFollowupListControllerBase', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'Ext.ux.M1.M1MenuButton'
  ],
  config: {},
  getOptionMenuItems: function (scope) {
    var id = scope.config._listid;
    return [
      {
        xtype: 'm1menubutton',
        cls: 'followups-menu',
        itemId: id + 'op_myopenfollowups',
        id: id + 'op_myopenfollowups',
        text: 'My Open Follow-ups',
        listeners: {
          scope: scope,
          release: function (field) {
            if (scope.config._listid != 'myfollowuplist') {
              this.optionsMyFollowupTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      },
      {
        xtype: 'm1menubutton',
        cls: 'followups-menu',
        itemId: id + 'op_allopenfollowups',
        id: id + 'op_allopenfollowups',
        text: 'All Open Follow-ups',
        listeners: {
          scope: scope,
          release: function (field) {
            if (scope.config._listid != 'allopenfollowuplist') {
              this.optionsAllOpenFollowupTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      },
      {
        xtype: 'm1menubutton',
        cls: 'followups-menu',
        itemId: id + 'op_closefollowups',
        id: id + 'op_closefollowups',
        text: 'Completed Follow-ups',
        listeners: {
          scope: scope,
          release: function (field) {
            if (scope.config._listid != 'closefollowuplist') {
              this.optionsCloseFollowupTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      },
      {
        xtype: 'm1menubutton',
        cls: 'followups-menu',
        itemId: id + 'op_allfollowups',
        id: id + 'op_allfollowups',
        text: 'All Follow-ups',
        listeners: {
          scope: scope,
          release: function (field) {
            if (scope.config._listid != 'allfollowuplist') {
              this.optionsAllFollowupTap();
            } else {
              this.hideOptionsMenu();
            }
          }
        }
      }


    ];
  },
  optionsMyFollowupTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getMyFollowups();
    view.config._parentformid = this.config._listid;
    view.config._detailformid = 'followupdetail';
    this.slideRight(view);
  },
  optionsAllOpenFollowupTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getAllOpenFollowups();
    view.config._parentformid = this.config._listid;
    view.config._detailformid = 'followupdetail';
    this.slideLeft(view);
  },
  optionsCloseFollowupTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getCloseFollowups();
    view.config._parentformid = this.config._listid;
    view.config._detailformid = 'followupdetail';
    this.slideLeft(view);
  },
  optionsAllFollowupTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getAllFollowups();
    view.config._parentformid = this.config._listid;
    view.config._detailformid = 'followupdetail';
    this.slideLeft(view);
  },
  addNewFollowupTap: function (button) {
    var view = M1CRM.util.crmViews.getFollowupAddedit();
    view.setRecord(null);
    view.config._parentformid = this.config._listid;
    Ext.getCmp('followupaddedit').config._mode = 'addnew';
    this.slideLeft(view);
  }

});
