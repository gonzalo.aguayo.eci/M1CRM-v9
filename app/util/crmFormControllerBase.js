Ext.define('M1CRM.util.crmFormControllerBase', {
  extend: 'M1CRM.util.crmControllerBase',
  config: {
    _dirty: false
  },
  isFormButtonHiddenBySecurity: function (buttonid) {
    if (this.ismatchingButton(buttonid, this.config._seckey)) {
      return Ext.getCmp(buttonid).isHidden();
    } else
      return false;
  },
  processEnableOrDisableToolbarButtons: function (viewid, key) {
    var toolbar = Ext.getCmp(viewid + '_toolbarid');
    var harray = [];
    var sarray = [];
    var itemid = '';
    var totItms = Ext.getCmp(viewid + '_toolbarid').getItems().length;
    for (var i = 0; i < totItms; i++) {
      itemid = toolbar.getItems().items[i].getId();
      if (!itemid.match(/_options/i) && !itemid.match(/_tohome/i) && !itemid.match(/_back/i)) {
        if (this.ismatchingButton(itemid, key)) {
          harray.push(itemid);
        } else {
          sarray.push(itemid);
        }
      }
    }
    if (harray.length > 0) {
      this.hideControls(harray);
    }
    if (sarray.length > 0) {
      this.showControls(sarray);
    }
  },
  processHideOrShowFormButtons: function (formbuttons, key) {
    if (formbuttons != null && formbuttons.length > 0) {
      var itemarray = [];
      var itemid = '';
      for (var i = 0; i < formbuttons.length; i++) {
        itemid = formbuttons[i];
        if (this.ismatchingButton(itemid, key)) {
          itemarray.push(itemid);
        }
      }
      if (itemarray.length > 0) {
        this.disableControls(itemarray);
      }
    }
  },
  viewDetail: function (currentview, item) {
    var view = M1CRM.util.crmViews.getViewByParentFormId(Ext.getCmp(currentview).config._detailformid);
    view.setRecord(item);
    view.config._parentformid = currentview;
    this.slideLeft(view);
  },
  displayValidationMessage: function (error) {
    var msg = '';
    for (var i = 0; i < error.items.length; i++) {
      msg += error.items[i].getMessage() + '<br>';
    }
    ;
    Ext.Msg.alert('Warning', msg);
  },
  displaySaveErrorMessage: function (process) {
    var msg = 'Error saving ' + process;
    Ext.Msg.alert('Error', msg);
  },
  displayWarningMessage: function (message) {
    Ext.Msg.alert('Warning', message);
  },

  getSelectFieldDisplayValue: function (control) {
    if (control != null && control.getRecord() != null && Ext.isObject(control.getRecord()) && control.getRecord().get(control.getDisplayField()) != null)
      return control.getRecord().get(control.getDisplayField());
    else
      return '';
  },
  viewInfo: function (type, parentform, custid, custname, locationid, locationname, contactid, contactname, partid, revisionid, partdesc) {
    M1CRM.util.crmViews.viewInfoWindow(type, parentform, custid, custname, locationid, locationname, contactid, contactname, partid, revisionid, partdesc);
  },
  formInitilize: function () {
    var fields = this.getForm().getFieldsArray();
    Ext.each(fields, function (field) {
      if (field.getInitialConfig().xtype == 'm1datepicker') {
        field.setDateFormat(SessionObj.getDateFormat());
      } else if (field.getInitialConfig().xtype == 'datetimepickerfield') {
        field.setDateTimeFormat(SessionObj.getDateTimeFormat());
      } else if (field.getInitialConfig().xtype == 'm1textfield' || field.getInitialConfig().xtype == 'm1numberfield') {
        if (field.getCurrecyField()) {
          field.setLabel(field.getLabel() + ' ' + SessionObj.getCurrencySymbol());
        }
      }
    }, this);
  },
  clearFields: function (fields) {
    Ext.each(fields, function (field) {
      field.setValue('');
    }, this);
  },
  clearFormFields: function () {
    var fields = this.getForm().getFieldsArray()
    Ext.each(fields, function (field) {
      if (field.getComponent().getType() == 'checkbox') {
        if (field.originalState)
          field.check();
        else
          field.uncheck();
      } else {
        if (field.getUi() != null && field.getUi() == 'select') {
          if (field.xtype == 'm1datepicker') {
            field.setValue(new Date());
          } else {
            field.setValue('');
          }
        } else {
          field.setValue(field.originalValue != null ? field.originalValue : '');
        }
      }
    }, this);
  },
  setEntryFormMode: function (mode) {
    if (mode.toLowerCase() == 'view') {
      this.setEntryFormAsView();
    } else if (mode.toLowerCase() == 'edit') {
      this.setEntryFormAsEdit();
    } else if (mode.toLowerCase() == 'addnew') {
      this.setEntryFormAsAddNew();
    }
  },
  getClassIDByView: function () {
    var clsid = 'm1-crmhomescreen-title';

    switch (this.getForm().id) {
      case 'followupaddedit' : {
        clsid = 'm1-crmhomescreen-title-followup';
        break;
      }
      case 'addeditcall' : {
        clsid = 'm1-crmhomescreen-title-calls';
        break;
      }
      case 'calllineaddedit' : {
        clsid = 'm1-crmhomescreen-title-calllines';
        break;
      }
      case 'customercontactaddedit' : {
        clsid = 'm1-crmhomescreen-title-contact';
        break;
      }
      case 'customeraddedit' :
        //case 'customerlocationaddedit':
      {
        clsid = 'm1-crmhomescreen-title-organisation';
        break;
      }

      case 'customerlocationaddedit': {
        clsid = 'm1-crmhomescreen-title-location';
        break;
      }

      case 'addnewquote' : {
        clsid = 'm1-crmhomescreen-title-quotes';
        break;
      }
      case 'addeditquotepart': {
        clsid = 'm1-crmhomescreen-title-quote-lines';
        break;
      }
      case 'addeditlead' : {
        clsid = 'm1-crmhomescreen-title-leads';
        break;
      }
      case 'leadlineaddedit' : {
        clsid = 'm1-crmhomescreen-title-leadlines';
        break;
      }
      case 'crmpartdateil' : {
        clsid = 'm1-crmhomescreen-title-parts';
        break;
      }
      default :
        break;
    }

    return clsid;
  },
  setEntryFormAsView: function () {
    var clsid = this.getClassIDByView(); //'m1-crmhomescreen-title';

    this.getToolbarid().setTitle(this.config._viewmodeTitle);
    if (this.getToolbarid().getIncludeSave() || this.getToolbarid().getIncludeAddNew()) {
      this.getSavebtn().hide();
      this.getSavebtn().getParent().setCls(clsid);
    }
    else if (this.getToolbarid().getCls() != null) {
      var arr = [clsid, this.getToolbarid().getCls()[1], this.getToolbarid().getCls()[2]];
      this.getToolbarid().setCls(arr);
    }
    if (this.getToolbarid().getIncludeSave() || this.getToolbarid().getIncludeAddNew()) {
      this.getCancelbtn().hide();
    }

    if (this.getToolbarid().getIncludeAddNew()) {
      if (this.isFormButtonHiddenBySecurity(this.getAddnewbtn().id)) {
        this.getAddnewbtn().hide();
      } else {
        this.getAddnewbtn().show();
      }
    }

    if (this.getToolbarid().getIncludeSave()) {
      if (this.isFormButtonHiddenBySecurity(this.getEditbtn().id)) {
        this.getEditbtn().hide();
      } else {
        this.getEditbtn().show();
      }
    }

    if (Ext.getCmp(this.getForm().id + '_options') != null) {
      Ext.getCmp(this.getForm().id + '_options').show()
    }
    //clsid = 'm1-subtitle-crm';
    if (this.getToolbarid().getIncludeSubTitle()) {
      if (this.getSubtitle() != null) {
        this.getSubtitle().getParent().setCls(clsid);
        this.getSubtitle().getParent().getParent().setCls(clsid);
        this.getSubtitle().getParent().getParent().show();
      }
    }

    var arr = [this.getClsByuId(), "x-dock-item", "x-docked-left"];
    Ext.getCmp(this.getForm().id + '_back').setCls(arr);
    Ext.getCmp(this.getForm().id + '_home').setCls(arr);

  },
  getClsByuId: function () {
    var tid = this.getForm().id;

    if (tid.indexOf('followup') >= 0) {
      return 'm1-title-button-followups';
    } else if (tid.indexOf('customerlocation') >= 0) {
      return 'm1-title-button-locations';
    } else if (tid.indexOf('callline') >= 0) {
      return 'm1-title-button-calllines';
    } else if (tid.indexOf('call') >= 0) {
      return 'm1-title-button-calls';
    } else if (tid.indexOf('contact') >= 0) {
      return 'm1-title-button-contacts';
    } else if (tid.indexOf('customer') >= 0 || tid.indexOf('cust') >= 0) {
      return 'm1-title-button-organisation';
    } else if (tid.indexOf('quote') >= 0) {
      return 'm1-title-button-quotes';
    } else if (tid.indexOf('leadline') >= 0) {
      return 'm1-title-button-leadlines';
    } else if (tid.indexOf('lead') >= 0) {
      return 'm1-title-button-leads';
    } else if (tid.indexOf('part') >= 0) {
      return 'm1-title-button-parts';
    } else {
      return 'm1-title-button';
    }
  },
  setEntryFormAsEdit: function () {
    this.getToolbarid().setTitle(this.config._editmodeTitle);
    this.getSavebtn().show()
    this.getCancelbtn().show();
    if (this.getToolbarid().getIncludeAddNew()) {
      this.getAddnewbtn().hide();
    }
    this.getEditbtn().hide();
    if (Ext.getCmp(this.getForm().id + '_options') != null) {
      Ext.getCmp(this.getForm().id + '_options').hide()
    }
    var clsid = 'm1-crmhomescreen-edit';
    this.getSavebtn().getParent().setCls(clsid);
    clsid = 'm1-subtitle-crm-edit';
    if (this.getSubtitle() != null) {
      this.getSubtitle().getParent().setCls(clsid);
      this.getSubtitle().getParent().getParent().setCls(clsid);
      this.getSubtitle().getParent().getParent().show();
    }
    var arr = ["m1-title-button-edit", "x-dock-item", "x-docked-left"];
    Ext.getCmp(this.getForm().id + '_back').setCls(arr);
    Ext.getCmp(this.getForm().id + '_home').setCls(arr);
  },
  setEntryFormAsAddNew: function () {
    this.getToolbarid().setTitle(this.config._addmodeTitle);
    this.getSavebtn().show()
    this.getCancelbtn().show();
    this.getAddnewbtn().hide();
    this.getEditbtn().hide();
    if (Ext.getCmp(this.getForm().id + '_options') != null) {
      Ext.getCmp(this.getForm().id + '_options').hide()
    }
    var clsid = 'm1-crmhomescreen-edit';
    this.getSavebtn().getParent().setCls(clsid);
    clsid = 'm1-subtitle-crm-edit';
    if (this.getSubtitle() != null) {
      this.getSubtitle().getParent().setCls(clsid);
      this.getSubtitle().getParent().getParent().setCls(clsid);
      this.getSubtitle().getParent().getParent().hide();
    }
    var arr = ["m1-title-button-edit", "x-dock-item", "x-docked-left"];
    Ext.getCmp(this.getForm().id + '_back').setCls(arr);
    Ext.getCmp(this.getForm().id + '_home').setCls(arr);

  },
  enableControls: function (controls) {
    for (var i = 0; i < controls.length; i++) {

      if (Ext.getCmp(controls[i]).isHidden() != null && Ext.getCmp(controls[i]).isHidden()) {
        Ext.getCmp(controls[i]).show();
      }
      Ext.getCmp(controls[i]).enable();
    }
  },
  disableControls: function (controls) {
    for (var i = 0; i < controls.length; i++) {
      Ext.getCmp(controls[i]).disable();
    }
  },
  hideControls: function (controls) {
    for (var i = 0; i < controls.length; i++) {
      Ext.getCmp(controls[i]).hide();
    }
  },
  showControls: function (controls) {
    for (var i = 0; i < controls.length; i++) {
      Ext.getCmp(controls[i]).show();
    }
  },
  setControlsReadOnly: function (flag, controls) {
    for (var i = 0; i < controls.length; i++) {
      Ext.getCmp(controls[i]).setReadOnly(flag);
    }
  },
  loadContacts: function (control, organizationid, locationid, sellocid) {
    if (organizationid != null && organizationid != '') {
      var custContacts = Ext.create('M1CRM.model.crmCustomerContacts', {
        OrganizationID: organizationid,
        LocationID: locationid
      });
      custContacts.loadContacts({
        onReturn: function (result) {
          control.setStore(custContacts.getContacts(result.DataList));
          if (sellocid != null)
            control.setValue(sellocid);
          else {
            control.setValue('');
          }
        }
      });
    } else {
      control.setStore(Ext.create('M1CRM.model.crmCustomerContacts', {}).getContacts(null));
    }

  },
  setDirty: function (flag) {
    this.config._dirty = flag;
  },
  isDirty: function () {
    return this.config._dirty;
  },
  isFormDirty: function (controls) {
    for (var i = 0; i < controls.length; i++) {
      if (Ext.getCmp(controls[i]).isDirty == true)
        return true;
    }
    return false;
  },
  resetEditableForms: function (controls) {
    for (var i = 0; i < controls.length; i++) {
      Ext.getCmp(controls[i]).isDirty = false;
    }
  },
  pairControlsShowAndHide: function (readonly, controlid) {
    var sel = 'sel';
    if (readonly) {
      Ext.getCmp(controlid).show();
      Ext.getCmp(controlid + sel).hide();
    } else {
      Ext.getCmp(controlid).hide();
      Ext.getCmp(controlid + sel).show();
    }
  },
  processAfterAutoSave: function (type, id) {
    Ext.getCmp(id).config._orgrec = null;
    if (type == 'home') {
      this.toHome();
    } else {
      this.toBackTap(id);
    }
  },
  hideorshowTextFieldLookupInfo: function (field) {
    var lookup = '_lookup';
    var info = '_info';
    if (Ext.getCmp(field.id.split('_')[0]).config._mode == 'edit' || Ext.getCmp(field.id.split('_')[0]).config._mode == 'addnew') {
      Ext.getCmp(field.getId() + lookup).show();
      Ext.getCmp(field.getId() + info).hide();
      Ext.getCmp(field.getId()).setReadOnly(false);
    } else {
      Ext.getCmp(field.getId() + lookup).hide();
      Ext.getCmp(field.getId() + info).show();
      Ext.getCmp(field.getId()).setReadOnly(true);
    }
  },
  hideorshowContactField: function (field, flag) {
    var dial = '_dial';
    var lookupbtn = '_lookupbtn';
    if (flag && ( Ext.getCmp(field.id.split('_')[0]).config._mode == 'edit' || Ext.getCmp(field.id.split('_')[0]).config._mode == 'addnew')) {
      Ext.getCmp(field.getId() + dial).hide();
      Ext.getCmp(field.getId() + lookupbtn).show();
    } else if (!flag && Ext.getCmp(field.id.split('_')[0]).config._mode == 'view') {
      Ext.getCmp(field.getId() + dial).show();
      Ext.getCmp(field.getId() + lookupbtn).hide();
    }
    field.show();
  },
  disableorenableCallLookup: function (field, flag) {
    var name = '_lookupbtn';
    if (!flag) {
      Ext.getCmp(field.getId() + name).enable();
    } else {
      Ext.getCmp(field.getId() + name).disable();
    }
    field.show();
  },
  disableorenableCallDialBtn: function (control, flag) {
    if (control != null && control.getParent() != null && control.getParent().items != null && this.getContact().getParent().items.keys.length > 0) {
      for (var i = 0; i < this.getContact().getParent().items.keys.length; i++) {
        if (this.getContact().id + '_dial' == this.getContact().getParent().items.keys[i]) {
          flag ? Ext.getCmp(this.getContact().getParent().items.keys[i]).disable() : Ext.getCmp(this.getContact().getParent().items.keys[i]).enable();
          break;
        }
      }
    }
  },
  changeTextToUpperCase: function (field, e) {
    field.setValue(field.getValue().toUpperCase());
  },
  hideOrShowControlerParent: function (field, value) {
    if (value != null && value != '') {
      field.getParent().show();
      field.setValue(value);
    } else {
      field.getParent().hide();
      field.setValue('');
    }
  },
  showorhideParentControl: function (controls, flag) {
    if (flag) {
      for (var i = 0; i < controls.length; i++) {
        if (controls[i].getParent() != null)
          controls[i].getParent().show();
        else
          controls[i].show();
      }
    } else {
      for (var i = 0; i < controls.length; i++) {
        if (controls[i].getParent() != null) {
          if (controls[i].getValue() != null || controls[i].getValue() != '')
            controls[i].getParent().show();
          else
            controls[i].getParent().hide();
        } else {
          controls[i].hide();
        }
      }
    }
  },
  getStoreFromListForDropDown: function (list, storeid, includeblank) {
    var tStore = new Ext.create('M1CRM.store.' + storeid, {});
    if (storeid == 'crmTaxCodes') {
      if (includeblank != null && includeblank == true) {
        tStore.add({TaxCodeID: '', Description: '<None>'});
      }
      for (var i = 0; i < list.length; i++) {
        tStore.add({TaxCodeID: list[i].TaxCodeID, Description: list[i].Description});
      }
    } else {
      if (includeblank != null && includeblank == true) {
        tStore.add({ID: '', Description: '<None>'});
      }
      for (var i = 0; i < list.length; i++) {
        tStore.add({ID: list[i].ID, Description: list[i].Description});
      }
    }
    return tStore;
  },
  getCorrectDate: function (tdate) {
    var m = tdate.indexOf('"') >= 0 ? moment(tdate.split('"')[1]) : moment(tdate);
    return m._d;
  }

});
