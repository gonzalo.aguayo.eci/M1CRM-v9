Ext.define('M1CRM.util.crmListControllerBase', {
  extend: 'M1CRM.util.crmControllerBase',
  // extend : 'Ext.app.Controller',
  config: {
    _nextPageId: null,
    _prevPageId: null,
    _navigateSummaryId: null,
    _listsort: null

  },

  getOptionsMenu: function (controller) {
    return crmViews.self.getOptionsMenu(controller);
  },
  optionsMenuTap: function (button) {
    this.getOptionsMenu(this).showBy(button);
  },
  hideOptionsMenu: function () {
    if (Ext.isDefined(Ext.Viewport.getMenus()) && !Ext.Viewport.getMenus().right.isHidden()) {
      Ext.Viewport.hideMenu('right');
    }
  },
  onClearSearch: function (listid) {
    Ext.getCmp(listid).getStore().clearFilter();
  },
  onSearchKeyUp: function (field, storeid, modelid) {
    if (field) {
      var value = field.getValue();
      var store = Ext.getCmp(storeid).getStore();
      store.clearFilter();
      var fieldArr = Ext.create('M1CRM.model.' + modelid, {}).getListSearchFields();
      var thisRegEx = new RegExp(value, 'i');
      store.filterBy(function (record) {
        for (idx = 0; idx < fieldArr.length; idx++) {
          if (thisRegEx.test(record.get(fieldArr[idx])))
            return true;
        }
        return false;
      });
    }
  },
  viewDetail: function (currentview, item) {
    var view = crmViews.self.getViewByParentFormId(Ext.getCmp(currentview).config._detailformid);  //M1CRM.util.crmViews.getViewByParentFormId(Ext.getCmp(currentview).config._detailformid);
    view.setRecord(item);
    view.config._parentformid = currentview;
    Ext.getCmp(view.id).config._mode = 'view';
    this.slideLeft(view);
  },
  createOptionsMenu: function (items) {
    crmViews.self.createOptionsMenu(this, items);
  },

  setClassIDByLilstID: function () {
    var cls = ["m1-crmhomescreen-title", "x-dock-item", "x-docked-top"];

    switch (this.config._listid) {
      case 'myfollowuplist' :
      case 'allfollowuplist':
      case 'closefollowuplist':
      case 'allopenfollowuplist':
      case 'customerfollowupslist': {
        clsid = 'm1-crmhomescreen-title-followup';
        break;
      }
      case 'myopencallslist' :
      case 'allopencallslist':
      case 'waitinglist':
      case 'pendingcalllist':
      case 'closecallslist':
      case 'customeropencallslist': {
        clsid = 'm1-crmhomescreen-title-calls';
        break;
      }
      case 'calllineslist': {
        clsid = 'm1-crmhomescreen-title-calllines';
        break;
      }
      case 'contactlist' :
      case 'customercontactlist' : {
        clsid = 'm1-crmhomescreen-title-contact';
        break;
      }
      case 'customerlist' : {
        clsid = 'm1-crmhomescreen-title-organisation';
        break;
      }
      case 'customerlocationlist': {
        clsid = 'm1-crmhomescreen-title-location';
        break;
      }
      case 'myopenquoteslist' :
      case 'allopenquoteslist':
      case 'closedquoteslist':
      case 'allquotesoncontactlist': {
        clsid = 'm1-crmhomescreen-title-quotes';
        break;
      }
      case 'quotepartslist': {
        clsid = 'm1-crmhomescreen-title-quote-lines';
        break;
      }
      case 'myopenleadslist' :
      case 'openleadslist':
      case 'followupleadslist':
      case 'leadsclosethismonthlist':
      case 'leadsclosenextmonthlist': {
        clsid = 'm1-crmhomescreen-title-leads';
        break;
      }
      case 'leadlineslist' : {
        clsid = 'm1-crmhomescreen-title-leadlines';
        break;
      }

      case 'partslist' : {
        clsid = 'm1-crmhomescreen-title-parts';
        break;
      }
      default :
        break;
    }

    cls = [clsid, cls[1], cls[2]];
    Ext.getCmp(this.config._listid + '_toolbarid').setCls(cls);

  },

  initilize: function () {
    this.resetPageId();
    this.setClassIDByLilstID();
  },
  // This method will be removed
  setSummary: function (value) {
    Ext.getCmp(this.getListId() + '_summary').setHtml(value);
  },

  getStoreByID: function (storeid, result) {
    var datalist = Ext.decode(result).DataObject.DataList;
    return Ext.create('M1CRM.store.' + storeid, {}).setData(datalist);
  },

  loadDatafromDB: function (modelid, dataObj, storeid) {
    var listid = this.getListId();
    var prevbtnid = this.config._prevPageId;
    var nextbtnid = this.config._nextPageId;
    var summarylabelid = this.config._navigateSummaryId;
    if (dataObj == null) {
      var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {});
    }

    var tsearch = Ext.ComponentQuery.query('#' + listid + ' searchfield')[0].id;
    var tsearchparent = Ext.getCmp(Ext.ComponentQuery.query('#' + listid + ' searchfield')[0].id).getParent().id;
    if (Ext.getCmp(tsearch).getValue() != '') {
      Ext.getCmp(tsearchparent).show();

    } else {
      Ext.getCmp(tsearchparent).hide();

    }

    dataObj.data.FilterValues = [Ext.getCmp(tsearch).getValue()];

    Ext.create('M1CRM.model.crmServiceAccessModel', {
      ListID: listid,
      PageID: Ext.getCmp(listid).config._pageid,
      MaxLimit: SessionObj.getMaxRecordsPerPage(),
      ListType: dataObj.data.ListType
    }).getAllData({
      scope: this,
      jsondataObj: JSON.stringify(dataObj.data),
      onReturn: function (result) {
        if (result != null) {
          var store = storeid == null ? this.getStoreByID(modelid, result, dataObj.data.ListType) : this.getStoreByID(storeid, result, dataObj.data.ListType);
          Ext.getCmp(listid).setStore(store);
          //store.load();
          if (store.getData().items.length > 0) {
            Ext.getCmp(prevbtnid).show();
            Ext.getCmp(nextbtnid).show();
            Ext.getCmp(listid).config._totalRecords = Ext.decode(result).DataObject.TotalRecords;

            if (Ext.getCmp(listid).config._pageid == 1) {
              Ext.getCmp(prevbtnid).disable();
              Ext.getCmp(listid + '_ToFirstPage').disable();
            } else {
              Ext.getCmp(prevbtnid).enable();
              Ext.getCmp(listid + '_ToFirstPage').enable();
            }

            var totalRecords = Ext.getCmp(listid).config._totalRecords;
            var pgCount = Math.ceil(totalRecords / SessionObj.getMaxRecordsPerPage());
            if (pgCount == Ext.getCmp(listid).config._pageid) {
              Ext.getCmp(nextbtnid).disable();
              Ext.getCmp(listid + '_ToLastPage').disable();
            } else {
              Ext.getCmp(nextbtnid).enable();
              Ext.getCmp(listid + '_ToLastPage').enable();
            }

            pgCount = pgCount == 0 ? 1 : pgCount;
            Ext.getCmp(summarylabelid).setHtml(Ext.getCmp(listid).config._pageid + ' of ' + pgCount);
            if (pgCount == 1) {
              Ext.getCmp(summarylabelid).setCls('m1-pagesummarylabelgray-crm');
            } else {
              Ext.getCmp(summarylabelid).setCls('m1-pagesummarylabel-crm');
            }
          } else {
            Ext.getCmp(prevbtnid).disable();
            Ext.getCmp(nextbtnid).disable();
            Ext.getCmp(summarylabelid).setHtml('No matching records');
            Ext.getCmp(listid + '_ToFirstPage').disable();
            Ext.getCmp(listid + '_ToLastPage').disable();
          }
        }
      }

    });
  },

  toFirstPageTap: function () {
    //this.resetPageId();
    Ext.getCmp(this.getListId()).config._pageid = 1;
    if (Ext.isDefined(Ext.getCmp(this.getListId()).config._haslistchange)) {
      Ext.getCmp(this.getListId()).config._haslistchange = true;
    }

    this.loadData();
  },
  toNextORPrevBtnTap: function (isnext) {
    listid = this.getListId();
    if (Ext.isDefined(Ext.getCmp(this.getListId()).config._haslistchange)) {
      Ext.getCmp(this.getListId()).config._haslistchange = true;
    }
    var pageid = Ext.getCmp(listid).config._pageid;
    Ext.getCmp(listid).config._pageid = isnext == true ? pageid + 1 : pageid - 1;
    this.loadData();
  },
  toLastPageTap: function () {
    if (Ext.isDefined(Ext.getCmp(this.getListId()).config._haslistchange)) {
      Ext.getCmp(this.getListId()).config._haslistchange = true;
    }
    var totalRecords = Ext.getCmp(this.getListId()).config._totalRecords;
    var pgCount = Math.ceil(totalRecords / SessionObj.statics.MaxRecordsPerPage);
    pgCount = pgCount == 0 ? 1 : pgCount;
    Ext.getCmp(this.getListId()).config._pageid = pgCount;
    this.loadData();
  },
  resetPageId: function () {
    //Ext.getCmp(this.getListId()).config._pageid = 1;
    if (Ext.getCmp(this.getListId()).config._pageid == null || Ext.getCmp(this.getListId()).config._pageid == -1) {
      Ext.getCmp(this.getListId()).config._pageid = 1;
    }
  },
  getListId: function () {
    return this.config._listid;
  },
  getSearchFieldValue: function () {
    return Ext.getCmp(this.config._seardfieldid).getValue();
  },
  getParentFormFieldValue: function (id) {
    var parentviewid = this.getParetViewId();
    var tStr = '#' + parentviewid + ' #' + parentviewid + id;
    return Ext.ComponentQuery.query(tStr)[0].getValue();
  },
  doUserSecurityCheck: function (controller, obj, flag) {
    if (!Ext.isDefined(Ext.getCmp(this.config._listid).config._haslistchange) || flag == null) {
      if (!this.isValiedUserKey(this.config._listid)) {
        this.loadUserSecurity(controller, obj);
      } else {
        controller.loadDataAfterSecurityCheck(obj, this.getUserPkey(), this.getUserSkey());
      }
    } else if (!Ext.getCmp(this.config._listid).config._haslistchange) {
      Ext.getCmp(this.config._listid).getStore().load();
    } else if (Ext.getCmp(this.config._listid).config._haslistchange && !this.isValiedUserKey(this.config._listid)) {
      this.loadUserSecurity(controller, obj);
    } else if (Ext.getCmp(this.config._listid).config._haslistchange && this.isValiedUserKey(this.config._listid)) {
      controller.loadDataAfterSecurityCheck(obj, this.getUserPkey(), this.getUserSkey());
    }
  },
  loadUserSecurity: function (controller, obj) {
    Ext.create('M1CRM.model.crmUserSecurity', {}).getUserSecurityLevel({
      ID: this.config._listid,
      scope: this,
      onReturn: function (rec) {
        if (rec) {
          this.setUserKey(this.config._listid, rec.PAccessLevel, rec.SAccessLevel);
          controller.loadDataAfterSecurityCheck(obj, rec.PAccessLevel, rec.SAccessLevel);
        } else {
          this.toBackTap(this.config._listid);
        }
      }
    });
  },
  setSearchFieldValue: function (value) {
    Ext.getCmp(Ext.ComponentQuery.query('#' + this.config._listid + ' searchfield')[0].id).suspendEvents();
    Ext.getCmp(Ext.ComponentQuery.query('#' + this.config._listid + ' searchfield')[0].id).setValue(value);
    Ext.getCmp(Ext.ComponentQuery.query('#' + this.config._listid + ' searchfield')[0].id).setClearIcon(true);
    Ext.getCmp(Ext.ComponentQuery.query('#' + this.config._listid + ' searchfield')[0].id).resumeEvents(true);
  }


});
