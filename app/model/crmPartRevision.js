Ext.define('M1CRM.model.crmPartRevision', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'PartID',
      'RevisionID',
      'ShortDescription',
      {name: 'EffectiveStartDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      //'EffectiveStartDate',
      {name: 'EffectiveEndDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      //'EffectiveEndDate',
      'QuantityOnHand',
      'InventoryUnitOfMeasure',
      'TaxCodeID',
      'SecondTaxCodeID',
      'NonTaxReasonID',
      'listId',
      'ParentId'
    ]
  },
  getListLayout: function () {
    return new Ext.XTemplate(
      '<div class="partrevisionmodel">' +
      ' <table style="width:100%;">' +
      '<tpl if="values.PartRevisionID !== \'\'">' +
      '<tr>' +
      '<td width= "100%"><b>{RevisionID} - {ShortDescription}</b></td>' +
      '</tr>' +
      '<tpl else>' +
      '<tr>' +
      '<td width= "100%"><b>{ShortDescription}</b></td>' +
      '</tr>' +
      '</tpl>' +
      // '<tr>' +
      //	   '<td width= "100%">{EffectiveStartDate:date("d-M-Y")}</td>'+
      //  '</tr>' +
      '<tr>' +
      '<td width= "100%">{QuantityOnHand}</td>' +
      '</tr>' +
      '</table>' +
      '</div>'
    );
  }
});

