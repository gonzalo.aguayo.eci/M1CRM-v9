Ext.define("M1CRM.model.crmContactDetail", {
  extend: "Ext.data.Model",
  config: {
    fields: [
      {name: 'PhoneNumber', type: 'string'},
      {name: 'AlternatePhoneNumber', type: 'string'},
      {name: 'EMailAddress', type: 'string'},
      {name: 'WebAddress', type: 'string'},
      {name: 'Fax', type: 'string', defaultValue: ''}
    ]
  }
});