Ext.define('M1CRM.model.crmCustomerContacts', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'ContactID',
      'ContactName',
      'Name',
      'LocationID',
      'LocationName',
      'OrganizationID',
      'OrganizationName',
      'Title',
      {name: 'Name', type: 'string', sortType: 'asUCString'},

      'Phone',
      'Phone2',
      'Email',
      'totalRec',
      'CallListID',
      'ListID'
    ]
  },

  getListLayout: function () {
    return new Ext.XTemplate(
      '<div class="contactmodel">' +
      '<b><font size="4">{Name}</font></b><br>' +
      '<tpl if="values.Title !==  \'\'">' +
      '{Title}' +
      '</tpl>' +
      '<tpl if="values.Phone !== \'\'">' +
      '<br>{Phone}' +
      '</tpl>' +
      '</div>'
    );
  },

  getListSearchFields: function () {
    return ['Organization', 'OrganizationID', 'Name', 'Phone']
  },


  /*
   getContactDetails : function (settings) {
   M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
   Ext.Ajax.request({
   url: '/getContactDetails',
   method: 'GET',
   headers: { 'Content-Type': 'application/json; charset=UTF-8' },
   params: {
   session: SessionObj.getSession(),
   paramObj : JSON.stringify(this.data)
   },
   success: function (r) {
   if (r.responseText.indexOf("error") == 3){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
   }else{
   M1CRM.util.crmCustomerControls.hideAnimation();
   var record =  Ext.create('M1CRM.model.crmCustomerContactAddEdit', Ext.decode(r.responseText));
   settings.onReturn.apply(settings.scope, [record.raw[0]]);
   }
   },
   failure: function (r){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
   },
   scope: this
   });
   }, */

  loadContacts: function (settings) {
    Ext.create('M1CRM.model.crmServiceAccessModel', {
      ListID: 'M1ORGANIZATIONLOCCONTACTSALL',
      PageID: 1,
      MaxLimit: 0,
      ListType: null
    }).getAllData({
      scope: this,
      jsondataObj: JSON.stringify(this.data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          M1CRM.util.crmCustomerControls.displayDBResponceError('Error accessing customer contact information', settings)
        }
      }
    });
  },

  getContacts: function (result) {
    var store = Ext.create('M1CRM.store.crmCustomerContacts', {});
    store.add({
      ContactID: '',
      ContactName: ''
    });
    if (result != null) {
      _.sortBy(result, 'Name');

      for (var i = 0; i < result.length; i++) {
        store.add(Ext.create('M1CRM.model.crmCustomerContacts', {
          ContactID: result[i].ContactID,
          ContactName: result[i].Name
        }));
      }
    }
    return store;
  },

  getContactName: function (store, contactid) {
    var name = '';
    for (var i = 0; i < store.getData().items.length; i++) {
      if (contactid == store.getData().items[i].data.ContactID) {
        name = store.getData().items[i].data.ContactName;
        break;
      }
    }
    return name;
  }


});
