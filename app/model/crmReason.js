Ext.define('M1CRM.model.crmReason', {
  extend: "Ext.data.Model",
  config: {
    fields: [
      'ReasonID',
      'ID',
      'ReasonType',
      'Description'
    ]
  }
});
