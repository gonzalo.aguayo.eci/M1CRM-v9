Ext.define('M1CRM.model.crmCallDetail', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'CallID', type: 'string'},
      {name: 'CallTypeID', type: 'string'},
      {name: 'OrganizationID', type: 'string'},
      {name: 'Organization', type: 'string'},
      {name: 'LocationID', type: 'string'},
      {name: 'LocationName', type: 'string'},
      {name: 'ContactID', type: 'string'},
      {name: 'Contact', type: 'string'},
      {name: 'PartID', type: 'string'},
      {name: 'PartRevisionID', type: 'string'},
      {name: 'PartShortDescription', type: 'string'},
      {name: 'ShortDescription', type: 'string'},
      {name: 'LongDescriptionText', type: 'string'},
      {name: 'ContactMethodID', type: 'string'},
      {name: 'InternalOnly', type: 'bool'},
      {name: 'PriorityID', type: 'int'},
      {name: 'CallStatus', type: 'string'},
      {name: 'ReasonID', type: 'string'},
      {name: 'OpenedBy', type: 'string'},
      {name: 'OpenedDate', type: 'date', defaultValue: new Date()},
      {name: 'AssignedTo', type: 'string'},
      {name: 'AssignedDate', type: 'date', defaultValue: new Date()},
      {name: 'AcceptedDate', type: 'date', defaultValue: new Date()},
      {name: 'ClosedBy', type: 'string'},
      {name: 'ClosedDate', type: 'date'},
      {name: 'CurrencyRateID', type: 'string'},
      {name: 'LeadID', type: 'string'},
      {name: 'QuoteID', type: 'string'},
      {name: 'SalesOrderID', type: 'string'},
      {name: 'TotalCallLines', type: 'number'},
      {name: 'CreatedFromMobile', type: 'int', defaultValue: 1}
    ],
    validations: [
      {type: 'presence', field: 'OrganizationID', message: 'Organization Not Selected'},
      {type: 'presence', field: 'ShortDescription', message: 'Enter Call Description'}
    ]
  },

  loadData: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getCallDetail',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession(),
        callid: this.data.CallID
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          settings.onReturn.apply(settings.scope, [Ext.create('M1CRM.model.crmCallDetail', Ext.decode(r.responseText))]);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  },

  loadTotals: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getMyTotalCallInfo',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession(),
        id: settings.id,
        employeeid: SessionObj.getEmpId(),
        callid: settings.callid
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          settings.onReturn.apply(settings.scope, [Ext.create('M1CRM.model.crmCallDetail', Ext.decode(r.responseText))]);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  },

  saveData: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Saving..');
    Ext.Ajax.request({
      url: '/saveCall',
      method: 'POST',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: Ext.encode({
        session: SessionObj.getSession(),
        paramObj: JSON.stringify(this.data)
      }),
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          settings.onReturn.apply(settings.scope, [Ext.decode(r.responseText)[0]]);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  }

});
