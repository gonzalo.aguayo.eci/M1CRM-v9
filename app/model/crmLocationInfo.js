Ext.define('M1CRM.model.crmLocationInfo', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'LocationID', type: 'string'},
      {name: 'LocationName', type: 'string'},
      {name: 'ContactID', type: 'string'},
      {name: 'ContactName', type: 'string'}
    ]
  }
});
		  