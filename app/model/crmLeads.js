Ext.define('M1CRM.model.crmLeads', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'LeadID', type: 'string'},
      {name: 'OrganizationID', type: 'string'},
      {name: 'OrganizationName', type: 'string'},
      {name: 'LeadLocationInfo', type: 'auto'},
      {name: 'ContactName', type: 'string'},
      {name: 'ShortDescription', type: 'string'}

    ]
  },
  getMyOpenLeadsLayout: function () {
    return new Ext.XTemplate(
      '<div class="quotemodel">' +
      ' <table style="width:100%;">' +

      '<tr>' +
      '<td width= "100%"><b><font size="4">{LeadID}</font></b></td>' +
      '</tr>' +

      '<tr>' +
      '<td width= "100%">{OrganizationID} - {OrganizationName}</td>' +
      '</tr>' +

      '<tpl if="values.LeadLocationInfo.LeadLocation !== \'\'">' +
      '<tr>' +
      '<td width= "100%">{LeadLocationInfo.LocationName}</td>' +
      '</tr>' +
      '</tpl>' +


      '<tpl if="values.ContactName !== \'\'">' +
      '<tr>' +
      '<td width= "100%">{ContactName}</td>' +
      '</tr>' +
      '</tpl>' +

      '<tr>' +
      '<td width= "100%"><b>{ShortDescription}</b></td>' +
      '</tr>' +

      '</table>' +
      '</div>'
    );
  },

  getOpenLeadsLayout: function () {
    return new Ext.XTemplate(
      '<div class="quotemodel">' +
      ' <table style="width:100%;">' +

      '<tr>' +
      '<td width= "100%"><b><font size="4">{LeadID}</font></b></td>' +
      '</tr>' +

      '<tr>' +
      '<td width= "100%">{SalesPersonName}</td>' +
      '</tr>' +

      '<tpl if="values.LeadLocationInfo.LocationID !== \'\'">' +
      '<tr>' +
      '<td width= "100%">{LeadLocationInfo.LocationName}</td>' +
      '</tr>' +
      '<tpl else>' +
      '<tr>' +
      '<td width= "100%">{OrganizationName}</td>' +
      '</tr>' +
      '</tpl>' +

      '<tpl if="values.ContactName !== \'\'">' +
      '<tr>' +
      '<td width= "100%">{ContactName}</td>' +
      '</tr>' +
      '</tpl>' +

      '<tr>' +
      '<td width= "100%">{ShortDescription}</td>' +
      '</tr>' +
      '</table>' +
      '</div>'
    );
  },

  getListSearchFields: function () {
    return ['LeadID', 'OrganizationID', 'OrganizationName', 'ContactID', 'ContactName', 'LocationID', 'SalesPersonName', 'ShortDescription'];
  }

});
