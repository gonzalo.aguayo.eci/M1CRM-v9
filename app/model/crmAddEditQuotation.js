Ext.define('M1CRM.model.crmAddEditQuotation', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'QuoteID', type: 'string', defaultValue: ''},
      {name: 'OrganizationID', type: 'string', defaultValue: ''},
      {name: 'OrganizationName', type: 'string', defaultValue: ''},
      {name: 'ARInvoiceLocationID', type: 'string', defaultValue: ''},
      {name: 'ARInvoiceContactID', type: 'string', defaultValue: ''},
      {name: 'QuoteLocationID', type: 'string', defaultValue: ''},
      {name: 'QuoteContactID', type: 'string', defaultValue: ''},
      {name: 'LocationName', type: 'string', defaultValue: ''},
      {name: 'ShipOrganizationID', type: 'string', defaultValue: ''},
      {name: 'ShipLocationID', type: 'string', defaultValue: ''},
      {name: 'ShipContactID', type: 'string', defaultValue: ''},
      {name: 'StandardMessageID', type: 'string', defaultValue: ''},
      {name: 'Quoter', type: 'string', defaultValue: ''},
      {name: 'QuoteDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      {name: 'DueDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      {name: 'ExpirationDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      {name: 'QuoteHeaderMessageText', type: 'string', defaultValue: ''},
      {name: 'QuoteFooterMessageText', type: 'string', defaultValue: ''},
      {name: 'QuoterEmployeeID', type: 'string', defaultValue: ''},
      {name: 'QuoteContactEmail', type: 'string', defaultValue: ''},
      {name: 'LocationID', type: 'string', defaultValue: ''},
      {name: 'InvLocationID', type: 'string', defaultValue: ''},
      {name: 'ContactID', type: 'string', defaultValue: ''},
      {name: 'ContactName', type: 'string', defaultValue: ''}
    ],
    validations: [
      // { type: 'presence',  field: 'OrganizationID', message : 'Select Organization' }
    ]
  },
  getData: function (settings) {
    var filtArr = [];
    settings = settings || {};

    if (settings.RequestID == 'prop') {
      filtArr = [];
    } else if (settings.RequestID == 'custdata') {
      filtArr = [this.data.OrganizationID];
    } else if (settings.RequestID == 'data') {
      filtArr = [this.data.QuoteID];
    }

    Ext.create('M1CRM.model.crmServiceAccessModel', {}).getIt({
      scope: this,
      ViewID: 'CRMADDEDITQUOTATION',
      RequestID: settings.RequestID,
      jsondataObj: JSON.stringify(Ext.create('M1CRM.model.crmDataFilterModel', {
        FilterValues: filtArr
      }).data),
      onReturn: function (result) {
        if (result) {
          var dataObject = Ext.decode(result).DataObject;
          if (dataObject.RecordFound) {
            settings.onReturn.apply(settings.scope, [dataObject]);
          }
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  },

  validate: function () {
    var error = this.callParent();
    if (this.data.OrganizationID == null || this.data.OrganizationID == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'OrganizationID',
        message: 'Select Customer' //+ SessionObj.getOrgLabel()
      }));
    }
    if (this.data.ShipOrganizationID == null || this.data.ShipOrganizationID == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'ShipOrganizationID',
        message: 'Select  Ship Org'
      }));
    }

    if (this.data.Quoter == null || this.data.Quoter == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'Quoter',
        message: 'Select Quoter'
      }));
    }


    if (this.data.CustomerTaxable == true && this.data.CustomerTaxCodeID == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'CustomerTaxCodeID',
        message: 'Select  a tax id'
      }));
    }
    return error;
  },

  saveData: function (settings) {
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).saveIt({
      scope: this,
      ViewID: 'CRMADDEDITQUOTATION',
      jsondataObj: JSON.stringify(this.data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  },
  emailIt: function (settings) {
    var filtArr = [this.data.QuoteID, this.data.OrganizationID, this.data.LocationID, this.data.ContactID];
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).emailIt({
      scope: this,
      ViewID: 'CRMADDEDITQUOTATION',
      jsondataObj: JSON.stringify(Ext.create('M1CRM.model.crmDataFilterModel', {
        FilterValues: filtArr,
        ContactName: this.data.ContactName
      }).data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  }

});
