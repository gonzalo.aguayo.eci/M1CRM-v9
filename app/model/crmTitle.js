Ext.define('M1CRM.model.crmTitle', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'ID',
      'Description'
    ]
  }
});
