Ext.define('M1CRM.model.crmParts', {
  extend: 'Ext.data.Model',
  requires: [
    // 'M1CRM.store.crmParts'
  ],
  config: {
    fields: [
      'PartID',
      'RevisionID',
      'PartGroupID',
      'Description',
      'ShortDescription',
      'PartImageFileName',
      'filterFields',
      'filterValues',
      'pageID',
      'maxLimit',
      'totalRec',
      'QuantityOnHand',
      'QuantityOnOrderPurchases',
      'QuantityOnOrderSales',
      'QuantityAllocated',
      'ProductionQuantity',
      'QuantityShipped',
      'QuantityReceivedToInventory',
      'QtyinProduction',
      'IUoM',
      'ParentId'
    ]
  },
  getListLayout: function () {
    return new Ext.XTemplate(
      '<div class="callmodel">' +
      ' <table style="width:100%;">' +
      '<tpl if="values.RevisionID !== \'\'">' +
      '<tr>' +
      '<td width= "100%"><b><font size="4">{PartID} - {RevisionID}</font></b></td>' +
      '</tr>' +
      '<tpl else>' +
      '<tr>' +
      '<td width= "100%"><b><font size="4">{PartID}</font></b></td>' +
      '</tr>' +
      '</tpl>' +
      '<tr>' +
      '<td width= "100%">{ShortDescription}</td>' +
      '</tr>' +
      '</table>' +
      '</div>'
    );
  },

  getListSearchFields: function () {
    return ['imrPartID', 'imrPartRevisionID', 'imrShortDescription'];
  },


  checkPart: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/checkPart',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession(),
        paramObj: JSON.stringify(this.data)
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          settings.onReturn.apply(settings.scope, [Ext.decode(r.responseText)]);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  }

});

