Ext.define("M1CRM.model.crmCustomerContactAddEdit", {
  extend: "Ext.data.Model",
  config: {
    fields: [
      {name: 'OrganizationID', type: 'string'},
      {name: 'Organization', type: 'string'},
      {name: 'LocationID', type: 'string'},
      {name: 'LocationName', type: 'string'},
      {name: 'ContactID', type: 'string'},
      {name: 'Title', type: 'string'},
      //{ name : 'Title',  type :'string' },
      {name: 'Name', type: 'string'},
      {name: 'Phone', type: 'string'},
      {name: 'Phone2', type: 'string'},
      {name: 'Mobile', type: 'string'},
      {name: 'Fax', type: 'string'},
      {name: 'Email', type: 'string'},
      {name: 'NoMailings', type: 'bool', defaultValue: false},
      {name: 'LongDescriptionText', type: 'string'},
      //{ name : 'NoteText',  type :'string' },
      //{ name : 'user',  type :'string' },
      {name: 'CustomerStatus', type: 'int', defaultValue: 0},
      'RtnStatus'

    ],
    validations: [
      {type: 'presence', field: 'OrganizationID', message: 'Enter Organization ID'},
      {type: 'presence', field: 'ContactID', message: 'Enter Contact ID'},
      {type: 'presence', field: 'Name', message: 'Enter Name'}
    ]
  },

  getIt: function (settings) {
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).getIt({
      scope: this,
      ViewID: 'CRMCUSTOMERCONTACTADDEDIT',
      RequestID: settings.RequestID,
      jsondataObj: JSON.stringify(Ext.create('M1CRM.model.crmDataFilterModel', {
        FilterValues: [this.data.OrganizationID, this.data.LocationID, this.data.ContactID]
      }).data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  },

  saveData: function (settings) {
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).saveIt({
      scope: this,
      ViewID: 'CRMCUSTOMERCONTACTADDEDIT',
      jsondataObj: JSON.stringify(this.data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  }


});

