Ext.define('M1CRM.model.crmMilestone', {
  extend: "Ext.data.Model",
  config: {
    fields: [
      'ID',
      'MilestoneID',
      'Description',
      'ConfidenceFactor'
    ]
  }
  /*
   getMileStones : function (settings) {
   M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
   Ext.Ajax.request({
   url: '/getMileStones',
   method: 'GET',
   headers: { 'Content-Type': 'application/json; charset=UTF-8' },
   params: {
   session: SessionObj.getSession()
   },
   success: function (r) {
   if (r.responseText.indexOf("error") == 3){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
   }else{
   M1CRM.util.crmCustomerControls.hideAnimation();
   var result =  Ext.create('M1CRM.store.crmMilestone', {}).setData(Ext.decode(r.responseText));
   var MilestoneStore  =  new Ext.create('M1CRM.store.crmMilestone', {});
   MilestoneStore.add( { MilestoneID : '', Description : '<None>' } );
   for (var i =0; i < result.data.items.length; i++ ){
   MilestoneStore.add( { MilestoneID : result.data.items[i].raw.MilestoneID ,
   Description : result.data.items[i].raw.Description,
   ConfidenceFactor : result.data.items[i].raw.ConfidenceFactor });
   }
   result = null;
   settings.onReturn.apply(settings.scope, [MilestoneStore]);

   }

   },
   failure: function (r){
   M1CRM.util.crmCustomerControls.hideAnimation(r);
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
   },
   scope: this
   });
   }  */
});
