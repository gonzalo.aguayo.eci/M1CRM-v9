Ext.define('M1CRM.model.crmCountry', {
  extend: "Ext.data.Model",
  requires: ['M1CRM.store.crmValueList'],
  config: {
    fields: [
      'CountryCode',
      'CountryName'
    ]
  },

  loadCountry: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getValueList',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession(),
        fieldid: 'cmlCountryCode'
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          var rows = Ext.create('M1CRM.store.crmValueList', {}).setData(Ext.decode(r.responseText)).first().raw.dfValueList.split('\n');
          countryStore = Ext.create('M1CRM.store.crmCountry', {});
          if (rows.length > 0) {
            countryStore.add({CountryCode: '', CountryName: '<None>'});
            for (var i = 0; i < rows.length; i++) {
              countryStore.add({
                CountryCode: rows[i].split(',')[0].split('"')[1].trim(),
                CountryName: rows[i].split(',')[1].trim()
              });
            }
          }
          M1CRM.util.crmCustomerControls.hideAnimation();
          settings.onReturn.apply(settings.scope, [countryStore]);
        }

      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation(r);
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  }

});
