Ext.define('M1CRM.model.crmProductionProperties', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'HDCallTypeID',
      'HDContactMethodID',
      'CMCustomerTaxable',
      'CMNonTaxReasonID'
    ]
  },

  getProductionProperties: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getProductionProperties',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession()
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          var record = Ext.create('M1CRM.model.crmProductionProperties', Ext.decode(r.responseText));
          settings.onReturn.apply(settings.scope, [record.data]);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  }
});
