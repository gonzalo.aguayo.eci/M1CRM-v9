Ext.define('M1CRM.model.crmLeadLineAddEdit', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'LeadID', type: 'string'},
      {name: 'LeadLineID', type: 'auto'},
      {name: 'PartID', type: 'string'},
      {name: 'PartRevisionID', type: 'string'},
      {name: 'PartGroupID', type: 'string'},
      {name: 'OrganizationID', type: 'string'},
      {name: 'LocationID', type: 'string'},
      {name: 'Quantity', type: 'float', defaultValue: 0.0},
      {name: 'Description', type: 'string'},
      {name: 'UnitOfMeasure', type: 'string'},
      {name: 'GrossAmount', type: 'float', defaultValue: 0.0},
      {name: 'DiscountAmount', type: 'float', defaultValue: 0.0},
      {name: 'RevenueForecast', type: 'float', defaultValue: 0.0},
      {name: 'ForecastDate', type: 'date', defaultValue: new Date()},
      {name: 'LeadDate', type: 'date', defaultValue: new Date()},
      {name: 'DiscountPercent', type: 'float', defaultValue: 0.0},
      {name: 'ResolutionReasonID', type: 'string'},
      {name: 'TransferredToQuote', type: 'bool', defaultValue: false},
      {name: 'UnitSalePrice', type: 'float', defaultValue: 0.0},
      {name: 'EmployeeID', type: 'string'}
    ],
    validations: [
      {type: 'presence', field: 'LeadID', message: 'Lead ID not defined'},
      {type: 'presence', field: 'PartID', message: 'Select Part'},
      {type: 'presence', field: 'Description', message: 'Enter Part Description'}
    ]
  },
  validate: function () {
    var error = this.callParent();
    if (this.data.Quantity == 0) {
      error.add(Ext.create('Ext.data.Error', {
        field: 'Quantity',
        message: 'Enter Quantity'
      }));
    }
    return error;
  },
  getData: function (settings) {
    var filtArr = [];
    if (settings.RequestID == 'prop') {
      filtArr = [];
    } else if (settings.RequestID == 'data') {
      filtArr = [this.data.LeadID, this.data.LeadLineID];
    } else if (settings.RequestID == 'qtyinfo' || settings.RequestID == 'priceinfo' || settings.RequestID == 'checkpart') {
      filtArr = [this.data.PartID, this.data.PartRevisionID, this.data.PartGroupID, this.data.OrganizationID, this.data.LocationID, this.data.Quantity];
    }

    Ext.create('M1CRM.model.crmServiceAccessModel', {}).getIt({
      scope: this,
      ViewID: 'CRMADDEDITLEADLINE',
      RequestID: settings.RequestID,
      jsondataObj: JSON.stringify(Ext.create('M1CRM.model.crmDataFilterModel', {
        FilterValues: filtArr
      }).data),
      onReturn: function (result) {
        if (result != null) {
          if (Ext.decode(result).DataObject != null) {
            settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
          } else {
            Ext.Msg.alert('Error', Ext.decode(result).Message, function (btn) {
              if (btn == 'ok') {
                settings.onReturn.apply(settings.scope, [null]);
              }
            }, this);
          }
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  },

  saveData: function (settings) {
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).saveIt({
      scope: this,
      ViewID: 'CRMADDEDITLEADLINE',
      jsondataObj: JSON.stringify(this.data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  }


});
 		  