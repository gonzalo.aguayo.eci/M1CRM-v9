Ext.define('M1CRM.model.crmUtility', {
  extend: 'Ext.data.Model',

  config: {
    fields: [
      {name: 'DBRegion', type: 'string'}
    ]
  },

  getDBRegion: function (settings) {
    Ext.Ajax.request({
      url: '/GetDbRegion',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Session-Key': SessionObj.getSession()
      },
      success: function (response) {
        settings.onReturn.apply(settings.scope, [response.responseText]);
      },
      failure: function (response) {
        settings.onReturn.apply(settings.scope, [response.responseText]);
      },
      scope: this
    });
  },

  getCookieToObject: function () {
    var cookieObject = {};
    var objects = document.cookie ? document.cookie.split(';') : [];
    
    var splitToObject = function (object) {
      var splittedObject = object.split('=');
      var name = splittedObject[0].trim();
      var value = splittedObject[1].trim();

      cookieObject[name] = (value === "true" || value === "false") ? JSON.parse(value) : value;
    };

    objects.forEach(splitToObject);
    return cookieObject;
  },

  deleteCookies: function () {
    var cookies = document.cookie.split(";") || [];
    var deleteCookie = function (cookie) {
      var name = cookie.split('=')[0];
      document.cookie = name + '=;expires=Thu, 01 Jan 1970 00:00:00 GMT'; 
    };

    cookies.forEach(deleteCookie)
  }
});
