Ext.define('M1CRM.model.crmCallTypes', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'CallTypeID',
      'ID',
      'Description'
    ]
  },

  loadCallTypes: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getCallsTypes',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.statics.session
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          settings.onReturn.apply(settings.scope, [Ext.create('M1CRM.store.crmCallTypes', {}).setData(Ext.decode(r.responseText))]);
        }

      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation(r);
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  }

});
