Ext.define('M1CRM.model.crmDataSet', {
  extend: 'Ext.data.Model',
  config: {
    identifier: 'uuid',
    fields: [
      {name: 'dataset', type: 'string'},
      {name: 'loginid', type: 'string'}
    ],
    proxy: {
      type: 'localstorage',
      id: 'localDataSet'
    }
  }
});
