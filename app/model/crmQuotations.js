Ext.define('M1CRM.model.crmQuotations', {
  extend: 'Ext.data.Model',

  config: {
    fields: [
      'QuoteID',
      'OrganizationID',
      'OrganizationName',
      'ContactID',
      'ContactName',
      'ARInvoiceLocationID',
      'ARInvoiceContactID',
      'QuoteLocationID',
      'QuoteLocation',
      'QuoteContactID',
      'ShipOrganizationID',
      'ShipOrganization',
      'ShipLocationID',
      'ShipContactID',
      'StandardMessageID',
      'LocationID',
      'ARContactName',
      'QuoteContactName',
      'ShipContactName',
      'QuoteLocationName',
      'QuoteContactName',
      'InvLocationName',
      'ARContactName',
      'ShipLocationName',
      'ShipContactName',
      'ShipOrganizationName',
      'QuoteHeaderMessageText',
      'QuoteFooterMessageText',
      {name: 'QuoteDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      {name: 'DueDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      {name: 'ExpirationDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      'EmployeeName',
      'LocationName',
      'QuoteContactEmail',
      'Quoter',
      'SalesEmpName',
      'SalesEmpID',
      'openQuotes',
      'filterFields',
      'filterValues',
      'pageID',
      'CreatedFromMobile',
      'Closed',
      'listId'
      // 'OrderBy'
    ],
    validations: [
      {type: 'presence', field: 'OrganizationID', message: 'Select Organization'}
    ]
  },

  getMyOpenQuoteLayout: function () {
    return new Ext.XTemplate(
      '<div class="quotemodel">' +
      ' <table style="width:100%;">' +

      '<tr>' +
      '<td width= "100%"><font size="4">{QuoteID}</font></td>' +
      '</tr>' +

      '<tr>' +
      '<td width= "100%"><b>{OrganizationName}</b></td>' +
      '</tr>' +

      '<tpl if="values.OrganizationName !==  values.LocationName ">' +
      '<tr>' +
      '<td width= "100%"><b>{LocationName}</b></td>' +
      '</tr>' +
      '</tpl>' +


      '<tpl if="values.ContactName !== \'\'">' +
      '<tr>' +
      '<td width= "100%">{ContactName}</td>' +
      '</tr>' +
      '</tpl>' +
      '</table>' +
      '</div>'
    );
  },

  getAllOpenQuoteLayout: function () {
    return new Ext.XTemplate(
      '<div class="quotemodel">' +
      ' <table style="width:100%;">' +

      '<tr>' +
      '<td width= "100%"><font size="4">{QuoteID}</font></b><b> {OrganizationName}</b></td>' +
      '</tr>' +

      '<tpl if="values.SalesEmpName !== \'\'">' +
      '<tr>' +
      '<td width= "100%">Sales:{SalesEmpName}</td>' +
      '</tr>' +
      '</tpl>' +

      '<tpl if="values.OrganizationName !==  values.LocationName ">' +
      '<tr>' +
      '<td width= "100%"><b>{LocationName}</b></td>' +
      '</tr>' +
      '</tpl>' +


      '<tpl if="values.ContactName !== \'\'">' +
      '<tr>' +
      '<td width= "100%">{ContactName}</td>' +
      '</tr>' +
      '</tpl>' +

      '</table>' +
      '</div>'
    );
  },
  getListSearchFields: function () {
    return ['QuoteID', 'OrganizationID', 'Organization', 'ContactID', 'ContactName', 'LocationID', 'QuoteDate', 'ExpirationDate'];
  }


});
