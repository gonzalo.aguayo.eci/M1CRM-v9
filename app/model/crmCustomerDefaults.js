Ext.define('M1CRM.model.crmCustomerDefaults', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'OrganizationID',
      'DefQuoteLocationID',
      'DefShipLocationID',
      'DefARInvoiceLocationID',
      'DARInvLocation',
      'DShipLoc',
      'DQuoteLoc',
      'DQuoteContactID',
      'DShipContactID',
      'DARInvoiceContactID'
    ]
  },

  getCustomerDefaults: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getCustomerDefaults',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession(),
        customer: JSON.stringify(this.data)
      },
      success: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        customerDefStore = Ext.create('M1CRM.store.crmCustomerDefaults', {});
        customerDefStore.setData(Ext.decode(r.responseText));
        settings.onReturn.apply(settings.scope, [customerDefStore]);
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  }

});
