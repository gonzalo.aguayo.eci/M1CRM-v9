Ext.define('M1CRM.model.crmPriority', {
  extend: "Ext.data.Model",
  config: {
    fields: [
      'ID',
      'Description',
      '_hasemptyentry'
    ]
  }
});
