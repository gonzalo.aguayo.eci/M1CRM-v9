Ext.define('M1CRM.model.crmStandardMessages', {
  extend: 'Ext.data.Model',

  config: {
    fields: [
      'ID',
      'Description'
    ]
  }
});
