Ext.define('M1CRM.model.crmCallLines', {
  extend: 'Ext.data.Model',
  store: ['crmCallLines'],
  config: {
    fields: [
      'CallID',
      'CallLineID',
      'ShortDescription',
      'ContactMethodID',
      'InternalOnly',
      'AddedDate',
      'totalRec'
    ]
  },
  getListLayout: function () {
    return new Ext.XTemplate(
      '<div class="callmodel">' +
      ' <table style="width:100%;">' +
      '<tr>' +
      '<td width= "100%">{AddedDate:date("d-M-Y")}</td>' +
      '</tr>' +
      '<tr>' +
      '<td width= "100%">{ShortDescription} </td>' +
      '</tr>' +
      '</table>' +
      '</div>'
    );
  },
  getListSearchFields: function () {
    return ['ShortDescription'];
  }


});
