Ext.define('M1CRM.model.crmCustomerDetail', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'cmoOrganizationID',
      'cmoName',
      'cmoAddressLine1',
      'cmoAddressLine2',
      'cmoAddressLine3',
      'cmoCity',
      'cmoState',
      'cmoPostCode',
      'cmoCountry',
      'cmoPhoneNumber',
      'cmoAlternatePhoneNumber',
      'cmoEMailAddress',
      'cmoWebAddress',
      'cmoCustomerStatus',
      'cmoResellerStatus',
      'cmoSupplierStatus',
      'TotalLocations',
      'TotalContacts',
      'TotalOpenCalls',
      'TotalOpenFollowup',
      'TotalLinkedParts',
      'TotalLinkedQuotes',
      'isNewCustomer',
      'cmoLongDescriptionText',
      'cmoCustomerProspectDate',
      {name: 'cmoCustomerTaxable', type: 'bool', defaultValue: false},
      'cmoCustomerTaxCodeID',
      'cmoCustomerSecondTaxCodeID'
    ],
    validations: [
      {type: 'presence', field: 'cmoOrganizationID', message: 'Enter Organisation ID'},
      {type: 'presence', field: 'cmoName', message: 'Enter Organisation Name'}
    ]
  },
  validate: function () {
    var error = this.callParent();
    if (this.data.cmoCustomerTaxable && this.data.cmoCustomerTaxCodeID == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'cmoCustomerTaxCodeID',
        message: 'Select Tax ID'
      }));

    }
    return error;
  },


  loadOrganizationDetails: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getCustomerDetails',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession(),
        organizationid: settings.organizationid,
        locationid: settings.locationid
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          settings.onReturn.apply(settings.scope, [Ext.create('M1CRM.model.crmCustomerDetail', Ext.decode(r.responseText))]);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });

  },

  getStatusCodes: function (code) {
    if (parseInt(code) == 0)
      return '<None>';
    else if (parseInt(code) == 1)
      return 'Prospective';
    else if (parseInt(code) == 2)
      return 'Active';
    else
      return 'Inactive';
  },
  getStatusCodeID: function (code) {
    if (code == '<None>')
      return 0;
    else if (code == 'Prospective')
      return 1;
    else if (code == 'Active')
      return 2;
    else
      return 3;
  },
  getAddress: function (model) {
    var address = null;
    if (model.cmoAddressLine1.length > 0)
      address = model.cmoAddressLine1;

    if (model.cmoAddressLine2.length > 0)
      address = address != null ? address + '\n' + model.cmoAddressLine2 : model.cmoAddressLine2;

    if (model.cmoAddressLine3.length > 0)
      address = address != null ? address + '\n' + model.cmoAddressLine3 : model.cmoAddressLine3;

    if (model.cmoCity.length > 0)
      address = address != null ? address + '\n' + model.cmoCity : model.cmoCity;

    if (model.cmoState.length > 0) {
      if (address != null) {
        address = model.cmoCity.length > 0 ? address = address + ' ' + model.cmoState : address + '\n' + model.cmoState;
      }
      else {
        address = model.cmoState;
      }
    }

    if (model.cmoPostCode.length > 0) {
      if (address != null) {
        address = model.cmoCity.length || model.cmoState.length > 0 ? address + '\n' + model.cmoPostCode : address + ' ' + model.cmoPostCode;
      }
      else {
        address = model.cmoPostCode;
      }
    }
    return address;
  },
  loadTotals: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getCustomerTotals',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession(),
        id: settings.id,
        organizationid: settings.organizationid,
        employeeid: SessionObj.getEmpId(),
        type: settings.id
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          var record = Ext.create('M1CRM.model.crmCustomerDetail', Ext.decode(r.responseText)).raw[0];
          settings.onReturn.apply(settings.scope, [record]);
        }

      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  },


  saveData: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Saving..');
    Ext.Ajax.request({
      url: '/saveCustomerInfo',
      method: 'POST',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: Ext.encode({
        session: SessionObj.getSession(),
        organizationid: this.data.cmoOrganizationID,
        name: this.data.cmoName,
        country: this.data.cmoCountry,
        state: this.data.cmoState,
        city: this.data.cmoCity,
        postcode: this.data.cmoPostCode,
        addressline1: this.data.cmoAddressLine1,
        addressline2: this.data.cmoAddressLine2,
        addressline3: this.data.cmoAddressLine3,
        phonenumber: this.data.cmoPhoneNumber,
        alternatephonenumber: this.data.cmoAlternatePhoneNumber,
        emailaddress: this.data.cmoEMailAddress,
        webaddress: this.data.cmoWebAddress,
        longdescription: this.data.cmoLongDescriptionText,
        user: SessionObj.getEmpId(),
        customertaxable: this.data.cmoCustomerTaxable,
        customertaxcodeid: this.data.cmoCustomerTaxCodeID,
        customersecondtaxcodeid: this.data.cmoCustomerSecondTaxCodeID,
        isnew: this.data.isNewCustomer

      }),
      success: function (r) {
        if (r.responseText.indexOf('error') == -1) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          settings.onReturn.apply(settings.scope, ['ok']);
        }
        else {
          Ext.Msg.alert('Error', r.responseText);
          M1CRM.util.crmCustomerControls.hideAnimation();
          settings.onReturn.apply(settings.scope, ['fail']);
        }
      },
      failure: function () {
        Ext.Msg.alert('Error', 'Error accessing database');
        settings.onReturn.apply(settings.scope, [null]);
      },
      scope: this
    });
  }


});
