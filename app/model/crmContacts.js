Ext.define('M1CRM.model.crmContacts', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'ContactID',
      'OrganizationID',
      'OrganizationName',
      'LocationID',
      'LocationName',
      'City',
      'Title',
      'Name',
      'Phone',
      'totalRec'
    ]
  },
  getListLayout: function () {
    return new Ext.XTemplate(
      '<div class="locationmodel">' +
      '<table style="width:100%;">' +
      '<tr>' +
      '<td width= "100%"><font size="4">{Name}</font></td>' +
      '</tr>' +
      '<tpl if="values.City !== \'\'">' +
      '<tr>' +
      '<td width= "100%"><b>{LocationName} - {City}</b></td>' +
      '</tr>' +
      '<tpl else>' +
      '<tr>' +
      '<td width= "100%"><b>{LocationName}</b></td>' +
      '</tr>' +
      '</tpl>' +
      '<tpl if="values.Title !== \'\'">' +
      '<tr>' +
      '<td width= "100%">{Title}</td>' +
      '</tr>' +
      '</tpl>' +

      /*
       '<tpl if="values.Phone !== \'\'">'  +
       '<tr>' +
       '<td width= "100%">{Phone}</td>'+
       '</tr>' +
       '</tpl>' +
       */
      '</table>' +
      '</div>'
    );
  },
  getListSearchFields: function () {
    return ['Organization', 'OrganizationID', 'Name', 'City', 'Title'];
  },
  searchData: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getContactsByFilter',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession(),
        contact: settings.jsondataObj
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          settings.onReturn.apply(settings.scope, [Ext.create('M1CRM.store.crmContacts', {}).setData(Ext.decode(r.responseText))]);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation(r);
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  }
});
