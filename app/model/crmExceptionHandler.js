Ext.define('M1CRM.model.crmExceptionHandler', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'error'
    ]
  }
});
