Ext.define('M1CRM.model.crmHome', {
  extend: 'Ext.data.Model',

  config: {
    fields: [
      {name: 'MyOpenFollowUps', type: 'int', defaultValue: 0},
      {name: 'MyOpenCalls', type: 'int', defaultValue: 0},
      {name: 'MyOpenQuotes', type: 'int', defaultValue: 0},
      {name: 'MyOpenLeads', type: 'int', defaultValue: 0},
      {name: 'EmployeeID', type: 'string'}
    ]
  },
  getTotals: function (settings) {
    Ext.Ajax.request({
      url: '/getTotals',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Session-Key': SessionObj.getSession(),
        'View-ID': settings.ID
      },
      params: {
        paramObj: JSON.stringify(this.data)
      },
      success: function (response) {
        if (Ext.decode(response.responseText).Status === 200) {
          settings.onReturn.apply(settings.scope, [Ext.decode(response.responseText).DataObject]);
        } else {
          Ext.Msg.alert('Warning', Ext.decode(response.responseText).Message, function (btn) {
            if (btn === 'ok') {
              if (Ext.decode(response.responseText).Message.indexOf('Session ID has expired') >= 0) {
                var view = M1CRM.util.crmViews.getCRMLogin();
                Ext.Viewport.animateActiveItem(view, {type: 'slide', direction: 'right'});
              } else {
                settings.onReturn.apply(settings.scope, [null]);
              }
            }
          }, this);
        }
      },
      failure: function (response) {
        Ext.Msg.alert('Error', 'Error Accessing Database', function (btn) {
          if (btn == 'ok') {
            settings.onReturn.apply(settings.scope, [null]);
          }
        });
      },
      scope: this
    });
  }
});
