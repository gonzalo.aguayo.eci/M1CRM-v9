Ext.define('M1CRM.model.crmCustomerLocations', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'OrganizationID', type: 'string'},
      {name: 'QuoteLocationID', type: 'string'},
      {name: 'InvLocationID', type: 'string'},
      {name: 'ShipLocationID', type: 'string'},
      {name: 'OrganizationName', type: 'string'},
      {name: 'LocationID', type: 'string'},
      {name: 'LocationName', type: 'string'},
      {model: 'M1CRM.model.crmAddress', name: 'Address'},
      {model: 'M1CRM.model.crmContactDetail', name: 'ContactDetail'}
    ]
  },
  getListLayout: function (type) {
    var locStr = '<tpl if="values.LocationID !== \'\'">' +
      '<tr>' +
      '<td width= "100%"><font size="4">{LocationID}- {LocationName}</font></td>' +
      '</tr>' +
      '</tpl>';

    if (type == 'QTLOC') {
      locStr = '<tpl if="values.QuoteLocationID !== \'\'">' +
        '<tr>' +
        '<td width= "100%"><font size="4">{QuoteLocationID}- {LocationName}</font></td>' +
        '</tr>' +
        '</tpl>';
    } else if (type == 'ARINV') {
      locStr = '<tpl if="values.InvLocationID !== \'\'">' +
        '<tr>' +
        '<td width= "100%"><font size="4">{InvLocationID}- {LocationName}</font></td>' +
        '</tr>' +
        '</tpl>';
    } else if (type == 'SHIPLOC') {
      locStr = '<tpl if="values.ShipLocationID !== \'\'">' +
        '<tr>' +
        '<td width= "100%"><font size="4">{ShipLocationID}- {LocationName}</font></td>' +
        '</tr>' +
        '</tpl>';
    }
    return new Ext.XTemplate(
      '<div class="locationmodel">' +
      '<table style="width:100%;">' +
      locStr +
      '<tpl if="values.City !== \'\'">' +
      '<tr>' +
      '<td width= "100%">{City}</td>' +
      '</tr>' +
      '</tpl>' +
      '<tpl if="values.cmlCity !== \'\'">' +
      '<tr>' +
      '<td width= "100%">{State}</td>' +
      '</tr>' +
      '</tpl>' +
      '</table>' +
      '</div>'
    );
  },
  getListSearchFields: function () {
    return ['cmlOrganizationID', 'cmlName', 'cmlLocationID', 'cmlAddressLine1', 'cmlCity', 'cmlState']
  } /* ,
   getLocationCurrency : function(settings){
   M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
   Ext.Ajax.request({
   url: '/getLocationCurrency',
   method: 'GET',
   headers: { 'Content-Type': 'application/json; charset=UTF-8' },
   params: {
   session: SessionObj.getSession(),
   paramObj : JSON.stringify(this.data)
   },
   success: function (r) {
   if (r.responseText.indexOf("error") == 3){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
   }else{
   M1CRM.util.crmCustomerControls.hideAnimation();
   var record =  Ext.create('M1CRM.model.crmCustomerLocations', Ext.decode(r.responseText));
   settings.onReturn.apply(settings.scope, [record]);
   }
   },
   failure: function (r){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
   },
   scope: this
   });
   },
   loadLocations : function (settings) {
   M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
   Ext.Ajax.request({
   url: '/getCustomerLocations',
   method: 'GET',
   headers: { 'Content-Type': 'application/json; charset=UTF-8' },
   params: {
   session: SessionObj.getSession(),
   customerid : settings.customerid
   },
   success: function (r) {
   if (r.responseText.indexOf("error") == 3){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
   }else{
   M1CRM.util.crmCustomerControls.hideAnimation();
   customerLocationStore = Ext.create('M1CRM.store.crmCustomerLocations', {});
   customerLocationStore.setData(Ext.decode(r.responseText));
   settings.onReturn.apply(settings.scope, [customerLocationStore]);
   }
   },
   failure: function (r){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
   },
   scope: this
   });
   },
   searchData : function (settings) {
   M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
   Ext.Ajax.request({
   url: '/getCustomerLocationsByFilter',
   method: 'GET',
   headers: { 'Content-Type': 'application/json; charset=UTF-8' },
   params: {
   session: SessionObj.getSession(),
   custlocations  : settings.jsondataObj
   },
   success: function (r) {
   if (r.responseText.indexOf("error") == 3){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
   }else{
   M1CRM.util.crmCustomerControls.hideAnimation();
   var store  = Ext.create('M1CRM.store.crmCustomerLocations', {}).setData(Ext.decode(r.responseText));
   store.sort('cmlLocationID', 'ASC');
   settings.onReturn.apply(settings.scope, [store]);
   }
   },
   failure: function (r){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
   },
   scope: this
   });
   },

   getCustomerQuoteLocations : function (settings) {
   M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
   Ext.Ajax.request({
   url: '/getCustomerQuoteLocations',
   method: 'GET',
   headers: { 'Content-Type': 'application/json; charset=UTF-8' },
   params: {
   session: SessionObj.getSession(),
   custlocations  : JSON.stringify(this.data)
   },
   success: function (r) {
   if (r.responseText.indexOf("error") == 3){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
   }else{
   M1CRM.util.crmCustomerControls.hideAnimation();
   var tStore =  Ext.create('M1CRM.store.crmCustomerLocations', {}).setData(Ext.decode(r.responseText));
   settings.onReturn.apply(settings.scope, [tStore]);
   }
   },
   failure: function (r){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
   },
   scope: this
   });
   },

   getLocations : function (result) {
   var locStore  =  Ext.create('M1CRM.store.crmCustomerLocations', {});
   locStore.add({ cmlAddressLine1: '<None>',
   cmlCity: '',
   cmlLocationID: '',
   cmlName: '',
   cmlOrganizationID: '',
   cmlState: ''
   });
   for (var i = 0; i < result.data.items.length; i++ ){
   if (result.data.items[i].raw.cmlAddressLine1 != ''){
   locStore.add({ cmlAddressLine1: result.data.items[i].raw.cmlAddressLine1,
   cmlCity: result.data.items[i].raw.cmlCity,
   cmlLocationID: result.data.items[i].raw.cmlLocationID,
   cmlName: result.data.items[i].raw.cmlName,
   cmlOrganizationID: result.data.items[i].raw.cmlOrganizationID,
   cmlState: result.data.items[i].raw.cmlState
   });
   }
   }
   return locStore;
   }
   */


});
