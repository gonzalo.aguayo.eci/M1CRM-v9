Ext.define('M1CRM.model.crmCalls', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'CallID', type: 'string'},
      {name: 'OrganizationID', type: 'string'},
      {name: 'OrganizationName', type: 'string'},
      {name: 'LocationID', type: 'string'},
      {name: 'LocationName', type: 'string'},
      {name: 'City', type: 'string'},
      {name: 'ContactID', type: 'string'},
      {name: 'ContactName', type: 'string'},
      {name: 'OpenedDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      {name: 'ShortDescription', type: 'string'},
      {name: 'AssignedToEmployeeID', type: 'string'},
      {model: 'M1CRM.model.crmAddress', name: 'Address'},
      {name: 'CallStatus', type: 'string'},
      {name: 'StatusRanking', type: 'string'},
      {name: 'QuoteID', type: 'string'},
      {name: 'LeadID', type: 'string'}
    ]
  },

  getMyOpenCallsListLayout: function () {
    return new Ext.XTemplate(
      '<div class="callmodel">' +
      ' <table style="width:100%;">' +

      //'<tr>' + 

      '<tpl if="values.LocationID !== \'\'">' +
      '<tr>' +
      '<td width= "100%"><b><font size="4">{LocationName} - {Address.City}</font></b></td>' +
      '</tr>' +
      '<tpl else>' +
      '<tr>' +
      '<td width= "100%"><b><font size="4">{OrganizationName}</font></b></td>' +
      '</tr>' +
      '</tpl>' +

      '<tpl if="values.ContactName !== \'\'">' +
      '<tr>' +
      '<td width= "100%">{ContactName}</td>' +
      '</tr>' +
      '</tpl>' +


      '<tr>' +
      '<td width= "100%">{ShortDescription}</td>' +
      '</tr>' +
      '<tr>' +
      '<td width= "70%">{OpenedDate:date("d-M-Y")}</td>' +
      '</tr>' +
      '</table>' +
      '</div>'
    );
  },

  getPendingCallsListLayout: function () {
    return new Ext.XTemplate(
      '<div class="callmodel">' +
      ' <table style="width:100%;">' +

      '<tpl if="values.LocationID !== \'\'">' +
      '<tr>' +
      '<td width= "100%"><b><font size="4">{LocationName} - {Address.City}</font></b></td>' +
      '</tr>' +
      '<tpl else>' +
      '<tr>' +
      '<td width= "100%"><b><font size="4">{OrganizationName}</font></b></td>' +
      '</tr>' +
      '</tpl>' +

      '<tpl if="values.ContactName !== \'\'">' +
      '<tr>' +
      '<td width= "70%">{ContactName}</td>' +
      '</tr>' +
      '</tpl>' +

      '<tr>' +
      '<td width= "70%">{ShortDescription}</td>' +
      '</tr>' +
      '<tr>' +
      '<td width= "70%">{OpenedDate:date("d-M-Y")}</td>' +
      '</tr>' +


      '</table>' +
      '</div>'
    );
  },

  getWaitingPendingCallsListLayout: function () {
    return new Ext.XTemplate(
      '<div class="callmodel">' +
      ' <table style="width:100%;">' +
      '<tpl if="values.LocationID !== \'\'">' +
      '<tr>' +
      '<td width= "100%"><b><font size="4">{LocationName} - {Address.City}</font></b></td>' +
      '</tr>' +
      '<tpl else>' +
      '<tr>' +
      '<td width= "100%"><b><font size="4">{OrganizationName}</font></b></td>' +
      '</tr>' +
      '</tpl>' +

      '<tpl if="values.ContactName !== \'\'">' +
      '<tr>' +
      '<td width= "100%">{ContactName}</td>' +
      '</tr>' +
      '</tpl>' +

      '<tr>' +
      '<td width= "70%">{ShortDescription}</td>' +
      '</tr>' +

      '<tr>' +
      '<td width= "70%">{OpenedDate:date("d-M-Y")}</td>' +
      '</tr>' +

      '</table>' +
      '</div>'
    );
  },

  getListSearchFields: function () {
    return ['cmoName', 'cmcName', 'kbpShortDescription', 'cmlName', 'cmlCity', 'cmlAddressLine1'];
  }
  /*
   searchData : function (settings) {
   M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
   Ext.Ajax.request({
   url: '/getCallsByFilter',
   method: 'GET',
   headers: { 'Content-Type': 'application/json; charset=UTF-8' },
   params: {
   session: SessionObj.getSession(),
   calls  : settings.jsondataObj 
   },
   success: function (r) {
   if (r.responseText.indexOf("error") == 3){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
   }else{
   M1CRM.util.crmCustomerControls.hideAnimation();
   var templistid = Ext.decode(settings.jsondataObj).listId;
   if ( templistid == 'myopencalls' || templistid == 'pending'  || templistid  == 'waiting' || templistid == 'closecalls'){
   store = Ext.create('M1CRM.store.crmPendingCalls', {}).setData(Ext.decode(r.responseText));
   }else{    
   store = Ext.create('M1CRM.store.crmCalls', {}).setData(Ext.decode(r.responseText));
   }   
   settings.onReturn.apply(settings.scope, [store]);  
   }	  
   },
   failure: function (r){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
   },
   scope: this
   });
   }  
   */


});
