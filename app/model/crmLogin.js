Ext.define('M1CRM.model.crmLogin', {
  extend: 'Ext.data.Model',
  requires: [
    'M1CRM.model.crmDataSet',
    'M1CRM.store.crmDataSet',
    'M1CRM.model.crmEmployee',
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmUtility'
  ],
  config: {
    fields: [
      {name: 'UserName', type: 'string', defaultValue: ''},
      {name: 'Database', type: 'string', defaultValue: ''},
      {name: 'Module', type: 'string', defaultValue: ''},
      {name: 'Datadictionary', type: 'string', defaultValue: ''},
      {name: 'Password', type: 'string', defaultValue: ''},
      {name: 'SessionID', type: 'string', defaultValue: ''},
      {name: 'Status', type: 'string'},
      {name: 'message', type: 'string'},
      {name: 'Logoutrequired', type: 'bool', defaultValue: false}
    ],
    validations: [
      {type: 'presence', field: 'Database', message: 'Enter Data Set'},
      {type: 'presence', field: 'UserName', message: 'Enter Login ID'},
      {type: 'presence', field: 'Password', message: 'Enter Password'}
    ]
  },
  userLogin: function (settings) {
    Ext.Ajax.request({
      url: '/getSession',
      method: 'GET',
      disableCaching: false,
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        paramObj: settings.jsondataObj ? settings.jsondataObj : JSON.stringify(this.data)
      },
      success: function (response) {
        if (Ext.decode(response.responseText).Status === 200) {
          var sessionID = Ext.decode(response.responseText).DataObject.SessionID;
          var expirationTime = Ext.decode(response.responseText).DataObject.ExpirationTime || 15;
          var expires = new Date((new Date()).getTime() + expirationTime * 60000);

          SessionObj.setExpirationSessionDate(expires.toUTCString());
          SessionObj.setSession(sessionID);

          this.clearSessionTabelSecurity();
          this.validateLogin({
            scope: this,
            onReturn: function (result) {
              settings.onReturn.apply(settings.scope, [result]);
            }
          });
        } else {
          settings.onReturn.apply(settings.scope, [-1]);
        }
      },
      failure: function (response) {
        settings.onReturn.apply(settings.scope, [-1]);
      },
      scope: this
    });
  },


  validateLogin: function (settings) {
    this.data.Password = hex_sha256(SessionObj.getSession() + hex_sha256(this.get('Password')));
    Ext.Ajax.request({
      url: '/LogIn',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Session-Key': SessionObj.getSession()
      },
      params: Ext.encode({
        paramObj: JSON.stringify(this.data)
      }),
      success: function (response) {
        if (Ext.decode(response.responseText).Status === 200) {
          var obj = Ext.decode(response.responseText).DataObject;

          SessionObj.setLoggedIn(true);
          SessionObj.setDataset(this.get('Database'));
          SessionObj.setUserid(this.get('UserName'));
          SessionObj.setCallManagement(obj.CallManagement);
          SessionObj.setMaxRecordsPerPage(100);
          SessionObj.setToken(obj.Token);
          
          var dataSetLocalStore = Ext.create('M1CRM.store.crmDataSet').load();
          var dataChange = false;
          if (dataSetLocalStore.getCount() > 0) {
            if (dataSetLocalStore.last().get('Database') !== this.get('Database'))
              dataChange = true;
            if (dataSetLocalStore.last().get('UserName') !== this.get('UserName'))
              dataChange = true;
          } else {
            dataChange = true;
          }
          if (dataChange) {
            var localData = Ext.create('M1CRM.model.crmDataSet', {
              dataset: this.get('Database'), loginid: this.get('UserName')
            });
            dataSetLocalStore.add(localData);
            dataSetLocalStore.sync();
          }

          this.checkComponentSecurity({
            scope: this,
            onReturn: function (result) {
              settings.onReturn.apply(settings.scope, [result]);
            }
          });

        } else {
          var msg = '';
          if (Ext.browser.is.IE && Ext.decode(response.responseText).Message.toString().indexOf('Make sure your User ID is correct') > 0) {
            msg = 'Incorrect username or password'
          } else {
            msg = Ext.decode(response.responseText).Message;
          }

          Ext.Msg.alert('Warning', msg, function (btn) {
            if (btn === 'ok') {
              settings.onReturn.apply(settings.scope, [-1]);
            }
          });

        }
      },
      failure: function (response) {
        Ext.Msg.alert('Error', Ext.decode(response.responseText).Message, function (btn) {
          if (btn === 'ok') {
            settings.onReturn.apply(settings.scope, [0]);
          }
        });
      },
      scope: this
    });
  },

  checkComponentSecurity: function (settings) {
    Ext.create('M1CRM.model.crmUserSecurity', {}).getComponentSecurity({
      module: 'CRM',
      scope: this,
      onReturn: function (result) {
        if (result) {
          if (Ext.decode(result).Status === 200) {
            var dataObj = Ext.decode(result).DataObject;
            var proceed = true;
            var msg = '';

            if (!dataObj.Basic && !dataObj.Advance) {
              proceed = false;
              msg = 'Component security not defined for this user';
            } else if (!dataObj.Basic && dataObj.Advance) {
              proceed = false;
              msg = 'Component security not defined for basic CRM';
            }

            if (proceed) {
              SessionObj.setCRMBasic(dataObj.Basic);
              if (dataObj.Advance) {
                this.checkCRMAdvance({
                  scope: this,
                  onReturn: function (result) {
                    settings.onReturn.apply(settings.scope, [result]);
                  }
                });
              } else {
                SessionObj.setQuotationManagement(false);
                SessionObj.setLeadManagement(false);
                SessionObj.setCRMAdvace(false);
                this.getRegionAndEmpInfo({
                  scope: this,
                  onReturn: function (result) {
                    settings.onReturn.apply(settings.scope, [result]);
                  }
                });
              }

            } else {
              Ext.Msg.alert('Warning', msg, function (btn) {
                if (btn === 'ok') {
                  settings.onReturn.apply(settings.scope, [0]);
                }
              });
            }
          }
        } else {
          Ext.Msg.alert('Warning', Ext.decode(result).Message, function (btn) {
            if (btn === 'ok') {
              settings.onReturn.apply(settings.scope, [0]);
            }
          });
        }
      },
      failure: function (result) {
        Ext.Msg.alert('Error', Ext.decode(result).Message, function (btn) {
          if (btn == 'ok') {
            settings.onReturn.apply(settings.scope, [0]);
          }
        });
      },
      scope: this
    });

  },


  logOutAndCallLogin: function () {
    var user = Ext.create('M1CRM.model.crmLogin', {});
    user.logOut({
      scope: this,
      onReturn: function (result) {
        if (result != null) {
          this.getLoginForm();
        }
      }
    });
  },
  getLoginForm: function () {
    var view = M1CRM.util.crmViews.getCRMLogin();
    M1CRM.util.crmViews.slideRight(view);
  },

  getRegionAndEmpInfo: function (settings) {
    Ext.create('M1CRM.model.crmUtility', {}).getDBRegion({
      onReturn: function (result) {
        if (result != null) {
          if (Ext.decode(result).Status == 200) {
            var dataObj = Ext.decode(result).DataObject;
            if (dataObj.DBRegion == '') {
              Ext.Msg.confirm("Warning", 'Database Region is not defined, the app will work as USA region. Do you wish to proceed?', function (btn, type) {
                if (btn == 'yes') {
                  SessionObj.setDBRegion(dataObj.DBRegion == '' ? 'USA' : dataObj.DBRegion);
                  SessionObj.setOrgLabel(dataObj.OrgLabel);
                  SessionObj.setPostCodeLabel(dataObj.PostCodeLabel);
                  SessionObj.setDateFormat(dataObj.DateFormat);
                  SessionObj.setSimpleDateFormat(dataObj.SimpleDateFormat);
                  SessionObj.setDisplayDateTime(dataObj.DisplayDateTime);
                  SessionObj.setDateTimeFormat(dataObj.DateTimeFormat);
                  SessionObj.setDatePickerSlotOrder(dataObj.DatePickerSlotOrder);
                  SessionObj.setDateTimePickerSlotOrder(dataObj.DateTimePickerSlotOrder);
                  SessionObj.setDateTimePicker(dataObj.DateTimePicker);
                  SessionObj.setOrgName(dataObj.OrgName);
                  SessionObj.setCurrencySymbol(dataObj.CurrencySymbol);
                  SessionObj.setEnableMultiCurrency(dataObj.EnableMultiCurrency);
                  SessionObj.setShowSecondTax(dataObj.ShowSecondTax);
                  Ext.create('M1CRM.model.crmEmployee', {
                    user: SessionObj.getUserid()
                  }).getEmployeeDetails({
                    scope: this,
                    onReturn: function (result) {
                      settings.onReturn.apply(settings.scope, [result]);
                    }
                  });
                } else {
                  settings.onReturn.apply(settings.scope, [0]);
                }
              }, this);
            } else {
              SessionObj.setDBRegion(dataObj.DBRegion);
              SessionObj.setOrgLabel(dataObj.OrgLabel);
              SessionObj.setPostCodeLabel(dataObj.PostCodeLabel);
              SessionObj.setDateFormat(dataObj.DateFormat);
              SessionObj.setSimpleDateFormat(dataObj.SimpleDateFormat);
              SessionObj.setDisplayDateTime(dataObj.DisplayDateTime);
              SessionObj.setDateTimeFormat(dataObj.DateTimeFormat);
              SessionObj.setDatePickerSlotOrder(dataObj.DatePickerSlotOrder);
              SessionObj.setDateTimePickerSlotOrder(dataObj.DateTimePickerSlotOrder);
              SessionObj.setDateTimePicker(dataObj.DateTimePicker);
              SessionObj.setOrgName(dataObj.OrgName);
              SessionObj.setCurrencySymbol(dataObj.CurrencySymbol);
              SessionObj.setEnableMultiCurrency(dataObj.EnableMultiCurrency);
              SessionObj.setShowSecondTax(dataObj.ShowSecondTax);
              Ext.create('M1CRM.model.crmEmployee', {
                user: SessionObj.getUserid()
              }).getEmployeeDetails({
                scope: this,
                onReturn: function (result) {
                  settings.onReturn.apply(settings.scope, [result]);
                }
              });
            }
          } else {
            Ext.Msg.alert('Warning', Ext.decode(result).Message, function (btn) {
              if (btn == 'ok') {
                settings.onReturn.apply(settings.scope, [0]);
              }
            });

          }
        }
      },
      failure: function (result) {
        Ext.Msg.alert('Error', Ext.decode(result).Message, function (btn) {
          if (btn == 'ok') {
            settings.onReturn.apply(settings.scope, [0]);
          }
        });
      },
      scope: this
    });
  },

  checkCRMAdvance: function (settings) {
    Ext.create('M1CRM.model.crmUserSecurity', {}).checkCRMAdvance({
      scope: this,
      onReturn: function (result) {
        if (Ext.decode(result).Status == 200) {
          var dataObj = Ext.decode(result).DataObject;
          if (dataObj.EnableCRMAdv) {
            SessionObj.setCRMAdvace(dataObj.EnableCRMAdv);
            SessionObj.setQuotationManagement(dataObj.QuoteManagement);
            SessionObj.setLeadManagement(dataObj.LeadManagement);

            this.getRegionAndEmpInfo({
              scope: this,
              onReturn: function (result) {
                settings.onReturn.apply(settings.scope, [result]);
              }
            });
          } else {

            Ext.Msg.confirm("Warning", dataObj.Message, function (btn) {
              if (btn == 'yes') {
                SessionObj.setCRMAdvace(false);
                SessionObj.setQuotationManagement(false);
                this.getRegionAndEmpInfo({
                  scope: this,
                  onReturn: function (result) {
                    settings.onReturn.apply(settings.scope, [result]);
                  }
                });
              } else {
                settings.onReturn.apply(settings.scope, [0]);
              }
            }, this);

          }

        } else {
          Ext.Msg.alert('Warning', Ext.decode(result).Message, function (btn) {
            if (btn == 'ok') {
              settings.onReturn.apply(settings.scope, [0]);
            }
          });
        }
      },
      failure: function (result) {
        Ext.Msg.alert('Warning', Ext.decode(result).Message, function (btn) {
          if (btn == 'ok') {
            settings.onReturn.apply(settings.scope, [0]);
          }
        });
      },
      scope: this

    });
  },

  clearSessionTabelSecurity: function () {
    SessionObj.setToken('');
    SessionObj.setFollowupAddEditTSecurity('');
    SessionObj.setCallAddEditTSecurity('');
    SessionObj.setContactAddEditTSecurity('');
    SessionObj.setCustomerAddEditTSecurity('');
    SessionObj.setQuoteAddEditTSecurity('');
    SessionObj.setQuoteQtyAddEditTSecurity('');
    SessionObj.setLeadAddEditTSecurity('');
    SessionObj.setLeadLineAddEditTSecurity('');
  },

  clearCookies: function () {
    var utilities = Ext.create('M1CRM.model.crmUtility', {});
    utilities.deleteCookies();
  }, 

  logOut: function (settings) {
    Ext.Ajax.request({
      url: '/logOut',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Session-Key': SessionObj.getSession()
      },
      success: function (response) {
        SessionObj.setLoggedIn(false);
        SessionObj.setDataset('');
        SessionObj.setUserid('');
        SessionObj.setEmpIsAQuoter('');
        SessionObj.setDBRegion('');
        SessionObj.setOrgLabel('');
        SessionObj.setCallManagement('');
        SessionObj.setQuotationManagement('');
        SessionObj.setCurrencySymbol('');
        SessionObj.setEnableMultiCurrency(false);
        this.clearSessionTabelSecurity();
        this.clearCookies();
        settings.onReturn.apply(settings.scope, ['ok']);
      },
      failure: function () {
        settings.onReturn.apply(settings.scope, [null]);
      },
      scope: this
    });
  }


});
