Ext.define('M1CRM.model.crmServiceAccessModel', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'ListID', type: 'string'},
      {name: 'ListType', type: 'string', defaultValue: ''},
      {name: 'PageID', type: 'int', defaultValue: 1},
      {name: 'MaxLimit', type: 'int', defaultValue: 1},
      {name: 'ZeroRecShowMessage', type: 'bool', defaultValue: false},
      {name: 'AllowPaging', type: 'bool', defaultValue: true}
    ]
  },

  getIt: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getIt',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Session-Key': SessionObj.getSession(),
        'UTC': new Date().getTimezoneOffset() / 60 * -1,
        'View-ID': settings.ViewID,
        'Request-ID': settings.RequestID
      },
      params: {
        paramObj: settings.jsondataObj
      },
      success: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        if (Ext.decode(r.responseText).Status == 200) {
          settings.onReturn.apply(settings.scope, [r.responseText]);
        } else {
          Ext.Msg.alert('Warning', Ext.decode(r.responseText).Message, function (btn) {
            if (btn == 'ok') {
              if (Ext.decode(r.responseText).Message.indexOf('Session ID has expired') >= 0) {
                var view = M1CRM.util.crmViews.getCRMLogin();
                Ext.Viewport.animateActiveItem(view, {type: 'slide', direction: 'right'});
              } else {
                settings.onReturn.apply(settings.scope, [null]);
              }
            }
          }, this);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        Ext.Msg.alert('Error', 'Error Accessing Database', function (btn) {
          if (btn == 'ok') {
            settings.onReturn.apply(settings.scope, [null]);
          }
        }, this);
      },
      scope: this
    });
  },

  getAllData: function (settings) {
    Ext.Ajax.request({
      url: '/getAll',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Session-Key': SessionObj.getSession(),
        'List-ID': this.data.ListID,
        'List-Type': this.data.ListType,
        'Page-ID': this.data.PageID,
        'Max-Limit': this.data.MaxLimit
      },
      params: {
        paramObj: settings.jsondataObj
      },
      success: function (r) {
        if (Ext.decode(r.responseText).Status == 200) {
          if (this.data.ZeroRecShowMessage == true) {
            if (Ext.decode(r.responseText).DataObject.TotalRecords > 0) {
              settings.onReturn.apply(settings.scope, [r.responseText]);
            } else {

              Ext.Msg.alert('Info', 'No Records Found', function (btn) {
                if (btn == 'ok') {
                  settings.onReturn.apply(settings.scope, [null]);
                }
              }, this);
            }
          } else {
            settings.onReturn.apply(settings.scope, [r.responseText]);
          }

        } else {
          Ext.Msg.alert('Warning', Ext.decode(r.responseText).Message, function (btn) {
            if (btn == 'ok') {
              if (Ext.decode(r.responseText).Message.indexOf('Session ID has expired') >= 0) {
                var view = M1CRM.util.crmViews.getCRMLogin();
                Ext.Viewport.animateActiveItem(view, {type: 'slide', direction: 'right'});
              } else {
                settings.onReturn.apply(settings.scope, [null]);
              }
            }
          }, this);


        }
      },
      failure: function (r) {
        Ext.Msg.alert('Error', 'Error Accessing Database', function (btn) {
          if (btn == 'ok') {
            settings.onReturn.apply(settings.scope, [null]);
          }
        }, this);
      },
      scope: this
    });
  },

  getValueList: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getValueList',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Session-Key': SessionObj.getSession()
      },
      params: {
        paramObj: settings.jsondataObj
      },
      success: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        if (Ext.decode(r.responseText).Status == 200) {
          settings.onReturn.apply(settings.scope, [r.responseText]);
        } else {
          Ext.Msg.alert('Warning', Ext.decode(r.responseText).Message, function (btn) {
            if (btn == 'ok') {
              settings.onReturn.apply(settings.scope, [null]);
            }
          }, this);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        Ext.Msg.alert('Error', 'Error Accessing Database', function (btn) {
          if (btn == 'ok') {
            settings.onReturn.apply(settings.scope, [null]);
          }
        }, this);
      },
      scope: this

    });
  },

  getTotals: function (settings) {
    Ext.Ajax.request({
      url: '/getTotals',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Session-Key': SessionObj.getSession(),
        'UTC': new Date().getTimezoneOffset() / 60 * -1,
        'View-ID': settings.ViewID,
        'Request-ID': settings.RequestID
      },
      params: {
        paramObj: settings.jsondataObj
      },
      success: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        if (Ext.decode(r.responseText).Status == 200) {
          settings.onReturn.apply(settings.scope, [r.responseText]);
        } else {
          Ext.Msg.alert('Warning', Ext.decode(r.responseText).Message, function (btn) {
            if (btn == 'ok') {
              if (Ext.decode(r.responseText).Message.indexOf('Session ID has expired') >= 0) {
                var view = M1CRM.util.crmViews.getCRMLogin();
                Ext.Viewport.animateActiveItem(view, {type: 'slide', direction: 'right'});
              } else {
                settings.onReturn.apply(settings.scope, [null]);
              }
            }
          }, this);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        Ext.Msg.alert('Error', 'Error Accessing Database', function (btn) {
          if (btn == 'ok') {
            settings.onReturn.apply(settings.scope, [null]);
          }
        }, this);
      },
      scope: this
    });
  },

  saveIt: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Saving..');

    Ext.Ajax.request({
      url: '/saveIt',
      method: 'POST',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Session-Key': SessionObj.getSession(),
        'UTC': new Date().getTimezoneOffset() / 60 * -1,
        'View-ID': settings.ViewID,
        'Request-ID': settings.RequestID == null || settings.RequestID == '' ? 'savedata' : settings.RequestID
      },
      params: Ext.encode({
        paramObj: settings.jsondataObj
      }),
      success: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        settings.onReturn.apply(settings.scope, [r.responseText]);
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        Ext.Msg.alert('Error', 'Error Accessing Database', function (btn) {
          if (btn == 'ok') {
            settings.onReturn.apply(settings.scope, [null]);
          }
        }, this);
      },
      scope: this
    });
  },

  emailIt: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Generating Email..');
    Ext.Ajax.request({
      url: '/emailIt',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Session-Key': SessionObj.getSession(),
        'UTC': new Date().getTimezoneOffset() / 60 * -1,
        'View-ID': settings.ViewID,
        'Request-ID': settings.RequestID
      },
      params: {
        paramObj: settings.jsondataObj
      },
      success: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        settings.onReturn.apply(settings.scope, [r.responseText]);
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        Ext.Msg.alert('Error', 'Error Accessing Database', function (btn) {
          if (btn == 'ok') {
            settings.onReturn.apply(settings.scope, [null]);
          }
        }, this);
      },
      scope: this
    });
  }

});	  