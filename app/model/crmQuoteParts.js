Ext.define('M1CRM.model.crmQuoteParts', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'QuoteID',
      'QuoteLineID',
      'PartID',
      'PartRevisionID',
      'PartGroupID',
      'PartDesc',
      'UOM',
      'TaxCodeID',
      'SecondTaxCodeID',
      'NonTaxReasonID',
      {name: 'QtQuantityList', type: 'auto'}
    ]
  },
  getListSearchFields: function () {
    return ['qmlPartID', 'qmlPartRevisionID', 'qmlPartShortDescription'];
  },
  getQuotePartsLayout: function () {
    return new Ext.XTemplate(
      '<div class="quotepartsmodel">' +
      ' <table style="width:100%;">' +

      '<tpl if="values.PartRevisionID !== \'\'">' +
      '<tr>' +
      '<td width= "100%"><font size="4">{QuoteLineID}. <b>{PartID}, {PartRevisionID} - {PartDesc}</font></b></td>' +
      '<tr>' +
      '<tpl else>' +
      '<tr>' +
      '<td width= "100%"><font size="4">{QuoteLineID}. <b>{PartID} - {PartDesc} </font></b></td>' +
      '<tr>' +
      '</tpl>' +
      '<tr>' +
      '<td style="padding-left:20px">' +
      ' <table style="width:100%;">' +
      '<tpl for="QtQuantityList">' +
      '<tpl if="values.DiscountPercent &gt; 0">' +
      '<tr>' +
      '<td width= "100%">{QuoteQuantity}- {FullRevisedUnitPriceBase:this.formatNumberAsCurrency}, [Discount {DiscountPercent}%] </td>' +
      '</tr>' +
      '<tpl else>' +
      '<tr>' +
      '<td width= "100%">{QuoteQuantity}- {FullRevisedUnitPriceBase:this.formatNumberAsCurrency} </td>' +
      '</tr>' +
      '</tpl>' +
      '</tpl>' +
      '</table>' +
      '</td>' +
      '</tr>' +
      '</table>' +
      '</div>',
      {
        formatNumberAsCurrency: function (value) {
          return Ext.String.format("{0} {1}", SessionObj.getCurrencySymbol(), value);
        }

      });
  },

  confirmQuoteParts: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Saving..');
    Ext.Ajax.request({
      url: '/confirmQuoteLines',
      method: 'POST',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: Ext.encode({
        session: SessionObj.getSession(),
        quoteline: settings.jsondataObj != null ? settings.jsondataObj : JSON.stringify(this.data)
      }),
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          store = Ext.create('M1CRM.store.crmQuoteParts', {});
          store.setData(Ext.decode(r.responseText));
          settings.onReturn.apply(settings.scope, [store.getAt(0).data.Status]);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  }

});
