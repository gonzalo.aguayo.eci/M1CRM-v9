Ext.define('M1CRM.model.crmMarketingProgram', {
  extend: "Ext.data.Model",
  config: {
    fields: [
      'ID',
      'Description'
    ]
  }
});
