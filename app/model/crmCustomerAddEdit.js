Ext.define("M1CRM.model.crmCustomerAddEdit", {
  extend: "Ext.data.Model",
  config: {
    fields: [
      {name: 'OrganizationID', type: 'string'},
      {name: 'OrganizationName', type: 'string'},
      {name: 'CountryCode', type: 'string'},
      {name: 'LocationID', type: 'string'},
      {name: 'LocationName', type: 'string'},
      {name: 'ContactID', type: 'string'},
      {name: 'Name', type: 'string'},
      {model: 'M1CRM.model.crmAddress', name: 'Address'},
      {model: 'M1CRM.model.crmContactDetail', name: 'ContactDetail'},
      {name: 'CustomerTaxable', type: 'bool', defaultValue: false},
      {name: 'CustomerTaxCodeID', type: 'string'},
      {name: 'CustomerSecondTaxCodeID', type: 'string'},
      {name: 'CustomerStatus', type: 'int', defaultValue: 0},
      {name: 'ResellerStatus', type: 'int', defaultValue: 0},
      {name: 'SupplierStatus', type: 'int', defaultValue: 0},
      {name: 'TotalLocations', type: 'int', defaultValue: 0},
      {name: 'TotalContacts', type: 'int', defaultValue: 0},
      {name: 'TotalOpenCalls', type: 'int', defaultValue: 0},
      {name: 'TotalOpenFollowup', type: 'int', defaultValue: 0},
      {name: 'TotalLinkedParts', type: 'int', defaultValue: 0},
      {name: 'TotalLinkedQuotes', type: 'int', defaultValue: 0},
      //{ name :'isNewCustomer', type : 'bool' , defaultValue : false},
      {name: 'LongDescriptionText', type: 'string'},
      {name: 'EmployeeID', type: 'string'}
    ],
    validations: [
      //{ type: 'presence',  field: 'OrganizationID', message : 'Enter Organisation ID'  },
      //{ type: 'presence',  field: 'OrganizationName', message : 'Enter Organisation Name'  }
    ]
  },
  getStatusCodes: function (code) {
    if (parseInt(code) == 0)
      return '<None>';
    else if (parseInt(code) == 1)
      return 'Prospective';
    else if (parseInt(code) == 2)
      return 'Active';
    else
      return 'Inactive';
  },
  getStatusCodeID: function (code) {
    if (code == '<None>')
      return 0;
    else if (code == 'Prospective')
      return 1;
    else if (code == 'Active')
      return 2;
    else
      return 3;
  },
  validate: function () {
    var error = this.callParent();

    if (this.data.OrganizationID == null || this.data.OrganizationID == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'OrganizationID',
        message: 'Enter Org' //+ SessionObj.getOrgLabel()
      }));
    }
    if (this.data.OrganizationName == true || this.data.OrganizationName == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'OrganizationName',
        message: 'Enter ' + SessionObj.getOrgLabel() + ' Name'
      }));
    }

    if (this.data.CustomerTaxable == true && this.data.CustomerTaxCodeID == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'CustomerTaxCodeID',
        message: 'Select  a tax id'
      }));
    }
    return error;
  },
  getData: function (settings) {
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).getIt({
      scope: this,
      ViewID: 'CRMCUSTOMERADDEDIT',
      RequestID: settings.RequestID,
      jsondataObj: JSON.stringify(Ext.create('M1CRM.model.crmDataFilterModel', {
        FilterValues: [this.data.OrganizationID]
      }).data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  },
  saveData: function (settings) {
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).saveIt({
      scope: this,
      ViewID: 'CRMCUSTOMERADDEDIT',
      jsondataObj: JSON.stringify(this.data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });


  }
});

