Ext.define("M1CRM.model.crmAddress", {
  extend: "Ext.data.Model",
  config: {
    fields: [
      {name: 'AddressLine1', type: 'string'},
      {name: 'AddressLine2', type: 'string'},
      {name: 'AddressLine3', type: 'string'},
      {name: 'City', type: 'string'},
      {name: 'State', type: 'string'},
      {name: 'PostCode', type: 'string'},
      {name: 'CountryCode', type: 'string'},
      {name: 'Country', type: 'string'}
    ]
  }
});