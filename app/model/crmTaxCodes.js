Ext.define('M1CRM.model.crmTaxCodes', {
  extend: "Ext.data.Model",
  config: {
    fields: [
      'TaxCodeID',
      'Description',
      'TaxCodeType',
      'TaxRate'
    ]
  },
  /*
   getTaxCodes : function (settings) {
   M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
   Ext.Ajax.request({
   url: '/getTaxCodes',
   method: 'GET',
   headers: { 'Content-Type': 'application/json; charset=UTF-8' },
   params: {
   session: SessionObj.getSession(),
   taxcodes : settings.jsondataObj != null ? settings.jsondataObj : JSON.stringify(this.data)
   },
   success: function (r) {
   if (r.responseText.indexOf("error") == 3){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
   }else{
   var result =  Ext.create('M1CRM.store.crmTaxCodes', {}).setData(Ext.decode(r.responseText));
   var tStore  =  new Ext.create('M1CRM.store.crmTaxCodes', {});
   tStore.add( { TaxCodeID : '', Description : '<None>' } );
   for (var i =0; i < result.data.items.length; i++ ){
   tStore.add( { TaxCodeID : result.data.items[i].raw.TaxCodeID , Description : result.data.items[i].raw.Description } );
   }
   result = null;
   settings.onReturn.apply(settings.scope, [tStore]);
   }
   },
   failure: function (r){
   M1CRM.util.crmCustomerControls.hideAnimation(r);
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
   },
   scope: this
   });
   }, */
  getTaxRate: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getTaxRate',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession(),
        taxcodes: settings.jsondataObj != null ? settings.jsondataObj : JSON.stringify(this.data)
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation(r);
          settings.onReturn.apply(settings.scope, [Ext.decode(r.responseText)[0]]);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation(r);
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  }

});
