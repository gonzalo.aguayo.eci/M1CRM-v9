Ext.define('M1CRM.model.crmQuotePartQty', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'QuoteID',
      'QuoteLineID',
      'QuoteQuantityID',
      'QuoteQuantity',
      'RevisedUnitPriceBase',
      'DiscountPercent',
      'Discount',
      'UnitPrice',
      'UnitDiscountBase',
      'FullRevisedUnitPriceBase',
      'UnitTaxAmountBase',
      'UnitSecondTaxAmountBase',
      'CreatedFromMobile',
      'totalRec'
    ]
  },
  getListSearchFields: function () {
    return ['QuoteQuantity', 'RevisedUnitPriceBase'];
  },
  getListLayout: function () {
    return new Ext.XTemplate(
      '<div class="quotepartqtymodel">' +
      ' <table style="width:100%;">' +
      '<tpl if="values.DiscountPercent &gt; 0">' +
      '<tr>' +
      '<td width= "100%">{QuoteQuantity}- ${FullRevisedUnitPriceBase}, [Discount {DiscountPercent}%] </td>' +
      '</tr>' +
      '<tpl else>' +
      '<tr>' +
      '<td width= "100%">{QuoteQuantity}- ${FullRevisedUnitPriceBase} </td>' +
      '</tr>' +
      '</tpl>' +
      '</table>' +
      '</div>'
    );
  },
  saveQuotePartQty: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Saving..');
    Ext.Ajax.request({
      url: '/saveQuotePartQty',
      method: 'POST',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: Ext.encode({
        session: SessionObj.getSession(),
        quotepartqty: settings.jsondataObj != null ? settings.jsondataObj : JSON.stringify(this.data)
      }),
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          settings.onReturn.apply(settings.scope, [Ext.decode(r.responseText).Status]);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  },
  searchData: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getQuotePartsQuantities',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession(),
        quotepartqty: settings.jsondataObj
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          store = Ext.create('M1CRM.store.crmQuotePartQty', {});
          store.setData(Ext.decode(r.responseText));
          settings.onReturn.apply(settings.scope, [store]);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  },
  calcRevisedUnitPrice: function () {
    var discount = this.data.Discount / 100;
    var curPrice = this.data.UnitPrice;
    if (discount != 0) {
      this.data.UnitDiscountBase = curPrice * discount;
      this.data.FullRevisedUnitPriceBase = curPrice - this.data.UnitDiscountBase;
    } else {
      this.data.UnitDiscountBase = 0;
      this.data.FullRevisedUnitPriceBase = curPrice;
    }
  }

});
