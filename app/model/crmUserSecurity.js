Ext.define('M1CRM.model.crmUserSecurity', {
  extend: 'Ext.data.Model',

  config: {
    fields: [
      {name: 'ID', type: 'auto'},
      {name: 'Module', type: 'string'},
      {name: 'USLeve', type: 'auto'},
      {name: 'Basic', type: 'auto'},
      {name: 'Advance', type: 'auto'}
    ]
  },

  getUserSecurityLevel: function (settings) {
    var defaultKey = '00000000-0000-0000-0000-000000000000';

    Ext.Ajax.request({
      url: '/getSecurityLevel',
      method: 'GET',
      disableCaching: false,
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Session-Key': SessionObj.getSession() || defaultKey,
        'View-ID': settings.ID
      },
      success: function (r) {
        if (Ext.decode(r.responseText).Status === 200) {
          settings.onReturn.apply(settings.scope, [Ext.decode(r.responseText).DataObject]);
        } else {
          M1CRM.util.crmCustomerControls.displayDBResponceError(Ext.decode(r.responseText).Message, settings);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.displayDBResponceError(Ext.decode(r.responseText).Message, settings);
      },
      scope: this
    });
  },

  checkCRMAdvance: function (settings) {
    Ext.Ajax.request({
      url: '/checkCRMAdvance',
      method: 'GET',
      disableCaching: false,
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Session-Key': SessionObj.getSession()
      },
      success: function (response) {
        settings.onReturn.apply(settings.scope, [response.responseText]);
      },
      failure: function (response) {
        settings.onReturn.apply(settings.scope, [response.responseText]);
      },
      scope: this
    });

  },

  getComponentSecurity: function (settings) {
    this.data.Module = settings.module;
    Ext.Ajax.request({
      url: '/getComponentSecurity',
      method: 'GET',
      disableCaching: false,
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Session-Key': SessionObj.getSession()
      },
      params: {
        paramObj: JSON.stringify(this.data)
      },
      success: function (response) {
        settings.onReturn.apply(settings.scope, [response.responseText]);
      },
      failure: function (response) {
        settings.onReturn.apply(settings.scope, [response.responseText]);
      },
      scope: this
    });
  }
});
