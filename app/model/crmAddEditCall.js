Ext.define("M1CRM.model.crmAddEditCall", {
  extend: "Ext.data.Model",
  config: {
    fields: [
      {name: 'CallID', type: 'string'},
      {name: 'CallTypeID', type: 'string'},
      {name: 'OrganizationID', type: 'string'},
      {name: 'OrganizationName', type: 'string'},
      {name: 'LocationID', type: 'string'},
      {name: 'LocationName', type: 'string'},
      {name: 'ContactID', type: 'string'},
      {name: 'Contact', type: 'string'},
      {name: 'PartID', type: 'string'},
      {name: 'PartRevisionID', type: 'string'},
      {name: 'PartShortDescription', type: 'string'},
      {name: 'ShortDescription', type: 'string'},
      {name: 'LongDescriptionText', type: 'string'},
      {name: 'ContactMethodID', type: 'string'},
      {name: 'InternalOnly', type: 'bool'},
      {name: 'PriorityID', type: 'int'},
      {name: 'CallStatus', type: 'string'},
      {name: 'ReasonID', type: 'string'},
      {name: 'OpenedBy', type: 'string'},
      {name: 'OpenedDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      {name: 'AssignedTo', type: 'string'},
      {name: 'AssignedDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      {name: 'AcceptedDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      {name: 'ClosedBy', type: 'string'},
      {name: 'ClosedDate', type: 'date'},
      {name: 'CurrencyRateID', type: 'string'},
      {name: 'LeadID', type: 'string'},
      {name: 'QuoteID', type: 'string'},
      {name: 'SalesOrderID', type: 'string'},
      {name: 'CreatedFromMobile', type: 'int', defaultValue: 1}
    ],
    validations: [
      //{type: 'presence',  field: 'OrganizationID', message : 'Organization Not Selected'  },
      //{type: 'presence',  field: 'ShortDescription', message : 'Enter Call Description'  }
    ]
  },
  getIt: function (settings) {
    var filtArr = settings.RequestID == 'prop' ? [SessionObj.getEmpId()] : settings.RequestID == 'checkpart' ? [this.data.PartID] : [this.data.CallID, SessionObj.getEmpId()];
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).getIt({
      scope: this,
      ViewID: 'CRMADDEDITCALL',
      RequestID: settings.RequestID,
      jsondataObj: JSON.stringify(Ext.create('M1CRM.model.crmDataFilterModel', {
        FilterValues: filtArr
      }).data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  },

  validate: function () {
    var error = this.callParent();
    if (this.data.OrganizationID == null || this.data.OrganizationID == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'OrganizationID',
        message: 'Select Org '//+ SessionObj.getOrgLabel()
      }));
    }
    if (this.data.ShortDescription == true || this.data.ShortDescription == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'ShortDescription',
        message: 'Enter Call Desc'
      }));
    }
    return error;
  },

  saveData: function (settings) {
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).saveIt({
      scope: this,
      ViewID: 'CRMADDEDITCALL',
      jsondataObj: JSON.stringify(this.data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  }

});      