Ext.define('M1CRM.model.crmDataFilterModel', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'FilterFields', type: 'auto'},
      {name: 'FilterValues', type: 'auto'},
      {name: 'UserID', type: 'string'},
      {name: 'EmployeeID', type: 'string'},
      {name: 'FollowupID', type: 'string'},
      {name: 'AssignedToEmployeeID', type: 'string'},
      {name: 'FieldID', type: 'string'},
      {name: 'OrganizationID', type: 'string'},
      {name: 'OrganizationName', type: 'string'},
      {name: 'LocationID', type: 'string'},
      {name: 'LocationName', type: 'string'},
      {name: 'LocType', type: 'string'},
      {name: 'ContactID', type: 'string'},
      {name: 'ContactName', type: 'string'},
      {name: 'CallID', type: 'string'},
      {name: 'LeadID', type: 'string'},
      {name: 'LeadLineID', type: 'auto'},
      {name: 'QuoteID', type: 'string'},
      {name: 'LeadID', type: 'string'},
      {name: 'Email', type: 'string'},
      {name: 'PartID', type: 'string'},
      {name: 'RevisionID', type: 'string'},
      {name: 'PartGroupID', type: 'string'},
      {name: 'Qty', type: 'float', defaultValue: 0.0},
      {name: 'ListType', type: 'string', defaultValue: ''},
      {name: 'OrderBy', type: 'string', defaultValue: ''},
      {name: 'ParentView', type: 'string', defaultValue: ''}
    ]
  }
});