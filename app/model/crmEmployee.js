Ext.define('M1CRM.model.crmEmployee', {
  extend: 'Ext.data.Model',
  requires: ['M1CRM.view.crmHome'],
  config: {
    fields: [
      {name: 'ID', type: 'string', defaultValue: ''},
      {name: 'Description', type: 'string', defaultValue: ''},
      {name: 'EmployeeID', type: 'string', defaultValue: ''},
      {name: 'EmployeeName', type: 'string', defaultValue: ''},
      {name: 'user', type: 'string'}
    ]
  },
  getEmployeeDetails: function (settings) {
    Ext.Ajax.request({
      url: '/getEmployeeDetails',
      method: 'GET',
      headers: {
        'Content-Type': 'application/json; charset=UTF-8',
        'Session-Key': SessionObj.getSession()
      },
      success: function (response) {
        if (Ext.decode(response.responseText).Status == 200) {
          var dataObj = Ext.decode(response.responseText).DataObject;
          if (dataObj != null && dataObj.EmployeeID != null) {
            SessionObj.setEmpId(dataObj.EmployeeID);
            SessionObj.setEmpName(dataObj.EmployeeName);
            SessionObj.setEmpIsAQuoter(dataObj.QuoterEmployee);
            settings.onReturn.apply(settings.scope, [Ext.decode(response.responseText).Status]);
          } else {
            Ext.Msg.alert('Warning', 'User ID not linked to an employee cannot proceed.', function (btn) {
              if (btn == 'ok') {
                settings.onReturn.apply(settings.scope, [0]);
              }
            });
          }
        } else {
          Ext.Msg.alert('Warning', Ext.decode(response.responseText).Message, function (btn) {
            if (btn == 'ok') {
              settings.onReturn.apply(settings.scope, [0]);
            }
          });
        }
      },
      failure: function (response) {
        Ext.Msg.alert('Error', Ext.decode(response.responseText).Message, function (btn) {
          if (btn == 'ok') {
            settings.onReturn.apply(settings.scope, [0]);
          }
        });
      },
      scope: this
    });
  },
  getActiveEmployees: function (settings) {
    Ext.create('M1CRM.model.crmServiceAccessModel', {
      ListID: 'M1EMPLOYEESACTIVE'
    }).getAllData({
      scope: this,
      jsondataObj: JSON.stringify(Ext.create('M1CRM.model.crmDataFilterModel', {}).data),
      onReturn: function (result) {
        if (result != null) {
          var store = Ext.create('M1CRM.store.crmEmployee', {}).setData(Ext.decode(result).DataObject.DataList);
          settings.onReturn.apply(settings.scope, [store]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  }

});
