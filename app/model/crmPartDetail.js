Ext.define('M1CRM.model.crmPartDetail', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'PartID',
      'RevisionID',
      'InvUOM',
      'Description',
      'LongDescription',
      'QtyOnHand',
      //'UnitSalePrice',
      'Discount',
      'UnitPrice',
      'RevisedUnitPrice',
      'ParentId',
      'PartGroupID',
      'OrganizationID',
      'LocationID',
      'Quantity',
      'CurrencyID',
      'PriceDate'
    ]
  },
  loadPartInfo: function (settings) {
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).getIt({
      scope: this,
      ViewID: this.data.ParentId,
      RequestID: 'detail',
      jsondataObj: JSON.stringify(Ext.create('M1CRM.model.crmDataFilterModel', {
        FilterValues: [this.data.PartID, this.data.RevisionID, this.data.PartGroupID,
          this.data.OrganizationID, this.data.LocationID, this.data.Quantity,
          this.data.CurrencyID, this.data.PriceDate
        ]
      }).data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  }
  /*,
   loadPartTaxInfo : function(settings){
   M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
   Ext.Ajax.request({
   url: '/getPartTaxInfo',
   method: 'GET',
   headers: { 'Content-Type': 'application/json; charset=UTF-8' },
   params: {
   session: SessionObj.getSession(),
   paramObj : settings.jsondataObj != null ? settings.jsondataObj : JSON.stringify(this.data)
   },
   success: function (r) {
   if (r.responseText.indexOf("error") == 3){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
   }else{
   M1CRM.util.crmCustomerControls.hideAnimation();
   settings.onReturn.apply(settings.scope, [Ext.decode(r.responseText)[0]]);
   }
   },
   failure: function (r){
   M1CRM.util.crmCustomerControls.hideAnimation();
   M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
   },
   scope: this
   });
   } */
});
