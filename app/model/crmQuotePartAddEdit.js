Ext.define('M1CRM.model.crmQuotePartAddEdit', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'QuoteID', type: 'string'},
      {name: 'QuoteLineID', type: 'string'},
      {name: 'PartID', type: 'string'},
      {name: 'PartRevisionID', type: 'string', defaultValue: ''},
      {name: 'PartGroupID', type: 'string', defaultValue: ''},
      {name: 'PartDesc', type: 'string', defaultValue: ''},
      {name: 'OrganizationID', type: 'string', defaultValue: ''},
      {name: 'LocationID', type: 'string', defaultValue: ''},
      {name: 'Qty', type: 'float', defaultValue: 0.0},
      {name: 'UOM', type: 'string', defaultValue: ''},
      {name: 'Firm', type: 'auto'},
      {name: 'TaxCodeID', type: 'string'},
      {name: 'SecondTaxCodeID', type: 'string'},
      {name: 'NonTaxReasonID', type: 'string'},
      {name: 'Discount', type: 'float', defaultValue: 0.0},
      {name: 'UnitPrice', type: 'float', defaultValue: 0.0},
      {name: 'UnitDiscountBase', type: 'float', defaultValue: 0.0},
      {name: 'FullRevisedUnitPriceBase', type: 'float', defaultValue: 0.0},
      {name: 'EmployeeID', type: 'string'},
      {name: 'QtQuantityList', type: 'auto'}
    ],
    validations: [
      {type: 'presence', field: 'PartID', message: 'Select Part ID'}
    ]
  },
  getIt: function (settings) {
    var filtArr = [];
    if (settings.RequestID === 'prop')
      filtArr = [];
    else if (settings.RequestID === 'partprice')
      filtArr = [this.data.PartID, this.data.PartRevisionID, this.data.PartGroupID, this.data.OrganizationID, this.data.LocationID, this.data.Qty];
    else if (settings.RequestID === 'checkrevisions')
      filtArr = [this.data.PartID];
    else if (settings.RequestID === 'partinfo')
      filtArr = [this.data.PartID, this.data.PartRevisionID];
    else if (settings.RequestID === 'parttaxcodes')
      filtArr = [this.data.PartID];
    else if (settings.RequestID === 'taxrates')
      filtArr = [this.data.TaxCodeID];
    else if (settings.RequestID === 'custtaxcodes')
      filtArr = [this.data.OrganizationID, this.data.LocationID];


    Ext.create('M1CRM.model.crmServiceAccessModel', {}).getIt({
      scope: this,
      ViewID: 'CRMADDEDITQUOTATPART',
      RequestID: settings.RequestID,
      jsondataObj: JSON.stringify(Ext.create('M1CRM.model.crmDataFilterModel', {
        FilterValues: filtArr
      }).data),
      onReturn: function (result) {
        if (result != null) {
          var dataObject = Ext.decode(result).DataObject;
          settings.onReturn.apply(settings.scope, [dataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  },
  saveData: function (settings) {

    var arr = [];
    for (var i = 0; i < this.data.QtQuantityList.length; i++) {
      arr.push(this.data.QtQuantityList[i].data);
    }
    this.data.QtQuantityList = arr;


    Ext.create('M1CRM.model.crmServiceAccessModel', {}).saveIt({
      scope: this,
      ViewID: 'CRMADDEDITQUOTATPART',
      jsondataObj: JSON.stringify(this.data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  },
  calcRevisedUnitPrice: function () {
    var discount = this.data.Discount / 100;
    var curPrice = this.data.UnitPrice;
    if (discount != 0) {
      this.data.UnitDiscountBase = curPrice * discount;
      this.data.FullRevisedUnitPriceBase = curPrice - this.data.UnitDiscountBase;
    } else {
      this.data.UnitDiscountBase = 0;
      this.data.FullRevisedUnitPriceBase = curPrice;
    }
  }

});
