Ext.define('M1CRM.model.crmShipInfo', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'LocationID', type: 'string'},
      {name: 'LocationName', type: 'string'},
      {name: 'ContactID', type: 'string'},
      {name: 'ShipOrganizationID', type: 'string'},
      {name: 'ShipOrganizationName', type: 'string'}
    ]
  }
});
		  