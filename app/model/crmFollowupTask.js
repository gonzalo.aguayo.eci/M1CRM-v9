Ext.define('M1CRM.model.crmFollowupTask', {
  extend: 'Ext.data.Model',
  requires: ['M1CRM.store.crmValueList'],
  config: {
    fields: [
      'ID',
      'Description'
    ]
  },
  getStoreFromValueList: function (list) {
    var rows = list.split('\n');
    var store = Ext.create('M1CRM.store.crmFollowupTask', {});
    for (var i = 0; i < rows.length; i++) {
      store.add({ID: rows[i].split(',')[0].split('"'), Description: rows[i].split(',')[1].trim()});
    }
    return store;
  }

});

