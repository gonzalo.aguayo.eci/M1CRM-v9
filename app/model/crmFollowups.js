Ext.define('M1CRM.model.crmFollowups', {
  extend: 'Ext.data.Model',

  config: {
    fields: [
      'FollowupID',
      'OrganizationID',
      'OrganizationID',
      'OrganizationName',
      //'cmlName',
      'City',
      'CallID',
      'ShortDescription',
      'LongDescriptionText',
      'LocationID',
      'LocationID',
      'LocationName',
      'FollowupType',
      'ContactID',
      'ContactID',
      'ContactName',
      'Name',
      {name: 'DueDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      {name: 'StartDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      'cmfAssignedToEmployeeID',
      'cmlAddressLine1',
      'filterFields',
      'filterValues',
      'PageID',
      'MaxLimit',
      'totalRec',
      'FollowupStatus',
      'StatusRanking',
      'DueMonthYear',
      'MonIdx',
      'ListID',
      'CallID',
      'QuoteID',
      'Status'
    ]
  },
  getListLayout: function () {
    return new Ext.XTemplate(
      '<div class="contactmodel">' +
      '<tpl if="values.Name !== \'\'">' +
      '{Name}<br>' +
      '</tpl>' +
      '{ShortDescription}' +

      '</div>'
    );
  },
  getMyFollowupListLayout: function () {
    return new Ext.XTemplate(
      '<div class="followupmodel">' +
      '<table style="width:100%;">' +
      '<tpl if="values.City !== \'\'">' +
      '<tr>' +
      '<td width= "100%"><b><font size="4">{LocationName} - {City}</font></b></td>' +
      '</tr>' +
      '<tpl else>' +
      '<tr>' +
      '<td width= "100%"><b><font size="4">{LocationName}</font></b></td>' +
      '</tr>' +
      '</tpl>' +

      '<tpl if="values.cmcName !== \'\'">' +
      '<tr>' +
      '<td width= "100%">{ContactName}</td>' +
      '</tr>' +
      '</tpl>' +

      '<tr>' +
      '<td width= "100%">{ShortDescription}</td>' +
      '</tr>' +

      '<tr>' +
      '<td width= "100%">{DueDate:date("d-M-Y")}</td>' +
      '</tr>' +
      '</table>' +
      '</div>'
    );
  },
  getCustomerListLayout: function () {
    return new Ext.XTemplate(
      '<div class="contactmodel">' +
      '<b>{FollowupID}</b><br>' +
      '{ShortDescription}' +
      '</div>'
    );
  },
  getListSearchFields: function () {
    return ['cmfOrganizationID', 'cmlName', 'cmcName', 'cmfShortDescription', 'cmlCity', 'cmfDueDate', 'cmfStartDate', 'cmlAddressLine1'];
  }

});
