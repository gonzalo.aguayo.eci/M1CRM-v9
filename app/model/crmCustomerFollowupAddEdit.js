Ext.define("M1CRM.model.crmCustomerFollowupAddEdit", {
  extend: "Ext.data.Model",
  config: {
    fields: [
      'FollowupID',
      'CustomerID',
      'OrganizationID',
      'LocationID',
      'ContactID',
      'Task',
      'FollowupType',
      {name: 'Duedate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      {name: 'StartDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      'ShortDescription',
      'AssignedToEmployeeID',
      'LongDescriptionText',
      'MeetingLocation',
      'AssignedToEmployeeID',
      'Priority',
      'Status',
      {name: 'CallID', type: 'string'},
      {name: 'QuoteID', type: 'string'},
      {name: 'LeadID', type: 'string'},
      {name: 'CompletedDate', type: 'date', dateFormat: 'c', defaultValue: new Date()}
    ],
    validations: [
      //{type: 'presence',  field: 'OrganizationID', message : 'Select Organization' },
      //{type: 'presence',  field: 'Duedate', message : 'Select Duedate' },
      //{type: 'presence',  field: 'ShortDescription', message : 'Enter Description'  }
    ]
  },
  validate: function () {
    var error = this.callParent();

    if (this.data.OrganizationID == null || this.data.OrganizationID == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'OrganizationID',
        message: 'Select Org' //  + SessionObj.getOrgLabel()
      }));
    }

    if (this.data.Duedate == null || this.data.Duedate == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'Duedate',
        message: 'Select Duedate'
      }));
    }

    if (this.data.ShortDescription == null || this.data.ShortDescription == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'ShortDescription',
        message: 'Enter Desc'
      }));
    }


    if (this.data.Priority == 0) {
      error.add(Ext.create('Ext.data.Error', {
        field: 'Priority',
        message: 'Select Priority.'
      }));
    }
    if (parseInt(this.data.Task) == 1 && new Date(this.data.StartDate) > new Date(this.data.Duedate) == true) {
      error.add(Ext.create('Ext.data.Error', {
        field: 'Duedate',
        message: 'Due date must be greater than the start date.'
      }));
    }
    return error;
  },
  getData: function (settings) {
    var filtArr = settings.RequestID == 'prop' ? [] : [this.data.FollowupID];
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).getIt({
      scope: this,
      ViewID: 'CRMFOLLOWUPADDEDIT',
      RequestID: settings.RequestID,
      jsondataObj: JSON.stringify(Ext.create('M1CRM.model.crmDataFilterModel', {
        FilterValues: filtArr
      }).data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  },
  saveData: function (settings) {
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).saveIt({
      scope: this,
      ViewID: 'CRMFOLLOWUPADDEDIT',
      jsondataObj: JSON.stringify(this.data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  }
});

