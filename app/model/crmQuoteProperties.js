Ext.define('M1CRM.model.crmQuoteProperties', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'FollowUpDays',
      'ExpirationDays',
      'MaterialMarkup',
      'SubcontractMarkup',
      'LaborMarkup',
      'OverheadMarkup',
      'QuotingMarkup',
      'QuotingMethod',
      'QuoteMarkupType',
      'MobileQuoteHeaderMessage',
      'MobileQuoteHeaderMessageRTF',
      'MobileQuoteFooterMessage',
      'MobileQuoteFooterMessageRTF'
    ]
  },

  loadQuoteProperties: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getQuoteProperties',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession()
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          QuotationPropeties = Ext.create('M1CRM.store.crmQuoteProperties', {});
          QuotationPropeties.setData(Ext.decode(r.responseText));
          settings.onReturn.apply(settings.scope, [QuotationPropeties]);
        }
      },
      failure: function () {
        M1CRM.util.crmCustomerControls.hideAnimation();
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  }


});
