Ext.define('M1CRM.model.crmAddEditLead', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'LeadID', type: 'string'},
      {name: 'OrganizationID', type: 'string'},
      //{name :'LeadLocationInfo', type : 'auto'},
      {name: 'LeadLocationInfo', model: 'M1CRM.model.crmLocationInfo'},
      {name: 'ShortDescription', type: 'string'},
      {name: 'LongDescription', type: 'string'},
      {name: 'MilestoneID', type: 'string'},
      {name: 'MilestoneDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      {name: 'LeadTotal', type: 'float', defaultValue: 0.0},
      {name: 'ResponseMethodID', type: 'string'},
      {name: 'MarketingProgramID', type: 'string'},
      {name: 'ReferedBy', type: 'string'},
      {name: 'Quoter', type: 'string'},
      {name: 'LeadDate', type: 'date', dateFormat: 'c', defaultValue: new Date()},
      {name: 'ExpectedCloseDate', type: 'date', dateFormat: 'c'},
      {name: 'ExpireDate', type: 'date', dateFormat: 'c'},
      {name: 'LeadStatus', type: 'string'},
      {name: 'ClosedReasonID', type: 'string'},
      {name: 'ClosedBy', type: 'string'},
      {name: 'ClosedDate', type: 'date', dateFormat: 'c'},
      {name: 'QuoteLocationInfo', model: 'M1CRM.model.crmLocationInfo'},
      {name: 'ShipLocationInfo', model: 'M1CRM.model.crmShipInfo'},
      {name: 'EmployeeID', type: 'string', defaultValue: ''}
    ],
    validations: [
      //{ type: 'presence',  field: 'OrganizationID', message : 'Select Organization' },
      //{ type: 'presence',  field: 'ShortDescription', message : 'Enter Description' },
      //{ type: 'presence',  field: 'Quoter', message : 'Select Quoter' },
      //{ type: 'presence',  field: 'MilestoneID', message : 'Select Milestone' }
    ]
  },

  validate: function () {
    var error = this.callParent();

    if (this.data.OrganizationID == null || this.data.OrganizationID == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'OrganizationID',
        message: 'Select Customer' //+ SessionObj.getOrgLabel()
      }));
    }
    if (this.data.ShortDescription == true || this.data.ShortDescription == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'ShortDescription',
        message: 'Enter Desc'
      }));
    }

    if (this.data.Quoter == null || this.data.Quoter == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'Quoter',
        message: 'Select Quoter'
      }));
    }

    if (this.data.MilestoneID == null || this.data.MilestoneID == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'MilestoneID',
        message: 'Select Milestone'
      }));
    }


    if (this.data.ShipLocationInfo == null || (this.data.ShipLocationInfo.ShipOrganizationID == null || this.data.ShipLocationInfo.ShipOrganizationID == '')) {
      error.add(Ext.create('Ext.data.Error', {
        field: 'ShipLocationInfo.ShipOrganizationID',
        message: 'Select Ship Org ' //+ SessionObj.getOrgLabel()
      }));

    }
    if (this.data.LeadStatus == 'C' && this.data.ClosedBy == '') {
      error.add(Ext.create('Ext.data.Error', {
        field: 'ClosedBy',
        message: 'Select Closed By'
      }));
    }
    return error;
  },

  getData: function (settings) {
    var filtArr = [];
    if (settings.RequestID == 'prop') {
      filtArr = [];
    } else if (settings.RequestID == 'custdefaults' || settings.RequestID == 'custdata') {
      filtArr = [this.data.OrganizationID];
    } else if (settings.RequestID == 'data' || settings.RequestID == 'checklines') {
      filtArr = [this.data.LeadID];
    }
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).getIt({
      scope: this,
      ViewID: 'CRMADDEDITLEAD',
      RequestID: settings.RequestID,
      jsondataObj: JSON.stringify(Ext.create('M1CRM.model.crmDataFilterModel', {
        FilterValues: filtArr
      }).data),
      onReturn: function (result) {
        if (result != null) {
          if (Ext.decode(result).DataObject != null) {
            settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
          } else {

            Ext.Msg.alert('Error', Ext.decode(result).Message, function (btn) {
              if (btn == 'ok') {
                settings.onReturn.apply(settings.scope, [null]);
              }
            }, this);
          }
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  },
  saveData: function (settings) {
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).saveIt({
      scope: this,
      ViewID: 'CRMADDEDITLEAD',
      RequestID: settings.RequestID,
      jsondataObj: JSON.stringify(this.data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  }


});		