Ext.define('M1CRM.model.crmCurrency', {
  extend: "Ext.data.Model",
  config: {
    fields: [
      {name: 'CurrencyRateID', type: 'string'},
      {name: 'ID', type: 'string'},
      {name: 'Description', type: 'string'},
      {name: 'DefaultCurrency', type: 'string'}
    ]
  },
  getCurrencyCodes: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getCurrencyCodes',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession()
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          if (this.data.DefaultCurrency != '') {
            settings.onReturn.apply(settings.scope, [Ext.create('M1CRM.store.crmCurrency', {}).setData(Ext.decode(r.responseText))]);
          } else {
            var tempstore = Ext.create('M1CRM.store.crmCurrency', {});
            tempstore.setData(Ext.decode(r.responseText));
            var store = Ext.create('M1CRM.store.crmCurrency', {});
            store.add({CurrencyRateID: '', Description: '<None>'});
            if (tempstore.data.items.length > 0) {
              for (var i = 0; i < tempstore.data.items.length; i++) {
                store.add({
                  CurrencyRateID: tempstore.data.items[i].data.CurrencyRateID,
                  Description: tempstore.data.items[i].data.Description
                });
              }
            }
            settings.onReturn.apply(settings.scope, [store]);
          }
        }

      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation(r);
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  }
});
