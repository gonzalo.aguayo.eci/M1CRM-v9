Ext.define('M1CRM.model.crmListItem', {
  extend: "Ext.data.Model",
  config: {
    fields: [
      'ID',
      'Description'
    ]
  },
  getShippingMethods: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getShippingMethods',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession()
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          var result = Ext.create('M1CRM.store.crmListItem', {}).setData(Ext.decode(r.responseText));
          var tStore = new Ext.create('M1CRM.store.crmListItem', {});
          tStore.add({ID: '', Description: '<None>'});
          for (var i = 0; i < result.data.items.length; i++) {
            tStore.add({ID: result.data.items[i].raw.ID, Description: result.data.items[i].raw.Description});
          }
          result = null;
          settings.onReturn.apply(settings.scope, [tStore]);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation(r);
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  }

});
