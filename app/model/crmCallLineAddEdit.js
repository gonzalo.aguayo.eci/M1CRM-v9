Ext.define("M1CRM.model.crmCallLineAddEdit", {
  extend: "Ext.data.Model",
  config: {
    fields: [
      'CallID',
      'Organization',
      'OrganizationID',
      'CallLineID',
      'Description',
      'LongDescription',
      'ContactMethodID',
      'InternalOnly',
      'AddedByEmployeeID',
      {name: 'AddedDate', type: 'date', defaultValue: new Date()},
      {name: 'CreatedFromMobile', type: 'int', defaultValue: 1}

    ],
    validations: [
      {type: 'presence', field: 'Description', message: 'Description'}
    ]
  },
  getIt: function (settings) {
    var filtArr = settings.RequestID == 'prop' ? [SessionObj.getEmpId()] : [this.data.CallID, this.data.CallLineID];
    Ext.create('M1CRM.model.crmServiceAccessModel', {}).getIt({
      scope: this,
      ViewID: 'CRMCALLLINESADDEDIT',
      RequestID: settings.RequestID,
      jsondataObj: JSON.stringify(Ext.create('M1CRM.model.crmDataFilterModel', {
        FilterValues: filtArr
      }).data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  },
  saveData: function (settings) {

    Ext.create('M1CRM.model.crmServiceAccessModel', {}).saveIt({
      scope: this,
      ViewID: 'CRMCALLLINESADDEDIT',
      jsondataObj: JSON.stringify(this.data),
      onReturn: function (result) {
        if (result != null) {
          settings.onReturn.apply(settings.scope, [Ext.decode(result).DataObject]);
        } else {
          settings.onReturn.apply(settings.scope, [null]);
        }
      }
    });
  }

});

