Ext.define('M1CRM.model.crmSalesOrderLines', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'PartID', type: 'string'},
      {name: 'PartRevisionID', type: 'string'}
    ]
  },
  loadTotalSalesOrderQty: function (settings) {
    M1CRM.util.crmCustomerControls.displayAnimation('Loading..');
    Ext.Ajax.request({
      url: '/getTotalSalesOrderQty',
      method: 'GET',
      headers: {'Content-Type': 'application/json; charset=UTF-8'},
      params: {
        session: SessionObj.getSession(),
        paramObj: settings.jsondataObj != null ? settings.jsondataObj : JSON.stringify(this.data)
      },
      success: function (r) {
        if (r.responseText.indexOf("error") == 3) {
          M1CRM.util.crmCustomerControls.hideAnimation();
          M1CRM.util.crmCustomerControls.displayDBResponceError(r.responseText, settings);
        } else {
          M1CRM.util.crmCustomerControls.hideAnimation();
          settings.onReturn.apply(settings.scope, [Ext.decode(r.responseText)[0]]);
        }
      },
      failure: function (r) {
        M1CRM.util.crmCustomerControls.hideAnimation();
        M1CRM.util.crmCustomerControls.displayDBResponceError(r.status, settings);
      },
      scope: this
    });
  }
});
