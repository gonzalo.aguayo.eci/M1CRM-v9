Ext.define('M1CRM.model.crmLeadLines', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      {name: 'LeadID', type: 'string'},
      {name: 'LeadLineID', type: 'int', defaultValue: 0},
      {name: 'PartID', type: 'string'},
      {name: 'PartRevisionID', type: 'string'},
      {name: 'Description', type: 'string'},
      {name: 'RevenueForecast', type: 'float', defaultValue: 0.0},
      {name: 'PartGroupID', type: 'string'}
    ]
  },

  getListSearchFields: function () {
    return ['LeadID', 'PartID', 'PartRevisionID', 'Description'];
  },
  getLeadLinesLayout: function () {
    return new Ext.XTemplate(
      '<div class="leadlinemodel">' +
      ' <table style="width:100%;">' +

      '<tpl if="values.PartRevisionID !== \'\'">' +
      '<tr>' +
      '<td width= "100%"><font size="4">{PartID}, {PartRevisionID} - {Description}</font></td>' +
      '</tr>' +
      '<tpl else>' +
      '<tr>' +
      '<td width= "100%"><font size="4">{PartID} - {Description}</font></td>' +
      '</tr>' +
      '</tpl>' +

      '<tr>' +
      '<td width= "100%">{RevenueForecast:this.formatNumberAsCurrency}</td>' +
      '</tr>' +

      '</table>' +
      '</div>',
      {
        formatNumberAsCurrency: function (value) {
          var negative = '',
            value = value - 0;
          if (value < 0) {
            value = -value;
            negative = '-';
          }
          x = value.toString().split('.');
          x1 = x[0];
          x2 = x.length > 1 ? '.' + x[1] : '.00';
          value = parseFloat(x1 + x2).toFixed(2);
          value = Ext.String.format("{0}{1}", negative, value);

          x = value.toString().split('.');
          x1 = x[0];
          x2 = x.length > 1 ? '.' + x[1] : '';
          var rgx = /(\d+)(\d{3})/;

          while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + ',' + '$2');
          }
          value = x1 + x2;
          return Ext.String.format("{0} {1}", SessionObj.getCurrencySymbol(), value);

        }
      }
    );
  }

});
