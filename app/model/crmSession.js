Ext.define('M1CRM.model.crmSession', {
  statics: {
    session: '',
    dataset: '',
    userid: '',
    password: '',
    username: '',
    model: '',
    loggedIn: '',
    EmpId: '',
    EmpName: '',
    EmpIsAQuoter: '',
    DBRegion: '',
    OrgLabel: '',
    PostCodeLabel: '',
    DateFormat: '',
    MaxRecordsPerPage: 20,
    CRMBasic: '',
    CRMAdvace: '',
    CallManagement: '',
    QuotationManagement: '',
    LeadManagement: '',
    SimpleDateFormat: '',
    DisplayDateTime: '',
    DateTimeFormat: '',
    DateTimePicker: '',
    DatePickerSlotOrder: '',
    DateTimePickerSlotOrder: '',
    OrgName: '',
    CurrencySymbol: '',
    EnableMultiCurrency: false,
    ShowSecondTax: true,
    /**
     * Table Security for Views
     */
    FollowupAddEditTSecurity: '',
    CallAddEditTSecurity: '',
    ContactAddEditTSecurity: '',
    CustomerAddEditTSecurity: '',
    QuoteAddEditTSecurity: '',
    QuoteQtyAddEditTSecurity: '',
    LeadAddEditTSecurity: '',
    LeadLineAddEditTSecurity: '',
    ExpirationSessionDate: '',
    Token: '',
    /**
     * Access Level
     */
    AccessLevel: {}
  },

  constructor: function () {
    var utility = Ext.create('M1CRM.model.crmUtility', {});
    var cookieSession = utility.getCookieToObject() || {};

    _.extend(this.statics, cookieSession);
  },

  setCookieVariable: function (name, value) {
    if (value) {
      document.cookie = name + '=' + value + '; expires=' + this.statics.ExpirationSessionDate + ';';
    }
  },

  getSession: function () {
    return this.statics.session;
  },
  setSession: function (value) {
    this.statics.session = value;
    this.setCookieVariable('session', value);
  },

  getDataset: function () {
    return this.statics.dataset;
  },
  setDataset: function (value) {
    this.statics.dataset = value;
    this.setCookieVariable('dataset', value);
  },

  getUserid: function () {
    return this.statics.userid;
  },
  setUserid: function (value) {
    this.statics.userid = value;
    this.setCookieVariable('userid', value);
  },

  getUsername: function () {
    return this.statics.username;
  },
  setUsername: function () {
    this.statics.username = value;
    this.setCookieVariable('username', value);
  },

  getModel: function () {
    return this.statics.model;
  },
  setModel: function (value) {
    this.statics.model = value;
    this.setCookieVariable('model', value);
  },

  getLoggedIn: function () {
    return this.statics.loggedIn;
  },
  setLoggedIn: function (value) {
    this.statics.loggedIn = value;
    this.setCookieVariable('loggedIn', value);
  },

  getEmpId: function () {
    return this.statics.EmpId;
  },
  setEmpId: function (value) {
    this.statics.EmpId = value;
    this.setCookieVariable('EmpId', value);
  },

  getEmpName: function () {
    return this.statics.EmpName;
  },
  setEmpName: function (value) {
    this.statics.EmpName = value;
    this.setCookieVariable('EmpName', value);
  },

  getEmpIsAQuoter: function () {
    return this.statics.EmpIsAQuoter;
  },
  setEmpIsAQuoter: function (value) {
    this.statics.EmpIsAQuoter = value;
    this.setCookieVariable('EmpIsAQuoter', value);
  },

  getDBRegion: function () {
    return this.statics.DBRegion;
  },
  setDBRegion: function (value) {
    this.statics.DBRegion = value;
    this.setCookieVariable('DBRegion', value);
  },

  getOrgLabel: function () {
    return this.statics.OrgLabel;
  },
  setOrgLabel: function (value) {
    this.statics.OrgLabel = value;
    this.setCookieVariable('OrgLabel', value);
  },

  getPostCodeLabel: function () {
    return this.statics.PostCodeLabel;
  },
  setPostCodeLabel: function (value) {
    this.statics.PostCodeLabel = value;
    this.setCookieVariable('PostCodeLabel', value);
  },

  getDateFormat: function () {
    return this.statics.DateFormat;
  },
  setDateFormat: function (value) {
    this.statics.DateFormat = value;
    this.setCookieVariable('DateFormat', value);
  },

  getMaxRecordsPerPage: function () {
    return this.statics.MaxRecordsPerPage;
  },
  setMaxRecordsPerPage: function (value) {
    this.statics.MaxRecordsPerPage = value;
    this.setCookieVariable('MaxRecordsPerPage', value);
  },

  getCRMBasic: function () {
    return this.statics.CRMBasic;
  },
  setCRMBasic: function (value) {
    this.statics.CRMBasic = value;
    this.setCookieVariable('CRMBasic', value);
  },

  getCRMAdvace: function () {
    return this.statics.CRMAdvace;
  },
  setCRMAdvace: function (value) {
    this.statics.CRMAdvace = value;
    this.setCookieVariable('CRMAdvace', value);
  },

  getCallManagement: function () {
    return this.statics.CallManagement;
  },
  setCallManagement: function (value) {
    this.statics.CallManagement = value;
    this.setCookieVariable('CallManagement', value);
  },

  getQuotationManagement: function () {
    return this.statics.QuotationManagement;
  },
  setQuotationManagement: function (value) {
    this.statics.QuotationManagement = value;
    this.setCookieVariable('QuotationManagement', value);
  },

  getLeadManagement: function () {
    return this.statics.LeadManagement;
  },

  setLeadManagement: function (value) {
    this.statics.LeadManagement = value;
    this.setCookieVariable('LeadManagement', value);
  },

  getSimpleDateFormat: function () {
    return this.statics.SimpleDateFormat;
  },
  setSimpleDateFormat: function (value) {
    this.statics.SimpleDateFormat = value;
    this.setCookieVariable('SimpleDateFormat', value);
  },

  getDisplayDateTime: function () {
    return this.statics.DisplayDateTime;
  },
  setDisplayDateTime: function (value) {
    this.statics.DisplayDateTime = value;
    this.setCookieVariable('DisplayDateTime', value);
  },

  getDateTimeFormat: function () {
    return this.statics.DateTimeFormat;
  },
  setDateTimeFormat: function (value) {
    this.statics.DateTimeFormat = value;
    this.setCookieVariable('DateTimeFormat', value);
  },

  getDateTimePicker: function () {
    return this.statics.DateTimePicker;
  },
  setDateTimePicker: function (value) {
    this.statics.DateTimePicker = value;
    this.setCookieVariable('DateTimePicker', value);
  },

  getDatePickerSlotOrder: function () {
    return this.statics.DatePickerSlotOrder;
  },
  setDatePickerSlotOrder: function (value) {
    this.statics.DatePickerSlotOrder = value;
    this.setCookieVariable('DatePickerSlotOrder', value);
  },

  getDateTimePickerSlotOrder: function () {
    return this.statics.DateTimePickerSlotOrder;
  },
  setDateTimePickerSlotOrder: function (value) {
    this.statics.DateTimePickerSlotOrder = value;
    this.setCookieVariable('DateTimePickerSlotOrder', value);
  },

  getOrgName: function () {
    return this.statics.OrgName;
  },
  setOrgName: function (value) {
    this.statics.OrgName = value;
    this.setCookieVariable('OrgName', value);
  },

  getCurrencySymbol: function () {
    return this.statics.CurrencySymbol;
  },
  setCurrencySymbol: function (value) {
    this.statics.CurrencySymbol = value;
    this.setCookieVariable('CurrencySymbol', value);
  },

  getEnableMultiCurrency: function () {
    return this.statics.EnableMultiCurrency;
  },
  setEnableMultiCurrency: function (value) {
    this.statics.EnableMultiCurrency = value;
    this.setCookieVariable('EnableMultiCurrency', value);
  },

  getShowSecondTax: function () {
    return this.statics.ShowSecondTax;
  },
  setShowSecondTax: function (value) {
    this.statics.ShowSecondTax = value;
    this.setCookieVariable('ShowSecondTax', value);
  },

  getToken: function () {
    return this.statics.Token;
  },
  setToken: function (value) {
    this.statics.Token = value;
    this.setCookieVariable('Token', value);
  },

  getFollowupAddEditTSecurity: function () {
    return this.statics.FollowupAddEditTSecurity;
  },
  setFollowupAddEditTSecurity: function (value) {
    this.statics.FollowupAddEditTSecurity = this.getEncrypted(value);
    this.setCookieVariable('FollowupAddEditTSecurity', this.getEncrypted(value));
  },

  getCallAddEditTSecurity: function () {
    return this.statics.CallAddEditTSecurity;
  },
  setCallAddEditTSecurity: function (value) {
    this.statics.CallAddEditTSecurity = this.getEncrypted(value);
    this.setCookieVariable('CallAddEditTSecurity', this.getEncrypted(value));
  },

  getContactAddEditTSecurity: function () {
    return this.statics.ContactAddEditTSecurity;
  },
  setContactAddEditTSecurity: function (value) {
    this.statics.ContactAddEditTSecurity = this.getEncrypted(value);
    this.setCookieVariable('ContactAddEditTSecurity', this.getEncrypted(value));
  },

  getCustomerAddEditTSecurity: function () {
    return this.statics.CustomerAddEditTSecurity;
  },
  setCustomerAddEditTSecurity: function (value) {
    this.statics.CustomerAddEditTSecurity = this.getEncrypted(value);
    this.setCookieVariable('CustomerAddEditTSecurity', this.getEncrypted(value));
  },

  getQuoteAddEditTSecurity: function () {
    return this.statics.QuoteAddEditTSecurity;
  },
  setQuoteAddEditTSecurity: function (value) {
    this.statics.QuoteAddEditTSecurity = this.getEncrypted(value);
    this.setCookieVariable('QuoteAddEditTSecurity', this.getEncrypted(value));
  },

  getQuoteQtyAddEditTSecurity: function () {
    return this.statics.QuoteQtyAddEditTSecurity;
  },
  setQuoteQtyAddEditTSecurity: function (value) {
    this.statics.QuoteQtyAddEditTSecurity = this.getEncrypted(value);
    this.setCookieVariable('QuoteQtyAddEditTSecurity', this.getEncrypted(value));
  },

  getLeadAddEditTSecurity: function () {
    return this.statics.LeadAddEditTSecurity;
  },
  setLeadAddEditTSecurity: function (value) {
    this.statics.LeadAddEditTSecurity = this.getEncrypted(value);
    this.setCookieVariable('LeadAddEditTSecurity', this.getEncrypted(value));
  },

  getLeadLineAddEditTSecurity: function () {
    return this.statics.LeadLineAddEditTSecurity;
  },
  setLeadLineAddEditTSecurity: function (value) {
    this.statics.LeadLineAddEditTSecurity = this.getEncrypted(value);
    this.setCookieVariable('LeadLineAddEditTSecurity', this.getEncrypted(value));
  },

  getEncrypted: function (value) {
    return value ? CryptoJS.AES.encrypt(value, this.getToken()) : '';
  },

  getExpirationSessionDate: function () {
    return this.statics.ExpirationSessionDate + ';';
  },
  setExpirationSessionDate: function (value) {
    this.statics.ExpirationSessionDate = value;
    document.cookie = 'ExpirationSessionDate=' + value;
  },

  getAccessLevel: function () {
    if (_.isString(this.statis.AccessLevel)) {
      return JSON.parse(this.statis.AccessLevel);
   }

   return this.statis.AccessLevel;
  },
  setAccessLevel: function () {
    var that = this;

    return function (value) {
      that.statics.AccessLevel = JSON.stringify(value);
      that.setCookieVariable('AccessLevel', JSON.stringify(value));
    }
  },
});
