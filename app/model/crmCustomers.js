Ext.define('M1CRM.model.crmCustomers', {
  extend: 'Ext.data.Model',
  config: {
    fields: [
      'OrganizationID',
      'OrganizationName',
      // 'cmoOrganizationID',
      //  {name : 'cmoName',  type :'string'},
      'cmoCity',
      'cmoPostCode',
      'cmoCountryCode',
      'filterFields',
      'filterValues',
      'listId',
      'pageID',
      'maxLimit',
      'totalRec'
    ]
  },
  getListLayout: function () {
    return '<div class="customermodel" font-size:smaller;>{OrganizationName}</div>';
  },
  getListSearchFields: function () {
    return ['cmoOrganizationID', 'cmoName'];
  }


});
