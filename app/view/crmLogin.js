Ext.define('M1CRM.view.crmLogin', {
  extend: 'Ext.form.Panel',
  xtype: 'loginform',

  id: 'loginform',
  requires: [
    'Ext.TitleBar',
    'Ext.ux.M1.M1Logo',
    'Ext.ux.M1.M1TextField',
    'Ext.ux.M1.M1FormButton'
  ],
  config: {
    items: [
      {
        xtype: "toolbar",
        name: "toolbar",
        docked: 'top',
        title: 'Login',
        cls: "m1-crmhomescreen-title",
        layout: {
          pack: 'center'
        },
        defaults: {
          iconMask: true,
          ui: 'plain'
        }
      },


      {
        xtype: 'fieldset',
        items: [
          {
            xtype: 'm1textfield',
            id: 'dataset',
            label: 'Dataset',
            readOnly: false,
            required: true,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'loginid',
            label: 'User ID',
            readOnly: false,
            required: true,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'password',
            label: 'Password',
            inputType: 'password',
            readOnly: false,
            scope: this
          }
        ]
      },
      {
        xtype: 'm1logo'
      },
      {
        xtype: 'container',
        docked: 'bottom',
        layout: {
          align: 'center',
          pack: 'center',
          type: 'hbox'
        },
        items: [
          {
            xtype: 'formbutton',
            id: 'btnlogin',
            iconCls: 'key',
            scope: this
          }
        ]
      }


    ]

  }/*,
   initilize : function () {
   this.callParent(arguments);
   } */


});
