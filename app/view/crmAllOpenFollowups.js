Ext.define('M1CRM.view.crmAllOpenFollowups', {
  extend: 'Ext.dataview.List',
  xtype: 'allopenfollowups',
  id: 'allopenfollowuplist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _optionsMenu: null,
    _parentformid: null,
    _detailformid: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm',
    title: 'All Follow-ups',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'allopenfollowuplist_toolbarid',
        title: 'All Open Follow-ups',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'allopenfollowuplist_searchtoolbar',
        searchfieldid: 'allopenfollowup_search',
        cls: 'm1-toolbar-search-followup',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'allopenfollowuplist_tabbar',
        titleid: 'labAllOpenFollowupsSummary',
        scope: this
      }
    ],
    //store : Ext.getStore('crmFollowups'),
    //itemTpl :  Ext.create('M1CRM.model.crmFollowups', {}).getMyFollowupListLayout(),
    grouped: false,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

