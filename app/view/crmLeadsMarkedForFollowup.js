Ext.define('M1CRM.view.crmLeadsMarkedForFollowup', {
  extend: 'Ext.dataview.List',
  xtype: 'followupleads',
  id: 'followupleadslist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _optionsMenu: null,
    _parentformid: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm-leads',
    title: 'Leads Followup',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'followupleadslist_toolbarid',
        title: 'Leads Followup',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'followupleads_searchtoolbar',
        searchfieldid: 'followupleads_search',
        cls: 'm1-toolbar-search-leads',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'followupleadslist_tabbar',
        titleid: 'labLeadsFollowupSummary',
        scope: this
      }

    ],
    grouped: true,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

