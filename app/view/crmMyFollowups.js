Ext.define('M1CRM.view.crmMyFollowups', {
  extend: 'Ext.dataview.List',
  xtype: 'myfollowups',
  id: 'myfollowuplist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _optionsMenu: null,
    _parentformid: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,

    cls: 'm1-list-crm',
    title: 'My Follow-ups',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'myfollowuplist_toolbarid',
        title: 'My Open Follow-ups',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'followuplist_searchtoolbar',
        searchfieldid: 'myfollowup_search',
        cls: 'm1-toolbar-search-followup',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'myfollowuplist_tabbar',
        titleid: 'labMyFollowupSummary',
        scope: this
      }


    ],
    // store : Ext.getStore('crmFollowups'),
    //  itemTpl :  Ext.create('M1CRM.model.crmFollowups', {}).getMyFollowupListLayout(),  
    grouped: false,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: false


  }

});

