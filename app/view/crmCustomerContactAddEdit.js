Ext.define('M1CRM.view.crmCustomerContactAddEdit', {
  extend: 'Ext.form.Panel',
  xtype: 'customercontactddeditform',
  id: 'customercontactaddedit',
  requires: [
    'Ext.tab.Panel',
    'Ext.field.Hidden',
    'Ext.ux.M1.M1ToolbarEntryForm',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1TextField',
    'Ext.ux.M1.M1AdvTextField',
    'Ext.ux.M1.M1PhoneNumberField',
    'Ext.ux.M1.M1EmailField',
    // 'Ext.ux.M1.M1UrlField',
    'Ext.ux.M1.Checkbox',
    'Ext.ux.M1.M1SelectField',
    'Ext.ux.M1.M1AdvTextArea',
    'Ext.ux.M1.M1Logo',
    'Ext.ux.M1.M1SubMenuButton',
    'Ext.ux.M1.M1SubMenuDummyButton',
    'Ext.ux.M1.M1InvisibleLookupButton'

  ],
  config: {
    _parentformid: null,
    _optionsMenu: null,
    _mode: 'view',
    items: [
      {
        xtype: 'hiddenfield',
        id: 'customercontactaddedit_locationid',
        name: 'customercontactaddedit_locationid'
      },
      {
        xtype: 'hiddenfield',
        id: 'customercontactaddedit_custid',
        name: 'customercontactaddedit_custid'

      },

      {
        xtype: 'm1toolbarentryform',
        id: 'customercontactaddedit_toolbarid',
        title: 'Contact Details',
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'customercontactaddedittitleid'
      },


      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'm1advtextfield',
            textfieldid: 'customercontactaddedit_customer',
            label: 'Org',
            readOnly: true,
            haslookupbtn: true,
            hasinfobtn: true,
            required: true,
            infoarray: ['customer', 'customercontactaddedit', 'customercontactaddedit_custid', 'customercontactaddedit_customer', null, null, null, null, null, null, null],
            scope: this
          },
          {
            xtype: 'm1advtextfield',
            textfieldid: 'customercontactaddedit_location',
            label: 'Location',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            infoarray: ['location', 'customercontactaddedit', 'customercontactaddedit_custid', 'customercontactaddedit_customer', 'customercontactaddedit_locationid', 'customercontactaddedit_location', null, null, null, null, null],
            scope: this
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'customercontactaddedit_contactid',
                label: 'Contact ID',
                readOnly: false,
                touppercase: true,
                required: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'customercontactaddedit_contactname',
                label: 'Name',
                readOnly: false,
                required: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'customercontactaddedit_titlesel',
                label: 'Title',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'customercontactaddedit_title',
                label: 'Title',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }

            ]
          }
        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        id: 'addeditcon_contactinfofieldset',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1phonenumberfield',
                id: 'customercontactaddedit_phone1',
                label: 'Phone',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1phonenumberfield',
                id: 'customercontactaddedit_phone2',
                label: 'Alt Phone',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }

            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1phonenumberfield',
                id: 'customercontactaddedit_mobile',
                label: 'Mobile',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1phonenumberfield',
                id: 'customercontactaddedit_fax',
                label: 'Fax',
                dialontap: false,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1emailfield',
                id: 'customercontactaddedit_email',
                label: 'Email',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }

            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1checkboxfield',
                id: 'customercontactaddedit_nomailing',
                label: 'No Mailings?',
                checked: false,
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },
      {
        xtype: 'm1advtextarea',
        textareaid: 'customercontactaddedit_notes',
        textarealabel: 'Long Desc',
        detaillabel: 'Long Desc',
        textareaflex: 4,
        scope: this
      },
      {
        xtype: 'm1logo'
      },
      {
        xtype: 'container',
        margin: 1,
        docked: 'bottom',
        layout: {
          type: 'hbox'
        },
        items: [
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-calls',
            id: 'customercontactaddedit_viewcalls',
            text: 'Calls',
            iconCls: 'calls',
            scope: this
          },
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-followup',
            id: 'customercontactaddedit_viewfollowups',
            text: 'Followups',
            iconCls: 'follow_up',
            scope: this
          },
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-quotes',
            //cls : 'm1-submenuactionbutton-crm-contact',
            id: 'customercontactaddedit_viewquotations',
            text: 'Quotes',
            iconCls: 'quotation',
            scope: this
          }
        ]

      }

    ]
  }
});
