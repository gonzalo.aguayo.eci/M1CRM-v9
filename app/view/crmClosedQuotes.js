Ext.define('M1CRM.view.crmClosedQuotes', {
  extend: 'Ext.dataview.List',
  xtype: 'closedquotes',
  id: 'closedquoteslist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _optionsMenu: null,
    _parentformid: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm',
    title: 'Closed Quotes',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'closedquoteslist_toolbarid',
        title: 'Closed Quotes',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'closedquotes_searchtoolbar',
        searchfieldid: 'closedquotes_search',
        cls: 'm1-toolbar-search-quotes',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'closedquoteslist_tabbar',
        titleid: 'labClosedQuotesSummary',
        scope: this
      }
    ],
    //store : Ext.getStore('crmQuotations'),
    //itemTpl :  Ext.create('M1CRM.model.crmQuotations', {}).getAllOpenQuoteLayout(),
    grouped: false,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

