Ext.define('M1CRM.view.crmPartRevisions', {
  extend: 'Ext.dataview.List',
  xtype: 'partrevision',
  id: 'partrevisionlist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm',
    title: 'Parts',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'partrevisionlist_toolbarid',
        title: 'Part Revisions',
        includeAdd: false,
        includeFilter: true,
        includeOptions: false,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'partrevisionlist_searchtoolbar',
        searchfieldid: 'partrevision_search',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'partrevisionlist_tabbar',
        titleid: 'labPartRevisionSummary'
      }
    ],
    indexBar: false,
    pinHeaders: false,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

