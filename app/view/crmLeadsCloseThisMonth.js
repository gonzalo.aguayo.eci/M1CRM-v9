Ext.define('M1CRM.view.crmLeadsCloseThisMonth', {
  extend: 'Ext.dataview.List',
  xtype: 'leadsclosethismonth',
  id: 'leadsclosethismonthlist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _optionsMenu: null,
    _parentformid: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm-leads',
    title: 'Leads Close This Month',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'leadsclosethismonthlist_toolbarid',
        title: 'Leads Close This Month',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'leadsclosethismonth_searchtoolbar',
        searchfieldid: 'leadsclosethismonth_search',
        cls: 'm1-toolbar-search-leads',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'leadsclosethismonthlist_tabbar',
        titleid: 'labLeadsCloseThisMonthSummary',
        scope: this
      }
    ],
    //store : Ext.getStore('crmLeads'),
    //itemTpl :  Ext.create('M1CRM.model.crmLeads', {}).getOpenLeadsLayout(),
    grouped: true,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

