Ext.define('M1CRM.view.crmCustQuoteIQSLocations', {
  extend: 'Ext.dataview.List',
  xtype: 'custquoteiqslocations',
  id: 'custquoteiqslocationslist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _custid: null,
    _loctype: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm-org',
    title: 'Organisation Locations',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'custquoteiqslocationslist_toolbarid',
        title: 'Organisation Locations',
        includeAdd: true,
        includeFilter: true,
        includeOptions: false,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'custquoteiqslocations_searchtoolbar',
        searchfieldid: 'custquoteiqslocations_search',
        cls: 'm1-toolbar-search-quotes',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'custquoteiqslocationslist_tabbar',
        titleid: 'labCustQuoteLocationSummary',
        scope: this
      }
    ],
    indexBar: false,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

