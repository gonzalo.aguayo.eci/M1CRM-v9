Ext.define('M1CRM.view.crmCustomerContacts', {
  extend: 'Ext.dataview.List',
  xtype: 'customercontacts',
  id: 'customercontactlist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    title: 'Customer Contacts',
    cls: 'm1-list-crm-contact',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'customercontactlist_toolbarid',
        title: 'Customer Contacts',
        includeAdd: true,
        includeFilter: true,
        includeOptions: false,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'customercontact_searchtoolbar',
        searchfieldid: 'customercontact_search',
        cls: 'm1-toolbar-search-contacts',
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'customercontactlist_summary',
        cls: 'm1-listsubtitle-crm-contact'
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'customercontactlist_tabbar',
        titleid: 'labCustContactsSummary',
        scope: this
      }
    ],
    /*grouped : true,
     detailCard : Ext.create('Ext.Container'),
     clearSelectionOnDeactivate : true
     */
    grouped: true,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true,
    scrollable: 'vertical'
  }

});

