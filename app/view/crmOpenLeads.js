Ext.define('M1CRM.view.crmOpenLeads', {
  extend: 'Ext.dataview.List',
  xtype: 'openleads',
  id: 'openleadslist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _optionsMenu: null,
    _parentformid: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm-leads',
    title: 'Open Leads',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'openleadslist_toolbarid',
        title: 'Open Leads',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'openleads_searchtoolbar',
        searchfieldid: 'openleads_search',
        cls: 'm1-toolbar-search-leads',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'openleadslist_tabbar',
        titleid: 'labOpenLeadsSummary',
        scope: this
      }
    ],
    grouped: true,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

