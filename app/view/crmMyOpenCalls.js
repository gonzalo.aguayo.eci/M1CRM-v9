Ext.define('M1CRM.view.crmMyOpenCalls', {
  extend: 'Ext.dataview.List',
  xtype: 'myopencalls',
  id: 'myopencallslist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm',
    title: 'My Open Calls',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'myopencallslist_toolbarid',
        title: 'My Open Calls',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'myopencalls_searchtoolbar',
        searchfieldid: 'myopencalls_search',
        cls: 'm1-toolbar-search-calls',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'myopencallslist_tabbar',
        titleid: 'labMyOpenCallSummary',
        scope: this
      }


    ],
    //itemTpl :  Ext.create('M1CRM.model.crmCalls', {}).getMyOpenCallsListLayout(),
    grouped: false,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true,
    scrollable: 'vertical'
  }

});

