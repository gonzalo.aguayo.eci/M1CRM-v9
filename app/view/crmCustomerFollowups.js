Ext.define('M1CRM.view.crmCustomerFollowups', {
  extend: 'Ext.dataview.List',
  requires: [
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  xtype: 'customerfollowups',
  id: 'customerfollowupslist',
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm-followups',
    title: 'Organisation Locations',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'customerfollowupslist_toolbarid',
        title: 'Followups',
        includeAdd: true,
        includeFilter: true,
        includeOptions: false,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'custfollowup_searchtoolbar',
        searchfieldid: 'custfollowup_search',
        cls: 'm1-toolbar-search-followup',
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'customerfollowupslist_summary',
        cls: 'm1-listsubtitle-crm-followup'
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'customerfollowupslist_tabbar',
        titleid: 'labCustomerFollowupSummary',
        scope: this
      }
    ],
    //store : Ext.getStore('crmFollowups'),
    //itemTpl :  Ext.create('M1CRM.model.crmFollowups', {}).getMyFollowupListLayout(),
    grouped: true,
    indexBar: false,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

