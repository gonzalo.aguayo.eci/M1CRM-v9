Ext.define('M1CRM.view.crmCallLineAddEdit', {
  extend: 'Ext.form.Panel',
  xtype: 'calllineaddeditform',
  id: 'calllineaddedit',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.field.Hidden',
    'Ext.ux.M1.M1ToolbarEntryForm',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1TextField',
    'Ext.ux.M1.M1SelectField',
    'Ext.ux.M1.M1AdvTextArea',
    'Ext.ux.M1.M1SubMenuButton',
    'Ext.ux.M1.M1InfoButton',
    'Ext.ux.M1.M1InvisibleLookupButton',
    'Ext.ux.M1.M1Logo'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _mode: 'view',
    items: [
      {
        xtype: 'hiddenfield',
        id: 'addeditcustcall_callid',
        name: 'addeditcustcall_callid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditcallline_customerid',
        name: 'addeditcallline_customerid'
      },

      {
        xtype: 'hiddenfield',
        id: 'addeditcallline_callid',
        name: 'addeditcallline_callid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditcallline_lineid',
        name: 'addeditcallline_lineid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditcallline_contactid',
        name: 'addeditcallline_contactid'
      },
      {
        xtype: 'm1toolbarentryform',
        id: 'calllineaddedit_toolbarid',
        title: 'Call Line',
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'calllineaddedit_infoid'
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditcallline_customer',
                label: 'Org',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1infobutton',
                id: 'addeditcallline_custinfo',
                iconCls: 'm1info',
                infotype: 'customer',
                parentformid: 'calllineaddedit',
                infoarray: ['addeditcallline_customerid', 'addeditcallline_customer', null, null, null, null, null, null, null],
                scope: this
              }

            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditcallline_contact',
                label: 'Contact',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1infobutton',
                id: 'addeditcallline_contactinfo',
                infotype: 'contact',
                parentformid: 'calllineaddedit',
                infoarray: ['addeditcallline_customerid', 'addeditcallline_customer', null, null, 'addeditcallline_contactid', 'addeditcallline_contact', null, null, null],
                scope: this
              }

            ]
          }
        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditcallline_description',
                label: 'Desc',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'm1advtextarea',
            textareaid: 'calllineaddedit_notes',
            textarealabel: 'Long Desc',
            detaillabel: 'Long Desc',
            textareaflex: 4,
            scope: this
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditcallline_method',
                label: 'Contact Method',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'addeditcallline_methodsel',
                label: 'Contact Method',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }


            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1checkboxfield',
                id: 'addeditcallline_internalonly',
                label: 'Internal Only?',
                checked: false,
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditcallline_addedby',
                label: 'Added By',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'addeditcallline_addedbysel',
                label: 'Added By',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditcallline_addeddate',
                label: 'Open Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },
      {
        xtype: 'm1logo'
      }
    ]
  }
});
