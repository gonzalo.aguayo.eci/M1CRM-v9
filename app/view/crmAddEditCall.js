Ext.define('M1CRM.view.crmAddEditCall', {
  extend: 'Ext.form.Panel',
  xtype: 'crmaddeditcallform',
  id: 'addeditcall',
  requires: [
    'Ext.tab.Panel',
    'Ext.field.Hidden',
    'Ext.ux.M1.M1ToolbarEntryForm',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1TextField',
    'Ext.ux.M1.M1ContactField',
    'Ext.ux.M1.M1AdvTextField',
    'Ext.ux.M1.Checkbox',
    'Ext.ux.M1.M1SelectField',
    'Ext.ux.M1.M1AdvTextArea',
    'Ext.ux.M1.M1Logo',
    'Ext.ux.M1.M1SubMenuButton',
    'Ext.ux.M1.M1DatePicker',
    'Ext.ux.M1.M1SubMenuDummyButton',
    'Ext.ux.M1.M1InvisibleLookupButton',
    'Ext.ux.M1.M1ExpandCollapsBar'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _optype: null,
    _mode: 'view',
    items: [
      {
        xtype: 'hiddenfield',
        id: 'addeditcall_callid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditcall_custid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditcall_locationid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditcall_contactid'
      },
      {
        xtype: 'm1toolbarentryform',
        id: 'addeditcall_toolbarid',
        title: 'Add/Edit Call',
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'addeditcalltitleid'
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditcall_calltypesel',
                label: 'Call Type',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditcall_calltype',
                label: 'Call Type',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'm1advtextfield',
            textfieldid: 'addeditcall_customer',
            label: 'Org',
            readOnly: true,
            haslookupbtn: true,
            hasinfobtn: true,
            required: true,
            infoarray: ['customer', 'addeditcall', 'addeditcall_custid', 'addeditcall_customer', null, null, null, null, null, null, null],
            scope: this
          },
          {
            xtype: 'm1advtextfield',
            textfieldid: 'addeditcall_location',
            label: 'Location',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            infoarray: ['location', 'addeditcall', 'addeditcall_custid', 'addeditcall_customer', 'addeditcall_locationid', 'addeditcall_location', null, null, null, null, null],
            scope: this
          },
          {
            xtype: 'm1contactfield',
            contactfieldid: 'addeditcall_contactname',
            contactfieldlabel: 'Contact',
            contactfielreadonly: true,
            scope: this
          }
        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'm1advtextfield',
            textfieldid: 'addeditcall_partid',
            label: 'Part ID',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            infoarray: ['part', 'addeditcall', null, null, null, null, null, null, 'addeditcall_partid', 'addeditcall_partrevision', null],
            scope: this
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditcall_partrevision',
                label: 'Rev',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditcall_partdesc',
                label: 'Part Desc',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditcall_desc',
                label: 'Call Desc',
                readOnly: true,
                required: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }


            ]
          },
          {
            xtype: 'm1advtextarea',
            textareaid: 'addeditcall_notes',
            textarealabel: 'Long Desc',
            detaillabel: 'Long Desc',
            textareaflex: 4,
            scope: this
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditcall_contactmethodsel',
                label: 'Contact Method',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditcall_contactmethod',
                label: 'Contact Method',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1checkboxfield',
                id: 'addeditcall_internalonly',
                label: 'Internal Only?',
                checked: false,
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditcall_prioritysel',
                label: 'Priority',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditcall_priority',
                label: 'Priority',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditcall_statussel',
                label: 'Status',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditcall_status',
                label: 'Status',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }

            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditcall_reasonsel',
                label: 'Reason',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditcall_reason',
                label: 'Reason',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }

            ]
          }
        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditcall_openbysel',
                label: 'Opened By',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditcall_openby',
                label: 'Opened By',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }

            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1datepicker',
                id: 'addeditcall_opendatesel',
                label: 'Open Date',
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditcall_opendate',
                label: 'Open Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditcall_assigntosel',
                label: 'Assigned To',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditcall_assignto',
                label: 'Assigned To',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }


            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1datepicker',
                id: 'addeditcall_assigndatesel',
                label: 'Assigned Date',
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditcall_assigndate',
                label: 'Assigned Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1datepicker',
                id: 'addeditcall_accepteddatesel',
                label: 'Accepted Date',
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditcall_accepteddate',
                label: 'Accepted Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }

            ]
          }
        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditcall_closedbysel',
                label: 'Closed By',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditcall_closedby',
                label: 'Closed By',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1datepicker',
                id: 'addeditcall_closeddatesel',
                label: 'Closed Date',
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditcall_closeddate',
                label: 'Closed Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },


      {
        xtype: 'fieldset',
        id: 'addeditcall_currencyinfofieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditcall_currencysel',
                label: 'Currency',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditcall_currency',
                label: 'Currency',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },


      {
        xtype: 'm1expandcollapsbar',
        id: 'addeditcall_sourceinfoexpandbtn',
        text: 'Source Info',
        ctrlfieldset: 'addeditcall_sourceinfofieldset',
        scope: this
      },

      {
        xtype: 'fieldset',
        id: 'addeditcall_sourceinfofieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditcall_leadid',
                label: 'Lead ID',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }

            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditcall_quoteid',
                label: 'Quote ID',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditcall_orderid',
                label: 'Order ID',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }

            ]
          }

        ]

      },
      {
        xtype: 'm1logo'
      },
      {
        xtype: 'container',
        margin: 1,
        docked: 'bottom',
        layout: {
          type: 'hbox'
        },
        items: [
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-calllines',
            id: 'addeditcall_btncalllines',
            text: 'Call Lines',
            iconCls: 'call_details',
            scope: this
          },
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-followup',
            //cls : 'm1-submenuactionbutton-crm-calls',
            id: 'addeditcall_btnlinkedfollowups',
            text: 'Follow-ups',
            iconCls: 'follow_up',
            scope: this
          }
        ]
      }
    ]
  }
});
