Ext.define('M1CRM.view.crmCloseFollowups', {
  extend: 'Ext.dataview.List',
  xtype: 'closefollowups',
  id: 'closefollowuplist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _optionsMenu: null,
    _parentformid: null,
    _detailformid: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm',
    title: 'Completed Follow-ups',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'closefollowuplist_toolbarid',
        title: 'Completed Follow-ups',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'closefollowup_searchtoolbar',
        searchfieldid: 'closefollowup_search',
        cls: 'm1-toolbar-search-followup',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'closefollowuplist_tabbar',
        titleid: 'labCloseFollowupsSummary',
        scope: this
      }

    ],
    //store : Ext.getStore('crmFollowups'),
    //itemTpl :  Ext.create('M1CRM.model.crmFollowups', {}).getMyFollowupListLayout(),
    grouped: false,
    indexBar: false,
    clearSelectionOnDeactivate: true
  }

});

