Ext.define('M1CRM.view.crmFollowupAddEdit', {
  extend: 'Ext.form.Panel',
  xtype: 'followupaddeditform',
  id: 'followupaddedit',
  requires: [
    'Ext.tab.Panel',
    'Ext.field.Hidden',
    'Ext.ux.M1.M1ToolbarEntryForm',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1TextField',
    'Ext.ux.M1.M1ContactField',
    'Ext.ux.M1.M1AdvTextField',
    'Ext.ux.M1.Checkbox',
    'Ext.ux.M1.M1SelectField',
    'Ext.ux.M1.M1AdvTextArea',
    'Ext.ux.M1.M1Logo',
    'Ext.ux.M1.M1SubMenuButton',
    'Ext.ux.M1.M1DatePicker',
    'Ext.ux.M1.M1SubMenuDummyButton',
    'Ext.ux.M1.M1InvisibleLookupButton',
    'Ext.ux.M1.M1DateTimePicker'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _mode: 'view',
    items: [
      {
        xtype: 'hiddenfield',
        id: 'followupid',
        name: 'followupid'
      },

      {
        xtype: 'hiddenfield',
        id: 'followupaddedit_contactid',
        name: 'followupaddedit_contactid'
      },
      {
        xtype: 'hiddenfield',
        id: 'followupaddedit_custid',
        name: 'followupcustomerid'
      },
      {
        xtype: 'hiddenfield',
        id: 'followupaddedit_locationid',
        name: 'followupcustomerlocationid'
      },
      {
        xtype: 'hiddenfield',
        id: 'followupaddedit_callid'
      },

      {
        xtype: 'm1toolbarentryform',
        id: 'followupaddedit_toolbarid',
        title: 'Follow-up Details',
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'followupaddedittitleid'
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'followup_typesel',
                label: 'Type',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'followup_type',
                label: 'Type',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'm1advtextfield',
            textfieldid: 'followupaddedit_customer',
            label: 'Org',
            readOnly: true,
            haslookupbtn: true,
            hasinfobtn: true,
            required: true,
            infoarray: ['customer', 'followupaddedit', 'followupaddedit_custid', 'followupaddedit_customer', null, null, null, null, null, null, null],
            scope: this
          },
          {
            xtype: 'm1advtextfield',
            textfieldid: 'followupaddedit_location',
            label: 'Location',
            readOnly: true,
            haslookupbtn: true,
            hasinfobtn: true,
            infoarray: ['location', 'followupaddedit', 'followupaddedit_custid', 'followupaddedit_customer', 'followupaddedit_locationid', 'followupaddedit_location', null, null, null, null, null],
            scope: this
          },
          {
            xtype: 'm1contactfield',
            contactfieldid: 'followupaddedit_contact',
            contactfieldlabel: 'Contact',
            contactfielreadonly: true,
            scope: this
          }
        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'datetimepickerfield',
                id: 'followup_startdatesel',
                flex: 4,
                label: 'Start',
                value: new Date(),
                picker: {
                  yearFrom: new Date().getFullYear(),
                  yearTo: new Date().getFullYear() + 2,
                  minuteInterval: 1,
                  ampm: true
                }
              },
              {
                xtype: 'm1textfield',
                id: 'followup_startdate',
                label: 'Start Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'datetimepickerfield',
                id: 'followup_duedatesel',
                flex: 4,
                label: 'Due',
                value: new Date(),
                picker: {
                  yearFrom: new Date().getFullYear(),
                  yearTo: new Date().getFullYear() + 2,
                  minuteInterval: 1,
                  ampm: true
                }
              },
              {
                xtype: 'm1textfield',
                id: 'followup_duedate',
                label: 'Due Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'followup_prioritysel',
                label: 'Priority',
                valueField: 'ID',
                displayField: 'Description',
                required: true,
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'followup_priority',
                label: 'Priority',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'followup_statussel',
                label: 'Status',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'followup_status',
                label: 'Status',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1datepicker',
                id: 'followup_completeddatesel',
                label: 'Completed Date',
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'followup_completeddate',
                label: 'Completed Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'followup_assigntosel',
                label: 'Assigned To',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'followup_assignto',
                label: 'Assigned To',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'followupaddedit_desc',
                label: 'Desc',
                readOnly: true,
                required: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'followupaddedit_meetinglocation',
                label: 'Meeting Location',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'm1advtextarea',
            textareaid: 'followupaddedit_notes',
            textarealabel: 'Long Desc',
            detaillabel: 'Long Desc',
            textareaflex: 4,
            scope: this
          }

        ]
      },

      {
        xtype: 'fieldset',
        id: 'calldetail_sourceinfofieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'followup_callid',
                label: 'Call ID',
                readOnly: true,
                scope: this
              },

              {
                xtype: 'm1infobutton',
                id: 'followup_callinfo',
                //iconCls : 'info',
                scope: this
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'followup_leadid',
                label: 'Lead ID',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1infobutton',
                id: 'followup_leadinfo',
                scope: this
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'followup_quoteid',
                label: 'Quote ID',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1infobutton',
                id: 'followup_quoteinfo',
                scope: this
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'followup_orderid',
                label: 'Order ID',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1infobutton',
                id: 'followup_orderinfo',
                scope: this
              }
              //{
              //    xtype:'m1invisiblelookupbutton'
              //}
            ]
          }
        ]
      },
      {
        xtype: 'm1logo'
      }
    ]
  }
});
