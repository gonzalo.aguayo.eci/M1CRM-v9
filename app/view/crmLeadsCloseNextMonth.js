Ext.define('M1CRM.view.crmLeadsCloseNextMonth', {
  extend: 'Ext.dataview.List',
  xtype: 'leadsclosenextmonth',
  id: 'leadsclosenextmonthlist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _optionsMenu: null,
    _parentformid: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm-leads',
    title: 'Leads Close Next Month',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'leadsclosenextmonthlist_toolbarid',
        title: 'Leads Close Next Month',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'leadsclosenextmonth_searchtoolbar',
        searchfieldid: 'leadsclosenextmonth_search',
        cls: 'm1-toolbar-search-leads',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'leadsclosenextmonthlist_tabbar',
        titleid: 'labLeadsCloseNextMonthSummary',
        scope: this
      }
    ],
    grouped: true,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

