Ext.define('M1CRM.view.crmWaitingCalls', {
  extend: 'Ext.dataview.List',
  xtype: 'waitingcall',
  id: 'waitinglist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    title: 'Waiting Calls',
    cls: 'm1-list-crm',
    items: [

      {
        xtype: 'm1toolbarlistview',
        id: 'waitinglist_toolbarid',
        title: 'Waiting Calls',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'waitingcall_searchtoolbar',
        searchfieldid: 'waitingcall_search',
        cls: 'm1-toolbar-search-calls',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'waitinglist_tabbar',
        titleid: 'labWaitingCallSummary',
        scope: this
      }

    ],
    //store : Ext.getStore('crmCalls'),
    //itemTpl :  Ext.create('M1CRM.model.crmCalls', {}).getWaitingPendingCallsListLayout(),
    grouped: false,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

