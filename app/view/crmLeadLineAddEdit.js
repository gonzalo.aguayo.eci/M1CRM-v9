Ext.define('M1CRM.view.crmLeadLineAddEdit', {
  extend: 'Ext.form.Panel',
  xtype: 'leadlineaddeditform',
  id: 'leadlineaddedit',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.field.Hidden',
    'Ext.ux.M1.M1ToolbarEntryForm',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1TextField',
    'Ext.ux.M1.M1NumberField',
    'Ext.ux.M1.M1SelectField',
    'Ext.ux.M1.M1DatePicker',
    'Ext.ux.M1.Checkbox',
    'Ext.ux.M1.M1SubMenuButton',
    'Ext.ux.M1.M1AdvTextField',
    'Ext.ux.M1.M1InfoButton',
    'Ext.ux.M1.M1Logo'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _mode: 'view',
    items: [
      {
        xtype: 'hiddenfield',
        id: 'leadlineaddedit_leadid'
      },
      {
        xtype: 'hiddenfield',
        id: 'leadlineaddedit_leadlineid'
      },
      {
        xtype: 'hiddenfield',
        id: 'leadlineaddedit_partid'
      },
      {
        xtype: 'hiddenfield',
        id: 'leadlineaddedit_partgroupid'
      },
      {
        xtype: 'm1toolbarentryform',
        id: 'leadlineaddedit_toolbarid',
        title: 'Lead Line',
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'leadlineaddeditsubtitle'
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'm1advtextfield',
            textfieldid: 'leadlineaddedit_part',
            label: 'Part ID',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            infoarray: ['part', 'leadlineaddedit', null, null, null, null, null, null, 'leadlineaddedit_partid', 'leadlineaddedit_partrevision', null],
            scope: this
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'leadlineaddedit_partrevision',
                label: 'Rev',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'leadlineaddedit_partdesc',
                label: 'Part Desc',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'leadlineaddedit_partuom',
                label: 'UoM',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'leadlineaddedit_qty',
                label: 'Quantity',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'leadlineaddedit_unitsaleprice',
                currecyField: true,
                label: 'Unit Sale Price',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'leadlineaddedit_qtyonhand',
                label: 'Qty On Hand',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'leadlineaddedit_qtyinproduction',
                label: 'In Production',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'leadlineaddedit_qtyonorder',
                label: 'On Order',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'leadlineaddedit_grossamount',
                currecyField: true,
                label: 'Gross Amount',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'leadlineaddedit_grossamountdisp',
                currecyField: true,
                label: 'Gross Amount',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }


            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'leadlineaddedit_discount',
                label: 'Discount%',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'leadlineaddedit_discountdisp',
                label: 'Discount%',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'leadlineaddedit_discountamt',
                label: 'Discount Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'leadlineaddedit_discountamtdisp',
                currecyField: true,
                label: 'Discount Amt',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'leadlineaddedit_revforcecast',
                label: 'Revenue Forecast',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'leadlineaddedit_revforcecastdisp',
                currecyField: true,
                label: 'Revenue Forecast',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1datepicker',
                id: 'aleadlineaddedit_revforcecastdatesel',
                label: 'Forecast Date',
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'aleadlineaddedit_revforcecastdate',
                label: 'Forecast Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'aleadlineaddedit_resolution',
                label: 'Resolution',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'aleadlineaddedit_resolutionsel',
                label: 'Resolution',
                valueField: 'ReasonID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }

            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1checkboxfield',
                id: 'aleadlineaddedit_toquote',
                label: 'To Quote',
                checked: false,
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }

        ]
      },
      {
        xtype: 'm1logo'
      }
    ]
  }
});
