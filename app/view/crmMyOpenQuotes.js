Ext.define('M1CRM.view.crmMyOpenQuotes', {
  extend: 'Ext.dataview.List',
  xtype: 'myopenquotes',
  id: 'myopenquoteslist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _optionsMenu: null,
    _parentformid: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm',
    title: 'My Open Calls',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'myopenquoteslist_toolbarid',
        title: 'My Open Quotes',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'myopenquotes_searchtoolbar',
        searchfieldid: 'myopenquotes_search',
        cls: 'm1-toolbar-search-quotes',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'myopenquoteslist_tabbar',
        titleid: 'labMyOpenQuotesSummary',
        scope: this
      }

    ],
    //store : Ext.getStore('crmQuotations'),
    //itemTpl :  Ext.create('M1CRM.model.crmQuotations', {}).getMyOpenQuoteLayout(),
    grouped: false,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

