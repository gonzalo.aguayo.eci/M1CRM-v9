Ext.define('M1CRM.view.crmAllOpenCalls', {
  extend: 'Ext.dataview.List',
  xtype: 'allopencalls',
  id: 'allopencallslist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm-call',
    title: 'All Open Calls',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'allopencallslist_toolbarid',
        title: 'All Open Calls',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'allopencalls_searchtoolbar',
        searchfieldid: 'allopencalls_search',
        cls: 'm1-toolbar-search-calls',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'allopencallslist_tabbar',
        titleid: 'labAllOpenCallSummary',
        scope: this
      }

    ],
    // store : Ext.getStore('crmCalls'),
    //itemTpl :  Ext.create('M1CRM.model.crmCalls', {}).getMyOpenCallsListLayout(),
    grouped: false,
    indexBar: false,
    clearSelectionOnDeactivate: true
  }

});

