Ext.define('M1CRM.view.crmLeadLines', {
  extend: 'Ext.dataview.List',
  xtype: 'leadlines',
  id: 'leadlineslist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm',
    title: 'Lead Lines',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'leadlineslist_toolbarid',
        title: 'Lead Lines',
        includeAdd: true,
        includeFilter: true,
        includeOptions: false,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'leadlines_searchtoolbar',
        searchfieldid: 'leadlines_search',
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'leadlineslist_summary',
        cls: 'm1-listsubtitle-crm-leadlines'
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'leadlineslist_tabbar',
        titleid: 'labLeadLinesSummary',
        scope: this
      }
    ],
    grouped: false,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

