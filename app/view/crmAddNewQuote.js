Ext.define('M1CRM.view.crmAddNewQuote', {
  extend: 'Ext.form.Panel',
  xtype: 'addnewquoteform',
  id: 'addnewquote',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.field.Hidden',
    'Ext.ux.M1.M1ToolbarEntryForm',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1TextField',
    'Ext.ux.M1.Checkbox',
    'Ext.ux.M1.M1SelectField',
    'Ext.ux.M1.M1AdvTextArea',
    'Ext.ux.M1.M1DatePicker',
    'Ext.ux.M1.M1SubMenuButton',
    'Ext.ux.M1.M1AdvTextField',
    'Ext.ux.M1.M1InfoButton',
    'Ext.ux.M1.M1InvisibleLookupButton',
    'Ext.ux.M1.M1ExpandCollapsBar',
    'Ext.ux.M1.M1Logo'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _callcontrol: null,
    _mode: 'view',
    items: [
      {
        xtype: 'hiddenfield',
        id: 'addnewquote_tempquoteid',
        name: 'addnewquote_tempquoteid'
      },

      {
        xtype: 'hiddenfield',
        id: 'addnewquote_quoteid',
        name: 'addnewquote_quoteid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addnewquote_custid',
        name: 'addnewquote_custid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addnewquote_quotelocationid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addnewquote_quotecontactid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addnewquote_actcontactid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addnewquote_shipcontactid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addnewquote_invlocationid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addnewquote_shipcustomerid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addnewquote_shiplocationid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addnewquote_quoterid'
      },
      {
        xtype: 'm1toolbarentryform',
        id: 'addnewquote_toolbarid',
        title: 'Quote Details',
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'addnewquotesubtitle'
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'm1advtextfield',
            textfieldid: 'addnewquote_customer',
            label: 'Customer',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            required: true,
            infoarray: ['customer', 'addnewquote', 'addnewquote_custid', 'addnewquote_customer', null, null, null, null, null, null, null],
            scope: this
          }
        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'm1advtextfield',
            textfieldid: 'addnewquote_quotelocation',
            label: 'Quote Loc',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            infoarray: ['location', 'addnewquote', 'addnewquote_custid', 'addnewquote_customer',
              'addnewquote_quotelocationid', 'addnewquote_quotelocation', null, null, null, null, null],
            scope: this
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addnewquote_quotecontact',
                label: 'Quote Contact',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'addnewquote_quotecontactsel',
                label: 'Quote Contact',
                valueField: 'ContactID',
                displayField: 'ContactName',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1infobutton',
                id: 'addnewquote_quotecontactinfo',
                iconCls: 'm1info',
                infotype: 'contact',
                parentformid: 'addnewquote',
                infoarray: ['addnewquote_custid', 'addnewquote_customer', 'addnewquote_quotelocationid', 'addnewquote_quotelocation', 'addnewquote_quotecontactid', 'addnewquote_quotecontact', null, null, null],
                scope: this
              }
            ]
          }
        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        id: 'addnewquote_morelocinfofieldset',
        items: [
          {
            xtype: 'm1advtextfield',
            textfieldid: 'addnewquote_invlocation',
            label: 'Inv Loc',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            infoarray: ['location', 'addnewquote', 'addnewquote_custid', 'addnewquote_customer', 'addnewquote_invlocationid', 'addnewquote_invlocation', null, null, null, null, null],
            scope: this
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addnewquote_actcontact',
                label: 'Acct Contact',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'addnewquote_actcontactsel',
                label: 'Acct Contact',
                valueField: 'ContactID',
                displayField: 'ContactName',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1infobutton',
                id: 'addnewquote_contactinfo',
                iconCls: 'm1info',
                infotype: 'contact',
                parentformid: 'addnewquote',
                infoarray: ['addnewquote_custid', 'addnewquote_customer', 'addnewquote_invlocationid', 'addnewquote_invlocation', 'addnewquote_actcontactid', 'addnewquote_actcontact', null, null, null],
                scope: this
              }

            ]
          },
          {
            xtype: 'm1advtextfield',
            textfieldid: 'addnewquote_shipcustomer',
            label: 'Ship Org',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            required: true,
            infoarray: ['location', 'addnewquote', 'addnewquote_shipcustomerid', 'addnewquote_shipcustomer', null, null, null, null, null, null, null],
            scope: this
          },
          {
            xtype: 'm1advtextfield',
            textfieldid: 'addnewquote_shiplocation',
            label: 'Ship Loc',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            infoarray: ['location', 'addnewquote', 'addnewquote_shipcustomerid', 'addnewquote_shipcustomer', 'addnewquote_shiplocationid', 'addnewquote_shiplocation', null, null, null, null, null],
            scope: this
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addnewquote_shipcontact',
                label: 'Ship Contact',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'addnewquote_shipcontactsel',
                label: 'Ship Contact',
                valueField: 'ContactID',
                displayField: 'ContactName',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1infobutton',
                id: 'addnewquote_shipcontactinfo',
                iconCls: 'm1info',
                infotype: 'contact',
                parentformid: 'addnewquote',
                infoarray: ['addnewquote_shipcustomerid', 'addnewquote_shipcustomer', 'addnewquote_shiplocationid', 'addnewquote_shiplocation', 'addnewquote_shipcontactid', 'addnewquote_shipcontact', null, null, null],
                scope: this
              }
            ]
          }

        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addnewquote_quotersel',
                label: 'Quoter',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                required: true,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addnewquote_quoter',
                label: 'Quoter',
                readOnly: true,
                required: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addnewquote_quotedate',
                label: 'Quote Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1datepicker',
                id: 'addnewquote_quotedatesel',
                label: 'Quote Date',
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addnewquote_quoteduedate',
                label: 'Due Date',
                readOnly: true,
                required: true,
                scope: this
              },
              {
                xtype: 'm1datepicker',
                id: 'addnewquote_quoteduedatesel',
                label: 'Due Date',
                required: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addnewquote_expdate',
                label: 'Exp. Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }

        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addnewquote_stdmessage',
                label: 'Std. Msg',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'addnewquote_stdmessagesel',
                label: 'Std. Msg',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }

        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1checkboxfield',
                id: 'addnewquote_closed',
                label: 'Closed?',
                checked: false,
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addnewquote_closeddate',
                label: 'Closed Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },
      {
        xtype: 'm1advtextarea',
        textareaid: 'addnewquote_header',
        textarealabel: 'Header Text',
        detaillabel: 'Header Text',
        textareaflex: 4,
        scope: this
      },
      {
        xtype: 'm1advtextarea',
        textareaid: 'addnewquote_footer',
        textarealabel: 'Footer Text',
        detaillabel: 'Footer Text',
        textareaflex: 4,
        scope: this
      },
      {
        xtype: 'm1logo'
      },
      {
        xtype: 'container',
        id: 'addnewquote_btnop1',
        margin: 1,
        docked: 'bottom',
        layout: {
          type: 'hbox'
        },
        items: [
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-quotelines',
            id: 'addnewquote_quoteparts',
            text: 'Quote Lines',
            iconCls: 'parts',
            scope: this
          },
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-followup',
            id: 'addnewquote_quotefollowups',
            text: 'Followups',
            iconCls: 'follow_up',
            scope: this
          },
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-calls',
            id: 'addnewquote_quotecalls',
            text: 'Calls',
            iconCls: 'calls',
            scope: this
          }
        ]
      }
    ]
  }
});
