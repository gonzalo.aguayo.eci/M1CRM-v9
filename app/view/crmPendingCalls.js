Ext.define('M1CRM.view.crmPendingCalls', {
  extend: 'Ext.dataview.List',
  xtype: 'pendingcall',
  id: 'pendingcalllist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    title: 'Close Calls',
    cls: 'm1-list-crm',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'pendingcalllist_toolbarid',
        title: 'Pending Calls',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'pendingcalllist_searchtoolbar',
        searchfieldid: 'pendingcalllist_search',
        cls: 'm1-toolbar-search-calls',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'pendingcalllist_tabbar',
        titleid: 'labPendingCallsSummary',
        scope: this
      }
    ],
    // store : Ext.getStore('crmCalls'),
    // itemTpl :  Ext.create('M1CRM.model.crmCalls', {}).getPendingCallsListLayout(),
    grouped: false,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

