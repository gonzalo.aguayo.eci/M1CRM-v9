Ext.define('M1CRM.view.crmParts', {
  extend: 'Ext.dataview.List',
  xtype: 'parts',
  id: 'partslist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  controllers: [
    'M1CRM.controller.crmParts'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm',
    title: 'Parts',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'partslist_toolbarid',
        title: 'Parts',
        includeAdd: false,
        includeFilter: true,
        includeOptions: false,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'partslist_searchtoolbar',
        searchfieldid: 'parts_search',
        cls: 'm1-toolbar-search-parts',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'partslist_tabbar',
        titleid: 'labPartsSummary'
      }
    ],
    indexBar: false,
    pinHeaders: false,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

