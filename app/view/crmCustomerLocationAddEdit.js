Ext.define('M1CRM.view.crmCustomerLocationAddEdit', {
  extend: 'Ext.form.Panel',
  xtype: 'customerlocationaddeditform',
  id: 'customerlocationaddedit',
  requires: [
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarEntryForm',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1TextField',
    'Ext.ux.M1.M1PhoneNumberField',
    'Ext.ux.M1.M1EmailField',
    // 'Ext.ux.M1.M1UrlField',
    'Ext.ux.M1.Checkbox',
    'Ext.ux.M1.M1SelectField',
    'Ext.ux.M1.M1AdvTextArea',
    'Ext.ux.M1.M1Logo',
    'Ext.ux.M1.M1SubMenuButton',
    'Ext.ux.M1.M1SubMenuDummyButton'
  ],
  config: {
    _parentformid: null,
    _optionsMenu: null,
    _loctype: null,
    items: [
      {
        xtype: 'm1toolbarentryform',
        id: 'customerlocationaddedit_toolbarid',
        title: 'Organisation Location',
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'addeditcustlocat_locationidtag'
      },


      {
        xtype: 'fieldset',
        id: 'addeditcustloc_info',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'm1textfield',
            id: 'customerlocationaddedit_custid',
            label: 'ID',
            readOnly: false,
            touppercase: true,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'customerlocationaddedit_customer',
            label: 'Name',
            readOnly: false,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'customerlocationaddedit_locationid',
            label: 'Location ID',
            readOnly: false,
            touppercase: true,
            required: true,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'customerlocationaddedit_location',
            label: 'Location Name',
            readOnly: false,
            required: true,
            scope: this
          }
        ]
      },

      {
        xtype: 'fieldset',
        id: 'addeditcustloc_addressfieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'm1textfield',
            id: 'addeditloc_line1',
            label: 'Address',
            readOnly: false,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'addeditloc_line2',
            label: ' ',
            readOnly: false,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'addeditloc_line3',
            label: ' ',
            readOnly: false,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'addeditloc_city',
            label: 'City',
            readOnly: false,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'addeditloc_state',
            label: 'State',
            readOnly: false,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'addeditloc_postcode',
            label: 'Zip',
            readOnly: false,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'addeditloc_country',
            label: 'Country',
            readOnly: false,
            scope: this
          }
        ]
      },

      {
        xtype: 'fieldset',
        id: 'addeditcustloc_contactfieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'm1phonenumberfield',
            id: 'addeditloc_phone1',
            label: 'Phone',
            readOnly: false,
            scope: this
          },
          {
            xtype: 'm1phonenumberfield',
            id: 'addeditloc_phone2',
            label: 'Alt Phone',
            readOnly: false,
            scope: this
          },
          {
            xtype: 'm1phonenumberfield',
            id: 'addeditloc_fax',
            label: 'Fax',
            dialontap: false,
            readOnly: false,
            scope: this
          },
          {
            xtype: 'm1emailfield',
            id: 'addeditloc_email',
            label: 'Email',
            readOnly: false,
            scope: this
          }
        ]

      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'm1checkboxfield',
            id: 'addeditloc_quoteloc',
            label: 'Quote Location',
            checked: false,
            flex: 4,
            scope: this
          },
          {
            xtype: 'm1checkboxfield',
            id: 'addeditloc_shiploc',
            label: 'Ship Location',
            checked: false,
            flex: 4,
            scope: this
          },
          {
            xtype: 'm1checkboxfield',
            id: 'addeditloc_ARInvoiceloc',
            label: 'AR Invoice Location',
            checked: false,
            flex: 4,
            scope: this
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditloc_shipmethodsel',
                label: 'Ship Method',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditloc_shipmethod',
                label: 'Ship Method',
                readOnly: false,
                scope: this
              }
            ]
          }
        ]
      },


      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'm1checkboxfield',
            id: 'addeditloc_taxable',
            label: 'Taxable',
            checked: false,
            flex: 4,
            scope: this
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditloc_taxidsel',
                label: 'Tax ID',
                valueField: 'TaxCodeID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditloc_taxid',
                label: 'Tax ID',
                readOnly: false,
                scope: this
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditloc_secondtaxidsel',
                label: 'Second Tax ID',
                valueField: 'TaxCodeID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditloc_secondtaxid',
                label: 'Second Tax ID',
                readOnly: true,
                scope: this
              }
            ]
          }
        ]
      },

      {
        xtype: 'm1logo'
      },
      {
        xtype: 'container',
        id: 'addeditloc_btnop2',
        margin: 1,
        docked: 'bottom',
        layout: {
          type: 'hbox'
        },
        items: [
          {
            xtype: 'm1submenubutton',
            id: 'addeditloc_calls',
            cls: 'm1-submenuactionbutton-crm-calls',
            //cls : 'm1-submenuactionbutton-crm-organisation',
            text: 'Calls',
            iconCls: 'calls',
            scope: this
          },
          {
            xtype: 'm1submenubutton',
            id: 'addeditloc_followups',
            cls: 'm1-submenuactionbutton-crm-followup',
            //cls : 'm1-submenuactionbutton-crm-organisation',
            text: 'Followups',
            iconCls: 'follow_up',
            scope: this
          },
          {
            xtype: 'm1submenubutton',
            id: 'addeditloc_quotes',
            cls: 'm1-submenuactionbutton-crm-quotes',
            //cls : 'm1-submenuactionbutton-crm-organisation',
            text: 'Quotes',
            iconCls: 'quotation',
            scope: this
          }
        ]
      },


      {
        xtype: 'container',
        id: 'addeditloc_btnop1',
        margin: 1,
        docked: 'bottom',
        layout: {
          type: 'hbox'
        },
        items: [
          {
            xtype: 'm1submenubutton',
            id: 'addeditloc_contacts',
            cls: 'm1-submenuactionbutton-crm-contact',
            //cls : 'm1-submenuactionbutton-crm-organisation',
            text: 'Contacts',
            iconCls: 'contacts',
            scope: this
          },
          {
            xtype: 'm1submenudummybutton'
          },
          {
            xtype: 'm1submenudummybutton'
          }
        ]
      }
    ]
  }
});
