Ext.define('M1CRM.view.crmAllFollowups', {
  extend: 'Ext.dataview.List',
  xtype: 'allfollowups',
  id: 'allfollowuplist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _optionsMenu: null,
    _parentformid: null,
    _detailformid: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm-followups',
    title: 'All Follow-ups',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'allfollowuplist_toolbarid',
        title: 'All Follow-ups',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'allfollowuplist_searchtoolbar',
        searchfieldid: 'allfollowup_search',
        cls: 'm1-toolbar-search-followup',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'allfollowuplist_tabbar',
        titleid: 'labAllFollowupsSummary',
        scope: this
      }

    ],
    //store : Ext.getStore('crmFollowups'),
    //itemTpl :  Ext.create('M1CRM.model.crmFollowups', {}).getMyFollowupListLayout(),
    grouped: true,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

