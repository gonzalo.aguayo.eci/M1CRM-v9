Ext.define('M1CRM.view.crmCloseCalls', {
  extend: 'Ext.dataview.List',
  xtype: 'closecalls',
  id: 'closecallslist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    title: 'Closed Calls',
    cls: 'm1-list-crm',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'closecallslist_toolbarid',
        title: 'Closed Calls',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'closecallslist_searchtoolbar',
        searchfieldid: 'closecall_search',
        cls: 'm1-toolbar-search-calls',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'closecallslist_tabbar',
        titleid: 'labClosedCallSummary',
        scope: this
      }

    ],
    // store : Ext.getStore('crmCalls'),
    // itemTpl :  Ext.create('M1CRM.model.crmCalls', {}).getMyOpenCallsListLayout(),  
    grouped: false,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

