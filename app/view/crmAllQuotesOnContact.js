Ext.define('M1CRM.view.crmAllQuotesOnContact', {
  extend: 'Ext.dataview.List',
  requires: [
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  xtype: 'allquotesoncontact',
  id: 'allquotesoncontactlist',
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm',
    title: 'Linked Quotes',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'allquotesoncontactlist_toolbarid',
        title: 'Linked Quotes',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'allquotesoncontact_searchtoolbar',
        searchfieldid: 'allquotesoncontact_search',
        cls: 'm1-toolbar-search-quotes',
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'allquotesoncontactlist_summary',
        cls: 'm1-listsubtitle-crm-quotes'
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'allquotesoncontactlist_tabbar',
        titleid: 'labAllQuotesOnContactSummary',
        scope: this
      }
    ],
    grouped: false,
    indexBar: false,
    clearSelectionOnDeactivate: true
  }

});

