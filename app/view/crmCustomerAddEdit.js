Ext.define('M1CRM.view.crmCustomerAddEdit', {
  extend: 'Ext.form.Panel',
  xtype: 'customeraddeditform',
  id: 'customeraddedit',
  requires: [
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarEntryForm',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1TextField',
    'Ext.ux.M1.M1PhoneNumberField',
    'Ext.ux.M1.M1EmailField',
    'Ext.ux.M1.M1UrlField',
    'Ext.ux.M1.Checkbox',
    'Ext.ux.M1.M1SelectField',
    'Ext.ux.M1.M1AdvTextArea',
    'Ext.ux.M1.M1Logo',
    'Ext.ux.M1.M1SubMenuButton',
    'Ext.ux.M1.M1SubMenuDummyButton',
    'Ext.ux.M1.M1InvisibleLookupButton'
  ],
  config: {
    _optionsMenu: null,
    _mode: 'view',
    items: [
      {
        xtype: 'm1toolbarentryform',
        id: 'customeraddedit_toolbarid',
        title: 'Organisation Details',
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'customeraddeditsubtitle'
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'customeraddedit_custid',
                label: 'Org ID',
                readOnly: false,
                touppercase: true,
                required: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'customeraddedit_customer',
                label: 'Name',
                readOnly: false,
                required: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        id: 'addeditcust_addressfieldset',
        items: [

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'customeraddedit_addline1',
                label: 'Address',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'customeraddedit_addline2',
                label: ' ',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'customeraddedit_addline3',
                label: ' ',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'customeraddedit_city',
                label: 'City',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'customeraddedit_state',
                label: 'State',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'customeraddedit_postcode',
                label: 'Post Code',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'customeraddedit_country',
                label: 'Country',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        id: 'addeditcust_contactfieldset',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1phonenumberfield',
                id: 'customeraddedit_phone1',
                label: 'Phone',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1phonenumberfield',
                id: 'customeraddedit_phone2',
                label: 'Alt Phone',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1emailfield',
                id: 'customeraddedit_email',
                label: 'Email',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          /*
           {
           xtype: 'container',
           layout: 'hbox',
           items : [
           {
           xtype : 'm1urlfield',
           id : 'customeraddedit_web',
           label : 'Web',
           readOnly : false,
           scope : this
           },
           {
           xtype:'m1invisiblelookupbutton'
           }
           ]
           } */
          {
            xtype: 'm1urlfield',
            urlfieldid: 'customeraddedit_web',
            label: 'Web',
            readOnly: true,
            hasbrowsebtn: true,
            scope: this
          }
        ]
      },

      {
        xtype: 'fieldset',
        id: 'custdetail_statusfieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'customeraddedit_custstatus',
                label: 'Customer',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'customeraddedit_supplierstatus',
                label: 'Supplier',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1checkboxfield',
                id: 'customeraddedit_taxable',
                label: 'Taxable',
                checked: false,
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'customeraddedit_taxidsel',
                label: 'Tax ID',
                valueField: 'TaxCodeID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'customeraddedit_taxid',
                label: 'Tax ID',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'customeraddedit_secondtaxidsel',
                label: 'Second Tax ID',
                valueField: 'TaxCodeID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'customeraddedit_secondtaxid',
                label: 'Second Tax ID',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }


        ]
      },
      {
        xtype: 'm1advtextarea',
        textareaid: 'customeraddedit_notes',
        textarealabel: 'Long Desc',
        detaillabel: 'Long Desc',
        textareaflex: 4,
        scope: this
      },
      {
        xtype: 'm1logo'
      },
      {
        xtype: 'container',
        id: 'customeraddedit_btnop2',
        margin: 1,
        docked: 'bottom',
        layout: {
          type: 'hbox'
        },
        items: [
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-calls',
            //cls : 'm1-submenuactionbutton-crm-organisation',
            id: 'customeraddedit_calls',
            text: 'Calls',
            iconCls: 'calls',
            scope: this
          },
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-followup',
            //cls : 'm1-submenuactionbutton-crm-organisation',
            id: 'customeraddedit_followups',
            text: 'Followups',
            iconCls: 'follow_up',
            scope: this
          },
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-quotes',
            //cls : 'm1-submenuactionbutton-crm-organisation',
            id: 'customeraddedit_quotes',
            text: 'Quotes',
            iconCls: 'quotation',
            scope: this
          }
        ]
      },

      {
        xtype: 'container',
        id: 'customeraddedit_btnop1',
        margin: 1,
        docked: 'bottom',
        layout: {
          type: 'hbox'
        },
        items: [
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-locations',
            id: 'customeraddedit_locations',
            text: 'Locations',
            iconCls: 'locations',
            scope: this
          },
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-contact',
            id: 'customeraddedit_contacts',
            text: 'Contacts',
            iconCls: 'contacts',
            scope: this
          },
          {
            xtype: 'm1submenudummybutton'
          }
        ]
      }
    ]
  }

});