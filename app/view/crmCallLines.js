Ext.define('M1CRM.view.crmCallLines', {
  extend: 'Ext.dataview.List',
  xtype: 'calllines',
  id: 'calllineslist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm-calllines',
    title: 'Call Lines',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'calllineslist_toolbarid',
        title: 'Call Lines',
        includeAdd: true,
        includeFilter: true,
        includeOptions: false,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'calllinelist_searchtoolbar',
        searchfieldid: 'callline_search',
        cls: 'm1-toolbar-search-calllines',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'calllineslist_tabbar',
        titleid: 'labCallLinesSummary',
        scope: this
      }


    ],
    //store : Ext.getStore('crmCallLines'),
    //itemTpl :  Ext.create('M1CRM.model.crmCallLines', {}).getListLayout(),
    indexBar: false,
    pinHeaders: false,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }
});

