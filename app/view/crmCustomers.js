Ext.define('M1CRM.view.crmCustomers', {
  extend: 'Ext.dataview.List',
  xtype: 'customers',
  id: 'customerlist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm-org',
    title: 'Organisations',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'customerlist_toolbarid',
        title: 'Organisations',
        includeAdd: true,
        includeFilter: true,
        includeOptions: false,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'customer_searchtoolbar',
        cls: 'm1-toolbar-search-organisation',
        searchfieldid: 'customer_search',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'customerlist_tabbar',
        titleid: 'labCustSummary'
      }

    ],
    /* grouped : true,
     indexBar : true,
     detailCard : Ext.create('Ext.Container'),
     clearSelectionOnDeactivate : true,
     scrollable : 'vertical'
     */
    grouped: true,
    indexBar: true,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true,
    scrollable: 'vertical'


  }

});

