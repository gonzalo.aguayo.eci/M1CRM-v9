Ext.define('M1CRM.view.crmHome', {
  extend: 'Ext.form.Panel',
  xtype: 'homeform',
  id: 'crmhome',
  requires: [
    'Ext.TitleBar',
    'Ext.ux.M1.M1HomeToolbar',
    'Ext.ux.M1.M1MainMenuButton',
    'Ext.ux.M1.M1MainMenuButton2',
    'Ext.ux.M1.M1MainMenuDummyButtonsButton'
  ],
  config: {
    height: '100%',
    width: '100%',
    fullscreen: true,
    scroll: 'vertical',

    items: [
      {
        xtype: 'm1hometoolbar',
        scope: this,
        itemId: 'crmhome_toolbartitle'
      },

      {
        xtype: 'container',
        layout: {
          type: 'hbox',
          align: 'center',
          pack: 'center'
        },
        items: [
          {
            xtype: 'mainmenubutton',
            id: 'crmhome_myopenfollowups',
            text: 'Follow-Ups',
            iconCls: 'followups',
            rowid: 1,
            scope: this
          },
          {
            xtype: 'mainmenubutton2',
            id: 'crmhome_addnewfollowup',
            iconCls: 'plus',
            rowid: 1,
            scope: this
          }
        ]
      },

      {
        xtype: 'container',
        layout: {
          type: 'hbox',
          align: 'center',
          pack: 'center'
        },
        items: [
          {
            xtype: 'mainmenubutton',
            id: 'crmhome_myopencalls',
            text: 'Calls',
            iconCls: 'calls',
            rowid: 2,
            scope: this
          },
          {
            xtype: 'mainmenubutton2',
            id: 'crmhome_addnewcall',
            iconCls: 'plus',
            rowid: 2,
            scope: this
          }

        ]
      },

      {
        xtype: 'container',
        layout: {
          type: 'hbox',
          align: 'center',
          pack: 'center'
        },
        items: [
          {
            xtype: 'mainmenubutton',
            id: 'crmhome_contacts',
            text: 'Contacts',
            iconCls: 'contacts',
            rowid: 3,
            scope: this
          },
          {
            xtype: 'mainmenubutton2',
            id: 'crmhome_addnewcontact',
            iconCls: 'plus',
            rowid: 3,
            scope: this
          }
        ]
      },

      {
        xtype: 'container',
        layout: {
          type: 'hbox',
          align: 'center',
          pack: 'center'
        },
        items: [
          {
            xtype: 'mainmenubutton',
            id: 'crmhome_searchorg',
            text: 'Organisations',
            iconCls: 'customers',
            rowid: 4,
            scope: this
          },
          {
            xtype: 'mainmenubutton2',
            itemId: 'crmhome_addneworg',
            id: 'crmhome_addneworg',
            iconCls: 'plus',
            rowid: 4,
            scope: this
          }
        ]
      },


      {
        xtype: 'container',
        layout: {
          type: 'hbox',
          align: 'center',
          pack: 'center'
        },
        items: [
          {
            xtype: 'mainmenubutton',
            id: 'crmhome_myopenquotes',
            text: 'Quotes',
            iconCls: 'quotes',
            rowid: 5,
            scope: this
          },
          {
            xtype: 'mainmenubutton2',
            id: 'crmhome_addnewquote',
            iconCls: 'plus',
            rowid: 5,
            scope: this
          }
        ]
      },
      {
        xtype: 'container',
        layout: {
          type: 'hbox',
          align: 'center',
          pack: 'center'
        },
        items: [
          {
            xtype: 'mainmenubutton',
            id: 'crmhome_myopenleads',
            text: 'Leads',
            iconCls: 'leads',
            rowid: 6,
            scope: this
          },

          {
            xtype: 'mainmenubutton2',
            id: 'crmhome_addnewlead',
            iconCls: 'plus',
            rowid: 6,
            scope: this
          }

        ]
      },

      {
        xtype: 'container',
        layout: {
          type: 'hbox',
          align: 'center',
          pack: 'center'
        },
        items: [
          {
            xtype: 'mainmenubutton',
            id: 'crmhome_searchparts',
            text: 'Parts',
            iconCls: 'parts',
            rowid: 7,
            scope: this
          },
          {
            xtype: 'mainmenubutton2',
            id: 'crmhome_logout',
            iconCls: 'exit',
            rowid: 6,
            scope: this
          }
        ]
      },
      {
        xtype: 'm1logo'
      }
    ]
  }
});
