Ext.define('M1CRM.view.crmAllOpenQuotes', {
  extend: 'Ext.dataview.List',
  xtype: 'allopenquotes',
  id: 'allopenquoteslist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _optionsMenu: null,
    _parentformid: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm',
    title: 'All Open Quotes',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'allopenquoteslist_toolbarid',
        title: 'Open Quotes',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'allopenquotes_searchtoolbar',
        searchfieldid: 'allopenquotes_search',
        cls: 'm1-toolbar-search-quotes',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'allopenquoteslist_tabbar',
        titleid: 'labAllOpenQuotesSummary',
        scope: this
      }
    ],
    //itemTpl :  Ext.create('M1CRM.model.crmQuotations', {}).getAllOpenQuoteLayout(),
    grouped: false,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

