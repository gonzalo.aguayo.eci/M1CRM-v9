Ext.define('M1CRM.view.crmCustomerOpenCalls', {
  extend: 'Ext.dataview.List',
  requires: [
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  xtype: 'customeropencalls',
  id: 'customeropencallslist',
  config: {
    customeropencall_optionsMenu: null,
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    title: 'Customer Open Calls',
    cls: 'm1-list-crm-call',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'customeropencallslist_toolbarid',
        title: 'Customer Open Calls',
        includeAdd: true,
        includeFilter: true,
        includeOptions: false,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'customeropencall_searchtoolbar',
        searchfieldid: 'customeropencall_search',
        cls: 'm1-toolbar-search-calls',
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'customeropencallslist_summary',
        cls: 'm1-listsubtitle-crm-calls'
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'customeropencallslist_tabbar',
        titleid: 'labCustOpenCallsSummary',
        scope: this
      }
    ],
    //store : Ext.getStore('crmCalls'),
    //itemTpl :  Ext.create('M1CRM.model.crmCalls', {}).getMyOpenCallsListLayout(),
    grouped: true,
    indexBar: false,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

