Ext.define('M1CRM.view.crmCustomerLocations', {
  extend: 'Ext.dataview.List',
  xtype: 'customerlocations',
  id: 'customerlocationlist',
  requires: [
    //'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,

    cls: 'm1-list-crm-locations',
    title: 'Organisation Locations',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'customerlocationlist_toolbarid',
        title: 'Organisation Locations',
        includeAdd: true,
        includeFilter: true,
        includeOptions: false,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'custlocations_searchtoolbar',
        searchfieldid: 'customerlocation_search',
        placeHolderText: 'search locations',
        cls: 'm1-toolbar-search-locations',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'customerlocationlist_tabbar',
        titleid: 'labCustLocationSummary',
        scope: this
      }


    ],
    grouped: true,
    indexBar: false,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

