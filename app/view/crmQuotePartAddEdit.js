Ext.define('M1CRM.view.crmQuotePartAddEdit', {
  extend: 'Ext.form.Panel',
  xtype: 'addeditquotepartform',
  id: 'addeditquotepart',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.field.Hidden',
    'Ext.ux.M1.M1ToolbarEntryForm',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1TextField',
    'Ext.ux.M1.Checkbox',
    'Ext.ux.M1.M1NumberField',
    'Ext.ux.M1.M1SelectField',
    'Ext.ux.M1.M1AdvTextArea',
    'Ext.ux.M1.M1DatePicker',
    'Ext.ux.M1.M1SubMenuButton',
    'Ext.ux.M1.M1AdvTextField',
    'Ext.ux.M1.M1InfoButton',
    'Ext.ux.M1.M1InvisibleLookupButton',
    'Ext.ux.M1.M1QuoteQtyAddClear',
    'Ext.ux.M1.M1Logo'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    items: [
      {
        xtype: 'hiddenfield',
        id: 'addeditquotepart_quoteid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditquotepart_partgroupid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditquotepart_quotelineid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditquotepart_tempquoteid',
        name: 'addeditquotepart_tempquoteid'
      },

      {
        xtype: 'hiddenfield',
        id: 'addeditquotepart_tempquotlineid',
        name: 'addeditquotepart_tempquot'
      },

      {
        xtype: 'hiddenfield',
        id: 'addeditquotepart_qtyid1'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditquotepart_qtyid2'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditquotepart_qtyid3'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditquotepart_qtyid4'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditquotepart_qtyid5'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditquotepart_qtyid6'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditquotepart_qtyid7'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditquotepart_qtyid8'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditquotepart_qtyid9'
      },
      {
        xtype: 'm1toolbarentryform',
        id: 'addeditquotepart_toolbarid',
        title: 'Quote Line',
        includeOptions: false,
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'addeditquotepartsubtitle'
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'm1advtextfield',
            textfieldid: 'addeditquotepart_partid',
            label: 'Part ID',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            required: true,
            infoarray: ['part', 'leadlineaddedit', null, null, null, null, null, null, 'addeditquotepart_partid', 'addeditquotepart_partrevision', null],
            scope: this
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditquotepart_partrevision',
                label: 'Rev',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditquotepart_partdesc',
                label: 'Desc',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditquotepart_uom',
                label: 'UoM',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1checkboxfield',
                id: 'addeditquotepart_firm',
                label: 'Firm?',
                checked: false,
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        id: 'addeditquotepart_taxinfofieldset',
        items: [

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditquotepart_taxid',
                label: 'Tax ID',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'addeditquotepart_taxidsel',
                label: 'Tax ID',
                valueField: 'TaxCodeID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditquotepart_taxid2',
                label: 'Tax ID2',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'addeditquotepart_taxid2sel',
                label: 'Tax ID2',
                valueField: 'TaxCodeID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditquotepart_nontaxreasonid',
                label: 'NonTax Reason',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'addeditquotepart_nontaxreasonidsel',
                label: 'NonTax Reason',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }


            ]
          }
        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [


          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_quoteqty1',
                label: '1st Quantity',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitprice1',
                label: 'Full Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_discount1',
                label: 'Discount%',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitdiscount1',
                label: 'Unit Discount',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }

            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_revisedunitprice1',
                label: 'Revised Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitfirsttax1',
                label: 'Unit Tax Amt',
                readOnly: false,
                currecyField: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitsecondtax1',
                label: 'Addl Tax Amt',
                readOnly: false,
                currecyField: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }
        ]
      },
      {
        xtype: 'm1quoteqtyaddclear',
        id: 'addeditquotepart_qty2',
        fieldsetid: 'addeditquotepart_qty2fieldset',
        scope: this
      },
      {
        xtype: 'fieldset',
        id: 'addeditquotepart_qty2fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_quoteqty2',
                label: '2nd Quantity',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitprice2',
                label: 'Full Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_discount2',
                label: 'Discount%',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitdiscount2',
                label: 'Unit Discount',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_revisedunitprice2',
                label: 'Revised Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitfirsttax2',
                label: 'Unit Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitsecondtax2',
                label: 'Addl Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }


        ]
      },


      {
        xtype: 'm1quoteqtyaddclear',
        id: 'addeditquotepart_qty3',
        fieldsetid: 'addeditquotepart_qty3fieldset',
        scope: this
      },


      {
        xtype: 'fieldset',
        id: 'addeditquotepart_qty3fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_quoteqty3',
                label: '3rd Quantity',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitprice3',
                label: 'Full Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_discount3',
                label: 'Discount%',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitdiscount3',
                label: 'Unit Discount',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_revisedunitprice3',
                label: 'Revised Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitfirsttax3',
                label: 'Unit Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitsecondtax3',
                label: 'Addl Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }


        ]
      },

      {
        xtype: 'm1quoteqtyaddclear',
        id: 'addeditquotepart_qty4',
        fieldsetid: 'addeditquotepart_qty4fieldset',
        scope: this
      },


      {
        xtype: 'fieldset',
        id: 'addeditquotepart_qty4fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_quoteqty4',
                label: '4th Quantity',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitprice4',
                label: 'Full Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_discount4',
                label: 'Discount%',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitdiscount4',
                label: 'Unit Discount',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_revisedunitprice4',
                label: 'Revised Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitfirsttax4',
                label: 'Unit Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitsecondtax4',
                label: 'Addl Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }


        ]
      },
      {
        xtype: 'm1quoteqtyaddclear',
        id: 'addeditquotepart_qty5',
        fieldsetid: 'addeditquotepart_qty5fieldset',
        scope: this
      },
      {
        xtype: 'fieldset',
        id: 'addeditquotepart_qty5fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_quoteqty5',
                label: '5th Quantity',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitprice5',
                label: 'Full Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_discount5',
                label: 'Discount%',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitdiscount5',
                label: 'Unit Discount',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_revisedunitprice5',
                label: 'Revised Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitfirsttax5',
                label: 'Unit Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitsecondtax5',
                label: 'Addl Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }


        ]
      },
      {
        xtype: 'm1quoteqtyaddclear',
        id: 'addeditquotepart_qty6',
        fieldsetid: 'addeditquotepart_qty6fieldset',
        scope: this
      },
      {
        xtype: 'fieldset',
        id: 'addeditquotepart_qty6fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_quoteqty6',
                label: '6th Quantity',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitprice6',
                label: 'Full Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_discount6',
                label: 'Discount%',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitdiscount6',
                label: 'Unit Discount',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_revisedunitprice6',
                label: 'Revised Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitfirsttax6',
                label: 'Unit Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitsecondtax6',
                label: 'Addl Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }


        ]
      },
      {
        xtype: 'm1quoteqtyaddclear',
        id: 'addeditquotepart_qty7',
        fieldsetid: 'addeditquotepart_qty7fieldset',
        scope: this
      },
      {
        xtype: 'fieldset',
        id: 'addeditquotepart_qty7fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_quoteqty7',
                label: '7th Quantity',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitprice7',
                label: 'Full Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_discount7',
                label: 'Discount%',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitdiscount7',
                label: 'Unit Discount',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_revisedunitprice7',
                label: 'Revised Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitfirsttax7',
                label: 'Unit Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitsecondtax7',
                label: 'Addl Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }


        ]
      },

      {
        xtype: 'm1quoteqtyaddclear',
        id: 'addeditquotepart_qty8',
        fieldsetid: 'addeditquotepart_qty8fieldset',
        scope: this
      },
      {
        xtype: 'fieldset',
        id: 'addeditquotepart_qty8fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_quoteqty8',
                label: '8th Quantity',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitprice8',
                label: 'Full Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_discount8',
                label: 'Discount%',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitdiscount8',
                label: 'Unit Discount',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_revisedunitprice8',
                label: 'Revised Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitfirsttax8',
                label: 'Unit Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitsecondtax8',
                label: 'Addl Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }


        ]
      },

      {
        xtype: 'm1quoteqtyaddclear',
        id: 'addeditquotepart_qty9',
        fieldsetid: 'addeditquotepart_qty9fieldset',
        scope: this
      },
      {
        xtype: 'fieldset',
        id: 'addeditquotepart_qty9fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_quoteqty9',
                label: '9th Quantity',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitprice9',
                label: 'Full Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_discount9',
                label: 'Discount%',
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepartqty_unitdiscount9',
                label: 'Unit Discount',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_revisedunitprice9',
                label: 'Revised Unit Price',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitfirsttax9',
                label: 'Unit Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1numberfield',
                id: 'addeditquotepart_unitsecondtax9',
                label: 'Addl Tax Amt',
                currecyField: true,
                readOnly: false,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }


        ]
      },

      {
        xtype: 'm1logo'
      }


    ]
  }
});
