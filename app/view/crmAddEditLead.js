Ext.define('M1CRM.view.crmAddEditLead', {
  extend: 'Ext.form.Panel',
  xtype: 'addeditleadform',
  id: 'addeditlead',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.field.Hidden',
    'Ext.ux.M1.M1ToolbarEntryForm',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1TextField',
    'Ext.ux.M1.M1NumberField',
    'Ext.ux.M1.M1SelectField',
    'Ext.ux.M1.M1AdvTextArea',
    'Ext.ux.M1.M1DatePicker',
    'Ext.ux.M1.M1SubMenuButton',
    'Ext.ux.M1.M1AdvTextField',
    'Ext.ux.M1.M1InfoButton',
    'Ext.ux.M1.M1InvisibleLookupButton',
    'Ext.ux.M1.M1Logo'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _callcontrol: null,
    _mode: 'view',
    items: [
      {
        xtype: 'hiddenfield',
        id: 'addeditlead_leadid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditlead_custid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditlead_invlocationid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditlead_leadcontactid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditlead_quotelocationid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditlead_quotecontactid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditlead_shipcustid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditlead_shiplocationid'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditlead_shipcontactid'
      },

      {
        xtype: 'm1toolbarentryform',
        id: 'addeditlead_toolbarid',
        title: 'Add Lead',
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'addeditleadsubtitle'
      },
      {
        xtype: 'hiddenfield',
        id: 'addeditlead_milestoneddatesel'
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [

          {
            xtype: 'm1advtextfield',
            textfieldid: 'addeditlead_customer',
            label: 'Customer',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            required: true,
            infoarray: ['customer', 'addeditlead', 'addeditlead_custid', 'addeditlead_customer',
              null, null, null, null, null, null, null],
            scope: this
          },
          {
            xtype: 'm1advtextfield',
            textfieldid: 'addeditlead_invlocation',
            label: 'Inv Loc',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            infoarray: ['location', 'addeditlead', 'addeditlead_custid', 'addeditlead_customer',
              'addeditlead_invlocationid', 'addeditlead_invlocation', null, null, null, null, null],
            scope: this
          },


          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_leadcontact',
                label: 'Contact',
                readOnly: true,
                scope: this
              },

              {
                xtype: 'm1selectfield',
                id: 'addeditlead_leadcontactsel',
                label: 'Contact',
                valueField: 'ContactID',
                displayField: 'ContactName',
                flex: 4,
                scope: this
              },

              {
                xtype: 'm1infobutton',
                id: 'addeditlead_leadcontactinfo',
                iconCls: 'm1info',
                infotype: 'contact',
                parentformid: 'addeditlead',
                infoarray: ['addeditlead_custid', 'addeditlead_customer', 'addeditlead_invlocationid', 'addeditlead_invlocation', 'addeditlead_leadcontactid', 'addeditlead_leadcontact', null, null, null],
                scope: this
              }
            ]
          }
        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [

          {
            xtype: 'm1advtextfield',
            textfieldid: 'addeditlead_quotelocation',
            label: 'Quote Loc',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            infoarray: ['location', 'addeditlead', 'addeditlead_custid', 'addeditlead_customer', 'addeditlead_quotelocationid', 'addeditlead_quotelocation', null, null, null, null, null],
            scope: this
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_quotecontact',
                label: 'Quote Contact',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'addeditlead_quotecontactsel',
                label: 'Quote Contact',
                valueField: 'ContactID',
                displayField: 'ContactName',
                flex: 4,
                scope: this
              },

              {
                xtype: 'm1infobutton',
                id: 'addeditlead_quotecontactinfo',
                infotype: 'contact',
                parentformid: 'addeditlead',
                infoarray: ['addeditlead_custid', 'addeditlead_customer', 'addeditlead_quotelocationid', 'addeditlead_quotelocation', 'addeditlead_quotecontactid', 'addeditlead_quotecontact', null, null, null],
                scope: this
              }
            ]
          },
          {
            xtype: 'm1advtextfield',
            textfieldid: 'addeditlead_shipcustomer',
            label: 'Ship Org',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            required: true,
            infoarray: ['customer', 'addeditlead', 'addeditlead_shipcustid', 'addeditlead_shipcustomer', null, null, null, null, null, null, null],
            scope: this
          },
          {
            xtype: 'm1advtextfield',
            textfieldid: 'addeditlead_shiplocation',
            label: 'Ship Loc',
            readOnly: true,
            touppercase: true,
            haslookupbtn: true,
            hasinfobtn: true,
            infoarray: ['location', 'addeditlead', 'addeditlead_shipcustid', 'addeditlead_shipcustomer', 'addeditlead_shiplocationid', 'addeditlead_shiplocation', null, null, null, null, null],
            scope: this
          },


          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_shipcontact',
                label: 'Ship Contact',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'addeditlead_shipcontactsel',
                label: 'Ship Contact',
                valueField: 'ContactID',
                displayField: 'ContactName',
                flex: 4,
                scope: this
              },

              {
                xtype: 'm1infobutton',
                id: 'addeditlead_shipcontactinfo',
                infotype: 'contact',
                parentformid: 'addeditlead',
                infoarray: ['addeditlead_shipcustid', 'addeditlead_shipcustomer', 'addeditlead_shiplocationid', 'addeditlead_shiplocation', 'addeditlead_shipcontactid', 'addeditlead_shipcontact', null, null, null],
                scope: this
              }
            ]
          }


        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_shortdesc',
                label: 'Desc',
                readOnly: true,
                required: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'm1advtextarea',
            textareaid: 'addeditlead_ldesc',
            textarealabel: 'Long Desc',
            detaillabel: 'Long Desc',
            textareaflex: 4,
            scope: this
          }
        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditlead_responsemethodsel',
                label: 'Response Method',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditlead_responsemethod',
                label: 'Response Method',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditlead_marketingprogsel',
                label: 'Marketing Prog',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditlead_marketingprog',
                label: 'Marketing Prog',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_referedby',
                label: 'Referred By',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1selectfield',
                id: 'addeditlead_quotersel',
                label: 'Quoter',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                required: true,
                scope: this
              },
              {
                xtype: 'm1textfield',
                id: 'addeditlead_quoter',
                label: 'Quoter',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }

        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_leaddate',
                label: 'Lead Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1datepicker',
                id: 'addeditlead_leaddatesel',
                label: 'Lead Date',
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_expecteddate',
                label: 'Due Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1datepicker',
                id: 'addeditlead_expecteddatesel',
                label: 'Due Date',
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_expdate',
                label: 'Exp. Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1datepicker',
                id: 'addeditlead_expdatesel',
                label: 'Exp. Date',
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }

        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [

              {
                xtype: 'm1selectfield',
                id: 'addeditlead_milestonesel',
                label: 'Milestone',
                valueField: 'ID',
                displayField: 'Description',
                required: true,
                flex: 4,
                scope: this
              },

              {
                xtype: 'm1textfield',
                id: 'addeditlead_milestone',
                label: 'Milestone',
                readOnly: true,
                required: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }

            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_milestoneddate',
                label: 'Milestone Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_leadtotal',
                currecyField: true,
                label: 'Lead Total',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [

              {
                xtype: 'm1numberfield',
                id: 'addeditlead_confidence',
                label: 'Confidence',
                readOnly: true,
                scope: this
              },

              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_expectrevenue',
                currecyField: true,
                label: 'Expect Revenue',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }


        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_status',
                label: 'Status',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'addeditlead_statussel',
                label: 'Status',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },
          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_closedreason',
                label: 'Closed Reason',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'addeditlead_closedreasonsel',
                label: 'Closed Reason',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_closedby',
                label: 'Closed By',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1selectfield',
                id: 'addeditlead_closedbysel',
                label: 'Closed By',
                valueField: 'ID',
                displayField: 'Description',
                flex: 4,
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          },

          {
            xtype: 'container',
            layout: 'hbox',
            items: [
              {
                xtype: 'm1textfield',
                id: 'addeditlead_closeddate',
                label: 'Closed Date',
                readOnly: true,
                scope: this
              },
              {
                xtype: 'm1datepicker',
                id: 'addeditlead_closeddatesel',
                label: 'Closed Date',
                scope: this
              },
              {
                xtype: 'm1invisiblelookupbutton'
              }
            ]
          }

        ]
      },

      {
        xtype: 'm1logo'
      },


      {
        xtype: 'container',
        id: 'addeditlead_btnop1',
        margin: 1,
        docked: 'bottom',
        layout: {
          type: 'hbox'
        },
        items: [
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-followup',
            id: 'addeditlead_leadfollowups',
            text: 'Followups',
            iconCls: 'follow_up',
            scope: this
          },
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-calls',
            id: 'addeditlead_leadcalls',
            text: 'Calls',
            iconCls: 'calls',
            scope: this
          }
        ]
      },

      {
        xtype: 'container',
        id: 'addeditlead_btnop2',
        margin: 1,
        docked: 'bottom',
        layout: {
          type: 'hbox'
        },
        items: [
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-leadlines',
            id: 'addeditlead_leadlines',
            text: 'Lead Lines',
            iconCls: 'leadlines',
            scope: this
          },
          {
            xtype: 'm1submenubutton',
            cls: 'm1-submenuactionbutton-crm-quotes',
            id: 'addeditlead_createquote',
            text: 'Create Quote',
            iconCls: 'quotation',
            scope: this
          }
        ]
      }


    ]
  }
});
