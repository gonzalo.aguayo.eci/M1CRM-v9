Ext.define('M1CRM.view.crmPartDetail', {
  extend: 'Ext.form.Panel',
  xtype: 'partdateilform',
  id: 'crmpartdateil',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    //'Ext.ux.M1.M1ToolbarWithNoOptionsNoAdd',
    'Ext.ux.M1.M1ToolbarEntryForm',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1TextField',
    'Ext.ux.M1.M1BasicTextArea',
    'Ext.ux.M1.M1FormButton'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _mode: 'view',
    items: [
      {
        xtype: 'hiddenfield',
        id: 'crmpartdateil_partgroupid'
      },
      {
        xtype: 'm1toolbarentryform',
        id: 'crmpartdateil_toolbarid',
        title: 'Part Information',
        includeSave: false,
        includeOptions: false,
        includeAddNew: false,
        includeSubTitle: false,
        scope: this
      },
      /*{
       xtype:'m1subtitlebar',
       titlelabelid : 'crmpartdateilsubtitle'
       },*/
      {
        xtype: 'fieldset',
        id: 'partdetail_orginfoset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'm1textfield',
            id: 'partdetail_orgid',
            label: 'Org',
            readOnly: true,
            scope: this
          }
        ]
      },
      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'm1textfield',
            id: 'partid',
            label: 'Part ID',
            readOnly: true,
            scope: this
          },

          {
            xtype: 'm1textfield',
            id: 'revision',
            label: 'Revision',
            readOnly: true,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'description',
            label: 'Desc.',
            readOnly: true,
            scope: this
          },
          {
            xtype: 'm1basictextarea',
            id: 'crmpartdateil_longdescription',
            label: 'Long Description',
            readOnly: true,
            scope: this
          }
        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        items: [
          {
            xtype: 'm1textfield',
            id: 'uominv',
            label: 'Inv. UOM',
            readOnly: true,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'unitsaleprice',
            currecyField: true,
            label: 'Sell Price',
            readOnly: true,
            scope: this
          }
        ]
      },

      {
        xtype: 'fieldset',
        cls: 'm1-fieldsetlabel-crm',
        //title: 'Stock Info',
        items: [
          {
            xtype: 'm1textfield',
            id: 'qtyonhand',
            label: 'On Hand',
            readOnly: true,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'qtyallocated',
            label: 'Allocated',
            readOnly: true,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'qtyonsale',
            label: 'Qty On Order (Sales)',
            readOnly: true,
            scope: this
          },
          {
            xtype: 'm1textfield',
            id: 'qtyonpurchase',
            label: 'Qty On Order (Purchase)',
            readOnly: true,
            scope: this
          }
        ]
      },
      {
        xtype: 'm1logo'
      },
      {
        xtype: 'container',
        margin: 5,
        docked: 'bottom',
        layout: {
          type: 'hbox'
        },
        items: [
          {
            xtype: 'formbutton',
            id: 'btnlinkpart_createquotation',
            text: 'Create Quotation',
            iconCls: 'save',
            scope: this
          }
        ]
      }
    ]
  }
});
