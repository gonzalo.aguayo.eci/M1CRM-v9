Ext.define('M1CRM.view.crmMyOpenLeads', {
  extend: 'Ext.dataview.List',
  xtype: 'myopenleads',
  id: 'myopenleadslist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _optionsMenu: null,
    _parentformid: null,
    _pageid: null,
    _totalRecords: null,
    cls: 'm1-list-crm-leads',
    title: 'My Open Leads',
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'myopenleadslist_toolbarid',
        title: 'My Open Leads',
        includeAdd: true,
        includeFilter: true,
        includeOptions: true,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'myopenleads_searchtoolbar',
        searchfieldid: 'myopenleads_search',
        cls: 'm1-toolbar-search-leads',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'myopenleadslist_tabbar',
        titleid: 'labMyOpenLeadsSummary',
        scope: this
      }
    ],
    grouped: false,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true
  }

});

