Ext.define('M1CRM.view.crmContacts', {
  extend: 'Ext.dataview.List',
  xtype: 'contacts',
  id: 'contactlist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _parentformid: null,
    _detailform: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,
    cls: 'm1-list-crm-contacts',
    title: 'Active Contacts',
    items: [

      {
        xtype: 'm1toolbarlistview',
        id: 'contactlist_toolbarid',
        title: 'Active Contacts',
        includeAdd: true,
        includeFilter: true,
        includeOptions: false,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'contact_searchtoolbar',
        searchfieldid: 'contact_search',
        cls: 'm1-toolbar-search-contacts',
        scope: this
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'contactlist_tabbar',
        titleid: 'labContactsSummary',
        scope: this
      }
    ],
    /* grouped : true,
     detailCard : Ext.create('Ext.Container'),
     clearSelectionOnDeactivate : true,
     scrollable : 'vertical'
     */
    grouped: false,
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true,
    scrollable: 'vertical'


  }

});

