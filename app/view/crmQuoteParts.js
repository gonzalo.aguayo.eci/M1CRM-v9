Ext.define('M1CRM.view.crmQuoteParts', {
  extend: 'Ext.dataview.List',
  xtype: 'quoteparts',
  id: 'quotepartslist',
  requires: [
    'Ext.TitleBar',
    'Ext.tab.Panel',
    'Ext.ux.M1.M1ToolbarListView',
    'Ext.ux.M1.M1SubTitleBar',
    'Ext.ux.M1.M1ToolbarListSearch',
    'Ext.ux.M1.M1ToolbarListNavigation'
  ],
  config: {
    _parentformid: null,
    _detailformid: null,
    _optionsMenu: null,
    _pageid: null,
    _totalRecords: null,
    _haslistchange: true,
    _sessionid: null,

    cls: 'm1-list-crm',
    //itemTpl :  Ext.create('M1CRM.model.crmQuoteParts', {}).getQuotePartsLayout(),  
    indexBar: false,
    pinHeaders: true,
    detailCard: Ext.create('Ext.Container'),
    clearSelectionOnDeactivate: true,
    items: [
      {
        xtype: 'm1toolbarlistview',
        id: 'quotepartslist_toolbarid',
        title: 'Quote Lines',
        includeAdd: true,
        includeFilter: true,
        includeOptions: false,
        scope: this
      },
      {
        xtype: 'm1toolbarlistsearch',
        id: 'quoteparts_searchtoolbar',
        searchfieldid: 'quoteparts_search',
        cls: 'm1-toolbar-search-quote-lines',
        scope: this
      },
      {
        xtype: 'm1subtitlebar',
        titlelabelid: 'quotepartslist_summary',
        cls: 'm1-listsubtitle-crm-quote-lines'
      },
      {
        xtype: 'm1toolbarlistnavigation',
        id: 'quotepartslist_tabbar',
        titleid: 'labQuoteLinesSummary',
        scope: this
      }
    ]
  }
});

