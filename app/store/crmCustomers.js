Ext.define("M1CRM.store.crmCustomers", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmCustomers'],
  config: {
    model: 'M1CRM.model.crmCustomers',
    sorters: [
      {property: 'OrganizationName', direction: 'ASC'}
    ],
    grouper: {
      groupFn: function (record) {
        return record.get('OrganizationName').substr(0, 1).toString().toUpperCase();
      }
      //sortProperty: 'OrganizationName'
    },
    autoLoad: false,
    groupDir: 'ASC'
  }

});
	
  