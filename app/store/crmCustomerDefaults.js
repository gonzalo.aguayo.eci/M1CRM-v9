Ext.define("M1CRM.store.crmCustomerDefaults", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmCustomerDefaults'],
  config: {
    model: 'M1CRM.model.crmCustomerDefaults',
    autoLoad: false
  }

});
	
  