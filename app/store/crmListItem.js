Ext.define("M1CRM.store.crmListItem", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmListItem'],
  config: {
    model: 'M1CRM.model.crmListItem',
    autoLoad: false
  }
});
	
  