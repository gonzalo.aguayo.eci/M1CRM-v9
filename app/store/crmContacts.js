Ext.define("M1CRM.store.crmContacts", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmContacts'],
  config: {
    model: 'M1CRM.model.crmContacts',
    sorters: [
      {property: 'Name', direction: 'ASC'}
    ],
    grouper: {
      groupFn: function (record) {
        return record.get('Name').substr(0, 1).toUpperCase();
      }
    },
    autoLoad: false,
    groupDir: 'ASC'
  }

});
	
  