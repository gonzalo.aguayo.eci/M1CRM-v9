Ext.define("M1CRM.store.crmValueList", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmValueList'],
  config: {
    model: 'M1CRM.model.crmValueList',
    autoLoad: false
  }
});
	