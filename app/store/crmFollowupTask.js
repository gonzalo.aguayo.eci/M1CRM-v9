Ext.define("M1CRM.store.crmFollowupTask", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmFollowupTask'],
  config: {
    model: 'M1CRM.model.crmFollowupTask',
    autoLoad: false
  }
});
	