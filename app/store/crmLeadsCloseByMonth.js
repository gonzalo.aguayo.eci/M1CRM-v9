Ext.define("M1CRM.store.crmLeadsCloseByMonth", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmLeads'],
  config: {
    model: 'M1CRM.model.crmLeads',
    sorters: [
      {property: 'ExpectedCloseDate', direction: 'ASC'},
      {property: 'LeadID', direction: 'ASC'}
    ],
    grouper: {
      groupFn: function (record) {
        return record.get('OrganizationID');
      }
    },
    autoLoad: false,
    groupDir: 'ASC'
  }
});
 