Ext.define("M1CRM.store.crmParts", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmParts'],
  config: {
    model: 'M1CRM.model.crmParts',
    autoLoad: false
  }

});
 