Ext.define("M1CRM.store.crmCountry", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmCountry'],
  config: {
    model: 'M1CRM.model.crmCountry',
    autoLoad: false
  }
});
	
  