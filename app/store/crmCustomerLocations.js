Ext.define("M1CRM.store.crmCustomerLocations", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmCustomerLocations'],
  config: {
    model: 'M1CRM.model.crmCustomerLocations',
    sorters: [
      {property: 'Address.State', direction: 'ASC'}
    ],
    grouper: {
      groupFn: function (record) {
        record.get('Address').State != '' ? record.get('Address').State.substr(0, 1).toString().toUpperCase() : '';
      }//,
      //sortProperty: 'Address.State' 
    },
    autoLoad: false
  }

});
	
  