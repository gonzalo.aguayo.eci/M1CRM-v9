Ext.define("M1CRM.store.crmDataSet", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmDataSet'],
  config: {
    model: 'M1CRM.model.crmDataSet',
    syncRemovedRecords: false
  }
});
	
  