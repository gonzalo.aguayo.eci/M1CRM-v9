Ext.define("M1CRM.store.crmCallTypes", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmCallTypes'],
  config: {
    model: 'M1CRM.model.crmCallTypes',
    autoLoad: true
  }
});
	
  