Ext.define("M1CRM.store.crmLeadsMarkedAsFollowups", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmLeads'],
  config: {
    model: 'M1CRM.model.crmLeads',
    sorters: [
      {property: 'StartDate', direction: 'ASC'},
      {property: 'DueDate', direction: 'ASC'}
    ],
    grouper: {
      groupFn: function (record) {
        return record.get('OrganizationID');
      }
    },
    autoLoad: false,
    groupDir: 'ASC'
  }
});
 