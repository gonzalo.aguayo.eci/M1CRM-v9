Ext.define("M1CRM.store.crmQuoteProperties", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmQuoteProperties'],
  config: {
    model: 'M1CRM.model.crmQuoteProperties'
  }
});
 