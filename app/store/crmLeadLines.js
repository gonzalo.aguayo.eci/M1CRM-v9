Ext.define("M1CRM.store.crmLeadLines", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmLeadLines'],
  config: {
    model: 'M1CRM.model.crmLeadLines',
    sorters: [
      {property: 'LeadLineID', direction: 'ASC'}
    ],
    autoLoad: false
  }
});
 