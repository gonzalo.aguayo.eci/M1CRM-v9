Ext.define("M1CRM.store.crmStatus", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmStatus'],
  config: {
    model: 'M1CRM.model.crmStatus',
    autoLoad: false
  }
});
	
  