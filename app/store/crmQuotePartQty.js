Ext.define("M1CRM.store.crmQuotePartQty", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmQuotePartQty'],
  config: {
    model: 'M1CRM.model.crmQuotePartQty',
    autoLoad: false
  }
});
 