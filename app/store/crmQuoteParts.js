Ext.define("M1CRM.store.crmQuoteParts", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmQuoteParts'],
  config: {
    model: 'M1CRM.model.crmQuoteParts',
    autoLoad: false
  }
});
 