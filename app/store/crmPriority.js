Ext.define("M1CRM.store.crmPriority", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmPriority'],
  config: {
    model: 'M1CRM.model.crmPriority',
    autoLoad: false
  }

});
 