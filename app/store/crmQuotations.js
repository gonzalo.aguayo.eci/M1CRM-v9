Ext.define("M1CRM.store.crmQuotations", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmQuotations'],
  config: {
    model: 'M1CRM.model.crmQuotations',
    sorters: [{property: 'QuoteDate', direction: 'ASC'}
    ],
    autoLoad: false
  }

});
 