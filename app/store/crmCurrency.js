Ext.define("M1CRM.store.crmCurrency", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmCurrency'],
  config: {
    model: 'M1CRM.model.crmCurrency',
    autoLoad: false
  }
});
	
  