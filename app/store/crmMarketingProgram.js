Ext.define("M1CRM.store.crmMarketingProgram", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmMarketingProgram'],
  config: {
    model: 'M1CRM.model.crmMarketingProgram',
    autoLoad: false
  }

});
 