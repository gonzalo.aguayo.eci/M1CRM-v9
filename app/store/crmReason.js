Ext.define("M1CRM.store.crmReason", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmReason'],
  config: {
    model: 'M1CRM.model.crmReason',
    autoLoad: false
  }

});
 