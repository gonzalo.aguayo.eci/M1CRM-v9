Ext.define("M1CRM.store.crmExceptionHandler", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmExceptionHandler'],
  config: {
    model: 'M1CRM.model.crmExceptionHandler'
  }

});
 