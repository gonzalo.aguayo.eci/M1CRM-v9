Ext.define("M1CRM.store.crmContactMethod", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmContactMethod'],
  config: {
    model: 'M1CRM.model.crmContactMethod',
    autoLoad: false
  }

});
 