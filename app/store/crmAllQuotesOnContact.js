Ext.define("M1CRM.store.crmAllQuotesOnContact", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmQuotations'],
  config: {
    model: 'M1CRM.model.crmQuotations',
    sorters: [
      {property: 'Closed', direction: 'ASC'}
    ],
    grouper: {
      groupFn: function (record) {
        return parseInt(record.get('Closed')) == 0 ? 'Open' : 'Closed';
      }//,
      //sortProperty: 'Closed'
    },
    autoLoad: false

  }
});
	
  