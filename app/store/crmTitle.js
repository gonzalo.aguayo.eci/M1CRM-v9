Ext.define("M1CRM.store.crmTitle", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmTitle'],
  config: {
    model: 'M1CRM.model.crmTitle',
    autoLoad: false
  }
});
	
  