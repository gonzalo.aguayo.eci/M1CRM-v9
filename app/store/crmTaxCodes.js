Ext.define("M1CRM.store.crmTaxCodes", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmTaxCodes'],
  config: {
    model: 'M1CRM.model.crmTaxCodes',
    autoLoad: false
  }
});
	
  