Ext.define("M1CRM.store.crmCustomerContacts", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmCustomerContacts'],
  config: {
    model: 'M1CRM.model.crmCustomerContacts',
    sorters: [
      {property: 'LocationID', direction: 'ASC'}
    ],
    grouper: {
      groupFn: function (record) {
        return record.get('LocationName'); //.substr(0, 1);
      }
    },
    autoLoad: false
  }

});
	
  