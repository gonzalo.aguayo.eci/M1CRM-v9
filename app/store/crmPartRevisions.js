Ext.define("M1CRM.store.crmPartRevisions", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmPartRevision'],
  config: {
    model: 'M1CRM.model.crmPartRevision',
    autoLoad: false
  }

});
 