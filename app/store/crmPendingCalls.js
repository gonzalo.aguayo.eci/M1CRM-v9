Ext.define("M1CRM.store.crmPendingCalls", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmCalls'],
  config: {
    model: 'M1CRM.model.crmCalls',
    sorters: [{
      property: 'kbpOpenedDate',
      direction: 'ASC'
    }],
    grouper: {
      groupFn: function (record) {
        if (record.get('kbpOpenedDate').toString().indexOf('T') > -1) {
          var tempDate = new Date(record.get('kbpOpenedDate')).toJSON().substring(0, 10).replace('T', ' ');
          return formatDate(new Date(tempDate), 'MMM yyyy');
        } else {
          return record.get('kbpOpenedDate');
        }
      },
      sortProperty: 'kbpOpenedDate'
    }
  }
});
	
  