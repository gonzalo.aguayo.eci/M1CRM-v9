Ext.define("M1CRM.store.crmFollowupStatus", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmFollowupStatus'],
  config: {
    model: 'M1CRM.model.crmFollowupStatus',
    autoLoad: false
  }
});
	
  