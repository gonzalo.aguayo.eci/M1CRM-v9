Ext.define("M1CRM.store.crmMilestone", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmMilestone'],
  config: {
    model: 'M1CRM.model.crmMilestone',
    autoLoad: false
  }

});
 