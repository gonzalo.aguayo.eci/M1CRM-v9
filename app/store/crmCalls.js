Ext.define("M1CRM.store.crmCalls", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmCalls'],
  config: {
    model: 'M1CRM.model.crmCalls',
    sorters: [{property: 'StatusRanking', direction: 'ASC'},
      {property: 'cmfDueDate', direction: 'ASC'}
    ],
    grouper: {
      groupFn: function (record) {
        if (record.get('StatusRanking') == 1)
          return 'Open';
        else if (record.get('StatusRanking') == 2)
          return 'Waiting';
        else if (record.get("StatusRanking") == 3)
          return "Pending";
        else
          return 'Closed';
      }
    },
    autoLoad: false,
    groupDir: 'DESC'
  }
});
	
  