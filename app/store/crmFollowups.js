Ext.define("M1CRM.store.crmFollowups", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmFollowups'],
  config: {
    model: 'M1CRM.model.crmFollowups',
    sorters: [{property: 'Status', direction: 'ASC'},
      {property: 'cmfDueDate', direction: 'ASC'}
    ],
    grouper: {
      groupFn: function (record) {
        if (record.get('Status') == 1)
          return 'Not Started';
        else if (record.get('Status') == 2)
          return 'In Progress';
        else
          return 'Completed';
      }

    },
    autoLoad: false,
    groupDir: 'DESC'

  }
});
	
  


