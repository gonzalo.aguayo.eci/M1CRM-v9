Ext.define("M1CRM.store.crmAllCallOnContact", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmCalls'],
  config: {
    model: 'M1CRM.model.crmCalls',
    sorters: [
      {property: 'StatusRanking', direction: 'ASC'}
    ],
    grouper: {
      groupFn: function (record) {
        return record.get('CallStatus');
      }//,
      //sortProperty: 'StatusRanking'
    },
    autoLoad: false
    //groupDir: 'ASC'      
  }
});
	
  