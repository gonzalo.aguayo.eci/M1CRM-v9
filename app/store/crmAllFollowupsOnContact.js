Ext.define("M1CRM.store.crmAllFollowupsOnContact", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmFollowups'],
  config: {
    model: 'M1CRM.model.crmFollowups',
    sorters: [
      {property: 'StatusRanking', direction: 'ASC'}
    ],
    grouper: {
      groupFn: function (record) {
        return record.get('FollowupStatus');
      }//,
      //sortProperty: 'StatusRanking'
    },
    autoLoad: false

  }
});
	
  