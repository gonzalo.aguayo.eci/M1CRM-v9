Ext.define("M1CRM.store.crmStandardMessages", {
  extend: 'Ext.data.Store',
  requires: ['M1CRM.model.crmStandardMessages'],
  config: {
    model: 'M1CRM.model.crmStandardMessages',
    autoLoad: false
  }

});
 