Ext.define('M1CRM.controller.crmLeadLineAddEdit', {
  extend: 'M1CRM.util.crmFormControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmLeadLineAddEdit',
    'M1CRM.model.crmReason',
    'M1CRM.store.crmReason',
    'M1CRM.model.crmSalesOrderLines'
    //'M1CRM.model.crmPartDetail',
    //'M1CRM.model.crmPartRevision'

  ],
  config: {
    _viewmodeTitle: 'Lead Line',
    _addmodeTitle: 'Add Lead Line',
    _editmodeTitle: 'Edit Lead Line',
    _propPartsMustExist: 0,
    _tmpitemCount: 0,
    _partrevverified: null,
    refs: {
      'form': '#leadlineaddedit',
      'toolbarid': '#leadlineaddedit_toolbarid',
      'subtitle': '#leadlineaddeditsubtitle',
      'toback': '#leadlineaddedit_back',
      'tohome': '#leadlineaddedit_home',
      'addnewbtn': '#leadlineaddedit_addnew',
      'cancelbtn': '#leadlineaddedit_cancelbtn',
      'savebtn': '#leadlineaddedit_savebtn',
      'editbtn': '#leadlineaddedit_editbtn',

      'leadid': '#leadlineaddedit_leadid',
      'leadlineid': '#leadlineaddedit_leadlineid',
      'partid': '#leadlineaddedit_partid',
      'part': '#leadlineaddedit_part',
      'partlookup': '#leadlineaddedit_part_lookup',
      'partidinfo': '#leadlineaddedit_partidinfo',
      'partrevision': '#leadlineaddedit_partrevision',
      'partdesc': '#leadlineaddedit_partdesc',
      'partuom': '#leadlineaddedit_partuom',
      'qty': '#leadlineaddedit_qty',
      'unitsaleprice': '#leadlineaddedit_unitsaleprice',
      'qtyonhand': '#leadlineaddedit_qtyonhand',
      'qtyinproduction': '#leadlineaddedit_qtyinproduction',
      'qtyonorder': '#leadlineaddedit_qtyonorder',
      'grossamount': '#leadlineaddedit_grossamount',
      'grossamountdisp': '#leadlineaddedit_grossamountdisp',
      'discount': '#leadlineaddedit_discount',
      'discountdisp': '#leadlineaddedit_discountdisp',
      'discountamt': '#leadlineaddedit_discountamt',
      'discountamtdisp': '#leadlineaddedit_discountamtdisp',
      'revforcecast': '#leadlineaddedit_revforcecast',
      'revforcecastdisp': '#leadlineaddedit_revforcecastdisp',
      'revforcecastdate': '#aleadlineaddedit_revforcecastdate',
      'revforcecastdatesel': '#aleadlineaddedit_revforcecastdatesel',
      'resolutionsel': '#aleadlineaddedit_resolutionsel',
      'resolution': '#aleadlineaddedit_resolution',
      'toquote': '#aleadlineaddedit_toquote',
      'partgroupid': '#leadlineaddedit_partgroupid'
    },
    control: {
      form: {
        initialize: 'initializeForm',
        show: 'showForm',
        hide: function () {
          this.scrollFormToTop();
        }
      },
      part: {
        clearicontap: 'partCleariconTap',
        blur: function() {
          this.checkPartID();
        },
        change: function () {
          this.getPartid().setValue(this.getPart().getValue());
          this.checkPartID();
        }
      },
      partrevision: {
        blur: 'partRevisionBlur',
        keyup: function (key) {
          this.config._partrevverified = false;
        },
        change: 'partRevisionChange',
        clearicontap: function () {
          this.clearPartInfo(false);
        }
      },
      partlookup: {
        tap: 'partlookupTap'
      },
      qty: {
        blur: 'qtyChange'
      },
      grossamount: {
        blur: 'grossamountChange'
      },
      discount: {
        focus: 'discountFocus',
        blur: 'discountChange'
      },
      discountamt: {
        focus: 'discountamtFocus',
        blur: 'discountamtChange'
      },
      revforcecast: {
        blur: 'revforcecastChange'
      },
      revforcecastdatesel: {
        change: 'revforcecastdateChange'
      },
      cancelbtn: {
        tap: 'btnCancelTap'
      },
      addnewbtn: {
        tap: 'btnAddNewTap'
      },
      resolutionsel: {
        change: 'resolutionselChange'
      }
    }
  },
  initializeForm: function () {
    this.formInitilize();
    Ext.create('M1CRM.model.crmLeadLineAddEdit', {}).getData({
      RequestID: 'prop',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          this.config._propPartsMustExist = result.ResultObject.PartsMustExist;
          this.getResolutionsel().setStore(this.getStoreFromListForDropDown(result.ResultObject.ReasonList, 'crmReason', true));
          this.getResolutionsel().isDirty = false;
        }
      }
    });
  },
  getFormButtons: function () {
    return [this.getSavebtn().id, this.getEditbtn().id];
  },
  getEditableFormFields: function () {
    return [this.getPartid().id, this.getPart().id, this.getPartrevision().id, this.getPartdesc().id,
      this.getPartuom().id, this.getQty().id, this.getGrossamount().id,
      this.getDiscount().id, this.getDiscountamt().id,
      this.getResolution().id, this.getToquote().id
    ]
  },
  showForm: function () {
    if (SessionObj.getLeadLineAddEditTSecurity() == null || SessionObj.getLeadLineAddEditTSecurity() == '') {
      Ext.create('M1CRM.model.crmUserSecurity', {}).getUserSecurityLevel({
        ID: 'CRMADDEDITLEADLINE',
        scope: this,
        onReturn: function (rec) {
          if (rec != null) {
            SessionObj.setLeadLineAddEditTSecurity(rec.PAccessLevel + ',' + rec.SAccessLevel);
            if (this.applyUserSecuritySettings(this.getForm().id, parseInt(rec.PAccessLevel), parseInt(rec.SAccessLevel), this.getFormButtons())) {
              this.processShowForm();
            }
          } else {
            this.toBackTap(this.getForm().id);
          }
        }
      });
    } else {
      if (this.applyUserSecuritySettings(this.getForm().id,
          parseInt(this.getPKeyFromSessionObj(SessionObj.getLeadLineAddEditTSecurity())), parseInt(this.getSKeyFromSessionObj(SessionObj.getLeadLineAddEditTSecurity())), this.getFormButtons())) {
        this.processShowForm();
      } else {
        this.toBackTap(this.getForm().id);
      }
    }
  },
  processShowForm: function () {

    var mode = Ext.getCmp(this.getForm().id).config._mode;
    if (this.getForm().getRecord() != null) {
      var rec = this.getForm().getRecord().getData();
      if (mode == 'view') {
        this.setEntryFormMode('view');
        this.makeFormEditable(false);
        this.populateLeadData(rec);
      } else if (mode == 'edit') {
        this.setEntryFormMode('edit');
        this.makeFormEditable(true);
      } else if (mode == 'addnew') {
        this.setEntryFormMode('addnew');
        this.makeFormEditable(true);
        if (rec != null /*&& rec.RevisionID != null*/) {
          this.populateLeadData(rec);
          this.config._tmpitemCount = 2;
        } else {
          this.config._partrevverified = true;
          this.getPartrevision().focus();
        }
      }
    } else {
      this.partCleariconTap();
    }
  },
  partCleariconTap: function () {
    this.config._tmpitemCount = 0;
    this.getPartid().setValue('');
    this.setEntryFormMode('addnew');
    this.makeFormEditable(true);
    tmpLeadID = Ext.getCmp('addeditlead_leadid').getValue()
    this.makeFormsAsNewEntry();
    this.getLeadid().setValue(tmpLeadID);
  },
  makeFormsAsNewEntry: function () {
    tmpLeadID = Ext.getCmp('addeditlead_leadid').getValue();
    this.clearFormFields();
    this.getLeadid().setValue(tmpLeadID);
    this.updateSubTitle();
    this.getRevforcecastdatesel().setValue(new Date()); //, SessionObj.getSimpleDateFormat()));
  },
  makeFormEditable: function (flag) {
    this.hideorshowTextFieldLookupInfo(this.getPart());
    this.setControlsReadOnly(!flag, [this.getPartrevision().id, this.getPartdesc().id, this.getPartuom().id, this.getQty().id, this.getUnitsaleprice().id]);
    if (flag) {
      this.hideControls([
        this.getGrossamountdisp().id, this.getDiscountdisp().id, this.getDiscountamtdisp().id,
        this.getRevforcecastdisp().id, this.getRevforcecastdate().id, this.getResolution().id
      ]);
      this.showControls([
        this.getGrossamount().id, this.getDiscount().id, this.getDiscountamt().id,
        this.getRevforcecast().id, this.getRevforcecastdatesel().id, this.getResolutionsel().id
      ]);
      this.getToquote().enable();

    } else {
      this.showControls([
        this.getGrossamountdisp().id, this.getDiscountdisp().id, this.getDiscountamtdisp().id,
        this.getRevforcecastdisp().id, this.getRevforcecastdate().id, this.getResolution().id
      ]);
      this.hideControls([
        this.getGrossamount().id, this.getDiscount().id, this.getDiscountamt().id,
        this.getRevforcecast().id, this.getRevforcecastdatesel().id, this.getResolutionsel().id
      ]);
      this.getToquote().disable();
    }

    if (this.config._propPartsMustExist == 0 && flag == false) {
      this.getPartrevision().setReadOnly(flag);
    }
  },
  populateLeadData: function (rec) {
    if (rec.LeadID != null) {
      this.getLeadid().setValue(rec.LeadID);
      this.getLeadlineid().setValue(rec.LeadLineID);
      this.updateSubTitle();
      if (this.getLeadid().getValue() != null && this.getLeadid().getValue() != '' && this.getLeadlineid().getValue() != null && parseInt(this.getLeadlineid().getValue()) > 0) {
        Ext.create('M1CRM.model.crmLeadLineAddEdit', {
          LeadID: this.getLeadid().getValue(),
          LeadLineID: this.getLeadlineid().getValue()
        }).getData({
          RequestID: 'data',
          scope: this,
          onReturn: function (result) {
            if (result != null && result.RecordFound == true) {
              var dataObj = result.ResultObject;
              this.config._tmpitemCount = 1;
              this.config._partrevverified = true;
              this.getPartid().setValue(dataObj.PartID)

              this.getPart().suspendEvents();
              this.getPart().setValue(dataObj.PartID);
              this.getPart().resumeEvents(true);

              this.getPartrevision().suspendEvents();
              this.getPartrevision().setValue(dataObj.PartRevisionID);
              this.getPartrevision().resumeEvents(true);

              this.getPartgroupid().setValue(dataObj.PartGroupID);

              this.getPartdesc().setValue(dataObj.Description);
              this.getPartuom().setValue(dataObj.UnitOfMeasure);
              this.getQty().setValue(dataObj.Quantity);
              this.getUnitsaleprice().setValue(this.getFormatNumberAsCurrency(dataObj.UnitSalePrice, true));
              this.getQtyonhand().setValue(this.getFormatNumberWithDecimal(dataObj.QuantityOnHand));

              this.getGrossamount().setValue(dataObj.GrossAmount);
              this.getGrossamountdisp().setValue(this.getFormatNumberAsCurrency(dataObj.GrossAmount, true));
              this.getDiscount().setValue(dataObj.DiscountPercent);
              this.getDiscountdisp().setValue(this.getFormatNumberWithDecimal(dataObj.DiscountPercent));
              this.getDiscountamt().setValue(dataObj.DiscountAmount);
              this.getDiscountamtdisp().setValue(this.getFormatNumberAsCurrency(dataObj.DiscountAmount, true));
              this.getRevforcecast().setValue(dataObj.RevenueForecast);
              this.getRevforcecastdisp().setValue(this.getFormatNumberAsCurrency(dataObj.RevenueForecast, true));
              this.getRevforcecastdate().setValue(formatDate(this.changeDisplayDateToCorrectDateTime(dataObj.ForecastDate), SessionObj.getSimpleDateFormat()));
              this.getRevforcecastdatesel().setValue(this.changeDisplayDateToCorrectDateTime(dataObj.ForecastDate));
              this.getResolutionsel().setValue(dataObj.ResolutionReasonID);
              //this.getProductionQty();
              //this.getOnOrderQty();
              this.getQtyinproduction().setValue(this.getFormatNumberWithDecimal(dataObj.QtyInfo.ProductionQuantity));
              this.getQtyonorder().setValue(this.getFormatNumberWithDecimal(dataObj.QtyInfo.TotalSalesOrderQty));


            }
          }
        });
      }
    } else if (rec.PartID != null) {
      var tmpLeadID = this.getLeadid().getValue();
      this.makeFormsAsNewEntry();
      this.getLeadid().setValue(tmpLeadID);
      this.config._tmpitemCount = 1;
      this.getPartid().setValue(rec.PartID)

      this.getPart().suspendEvents();
      this.getPart().setValue(rec.PartID);
      this.getPart().isDirty = true;
      this.getPart().resumeEvents(true);

      this.getPartrevision().suspendEvents();
      this.getPartrevision().setValue(rec.RevisionID);
      this.getPartrevision().resumeEvents(true);

      this.getPartgroupid().setValue(rec.PartGroupID);


      this.getPartdesc().setValue(rec.Description != null ? rec.Description : rec.ShortDescription != null ? rec.ShortDescription : '');
      this.getPartuom().setValue(rec.IUoM != null ? rec.IUoM : rec.InventoryUnitOfMeasure != null ? rec.InventoryUnitOfMeasure : '');
      this.getPartQtyInfo();
    }

  },
  getPartQtyInfo: function () {
    Ext.create('M1CRM.model.crmLeadLineAddEdit', {
      PartID: this.getPartid().getValue(),
      PartRevisionID: this.getPartrevision().getValue(),
      PartGroupID: this.getPartgroupid().getValue(),
      OrganizationID: Ext.getCmp('addeditlead_custid').getValue(),
      LocationID: Ext.getCmp('addeditlead_invlocationid').getValue(),
      Quantity: 1
    }).getData({
      RequestID: 'qtyinfo',
      scope: this,
      onReturn: function (result) {
        var qtyOnHand = 0.0;
        var qtyProduction = 0.0;
        var qtyOnOrder = 0.0;
        var unitSalePrice = 0.0;
        if (result != null && result.RecordFound == true) {
          qtyOnHand = result.ResultObject.QuantityOnHand;
          qtyProduction = result.ResultObject.ProductionQuantity;
          qtyOnOrder = result.ResultObject.TotalSalesOrderQty;
          unitSalePrice = result.ResultObject.UnitPrice;
        }
        this.getQtyonhand().setValue(this.getFormatNumberWithDecimal(qtyOnHand));
        this.getQtyinproduction().setValue(this.getFormatNumberWithDecimal(qtyProduction));
        this.getQtyonorder().setValue(this.getFormatNumberWithDecimal(qtyOnOrder));
        this.getUnitsaleprice().setValue(this.getFormatNumberAsCurrency(unitSalePrice, true));
      }
    });
  },
  updateSubTitle: function () {
    if (this.getLeadid().getValue() != null && this.getLeadid().getValue() != '' && this.getLeadlineid().getValue() != null && parseInt(this.getLeadlineid().getValue()) > 0) {
      this.getSubtitle().setHtml('ID : ' + this.getLeadid().getValue() + ' Line:' + this.getLeadlineid().getValue());
    } else {
      this.getSubtitle().setHtml('');
    }
  },
  partlookupTap: function () {
    var view = M1CRM.util.crmViews.getParts();
    view.config._parentformid = this.getForm().id;
    this.slideLeft(view);
  },
  calculateRevenueForcast: function () {
    var RevenueForecast = 0;
    var GrossAmount = this.getGrossamount().getValue();
    var Discount = this.getDiscount().getValue();
    RevenueForecast = GrossAmount - GrossAmount * (Discount / 100);
    RevenueForecast = parseFloat(RevenueForecast).toFixed(2);
    this.getRevforcecast().suspendEvents();
    this.getRevforcecast().setValue(RevenueForecast);
    this.getRevforcecastdisp().setValue(this.getFormatNumberAsCurrency(RevenueForecast, true));
    this.getRevforcecast().resumeEvents(true);
  },
  qtyChange: function () {
    Ext.create('M1CRM.model.crmLeadLineAddEdit', {
      PartID: this.getPartid().getValue(),
      PartRevisionID: this.getPartrevision().getValue(),
      PartGroupID: this.getPartgroupid().getValue(),
      OrganizationID: Ext.getCmp('addeditlead_custid').getValue(),
      LocationID: Ext.getCmp('addeditlead_invlocationid').getValue(),
      Quantity: this.getQty().getValue() != null ? this.getQty().getValue() : 0
    }).getData({
      RequestID: 'priceinfo',
      scope: this,
      onReturn: function (result) {
        var discount = 0.0;
        var unitUnitPrice = 0.0;
        if (result != null && result.RecordFound == true) {
          discount = parseFloat(result.ResultObject.Discount).toFixed(2);
          unitUnitPrice = parseFloat(result.ResultObject.UnitPrice);
        }
        this.getUnitsaleprice().setValue(unitUnitPrice);
        var grossAmount = 0;
        grossAmount = parseFloat(this.getUnitsaleprice().getValue() * this.getQty().getValue()).toFixed(2);
        this.getGrossamount().setValue(grossAmount);
        this.getGrossamountdisp().setValue(this.getFormatNumberAsCurrency(grossAmount, true));

        this.getDiscount().setValue(discount);
        this.getDiscountdisp().setValue(this.getFormatNumberWithDecimal(discount));
        this.discountChange();
      }
    });
  },
  setDiscountAmount: function (value) {
    value = parseFloat(value).toFixed(2);
    this.getDiscountamt().setValue(value);
    this.getDiscountamtdisp().setValue(this.getFormatNumberAsCurrency(value, true));
  },

  grossamountChange: function () {
    if (this.getDiscount().getValue() == 0) {
      this.calculateRevenueForcast();
    } else {
      this.discountChange();
    }
  },
  discountFocus: function () {
    this.discountamtFocus();
  },
  discountChange: function () {
    this.getDiscountamt().suspendEvents();
    this.setDiscountAmount(this.getDiscount().getValue() != 0 ? this.getGrossamount().getValue() * (this.getDiscount().getValue() / 100) : 0);
    this.getDiscountamt().resumeEvents(true);
    this.calculateRevenueForcast();
  },
  discountamtFocus: function () {
    if (this.getGrossamount().getValue() == null || this.getGrossamount().getValue() == 0) {
      this.getGrossamount().focus();
    }
  },
  discountamtChange: function () {
    this.getDiscount().suspendEvents();
    var discount = parseFloat(100 - ((this.getGrossamount().getValue() - this.getDiscountamt().getValue()) * 100 / this.getGrossamount().getValue())).toFixed(2);
    this.getDiscount().setValue(discount);
    this.getDiscountdisp().setValue(this.getFormatNumberWithDecimal(discount));
    this.getDiscount().resumeEvents(true);
    this.calculateRevenueForcast();
  },
  revforcecastChange: function () {
    this.getGrossamount().suspendEvents();
    var GrossAmount = this.getGrossamount().getValue();
    if (this.getRevforcecast().getValue() != 0) {
      GrossAmount = parseFloat(this.getRevforcecast().getValue() / (Ext.getCmp('addeditlead_confidence').getValue() != null || Ext.getCmp('addeditlead_confidence').getValue() != 0 ? Ext.getCmp('addeditlead_confidence').getValue() : 1)).toFixed(2);
      this.getGrossamount().setValue(GrossAmount);
      this.getGrossamountdisp().setValue(this.getFormatNumberAsCurrency(GrossAmount, true));
    }
    this.getGrossamount().resumeEvents(true);
    this.getDiscount().suspendEvents();
    this.getDiscount().setValue(0);
    this.getDiscountdisp().setValue(this.getFormatNumberWithDecimal(0));
    this.getDiscount().resumeEvents(true);
    this.getDiscountamt().suspendEvents();
    this.setDiscountAmount(0);
    this.getDiscountamt().resumeEvents(true);


  },
  checkPartID: function () {
    var $partID = this.getPart();
    this.config._tmpitemCount = 0;

    if (!$partID.isFocused && $partID.getValue()) {
      Ext.create('M1CRM.model.crmLeadLineAddEdit', {
        PartID: this.getPartid().getValue(),
        PartRevisionID: this.getPartrevision().getValue(),
        PartGroupID: '',
        OrganizationID: Ext.getCmp('addeditlead_custid').getValue(),
        LocationID: Ext.getCmp('addeditlead_invlocationid').getValue(),
        Quantity: this.getQty().getValue() != null ? this.getQty().getValue() : 0
      }).getData({
        RequestID: 'checkpart',
        scope: this,
        onReturn: function (result) {
          if (result && result.ResultObject) {
            if (this.config._propPartsMustExist == true && (parseInt(result.ResultObject.TotalActiveRevisions) <= 0 || result.RecordFound == false)) {
              Ext.Msg.alert("Warning", "Part does not exist in the database",
                function (btn) {
                  if (btn == 'ok') {
                    this.getPart().setValue('');
                    this.getPart().focus();
                    this.config._partrevverified = true;
                  }
                }, this);
            } else if (this.config._propPartsMustExist == false && parseInt(result.ResultObject.TotalActiveRevisions) <= 0 && Ext.getCmp(this.getForm().id).config._mode != 'view') {
              this.getPartrevision().setReadOnly(false);
              this.config._partrevverified = true;
              this.getPartdesc().setReadOnly(false);
              this.getPartrevision().setReadOnly(true);
              this.getPartrevision().focus();
            } else if (parseInt(result.ResultObject.TotalActiveRevisions) > 1) {
              this.config._tmpitemCount = parseInt(result.ResultObject.TotalActiveRevisions);
              this.getPartrevision().setReadOnly(false);
              this.config._partrevverified = false;
              this.displayPartRevisions();
            } else if (parseInt(result.ResultObject.TotalActiveRevisions) == 1) {
              var dataObj = result.ResultObject;
              this.config._partrevverified = true;
              this.getPartrevision().setValue(dataObj.PartRevisionID);
              this.getPartrevision().setReadOnly(false);
              this.getPartdesc().setValue(dataObj.Description);
              this.getPartuom().setValue(dataObj.UnitOfMeasure);
              var qtyOnHand = dataObj.QtyInfo.QuantityOnHand;
              var qtyProduction = dataObj.QtyInfo.ProductionQuantity;
              var qtyOnOrder = dataObj.QtyInfo.TotalSalesOrderQty;
              var unitSalePrice = dataObj.QtyInfo.UnitPrice;
              this.getQtyonhand().setValue(this.getFormatNumberWithDecimal(qtyOnHand));
              this.getQtyinproduction().setValue(this.getFormatNumberWithDecimal(qtyProduction));
              this.getQtyonorder().setValue(this.getFormatNumberWithDecimal(qtyOnOrder));
              this.getUnitsaleprice().setValue(this.getFormatNumberAsCurrency(unitSalePrice, true));
            }

          } else {

            Ext.Msg.alert("Error", "Error accessing part information",
              function (btn) {
                if (btn == 'ok') {
                  this.getPart().setValue('');
                  this.getPart().focus();
                  this.config._partrevverified = true;
                }
              }, this);
          }
        }
      });
    }
  },
  displayPartRevisions: function () {
    var view = M1CRM.util.crmViews.getPartRevisions();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = null;
    this.getForm().setRecord(this.getPartRevisionDataObject());
    this.slideLeft(view);
  },
  getPartRevisionDataObject: function () {
    var dataObj = Ext.create('M1CRM.model.crmPartRevision', {});
    dataObj.data.PartID = this.getPartid().getValue();
    dataObj.data.ParentId = this.getForm().id;
    return dataObj;
  },
  partRevisionBlur: function () {
    if (this.getPartid().getValue() != '' && !this.getPartrevision().getReadOnly() && this.config._partrevverified == true && this.config._tmpitemCount > 1 && this.config._propPartsMustExist == true) {
      Ext.Msg.alert('Info', 'There are multiple revisions for this part, enter required revision id.',
        function (btn) {
          if (btn == 'ok') {
            this.getPartrevision().focus();
          }
        }, this);
      this.getPartrevision().focus();
    }
  },
  partRevisionChange: function () {
    if (this.getPartid().getValue() != '' && !this.getPartrevision().getReadOnly() && this.config._tmpitemCount > 0 /*&& this.config._propPartsMustExist != 0 */) {
      this.getPartInfo(true);
    }
  },
  getPartInfo: function (flag) {

    Ext.create('M1CRM.model.crmLeadLineAddEdit', {
      PartID: this.getPartid().getValue(),
      PartRevisionID: this.getPartrevision().getValue(),
      PartGroupID: '',
      OrganizationID: Ext.getCmp('addeditlead_custid').getValue(),
      LocationID: Ext.getCmp('addeditlead_invlocationid').getValue(),
      Quantity: this.getQty().getValue() != null ? this.getQty().getValue() : 0
    }).getData({
      RequestID: 'partinfo',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          if (flag) {
            if (result.ResultObject.EffectiveEndDate != null && parseDate(this.getFormatDateFromJSonDate(result.ResultObject.EffectiveEndDate)) < new Date()) {
              msg = 'Part Revision has expired';
              Ext.Msg.alert('', msg,
                function (btn) {
                  if (btn == 'ok') {
                    if (flag) {
                      this.getPartrevision().setValue('');
                      this.getPartdesc().setValue('');
                      this.getPartuom().setValue('');
                      this.getQtyonhand().setValue(this.getFormatNumberWithDecimal(0));
                      this.getQtyinproduction().setValue(this.getFormatNumberWithDecimal(0));
                      this.getQtyonorder().setValue(this.getFormatNumberWithDecimal(0));
                      this.getUnitsaleprice().setValue(this.getFormatNumberAsCurrency(0, true));
                      this.config._partrevverified = true;
                      this.getPartrevision().focus();
                    }
                  }
                }, this);
            } else {
              var dataObj = result.ResultObject;
              this.getPartdesc().setValue(dataObj.Description);
              this.getPartuom().setValue(dataObj.UnitOfMeasure);
              var qtyOnHand = dataObj.QtyInfo.QuantityOnHand;
              var qtyProduction = dataObj.QtyInfo.ProductionQuantity;
              var qtyOnOrder = dataObj.QtyInfo.TotalSalesOrderQty;
              var unitSalePrice = dataObj.QtyInfo.UnitPrice;
              this.getQtyonhand().setValue(this.getFormatNumberWithDecimal(qtyOnHand));
              this.getQtyinproduction().setValue(this.getFormatNumberWithDecimal(qtyProduction));
              this.getQtyonorder().setValue(this.getFormatNumberWithDecimal(qtyOnOrder));
              this.getUnitsaleprice().setValue(this.getFormatNumberAsCurrency(unitSalePrice, true));

            }

          } else {
            this.viewInfo('part', 'addeditquotepart', null, null, null, null, null, null, this.getPartid().getValue(), this.getPartrevision().getValue(), null);
          }
        } else if (this.config._propPartsMustExist == true) {
          msg = flag ? 'Part Revision does not exist in the database.' : 'Part does not exist in the database.';
          Ext.Msg.alert('Warning', msg,
            function (btn) {
              if (btn == 'ok') {
                if (flag) {
                  this.getPartrevision().setValue('');
                  this.getPartdesc().setValue('');
                  this.getPartuom().setValue('');
                  this.getQtyonhand().setValue(this.getFormatNumberWithDecimal(0));
                  this.getQtyinproduction().setValue(this.getFormatNumberWithDecimal(0));
                  this.getQtyonorder().setValue(this.getFormatNumberWithDecimal(0));
                  this.getUnitsaleprice().setValue(this.getFormatNumberAsCurrency(0, true));
                  this.config._partrevverified = true;
                  this.getPartrevision().focus();
                }
              }
            }, this);
        }
      }
    });
  },
  getSaveObject: function () {
    return Ext.create('M1CRM.model.crmLeadLineAddEdit', {
      LeadID: this.getLeadid().getValue(),
      LeadLineID: (this.getLeadlineid().getValue() == null || this.getLeadlineid().getValue() == '') ? 0 : this.getLeadlineid().getValue(),
      PartID: this.getPart().getValue(),
      PartGroupID: '',
      OrganizationID: Ext.getCmp('addeditlead_custid').getValue(),
      LocationID: Ext.getCmp('addeditlead_invlocationid').getValue(),
      PartRevisionID: this.getPartrevision().getValue(),
      Description: this.getPartdesc().getValue(),
      UnitOfMeasure: this.getPartuom().getValue(),
      Quantity: this.getQty().getValue() != null ? this.getQty().getValue() : 0,
      GrossAmount: this.getGrossamount().getValue(),
      DiscountAmount: this.getDiscountamt().getValue(),
      RevenueForecast: this.getRevforcecast().getValue(),
      ForecastDate: this.getRevforcecastdatesel().getValue(),
      LeadDate: Ext.getCmp('addeditlead_leaddatesel').getValue(),
      TransferredToQuote: this.getToquote().isChecked(),
      ResolutionReasonID: Ext.isDefined(this.getResolutionsel().getValue()) ? this.getResolutionsel().getValue() : '',
      DiscountPercent: this.getDiscount().getValue() != null || this.getDiscount().getValue() != '' ? this.getDiscount().getValue() : 0,
      UnitSalePrice: this.getUnitsaleprice().getValue(),
      EmployeeID: SessionObj.getEmpId()
    });
  },
  btnSaveTap: function () {
    var leadLine = this.getSaveObject();
    var error = leadLine.validate();
    if (leadLine.isValid()) {
      leadLine.saveData({
        scope: this,
        onReturn: function (result) {
          if (result && result.SaveStatus) {
            this.setEntryFormMode('view');
            Ext.getCmp(this.getForm().id).config._mode = 'view';
            this.getLeadlineid().setValue(result.ResultObject.LeadLineID);
            this.populateLeadData(new Object({
              LeadID: this.getLeadid().getValue(),
              LeadLineID: this.getLeadlineid().getValue()
            }));
            this.makeFormEditable(false);
            this.resetEditableForms(this.getEditableFormFields());
          } else {
            Ext.Msg.alert('Error', 'Error saving lead lines info');
          }
        }
      });

    } else {
      this.displayValidationMessage(error);
    }
  },
  processAutoSave: function (type) {
    if (Ext.getCmp(this.getForm().id).config._mode != 'view' && this.isFormDirty(this.getEditableFormFields())) {
      var leadLine = this.getSaveObject();
      var error = leadLine.validate();
      if (leadLine.isValid()) {
        leadLine.saveData({
          scope: this,
          onReturn: function (result) {
            if (result.SaveStatus == true) {
              this.setEntryFormMode('view');
              Ext.getCmp(this.getForm().id).config._mode = 'view';
              this.processAfterAutoSave(type, this.getForm().id);
            } else {
              Ext.Msg.alert('Error', 'Error saving call lines info');
            }
          }
        });

      } else {
        this.displayValidationMessage(error);
      }
    } else {
      this.processAfterAutoSave(type, this.getForm().id);
    }
  },
  btnEditTap: function () {
    Ext.getCmp(this.getForm().id).config._mode = 'edit';
    this.setEntryFormMode('edit');
    this.makeFormEditable(true);
  },
  btnCancelTap: function () {
    if (this.getLeadlineid().getValue() != '') {
      Ext.getCmp(this.getForm().id).config._mode = 'view';
      this.setEntryFormMode('view');
      this.populateLeadData(new Object({
        LeadID: this.getLeadid().getValue(),
        LeadLineID: this.getLeadlineid().getValue()
      }));
      this.makeFormEditable(false);
    } else {
      Ext.getCmp(this.getForm().id).config._mode = 'addnew';
      this.setEntryFormMode('addnew');
      this.makeFormEditable(true);
      this.makeFormsAsNewEntry();
    }
    this.resetEditableForms(this.getEditableFormFields());
    this.config._partrevverified = false;
  },
  btnAddNewTap: function () {
    this.resetEditableForms(this.getEditableFormFields());
    Ext.getCmp(this.getForm().id).config._mode = 'addnew';
    this.setEntryFormMode('addnew');
    this.makeFormEditable(true);
    this.makeFormsAsNewEntry();
  },
  clearPartInfo: function (flag) {
    this.getPartrevision().suspendEvents();
    this.config._partrevverified = false;
    this.getPartrevision().setValue('');
    this.getPartrevision().setReadOnly(flag);
    this.getPartdesc().setValue('');
    this.getPartuom().setValue('');
    this.getPartrevision().focus();
    this.getPartrevision().resumeEvents(true);
  },
  resolutionselChange: function () {
    this.getResolution().setValue(this.getSelectFieldDisplayValue(this.getResolutionsel()));
  },
  revforcecastdateChange: function () {
    this.getRevforcecastdate().setValue(formatDate(this.getRevforcecastdatesel().getValue(), SessionObj.getSimpleDateFormat()));
  }

});
