Ext.define('M1CRM.controller.crmWaitingCalls', {
  extend: 'M1CRM.util.crmCallsListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmCalls',
    'M1CRM.store.crmPendingCalls'
  ],
  config: {
    _listid: 'waitinglist',
    _seardfieldid: 'waitingcall_search',
    control: {
      waitingcall: {
        initialize: 'initializeList',
        show: 'onShow',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.viewDetail(this.config._listid, item);
        },
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },

  init: function () {
    this.control({
      '#waitinglist_addnew': {
        scope: this,
        tap: function () {
          Ext.Viewport.getActiveItem().setRecord(null);
          this.addNewCallTap();
        }
      }
    });
  },

  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labWaitingCallSummary';
    //this.createOptionMenu();
    Ext.getCmp(this.config._listid).config._parentformid = 'myopencallslist';
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmCalls', {}).getWaitingPendingCallsListLayout());
  },
  createOptionMenu: function () {
    var items = this.getOptionMenuItems(this);
    Ext.Viewport.setMenu(this.createMenu(items), {
      side: 'right',
      reveal: true
    });
  },
  onShow: function (obj, e) {
    this.initilize();
    this.createOptionMenu();
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmCalls', Ext.create('M1CRM.model.crmDataFilterModel', {
      AssignedToEmployeeID: SessionObj.statics.EmpId
    }));
  }


});
