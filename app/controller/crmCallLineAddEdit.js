Ext.define('M1CRM.controller.crmCallLineAddEdit', {
  extend: 'M1CRM.util.crmFormControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmContactMethod',
    'M1CRM.model.crmEmployee',
    'M1CRM.model.crmCallLineAddEdit'
  ],
  config: {
    _viewmodeTitle: 'Call Line',
    _addmodeTitle: 'Add Call Line',
    _editmodeTitle: 'Edit Call Line',

    refs: {
      'form': '#calllineaddedit',
      'toolbarid': '#calllineaddedit_toolbarid',
      'subtitle': '#calllineaddedit_infoid',
      'addnewbtn': '#calllineaddedit_addnew',
      'cancelbtn': '#calllineaddedit_cancelbtn',
      'savebtn': '#calllineaddedit_savebtn',
      'editbtn': '#calllineaddedit_editbtn',
      'customerid': '#addeditcallline_customerid',
      'customer': '#addeditcallline_customer',
      'customerinfo': '#addeditcallline_custinfo',
      'contactid': '#addeditcallline_contactid',
      'contact': '#addeditcallline_contact',
      'contactinfo': '#addeditcallline_contactinfo',
      'description': '#addeditcallline_description',
      'notes': '#calllineaddedit_notes', //'#addeditcallline_notes',
      'methodsel': '#addeditcallline_methodsel',
      'method': '#addeditcallline_method',
      'internalonly': '#addeditcallline_internalonly',
      'addedbysel': '#addeditcallline_addedbysel',
      'addedby': '#addeditcallline_addedby',
      'addeddate': '#addeditcallline_addeddate'
    },
    control: {
      form: {
        initialize: 'initializeForm',
        show: 'showForm',
        hide: function () {
          this.scrollFormToTop();
        }
      },
      cancelbtn: {
        tap: function () {
          this.btnCancelTap();
          this.scrollFormToTop();
        }
      },
      addnewbtn: {
        tap: 'btnAddNewTap'
      },
      methodsel: {
        change: 'contactmethodselChange'
      }
    }
  },
  initializeForm: function () {
    this.loadFormProperties();
    this.getAddedbysel().hide();
    this.getAddedby().setValue(SessionObj.getEmpName());

  },
  initializeFields: function (args) {
    this.getDescription().setMaxLength(70);
  },
  loadFormProperties: function () {

    Ext.create("M1CRM.model.crmCallLineAddEdit", {}).getIt({
      RequestID: 'prop',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {

          this.getMethodsel().setStore(this.getStoreFromListForDropDown(result.ResultObject.ContactMethodsList, 'crmContactMethod', false));
          this.getMethodsel().isDirty = false;

          var tempStore = this.getStoreFromListForDropDown(result.ResultObject.EmployeeList, 'crmEmployee', false);
          this.getAddedbysel().setStore(tempStore);
          this.getAddedbysel().setValue(SessionObj.getEmpId());
          this.getAddedbysel().isDirty = false;

        }
      }
    });
  },

  getFormButtons: function () {
    return [this.getSavebtn().id, this.getEditbtn().id];
  },
  getEditableFormFields: function () {
    return [this.getDescription().id, this.getNotes().id,
      this.getInternalonly().id, this.getAddedbysel().id
    ]
  },
  showForm: function () {
    this.clearFields();
    this.processShowForm();
    /*Ext.create('M1CRM.model.crmUserSecurity', {}).getUserSecurityLevel({
     ID :  this.getForm().id,
     scope : this,
     onReturn : function (rec){
     if (rec.Status == 'OK'){
     if ( this.applyUserSecuritySettings(this.getForm().id, parseInt(rec.PAccessLevel), parseInt(rec.SAccessLevel), this.getFormButtons()))
     {
     this.processShowForm();
     }
     }else{
     this.toBackTap(this.getForm().id);
     }
     }
     }); */
  },
  contactmethodselChange: function () {
    this.getMethod().setValue(this.getSelectFieldDisplayValue(this.getMethodsel()));
  },
  processShowForm: function () {
    this.resetEditableForms(this.getEditableFormFields());
    var lineid = this.getCallID();
    var rec = null;
    var custname = ''
    if (this.getForm().config._parentformid == 'addeditcall') {
      var rec = Ext.getCmp('addeditcall').getRecord().getData();
      this.getCustomer().setValue(rec.Organization);
      this.getMethodsel().setValue(rec.ContactMethodID);
      this.getMethod().setValue(this.getSelectFieldDisplayValue(this.getMethodsel()));

      if (rec.InternalOnly == true)
        this.getInternalonly().check();
      else
        this.getInternalonly().uncheck();
    } else if (this.getForm().config._parentformid == 'calllineslist') {
      var mode = Ext.getCmp(this.getForm().id).config._mode != null ? Ext.getCmp(this.getForm().id).config._mode : 'view';
      this.setEntryFormMode(mode);
      var rec = Ext.getCmp('addeditcall').getRecord();
      this.getCustomerid().setValue(rec.getData().OrganizationID != null ? rec.getData().OrganizationID : rec.getData().kbpOrganizationID);
      this.getCustomer().setValue(rec.getData().OrganizationName != null ? rec.getData().OrganizationName : rec.getData().cmoName);
      this.getContactid().setValue(rec.getData().ContactID != null ? rec.getData().ContactID : rec.getData().cmcContactID);
      this.getContact().setValue(rec.getData().Contact != null ? rec.getData().Contact : rec.getData().cmcName);
    }
    this.getAddedbysel().setValue(SessionObj.getEmpId());
    this.getAddeddate().setValue(formatDate(new Date(), SessionObj.getSimpleDateFormat()));
    this.loadData();
  },
  loadData: function () {
    var rec = Ext.getCmp('calllineaddedit').getRecord() != null ? Ext.getCmp('calllineaddedit').getRecord().getData() : Ext.getCmp('addeditcall').getRecord().getData();
    var calllineid = this.getForm().config._parentformid == 'addeditcall' ? null : Ext.getCmp('calllineaddedit').getRecord() != null ? rec.CallLineID : null;
    Ext.getCmp('addeditcallline_callid').setValue(this.getCallID());
    Ext.getCmp('addeditcallline_lineid').setValue(calllineid);
    if (calllineid != null) {
      Ext.getCmp('calllineaddedit_infoid').setHtml('Call: ' + this.getCallID() + '  Line : ' + calllineid);
    } else {
      Ext.getCmp('calllineaddedit_infoid').setHtml('Call: ' + this.getCallID());
    }

    if (this.getForm().config._parentformid == 'addeditcall') {
      this.makeFormReadOnly(false);

    }
    else if (Ext.getCmp('calllineaddedit').getRecord() != null) {
      this.makeFormReadOnly(true);
      var rec = Ext.getCmp('calllineaddedit').getRecord().getData();
      this.getMethodsel().setValue(rec.ContactMethodID);
      rec.InternalOnly == true ? this.getInternalonly().check() : this.getInternalonly().uncheck();
      this.getDescription().setValue(rec.ShortDescription);
      Ext.create("M1CRM.model.crmCallLineAddEdit", {
        CallID: rec.CallID,
        CallLineID: rec.CallLineID
      }).getIt({
        RequestID: 'info',
        scope: this,
        onReturn: function (result) {
          if (result != null && result.RecordFound == true) {
            this.getNotes().setValue(result.ResultObject.LongDescription);
            this.getAddedbysel().setValue(result.ResultObject.AddedByEmployeeID);
            this.getAddedby().setValue(this.getSelectFieldDisplayValue(this.getAddedbysel()));
            var tdate = this.getCorrectDate(result.ResultObject.AddedDate);
            this.getAddeddate().setValue(formatDate(tdate, SessionObj.getSimpleDateFormat()));
          }
        }
      });

    }
    else {
      this.makeFormReadOnly(false);
    }
  },
  clearFields: function () {
    Ext.getCmp('addeditcallline_callid').setValue('');
    Ext.getCmp('addeditcallline_lineid').setValue('');
    this.getMethodsel().setValue('EMAIL');
    this.getInternalonly().uncheck();
    this.getDescription().setValue('');
    this.getNotes().setValue('');
  },
  makeFormReadOnly: function (flag) {
    this.getMethodsel().setReadOnly(flag);
    flag ? this.getInternalonly().disable() : this.getInternalonly().enable();
    this.getDescription().setReadOnly(flag);
    this.getNotes().setReadOnly(flag);
    this.pairControlsShowAndHide(flag, this.getMethod().id);
    if (flag) {
      this.resetEditableForms(this.getEditableFormFields());
      this.getCustomerinfo().enable();
      //this.getContactinfo().enable();

      if (this.getContactid().getValue() == null || this.getContactid().getValue() == '') {
        this.getContactinfo().disable();
      } else {
        this.getContactinfo().enable();
      }

    } else {
      this.getCustomerinfo().disable();
      this.getContactinfo().disable();
    }
  },
  getCallID: function () {
    var rec = null;
    if (this.getForm().config._parentformid == 'addeditcall')
      return Ext.getCmp('addeditcall').getRecord().getData().CallID;
    else if (this.getForm().config._parentformid == 'calllineslist')
      return Ext.getCmp('addeditcall').getRecord().getData().CallID != null ? Ext.getCmp('addeditcall').getRecord().getData().CallID : Ext.getCmp('addeditcall').getRecord().getData().kbpCallID;
    else
      return Ext.getCmp('calllineaddedit').getRecord() != null ? Ext.getCmp('calllineaddedit').getRecord().getData().CallID : null;  //rec.kbpCallID;
  },
  getSaveObject: function () {
    var lineid = this.getCallID();
    return Ext.create('M1CRM.model.crmCallLineAddEdit', {
      CallID: this.getCallID(),
      CallLineID: Ext.getCmp('addeditcallline_lineid').getValue() != null && Ext.getCmp('addeditcallline_lineid').getValue() != '' ? Ext.getCmp('addeditcallline_lineid').getValue() : -1,
      Description: this.getDescription().getValue(),
      LongDescription: this.getNotes().getValue(),
      ContactMethodID: this.getMethodsel().getValue(),
      InternalOnly: this.getInternalonly().isChecked(),
      AddedDate: parseDate(this.getAddeddate().getValue()),
      AddedByEmployeeID: this.getAddedbysel().getValue()
    });
  },
  btnSaveTap: function () {
    var callLine = this.getSaveObject();
    var error = callLine.validate();
    if (callLine.isValid()) {
      callLine.saveData({
        scope: this,
        onReturn: function (result) {
          if (result.SaveStatus == true) {
            this.setEntryFormMode('view');
            Ext.getCmp(this.getForm().id).config._mode = 'view';
            this.makeFormReadOnly(true);
            Ext.getCmp('calllineaddedit_infoid').setHtml('Call: ' + this.getCallID() + '  Line : ' + result.ResultObject.CallLineID.toString());
          } else {
            Ext.Msg.alert('Error', 'Error saving call lines info');
          }
        }
      });
    } else {
      this.displayValidationMessage(error);
    }
  },
  processAutoSave: function (type) {
    if (Ext.getCmp(this.getForm().id).config._mode != 'view' && this.isFormDirty(this.getEditableFormFields())) {
      var followup = this.getSaveObject();
      followup.validate();
      if (followup.isValid()) {
        followup.saveData({
          scope: this,
          onReturn: function (result) {
            if (result == 'ok') {
              if (Ext.getCmp('addeditcallline_lineid').getValue() != '') {
                this.btnCancelTap();
              } else {
                this.clearFields();
              }
              this.processAfterAutoSave(type, this.getForm().id);
            } else {
              Ext.Msg.alert('Error', 'Error saving call lines info');
            }
          }
        });
      } else {
        Ext.Msg.confirm("", this.getRecordInCompleteMessage(), function (btn, type) {
          if (btn == 'yes') {
            this.clearFields();
            this.processAfterAutoSave(type, this.getForm().id);
          } else {
            if (this.getCallID() != '' && Ext.getCmp('addeditcallline_lineid').getValue() != '') {
              this.btnCancelTap();
            }
          }
        }, this);
      }
    }
    else {
      if (Ext.getCmp('addeditcallline_lineid').getValue() != '') {
        this.btnCancelTap();
      } else {
        this.clearFields();
      }
      this.processAfterAutoSave(type, this.getForm().id);
    }
  },
  btnEditTap: function () {
    this.makeFormReadOnly(false);
    Ext.getCmp(this.getForm().id).config._mode = 'edit';
  },
  btnCancelTap: function () {
    this.makeFormReadOnly(true);
    this.setEntryFormMode('view');
    Ext.getCmp(this.getForm().id).config._mode = 'view';
    this.clearFields();
    this.resetEditableForms(this.getEditableFormFields());
    this.loadData();
  },
  btnAddNewTap: function () {
    this.setEntryFormMode('addnew');
    Ext.getCmp(this.getForm().id).config._mode = 'addnew';
    Ext.getCmp('addeditcallline_lineid').setValue('');
    this.getMethodsel().setValue('EMAIL');
    this.getInternalonly().uncheck();
    this.getDescription().setValue('');
    this.getNotes().setValue('');
    this.makeFormReadOnly(false);
    Ext.getCmp('addeditcallline_lineid').hide();
    Ext.getCmp('calllineaddedit_infoid').setHtml('Call ID : ' + this.getCallID());
  }

});
