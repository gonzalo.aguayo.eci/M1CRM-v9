Ext.define('M1CRM.controller.crmCustQuoteIQSLocations', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmCustomerLocations',
    'M1CRM.store.crmCustomerLocations'
  ],
  config: {
    _listid: 'custquoteiqslocationslist',
    _seardfieldid: 'custquoteiqslocations_search',
    control: {
      ref: {},
      custquoteiqslocations: {
        initialize: 'initializeList',
        show: 'onShowList',
        itemtap: 'onLocationTap',
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },

  init: function () {
    this.control({
      '#custquoteiqslocationslist_addnew': {
        scope: this,
        tap: this.addNewCustQuoteLocationTap
      }
    });
  },
  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labCustQuoteLocationSummary';

  },

  onShowList: function (obj, e) {
    this.initilize();
    Ext.getCmp('custquoteiqslocationslist_toolbarid').setTitle(this.getTitle() + ' Locations');
    Ext.getCmp('custquoteiqslocationslist_addnew').enable();
    this.loadData(obj);

  },

  getTitle: function () {
    if (Ext.getCmp('custquoteiqslocationslist').config._loctype == 'ARINV') {
      return 'Invoice';
    } else if (Ext.getCmp('custquoteiqslocationslist').config._loctype == 'QTLOC') {
      return 'Quote'
    } else if (Ext.getCmp('custquoteiqslocationslist').config._loctype == 'SHIPLOC') {
      return 'Ship'
    } else {
      return '';
    }
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmCustomerLocations', {}).getListLayout(Ext.getCmp('custquoteiqslocationslist').config._loctype));
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmCustomerLocations', this.getDataObject());
  },
  getDataObject: function () {
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
      OrganizationID: Ext.getCmp('custquoteiqslocationslist').config._custid,
      OrganizationName: Ext.getCmp('custquoteiqslocationslist').config._custname,
      ListType: Ext.getCmp(this.config._listid).config._loctype,
      AssignedToEmployeeID: SessionObj.getEmpId()
    });
    return dataObj;
  },
  onLocationTap: function (nestedlist, list, index, item, rec, e) {
    var parentid = Ext.getCmp(this.config._listid).config._parentformid;
    var view = M1CRM.util.crmViews.getHome();
    if (parentid == 'addeditlead') {
      view = M1CRM.util.crmViews.getAddEditLead();
    } else {
      view = M1CRM.util.crmViews.getAddNewQuote();
    }
    view.setRecord(item);
    M1CRM.util.crmViews.slideLeft(view);
  },
  addNewCustQuoteLocationTap: function () {
    var loctype = Ext.getCmp('custquoteiqslocationslist').config._loctype;
    var dataObject = this.getDataObject();
    var view = M1CRM.util.crmViews.getCustomerLocationAddEdit();
    view.config._parentformid = this.config._listid;
    view.config._loctype = loctype;
    view.setRecord(dataObject);
    Ext.getCmp(view.id).config._mode = 'addnew';
    this.slideLeft(view);
  }
});
