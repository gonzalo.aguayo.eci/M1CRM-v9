Ext.define('Ext.overrides.field.Select', {
  override: 'Ext.field.Select',

  updateStore: function (newStore) {
    if (newStore) {
      this.onStoreDataChanged(newStore);
    }
    if (this.getUsePicker() && this.picker) {
      this.picker.down('pickerslot').setStore(newStore);
    } else if (this.listPanel) {
      this.listPanel.down('dataview').setStore(newStore);
    }
  }
});


Ext.define('M1CRM.controller.crmCustomerContactAddEdit', {
  extend: 'M1CRM.util.crmFormControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmTitle',
    'M1CRM.store.crmTitle',
    'M1CRM.model.crmCustomerContactAddEdit',
    'M1CRM.model.crmCustomerLocationAddEdit'
    //'M1CRM.model.crmCustomerContacts',
    //'M1CRM.store.crmCustomerContacts'
  ],
  config: {
    _viewmodeTitle: 'Contact',
    _addmodeTitle: 'Add Contact',
    _editmodeTitle: 'Edit Contact',
    refs: {
      // 'addnewcontact'
      'form': '#customercontactaddedit',
      'subtitle': '#customercontactaddedittitleid',
      'toolbarid': '#customercontactaddedit_toolbarid',
      'addnewbtn': '#customercontactaddedit_addnew',
      'cancelbtn': '#customercontactaddedit_cancelbtn',
      'savebtn': '#customercontactaddedit_savebtn',
      'editbtn': '#customercontactaddedit_editbtn',
      'customerid': '#customercontactaddedit_custid',
      'customer': '#customercontactaddedit_customer',
      'customerlookup': '#customercontactaddedit_customer_lookup',
      'custinfo': '#customercontactaddedit_customer_info',
      'locationid': '#customercontactaddedit_locationid',
      'location': '#customercontactaddedit_location',
      'locationlookup': '#customercontactaddedit_location_lookup',
      'locationInfo': '#customercontactaddedit_location_info',
      'contactid': '#customercontactaddedit_contactid',
      'contactname': '#customercontactaddedit_contactname',
      'titlesel': '#customercontactaddedit_titlesel',
      'title': '#customercontactaddedit_title',
      'phone1': '#customercontactaddedit_phone1',
      'phone2': '#customercontactaddedit_phone2',
      'mobile': '#customercontactaddedit_mobile',
      'fax': '#customercontactaddedit_fax',
      'email': '#customercontactaddedit_email',
      'nomailing': '#customercontactaddedit_nomailing',
      'notesexpandbtn': '#customercontactaddedit_notesexpandbtn',
      'notes': '#customercontactaddedit_notes',
      'viewcalls': '#customercontactaddedit_viewcalls',
      'viewfollowups': '#customercontactaddedit_viewfollowups',
      'viewquotations': '#customercontactaddedit_viewquotations'
    },
    control: {
      form: {
        initialize: 'initializeForm',
        show: 'showForm',
        hide: function () {
          this.scrollFormToTop();
        }
      },
      cancelbtn: {
        tap: 'btnCancelTap'
      },
      addnewbtn: {
        tap: 'btnAddnewTap'
      },
      locationlookup: {
        tap: 'locationlookupTap'
      },
      customer: {
        clearicontap: function () {
          this.getLocationid().setValue('');
          this.getLocation().setValue('');
        }
      },
      contactid: {
        blur: 'contactidChange',
        clearicontap: 'contactidClearIconTap'
      },
      viewcalls: {
        tap: 'viewcallsTap'
      },
      viewfollowups: {
        tap: 'viewfollowupsTap'
      },
      viewquotations: {
        tap: 'viewquotationsTap'
      },
      customerlookup: {
        tap: 'customerlookupTap'
      },
      titlesel: {
        change: 'titleselChange'
      }

    }
  },
  initializeForm: function () {
    this.formInitilize();
    this.initializeFields();
    this.loadFormProperties();
  },
  initializeFields: function () {
    this.getContactid().setMaxLength(5);
    this.getPhone1().setMaxLength(20);
    this.getPhone2().setMaxLength(20);
    this.getFax().setMaxLength(20);
    this.getMobile().setMaxLength(20);
  },
  loadFormProperties: function () {
    Ext.create('M1CRM.model.crmCustomerContactAddEdit', {}).getIt({
      RequestID: 'prop',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          this.getTitlesel().setStore(this.getStoreFromListForDropDown(result.ResultObject.TitleList, 'crmTitle', true));
          this.getTitlesel().isDirty = false;
        }
      }
    });
  },
  addeditcon_locationChange: function () {
    this.getTitlesel().setValue('');
    this.getTitlesel().isDirty = false;
  },
  getFormButtons: function () {
    return [this.getCancelbtn().id, this.getSavebtn().id];
  },
  getEditableFormFields: function () {
    return [this.getLocation().id, this.getContactname().id, this.getContactid().id, this.getTitlesel().id, this.getPhone1().id,
      this.getPhone2().id, this.getMobile().id, this.getFax().id, this.getEmail().id, this.getNomailing().id,
      this.getNotes().id];
  },
  showForm: function () {
    if (SessionObj.getContactAddEditTSecurity() == null || SessionObj.getContactAddEditTSecurity() == '') {
      Ext.create('M1CRM.model.crmUserSecurity', {}).getUserSecurityLevel({
        ID: 'CRMCUSTOMERCONTACTADDEDIT',
        scope: this,
        onReturn: function (rec) {
          if (rec != null) {
            SessionObj.setContactAddEditTSecurity(rec.PAccessLevel + ',' + rec.SAccessLevel);
            if (this.applyUserSecuritySettings(this.getForm().id, parseInt(rec.PAccessLevel), parseInt(rec.SAccessLevel), this.getFormButtons())) {
              this.processShowForm();
            }
          } else {
            this.toBackTap(this.getForm().id);
          }
        }
      });
    } else {
      if (this.applyUserSecuritySettings(this.getForm().id,
          parseInt(this.getPKeyFromSessionObj(SessionObj.getContactAddEditTSecurity())), parseInt(this.getSKeyFromSessionObj(SessionObj.getContactAddEditTSecurity())), this.getFormButtons())) {
        this.processShowForm();
      } else {
        this.toBackTap(this.getForm().id);
      }
    }
  },
  processShowForm: function () {
    this.getViewquotations().setDisabled(false);
    this.setComponentSecurity(this.getFormButtons());
    this.loadData();
  },
  loadData: function () {
    var mode = Ext.getCmp(this.getForm().id).config._mode;
    if (mode == 'addnew') {
      this.setEntryFormMode('addnew');

      this.getViewcalls().setHidden(true);
      this.getViewfollowups().setHidden(true);
      this.getViewquotations().setHidden(true);
      this.pairControlsShowAndHide(false, this.getTitle().id);
      Ext.getCmp(this.getForm().id).config._mode = 'addnew';

      this.makeFormAsNewEntry();

      this.getCustinfo().disable();
      this.getLocationInfo().disable();

      if (this.getForm().getRecord() != null && this.getForm().getRecord().data != null) {
        rec = this.getForm().getRecord().getData();

        if (rec.OrganizationID != null) {
          this.getCustomerid().setValue(rec.OrganizationID);
          this.getCustomer().setValue(rec.OrganizationName);
          this.getLocationid().setValue('');
          this.getLocation().setValue('');
        }

        if (this.getCustomerid().getValue() == null || this.getCustomerid().getValue() == '') {
          this.getLocationlookup().disable();
        } else {
          this.getLocationlookup().enable();
        }
        if (rec.LocationID != null) {
          this.getLocationid().setValue(rec.LocationID);
          this.getLocation().setValue(rec.LocationName);
        }
        if (rec.ContactID != null) {
          this.getLocationid().setValue(rec.ContactID);
          this.getLocation().setValue(rec.Name);
        }
        this.getForm().setRecord(null);
        this.getCustomer().enable();
        this.getLocation().enable();
        //this.getViewquotations().setHidden(true);
      } else {
        //this.makeFormAsNewEntry();
        if (this.getCustomerid().getValue() == null || this.getCustomerid().getValue() == '') {
          this.getLocationlookup().disable();
        } else {
          this.getLocationlookup().enable();
        }

      }
    } else if (mode == 'edit') {
      this.setEntryFormMode('edit');
      this.getCustomer().disable();
      this.getLocation().disable();
      this.getViewcalls().setHidden(true);
      this.getViewfollowups().setHidden(true);
      this.getViewquotations().setHidden(true);
      this.pairControlsShowAndHide(false, this.getTitle().id);
      this.getCustinfo().setHidden(true);
      this.getLocationInfo().setHidden(true);
      Ext.getCmp(this.getForm().id).config._mode = 'edit';
    } else if (mode == 'view') {
      this.setEntryFormMode('view');
      Ext.getCmp(this.getForm().id).config._mode = 'view';
      this.getCustomer().enable();
      this.getLocation().enable();
      if (this.getForm().getRecord() != null  /*&&  this.getForm().getRecord().data != null*/) {
        var rec = this.getForm().getRecord();
        if (rec.data.OrganizationID != null) {
          this.setEntryFormMode('view');
          this.clearForm();
          this.makeFormReadOnly(true);
          this.loadCustomerName();
          this.loadLocation();

          this.getContactid().parent.hide();

          this.getCustinfo().show();
          this.getCustomerlookup().hide();

          this.getLocationInfo().show();
          this.getLocationlookup().hide();

          if (rec.data.LocationID != null && rec.data.LocationID != '') {
            this.getLocationid().setValue(rec.data.LocationID);
            this.getLocation().setValue(rec.data.LocationName);
            this.getLocationInfo().enable();
          } else {
            this.getLocationid().setValue('');
            this.getLocation().setValue('');
            this.getLocationInfo().disable();
          }

          if (rec.data.ContactID != null) {
            this.getContactid().setValue(rec.data.ContactID);
            this.getSubtitle().setHtml('ID : ' + rec.data.ContactID);
            this.loadCustomerContactInfo(
              rec.data.OrganizationID,
              rec.data.LocationID,
              rec.data.ContactID
            );

            this.resetEditableForms(this.getEditableFormFields());
            //this.loadCounts();
          } else {
            this.makeFormAsNewEntry();
          }
        } else if (rec.data.LocationID != null) {
          this.getLocationid().setValue(rec.data.LocationID);
          this.getLocation().setValue(rec.data.Name);
        }
      }
    }
  },
  setTitleDescription: function (id) {
    if (this.getTitlesel().getStore() != null && this.getTitlesel().getStore().getData() != null &&
      this.getTitlesel().getStore().getData().items != null && this.getTitlesel().getStore().getData().items.length > 0) {
      var max = this.getTitlesel().getStore().getData().items.length;
      for (var i = 0; i < max; i++) {
        if (this.getTitlesel().getStore().getData().items[i].data.cmeContactTitleID == id) {
          this.getTitle().setValue(this.getTitlesel().getStore().getData().items[i].data.cmeDescription);
          break;
        }
      }
    } else {
      this.getTitle().setValue('');
    }
  },

  clearForm: function () {
    this.getCustomerid().setValue('');
    this.getLocationid().setValue('');
    this.getLocation().setValue('');
    this.getContactid().setValue('');
    this.getTitlesel().setValue('');
    this.getCustomer().setValue('');
    this.getContactname().setValue('');
    this.getPhone1().setValue('');
    this.getPhone2().setValue('');
    this.getMobile().setValue('');
    this.getFax().setValue('');
    this.getEmail().setValue('');
    this.getNomailing().uncheck();
    this.getNotes().setValue('');
  },
  makeFormAsNewEntry: function () {
    this.getSubtitle().setHtml('');
    this.setControlsReadOnly(false, [this.getContactid().id, this.getCustomer().id, this.getLocation().id, this.getContactname().id,
      this.getTitlesel().id, this.getPhone1().id, this.getPhone2().id, this.getMobile().id, this.getFax().id, this.getEmail().id, this.getNotes().id]);
    this.pairControlsShowAndHide(false, this.getTitle().id);
    this.getCustinfo().hide();
    this.getCustomerlookup().show();
    this.getLocationInfo().hide();
    this.getLocationlookup().show();
    this.getContactid().parent.show();
    this.clearContactInfo();
    this.getViewcalls().setHidden(true);
    this.getViewfollowups().setHidden(true);
    this.getViewquotations().setHidden(true);
    this.getNomailing().enable();
  },
  makeFormReadOnly: function (flag) {
    this.setControlsReadOnly(true, [this.getContactid().id, this.getCustomer().id, this.getLocation().id]);
    this.setControlsReadOnly(flag, [this.getContactname().id, this.getTitlesel().id, this.getPhone1().id,
      this.getPhone2().id, this.getMobile().id, this.getFax().id, this.getEmail().id]);
    if (flag) {

      if (!this.getCustomerlookup().isHidden()) {
        this.getCustinfo().show();
        this.getCustomerlookup().hide();
      } else {
        this.getCustinfo().show();
      }
      if (!this.getLocationlookup().isHidden()) {
        this.getLocationlookup().hide();
        this.getLocationInfo().show();
      } else {
        this.getLocationInfo().enable();
      }
      this.getNomailing().disable();
    } else {

      this.getCustinfo().disable();
      this.getLocationInfo().disable();

      this.getNomailing().enable();
      // this.getToolbarid().setTitle('Contact Details');
    }
    this.pairControlsShowAndHide(flag, this.getTitle().id);
    this.getNotes().setReadOnly(flag);

    var id = this.getForm().config._parentformid;
    if (id == 'customercalladdedit' || id == 'addnewmyfollowup') {
      this.getViewcalls().setHidden(true);
      this.getViewfollowups().setHidden(true);
      this.getViewquotations().setHidden(true);
      //this.getViewquotations().setHidden(true);
    } else {
      this.getViewcalls().setHidden(!flag);
      this.getViewfollowups().setHidden(!flag);
      this.getViewquotations().setHidden(!flag);
      if (flag) {
        this.setComponentSecurity(this.getFormButtons());
      }
    }

  },
  loadCustomerName: function (args) {
    var rec = null;
    if (Ext.getCmp('addnewcall') != null && Ext.getCmp('addnewcall').getRecord() != null && Ext.getCmp('addnewcall').getRecord().getData() != null) {
      rec = Ext.getCmp('addnewcall').getRecord();
      this.getCustomerid().setValue(rec.getData().OrganizationID);
      this.getCustomer().setValue(rec.getData().OrganizationName);
    } else if (Ext.getCmp('crmcustomerdateil') != null && Ext.getCmp('crmcustomerdateil').getRecord() != null) {
      rec = Ext.getCmp('crmcustomerdateil').getRecord();
      this.getCustomerid().setValue(rec.getData().OrganizationID);
      this.getCustomer().setValue(rec.getData().OrganizationName);
    }
    else if (Ext.getCmp('customercontactaddedit').getRecord() != null) {
      rec = Ext.getCmp('customercontactaddedit').getRecord();
      if (rec.getData().OrganizationID != null) {
        this.getCustomerid().setValue(rec.getData().OrganizationID);
        this.getCustomer().setValue(rec.getData().OrganizationName != null ? rec.getData().OrganizationName : rec.getData().Organization);
      }
    }
    this.getCustomer().setReadOnly(true);
    if (Ext.getCmp(this.getForm().config._parentformid).config._parentformid != null && Ext.getCmp(this.getForm().config._parentformid).config._parentformid == 'customeraddedit') {
      this.getCustinfo().disable();
    } else {
      this.getCustinfo().enable();
    }
  },
  loadLocation: function () {
    if (this.getForm().getRecord() != null) {
      rec = this.getForm().getRecord();
      this.getLocationid().setValue(rec.getData().LocationID);
      this.getLocation().setValue(rec.getData().LocationName);
    }
  },
  getSaveObject: function () {
    return Ext.create('M1CRM.model.crmCustomerContactAddEdit', {
      OrganizationID: this.getCustomerid().getValue(),
      LocationID: this.getLocationid().getValue() != null ? this.getLocationid().getValue() : '',
      ContactID: this.getContactid().getValue(),
      Title: this.getTitlesel().getValue(),
      OrganizationName: this.getCustomer().getValue(),
      Name: this.getContactname().getValue(),
      Phone: this.getPhone1().getValue(),
      Phone2: this.getPhone2().getValue(),
      Mobile: this.getMobile().getValue(),
      Fax: this.getFax().getValue(),
      Email: this.getEmail().getValue(),
      NoMailings: this.getNomailing().isChecked(),
      LongDescriptionText: this.getNotes().getValue()
      //user : SessionObj.statics.EmpId
    });
  },
  btnSaveTap: function () {
    var custContact = this.getSaveObject();
    var error = custContact.validate();
    if (custContact.isValid()) {
      custContact.saveData({
        scope: this,
        onReturn: function (result) {
          if (result.SaveStatus == true) {
            this.makeFormReadOnly(true);
            this.setEntryFormMode('view');
            this.getSubtitle().setHtml('ID : ' + this.getContactid().getValue());
            //this.loadCounts();
            this.scrollFormToTop();
          }
        }
      });
    } else {
      this.displayValidationMessage(error);
    }
  },
  processAutoSave: function (type) {
    if (this.isFormDirty(this.getEditableFormFields()) && Ext.getCmp(this.getForm().id).config._mode != 'view') {
      var custContact = this.getSaveObject();
      custContact.validate();
      if (custContact.isValid()) {
        custContact.saveData({
          scope: this,
          onReturn: function (result) {
            if (result.SaveStatus == true) {
              this.clearForm();
              this.processAfterAutoSave(type, this.getForm().id);
            } else {
              Ext.Msg.alert('Error', 'Error saving customer contact');
            }
          }
        });
      } else {
        Ext.Msg.confirm("", this.getRecordInCompleteMessage(), function (btn, type) {
          if (btn == 'yes') {
            this.clearForm();
            this.getForm().setRecord(null);
            this.processAfterAutoSave(type, this.getForm().id);
          }
        }, this);
      }
    } else {
      this.clearForm();
      this.getForm().setRecord(null);
      this.processAfterAutoSave(type, this.getForm().id);
    }
  },
  btnCancelTap: function () {
    if (this.getForm().getRecord() != null) {
      this.loadData();
      this.makeFormReadOnly(true);
      this.setEntryFormMode('view');
      Ext.getCmp(this.getForm().id).config._mode = 'view';
    } else {
      this.clearForm();
      this.setEntryFormMode('addnew');
      Ext.getCmp(this.getForm().id).config._mode = 'addnew';
      this.makeFormAsNewEntry();
    }
    this.getCustomer().enable();
    this.getLocation().enable();
    this.resetEditableForms(this.getEditableFormFields());
    this.scrollFormToTop();
  },
  btnEditTap: function () {
    this.makeFormReadOnly(false);
    Ext.getCmp(this.getForm().id).config._mode = 'edit';
    this.setEntryFormMode('edit');
    this.getCustomer().disable();
    this.getLocation().disable();
    this.scrollFormToTop();
  },
  btnCustomerContactAddEdit_ToCustomerDetailTap: function () {
    Ext.Viewport.getActiveItem().setRecord(null);
    this.slideRight(M1CRM.util.crmViews.getCustomerDetail());
  },
  btnCustContAddEdit_ToCustContTap: function () {
    Ext.Viewport.getActiveItem().setRecord(null);
    this.slideRight(M1CRM.util.crmViews.getCustomerContacts())
  },
  locationlookupTap: function () {
    var view = M1CRM.util.crmViews.getCustomerLocations();
    view.config._parentformid = this.getForm().id;
    this.slideLeft(view);
  },
  contactidChange: function () {
    if (this.getContactid().getValue() != '' && Ext.getCmp(this.getForm().id).config._mode != 'view') {
      this.loadCustomerContactInfo(this.getCustomerid().getValue(), this.getLocationid().getValue(), this.getContactid().getValue());
    }
  },
  contactidClearIconTap: function () {
    this.clearContactInfo();
  },
  clearContactInfo: function () {
    this.getContactid().setValue('');
    this.getContactname().setValue('');
    this.getTitlesel().setValue('');
    this.getPhone1().setValue('');
    this.getPhone2().setValue('');
    this.getMobile().setValue('');
    this.getFax().setValue('');
    this.getEmail().setValue('');
    this.getNomailing().uncheck();
    this.getNotes().setValue('');
  },
  loadCustomerContactInfo: function (orgid, locationid, contactid) {
    Ext.create('M1CRM.model.crmCustomerContactAddEdit', {
      OrganizationID: orgid,
      LocationID: locationid != null ? locationid : '',
      ContactID: contactid
    }).getIt({
      RequestID: 'data',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          if (Ext.getCmp(this.getForm().id).config._mode == 'view') {
            this.populateCustomerContactInfo(result.ResultObject);
          } else {
            msg = "Contact ID already exists for this customer. Do you wish to view this contact info?";
            Ext.Msg.confirm("Warning", msg, function (btn, type) {
              if (btn == 'yes') {
                this.populateCustomerContactInfo(result.ResultObject);
              } else {
                this.getContactid().setValue('');
              }
            }, this);
          }
        }
      }
    });
  },
  populateCustomerContactInfo: function (result) {
    this.getTitlesel().setValue(result.Title);
    this.getTitlesel().isDirty = false;
    //this.setTitleDescription(result.ContactTitleID);
    this.getLocationid().setValue(result.LocationID);
    this.getContactname().setValue(result.Name);
    this.getPhone1().setValue(result.Phone);
    this.getPhone2().setValue(result.Phone2);
    this.getMobile().setValue(result.Mobile);
    this.getFax().setValue(result.Fax);
    this.getEmail().setValue(result.Email);
    result.NoMailings == true ? this.getNomailing().check() : this.getNomailing().uncheck();
    this.getNotes().setValue(result.LongDescriptionText);
    if ((result.CustomerStatus == 0 || result.CustomerStatus == 3 ) && !this.getViewquotations().isDisabled()) {
      this.getViewquotations().setDisabled(true);
    }
    if (result.Summary != null) {
      this.setButtonBadgeText(this.getViewcalls().id, parseInt(result.Summary.OrgCalls));
      this.setButtonBadgeText(this.getViewfollowups().id, parseInt(result.Summary.OrgFollowups));
      if (!this.getViewquotations().isDisabled()) {
        this.setButtonBadgeText(this.getViewquotations().id, parseInt(result.Summary.OrgQuotes));
      }
    }
  },
  btnAddnewTap: function () {
    var temporgid = this.getCustomerid().getValue();
    var templocid = this.getLocationid().getValue();
    this.getCustomer().enable();
    this.getLocation().enable();
    this.makeFormAsNewEntry();
    this.setEntryFormMode('addnew');
    if (temporgid != '') {
      this.getCustinfo().show();
      this.getCustinfo().disable();
      this.getCustomerlookup().hide();
      Ext.create('M1CRM.model.crmCustomerContactAddEdit', {
        OrganizationID: orgid,
        LocationID: locationid != null ? locationid : '',
        ContactID: contactid
      }).getIt({
        RequestID: 'data',
        scope: this,
        onReturn: function (result) {
          if (result != null && result.RecordFound == true) {
            this.getPhone1().setValue(result.ResultObject.Phone);
            this.getPhone2().setValue(result.ResultObject.Phone2);
            this.getFax().setValue(result.ResultObject.Fax);
          }
        }
      });
    } else {
      this.getCustinfo().hide();
      this.getCustomerlookup().show();
    }
    this.setControlsReadOnly(temporgid != '' ? true : false, [this.getCustomer().id]);
  },

  getDataObject: function () {
    var dataObj = Ext.create('M1CRM.model.crmCustomerContactAddEdit', {});
    dataObj.data.OrganizationID = this.getCustomerid().getValue();
    dataObj.data.Organization = this.getCustomer().getValue();
    dataObj.data.LocationID = this.getLocationid().getValue();
    dataObj.data.LocationName = this.getLocation().getValue();
    dataObj.data.ContactID = this.getContactid().getValue();
    dataObj.data.Name = this.getContactname().getValue();

    return dataObj;
  },
  viewcallsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerOpenCalls(); // getAllCallsOnContact();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addeditcall';
    this.getForm().setRecord(this.getDataObject());
    this.slideLeft(view);
  },
  viewfollowupsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerFollowups();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'followupdetail';
    this.getForm().setRecord(this.getDataObject())
    this.slideLeft(view);
  },
  viewquotationsTap: function () {
    var view = M1CRM.util.crmViews.getAllQuotesOnContact();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addnewquote';
    Ext.getCmp(view.id).config._paramObj = this.getDataObject();
    this.slideLeft(view);
  },
  customerlookupTap: function () {
    var view = M1CRM.util.crmViews.getCustomers();
    view.config._parentformid = this.getForm().id;
    this.slideLeft(view);
  },
  titleselChange: function () {
    this.getTitle().setValue(this.getSelectFieldDisplayValue(this.getTitlesel()));
  }
});
