Ext.define('M1CRM.controller.crmPartDetail', {
  extend: 'M1CRM.util.crmFormControllerBase',
  config: {
    _viewmodeTitle: 'Part Information',
    _addmodeTitle: '',
    _editmodeTitle: '',
    refs: {
      'form': '#crmpartdateil',
      'toolbarid': '#crmpartdateil_toolbarid',
      'createQuote': '#btnlinkpart_createquotation'
    },
    control: {
      form: {
        initialize: 'initializeForm',
        show: 'showForm'
      },
      createQuote: {
        tap: 'createQuoteTap'
      }
    }
  },
  initializeForm: function () {
    this.formInitilize();
  },
  showForm: function () {
    this.clearForm();
    this.setEntryFormMode('view');
    var rec = null;
    if (Ext.getCmp('crmpartdateil').config._parentformid == 'crmcalldateil') {
      var rec = Ext.getCmp('crmpartdateil').getRecord().getData();
      Ext.getCmp('btnlinkpart_createquotation').hide();
      Ext.getCmp('partdetail_orginfoset').show();
      Ext.getCmp('partdetail_orgid').setValue(rec.cmoName);
      Ext.getCmp('partid').setValue(Ext.getCmp('calldetail_partid').getValue());
      Ext.getCmp('revision').setValue(Ext.getCmp('calldetail_partrevision').getValue());
    }
    else {
      if (Ext.getCmp('crmcustomerdateil') != null && Ext.getCmp('crmcustomerdateil').getRecord() != null) {
        rec = Ext.getCmp('crmcustomerdateil').getRecord().getData();
        Ext.getCmp('partdetail_orginfoset').show();
        Ext.getCmp('partdetail_orgid').setValue(rec.cmoOrganizationID);
        Ext.getCmp('btnlinkpart_createquotation').show();
      } else {
        Ext.getCmp('partdetail_orginfoset').hide();
        Ext.getCmp('btnlinkpart_createquotation').hide();
      }
      rec = Ext.getCmp('crmpartdateil').getRecord().getData();
      Ext.getCmp('partid').setValue(rec.PartID);
      Ext.getCmp('revision').setValue(rec.RevisionID);
      Ext.getCmp('crmpartdateil_partgroupid').setValue(rec.PartGroupID);
    }
    Ext.create('M1CRM.model.crmPartDetail', {
      PartID: Ext.getCmp('partid').getValue(),
      RevisionID: Ext.getCmp('revision').getValue(),
      PartGroupID: Ext.getCmp('crmpartdateil_partgroupid').getValue(),
      ParentId: this.getForm().id,
      Quantity: 1
    }).loadPartInfo({
      scope: this,
      onReturn: function (result) {
        if (result != null) {
          if (result.RecordFound == false && result.ResultObject != null) {
            var RevisionObj = result.ResultObject;
            Ext.getCmp('uominv').setValue(RevisionObj.IUoM);
            Ext.getCmp('description').setValue(RevisionObj.ShortDescription);
            Ext.getCmp('crmpartdateil_longdescription').setValue(RevisionObj.LongDescriptionText != null ? RevisionObj.LongDescriptionText : '');
            Ext.getCmp('qtyonhand').setValue(this.getFormatNumberWithDecimal(RevisionObj.QuantityOnHand));
            Ext.getCmp('qtyallocated').setValue(this.getFormatNumberWithDecimal(RevisionObj.QuantityAllocated));
            Ext.getCmp('qtyonpurchase').setValue(this.getFormatNumberWithDecimal(RevisionObj.QuantityOnOrderPurchases));
            Ext.getCmp('qtyonsale').setValue(this.getFormatNumberWithDecimal(RevisionObj.QuantityOnOrderSales));
            Ext.getCmp('unitsaleprice').setValue(this.getFormatNumberAsCurrency(RevisionObj.RevisedUnitPrice, true));
          } else {
            var msg = result.Message != null ? result.Messages : 'Part detail information not available';
            Ext.Msg.alert('Warning', msg, function (btn) {
              if (btn == 'ok') {
                this.toBackTap(this.getForm().id);
              }
            }, this);
          }
        } else {
          Ext.Msg.alert('Error', 'Error Accessing Part Details.', function (btn) {
            if (btn == 'ok') {
              this.toBackTap(this.getForm().id);
            }
          }, this);

        }
      }
    });
  },
  clearForm: function () {
    this.clearFormFields();
  },
  createQuoteTap: function () {
    var rec = Ext.create('M1CRM.model.crmLinkedPartQuote', {
      OrganizationID: Ext.getCmp('partdetail_orgid').getValue(),
      PartID: Ext.getCmp('partid').getValue(),
      PartRevisionID: Ext.getCmp('revision').getValue(),
      Description: Ext.getCmp('description').getValue(),
      UOM: Ext.getCmp('uominv').getValue(),
      QtyOnHand: Ext.getCmp('qtyonhand').getValue(),
      UnitSalePrice: Ext.getCmp('unitsaleprice').getValue()
    });
    var view = M1CRM.util.crmViews.getAddNewQuote();
    view.config._parentformid = 'crmpartdateil';
    Ext.getCmp('addnewquote').setRecord(rec);
    this.slideLeft(view);

  }

});
