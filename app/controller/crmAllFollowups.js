Ext.define('M1CRM.controller.crmAllFollowups', {
  extend: 'M1CRM.util.crmFollowupListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmFollowups',
    'M1CRM.store.crmFollowups'
  ],
  config: {
    _listid: 'allfollowuplist',
    _seardfieldid: 'allfollowup_search',
    control: {
      allfollowups: {
        initialize: 'initializeList',
        show: 'onShow',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.viewDetail('allfollowuplist', item);
        },
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },
  init: function () {
    this.control({
      '#allfollowuplist_addnew': {
        scope: this,
        tap: function () {
          Ext.Viewport.getActiveItem().setRecord(null);
          this.addNewFollowupTap();
        }
      }
    });
  },
  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labAllFollowupsSummary';
    this.createOptionsMenu();
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmFollowups', {}).getMyFollowupListLayout());
  },
  createOptionsMenu: function () {
    Ext.Viewport.setMenu(this.createMenu(this.getOptionMenuItems(this)), {
      side: 'right',
      reveal: true
    });
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmFollowups', {}).getMyFollowupListLayout());
  },
  onShow: function (obj, e) {
    this.initilize();
    //this.createOptionsMenu();
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmFollowups', null);
  }

});
