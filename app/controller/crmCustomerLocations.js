Ext.define('M1CRM.controller.crmCustomerLocations', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmCustomerLocations',
    'M1CRM.store.crmCustomerLocations'
  ],
  config: {
    _listid: 'customerlocationlist',
    _seardfieldid: 'customerlocation_search',
    control: {
      customerlocations: {
        initialize: 'initializeList',
        show: 'onCustomerLocationsShow',
        itemtap: 'onLocationTap',
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },

  init: function () {
    this.control({
      '#customerlocationlist_addnew': {
        scope: this,
        tap: this.addNewCustLocationTap
      }
    });
  },

  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labCustLocationSummary';
    // Ext.getCmp('customerlocationlist_options').hide();
    Ext.getCmp('customerlocationlist_toolbarid').setTitle(SessionObj.statics.OrgLabel + ' Locations');
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmCustomerLocations', {}).getListLayout());
  },
  onCustomerLocationsShow: function (obj, e) {
    this.initilize();
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  getDataObject: function () {
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
      AssignedToEmployeeID: SessionObj.statics.EmpId,
      OrganizationID: this.getCustomerID(),
      OrganizationName: this.getCustomerName()

    });
    return dataObj;
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmCustomerLocations', this.getDataObject());
  },
  addNewCustLocationTap: function () {
    var view = M1CRM.util.crmViews.getCustomerLocationAddEdit();
    view.config._parentformid = this.config._listid;
    view.setRecord(this.getDataObject());
    Ext.getCmp(view.id).config._mode = 'addnew';
    this.slideLeft(view);
  },
  getCustomerID: function () {
    var custID = "";
    var parentid = Ext.getCmp(this.config._listid).config._parentformid;
    if (parentid == 'addnewmyfollowup') {
      custID = Ext.getCmp('newmyfollowup_newcustid').getValue();
    } else if (parentid == 'followupaddedit') {
      custID = Ext.getCmp('followupaddedit_custid').getValue();
    } else if (parentid == 'addeditcall') {
      custID = Ext.getCmp('addeditcall_custid').getValue();
    } else if (parentid == 'crmcalldateil') {
      custID = Ext.getCmp('calldetail_customerid').getValue();
    } else if (parentid == 'addnewquote') {
      custID = Ext.getCmp('addnewquote_customerid').getValue();
    } else if (parentid == 'customercontactaddedit') {
      custID = Ext.getCmp('customercontactaddedit_custid').getValue();
    } else if (parentid == 'addeditlead') {
      custID = Ext.getCmp('addeditlead_custid').getValue();
    } else {
      custID = Ext.getCmp('customeraddedit_custid').getValue(); //Ext.getCmp('customeraddedit').getRecord().OrganizationID;
    }
    return custID;
  },
  getCustomerName: function () {
    return Ext.getCmp(Ext.getCmp(this.config._listid).config._parentformid + '_customer').getValue();
  },

  onLocationTap: function (nestedlist, list, index, item, rec, e) {
    var view = null;
    var searchfield = Ext.ComponentQuery.query('#' + this.config._listid + ' searchfield')[0].id;
    Ext.getCmp(searchfield).setValue('');

    if (Ext.getCmp(this.config._listid).config._parentformid != null) {
      if (Ext.getCmp(this.config._listid).config._parentformid != 'customeraddedit') {
        view = this.getPreviousView();
        view.setRecord(item);
        this.slideRight(view);
      } else {
        this.viewDetail(this.config._listid, item);
      }
    }
    /*else {
     view = M1CRM.util.crmViews.getCustomerDetail();
     view.setRecord(item);
     M1CRM.util.crmViews.slideLeft(view);
     } */
  },
  getPreviousView: function () {
    var view = null;
    if (Ext.getCmp(this.config._listid).config._parentformid != null) {
      switch (Ext.getCmp(this.config._listid).config._parentformid) {
        case  'addnewmyfollowup' : {
          view = M1CRM.util.crmViews.getAddNewFollowup();
          break;
        }
        case 'followupaddedit' : {
          view = M1CRM.util.crmViews.getFollowupAddedit();
          break;
        }
        case 'addeditcall': {
          view = M1CRM.util.crmViews.getAddEditCall();
          break;
        }
        case 'addnewcontact' : {
          view = M1CRM.util.crmViews.getAddNewContact();
          break;
        }
        case 'addnewquote' : {
          view = M1CRM.util.crmViews.getAddNewQuote();
          break;
        }
        case 'addeditlead' : {
          view = M1CRM.util.crmViews.getAddEditLead();
          break;
        }

        case 'customercontactaddedit' : {
          view = M1CRM.util.crmViews.getCustomerContactAddEdit();
          break;
        }
        default : {
          view = M1CRM.util.crmViews.getHome();
          break;
        }
      }
      Ext.getCmp(this.config._listid).config._parentformid = null;
    }
    else {
      view = M1CRM.util.crmViews.getHome();
    }
    return view;
  }


});
