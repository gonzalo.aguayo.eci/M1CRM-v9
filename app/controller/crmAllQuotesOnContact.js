Ext.define('M1CRM.controller.crmAllQuotesOnContact', {
  extend: 'M1CRM.util.crmQuotesListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmQuotations',
    'M1CRM.store.crmQuotations'
  ],
  config: {
    _listid: 'allquotesoncontactlist',
    _seardfieldid: 'allquotesoncontact_search',
    control: {
      allquotesoncontact: {
        initialize: 'initializeList',
        show: 'onShow',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.viewDetail('allquotesoncontactlist', item);
        },
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },

  init: function () {
    this.control({
      '#allquotesoncontactlist_options': {
        scope: this,
        tap: this.optionsMenuTap
      },

      '#allquotesoncontactlist_addnew': {
        scope: this,
        tap: function () {
          Ext.Viewport.getActiveItem().setRecord(null);
          this.addNewQuoteOnContactTap();
        }
      }

    });
  },

  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labAllQuotesOnContactSummary';
    this.createOptionMenu();
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmQuotations', {}).getAllOpenQuoteLayout());
  },
  createOptionMenu: function () {
    var items = this.getOptionMenuItems(this);
    M1CRM.util.crmViews.createOptionsMenu(this, items);
  },

  onShow: function (obj, e) {
    this.initilize();
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  getDataObject: function () {
    var listid = 'QUOTESONORGCONTACT';
    if (this.getParetViewId() == 'customeraddedit')
      listid = 'QUOTESONORG';
    else if (this.getParetViewId() == 'customerlocationaddedit')
      listid = 'QUOTESONORGLOC';

    var rec = this.getParamObj(this.config._listid).data != null ? this.getParamObj(this.config._listid).data : Ext.getCmp(Ext.getCmp(this.config._listid).config._parentformid).getRecord().getData();
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
      ListType: listid,
      OrganizationID: rec.OrganizationID,
      ContactID: rec.ContactID,
      LocationID: rec.LocationID
    });

    var str = 'Org:' + dataObj.data.OrganizationID;
    if (dataObj.data.LocationID != null && dataObj.data.LocationID != '')
      str += ' Loc:' + dataObj.data.LocationID;
    if (dataObj.data.ContactID != null && dataObj.data.ContactID != '') {
      str += ' Contact:' + dataObj.data.ContactID;
    }
    this.setSummary(str);

    return dataObj;
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmQuotations', this.getDataObject());
  },
  addNewQuoteOnContactTap: function () {
    var view = M1CRM.util.crmViews.getAddNewQuote();
    view.config._parentformid = this.config._listid;
    Ext.getCmp(view.id).config._mode = 'addnew';
    Ext.getCmp(view.id).setRecord(this.getParamObj(this.config._listid).data != null ? this.getParamObj(this.config._listid) : Ext.getCmp(Ext.getCmp(this.config._listid).config._parentformid).getRecord());
    M1CRM.util.crmViews.slideLeft(view);
  }
});
