Ext.define('M1CRM.controller.crmAddEditCall', {
  extend: 'M1CRM.util.crmFormControllerBase', //crmControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmAddEditCall',
    'M1CRM.model.crmLeadLines',
    'M1CRM.model.crmEmployee',
    'M1CRM.model.crmProductionProperties',
    'M1CRM.model.crmCallTypes',
    'M1CRM.store.crmCallTypes',
    'M1CRM.model.crmDatasetProperties',
    'M1CRM.model.crmCurrency',
    'M1CRM.store.crmCurrency',
    'M1CRM.model.crmEmployee',
    'M1CRM.store.crmEmployee',
    'M1CRM.model.crmStatus',
    'M1CRM.store.crmStatus',
    'M1CRM.model.crmPriority',
    'M1CRM.store.crmPriority',
    'M1CRM.model.crmContactMethod',
    'M1CRM.model.crmReason',
    'M1CRM.model.crmCallDetail',
    'M1CRM.model.crmPartRevision',
    'M1CRM.store.crmPartRevisions',
    'M1CRM.model.crmFinancialProperties'
  ],
  config: {
    _prodempDefCallType: '',
    _prodCallTypeStatus: '',
    _prodcalltypeID: '',
    _prodContactMethodID: '',
    _dtCurrencyRateID: '',
    _viewmodeTitle: 'Call',
    _addmodeTitle: 'Add Call',
    _editmodeTitle: 'Edit Call',
    _propPartsMustExist: 0,
    _tmpitemCount: 0,
    refs: {
      'form': '#addeditcall',
      'toolbarid': '#addeditcall_toolbarid',
      'subtitle': '#addeditcalltitleid',
      'calltypesel': '#addeditcall_calltypesel',
      'calltype': '#addeditcall_calltype',
      'callid': '#addeditcall_callid',
      'customerid': '#addeditcall_custid',
      'customer': '#addeditcall_customer',
      'lookupcustid': '#addeditcall_customer_lookup',
      'custinfo': '#addeditcall_customer_info',
      'locationid': '#addeditcall_locationid',
      'location': '#addeditcall_location',
      'lookuplocationid': '#addeditcall_location_lookup',
      'locationinfo': '#addeditcall_location_info',
      'contactid': '#addeditcall_contactid',
      'contact': '#addeditcall_contactname',
      'partid': '#addeditcall_partid',
      'lookuppartid': '#addeditcall_partid_lookup',
      'partidinfo': '#addeditcall_partid_info',
      'partrevision': '#addeditcall_partrevision',
      'partdesc': '#addeditcall_partdesc',
      'desc': '#addeditcall_desc',
      'notes': '#addeditcall_notes',
      'contactmethodsel': '#addeditcall_contactmethodsel',
      'contactmethod': '#addeditcall_contactmethod',
      'internalonly': '#addeditcall_internalonly',
      'prioritysel': '#addeditcall_prioritysel',
      'priority': '#addeditcall_priority',
      'statussel': '#addeditcall_statussel',
      'status': '#addeditcall_status',
      'reasonsel': '#addeditcall_reasonsel',
      'reason': '#addeditcall_reason',
      'openbysel': '#addeditcall_openbysel',
      'openby': '#addeditcall_openby',
      'opendatesel': '#addeditcall_opendatesel',
      'opendate': '#addeditcall_opendate',
      'assigntosel': '#addeditcall_assigntosel',
      'assignto': '#addeditcall_assignto',
      'assigndatesel': '#addeditcall_assigndatesel',
      'assigndate': '#addeditcall_assigndate',
      'accepteddatesel': '#addeditcall_accepteddatesel',
      'accepteddate': '#addeditcall_accepteddate',
      'closedbysel': '#addeditcall_closedbysel',
      'closedby': '#addeditcall_closedby',
      'closeddatesel': '#addeditcall_closeddatesel',
      'closeddate': '#addeditcall_closeddate',
      'currencysel': '#addeditcall_currencysel',
      'currency': '#addeditcall_currency',
      'leadid': '#addeditcall_leadid',
      'quoteid': '#addeditcall_quoteid',
      'orderid': '#addeditcall_orderid',
      'sourceinfoexpandbtn': '#addeditcall_sourceinfoexpandbtn',
      'sourceinfofieldset': '#addeditcall_sourceinfofieldset',
      'btncalllines': '#addeditcall_btncalllines',
      'btnlinkedfollowups': '#addeditcall_btnlinkedfollowups',
      'addnewbtn': '#addeditcall_addnew',
      'cancelbtn': '#addeditcall_cancelbtn',
      'savebtn': '#addeditcall_savebtn',
      'editbtn': '#addeditcall_editbtn',
      'toback': '#addeditcall_back',
      'tohome': '#addeditcall_home'
    },
    control: {
      form: {
        initialize: 'initializeForm',
        show: 'showForm',
        hide: function () {
          this.scrollFormToTop();
        }
      },
      partid: {
        clearicontap: function () {
          this.config._tmpitemCount = 0;
          this.clearPartInfo(true);
        },
        blur: 'checkPartID'
      },
      partrevision: {
        blur: 'partRevisionBlur',
        clearicontap: function () {
          this.clearPartInfo(false);
          this.getPartrevision().focus();
        }
      },
      calltypesel: {
        change: 'calltypeselChange'
      },
      lookupcustid: {
        tap: 'custLookupTap'
      },
      lookuplocationid: {
        tap: 'lookuplocationTap'
      },
      lookuppartid: {
        tap: 'lookuppartTap'
      },
      contact: {
        hide: function (field) {
          this.hideorshowContactField(field, true);
        }
      },
      contactmethodsel: {
        change: 'contactmethodselChange'
      },
      prioritysel: {
        change: 'priorityselChange'
      },
      statussel: {
        change: 'statusselChange'
      },
      reasonsel: {
        change: 'reasonselChange'
      },
      openbysel: {
        change: 'openbyselChange'
      },
      assigntosel: {
        change: 'assigntoselChange'
      },
      currencysel: {
        change: 'currencyselChange'
      },
      opendatesel: {
        change: 'opendateselChange'
      },
      assigndatesel: {
        change: 'assigndateselChange'
      },
      accepteddatesel: {
        change: 'accepteddateselChange'
      },
      closeddatesel: {
        change: 'closeddateselChange'
      },
      cancelbtn: {
        tap: 'cancelbtnTap'
      },
      addnewbtn: {
        tap: 'opaddnewbtnTap'
      },
      btncalllines: {
        tap: 'btncalllinesTap'
      },
      btnlinkedfollowups: {
        tap: 'btnlinkedfollowupsTap'
      },
      quoteinfo: {
        tap: 'quoteinfoTap'
      }
    }
  },
  initializeForm: function () {
    this.formInitilize();
    this.getDesc().setMaxLength(70);
    this.loadFormProperties();
  },

  loadFormProperties: function () {

    Ext.create('M1CRM.model.crmAddEditCall', {}).getIt({
      RequestID: 'prop',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {

          this.config._prodcalltypeID = result.ResultObject.HDCallTypeID;
          this.config._prodContactMethodID = result.ResultObject.HDContactMethodID;
          this.config._propPartsMustExist = result.ResultObject.PartsMustExist;
          this.config._prodempDefCallType = result.ResultObject.EmpDefaultCallTypeID;
          this.config._prodCallTypeStatus = result.ResultObject.CallTypeStatus;

          this.getCalltypesel().setStore(this.getStoreFromListForDropDown(result.ResultObject.CallTypesList, 'crmCallTypes', false));
          this.getCalltypesel().isDirty = false;

          this.config._dtCurrencyRateID = result.ResultObject.CurrencyRateID;
          var flag = true;
          if (this.config._dtCurrencyRateID != '') {
            flag = false;
          }
          this.getCurrencysel().setStore(this.getStoreFromListForDropDown(result.ResultObject.CurrencyCodeList, 'crmCurrency', flag));
          this.getCurrencysel().setValue(this.config._dtCurrencyRateID);
          this.getCurrencysel().isDirty = false;
          this.getCurrency().setValue(this.getSelectFieldDisplayValue(this.getCurrencysel()));

          var tempStore = this.getStoreFromListForDropDown(result.ResultObject.EmployeeList, 'crmEmployee', false);
          this.getAssigntosel().setStore(tempStore);
          this.getAssigntosel().setValue(SessionObj.getEmpId());
          this.getAssigntosel().isDirty = false;

          this.getOpenbysel().setStore(tempStore);
          this.getOpenbysel().setValue(SessionObj.getEmpId());
          this.getOpenbysel().isDirty = false;

          tempStore = this.getStoreFromListForDropDown(result.ResultObject.EmployeeList, 'crmEmployee', true);
          this.getClosedbysel().setStore(tempStore);
          this.getClosedbysel().setValue(SessionObj.getEmpId());
          this.getClosedbysel().isDirty = false;

          tempStore = null;
          this.getStatussel().setStore(Ext.create('M1CRM.model.crmStatus').getStoreFromValueList(result.ResultObject.CallStatus));
          this.getStatussel().isDirty = false;

          this.getPrioritysel().setStore(this.getStoreFromListForDropDown(result.ResultObject.PriorityList, 'crmPriority', false));
          this.getPrioritysel().isDirty = false;

          this.getContactmethodsel().setStore(this.getStoreFromListForDropDown(result.ResultObject.ContactMethodList, 'crmContactMethod', false));
          this.getContactmethodsel().isDirty = false;

          this.getReasonsel().setStore(this.getStoreFromListForDropDown(result.ResultObject.ReasonList, 'crmReason', true));
          this.getReasonsel().isDirty = false;

        }
      }
    });
  },

  getFormButtons: function () {
    return [this.getSavebtn().id, this.getCancelbtn().id];
  },
  getEditableFormFields: function () {
    return [this.getCalltypesel().id, this.getCustomer().id, this.getLocation().id, this.getContact().id, this.getPartid().id, this.getPartrevision().id, this.getPartdesc().id,
      this.getDesc().id, this.getNotes().id, this.getContactmethodsel().id, this.getInternalonly().id, this.getPrioritysel().id, this.getStatussel().id,
      this.getReasonsel().id, this.getOpenbysel().id, this.getOpendatesel().id, this.getAssigntosel().id, this.getAssigndatesel().id, this.getAccepteddatesel().id,
      this.getClosedbysel().id, this.getCloseddatesel().id, this.getCurrencysel().id
    ]
  },
  showForm: function () {
    if (SessionObj.getCallAddEditTSecurity() == null || SessionObj.getCallAddEditTSecurity() == '') {
      Ext.create('M1CRM.model.crmUserSecurity', {}).getUserSecurityLevel({
        ID: 'CRMADDEDITCALL',
        scope: this,
        onReturn: function (rec) {
          if (rec != null) {
            SessionObj.setCallAddEditTSecurity(rec.PAccessLevel + ',' + rec.SAccessLevel);
            if (this.applyUserSecuritySettings(this.getForm().id, parseInt(rec.PAccessLevel), parseInt(rec.SAccessLevel), this.getFormButtons())) {
              this.processShowForm();
            }
          } else {
            this.toBackTap(this.getForm().id);
          }
        }
      });
    } else {

      if (this.applyUserSecuritySettings(this.getForm().id,
          parseInt(this.getPKeyFromSessionObj(SessionObj.getCallAddEditTSecurity())), parseInt(this.getSKeyFromSessionObj(SessionObj.getCallAddEditTSecurity())), this.getFormButtons())) {
        this.processShowForm();
      } else {
        this.toBackTap(this.getForm().id);
      }

    }

    //this.processShowForm();
  },
  processShowForm: function () {
    var _parentid = Ext.getCmp(this.getForm().id).config._parentformid;
    if (_parentid != 'myopencallslist' || _parentid != 'allopencallslist' || _parentid != 'waitinglist'
      || _parentid != 'pendingcalllist' || _parentid != 'closecallslist' || _parentid != 'crmhome') {
      this.makeFormAsNewEntry(true);
    } else {
      this.makeFormAsNewEntry(false);
    }
    this.resetEditableForms(this.getEditableFormFields());
    this.loadFormData();
  },
  loadFormData: function () {
    var mode = Ext.getCmp(this.getForm().id).config._mode;
    if (mode == 'view') {
      this.setEntryFormMode('view');
      if (Ext.getCmp(this.getForm().id).getRecord() != null) {
        var recObj = Ext.getCmp(this.getForm().id).config._orgrec == null ? Ext.getCmp(this.getForm().id).getRecord().getData() : Ext.getCmp(this.getForm().id).config._orgrec;
        if (recObj.CallID != null) {
          this.getCallid().setValue(recObj.CallID);
          Ext.getCmp(this.getForm().id).config._orgrec = recObj;
          this.getSubtitle().setHtml('ID: ' + recObj.CallID);
        } else {
          if (this.getCallid().getValue() != null) {
            this.getSubtitle().setHtml('ID: ' + this.getCallid().getValue());
          }
        }
        this.makeFormAsNewEntry(false);
        this.getSourceinfoexpandbtn().show();
        this.getSourceinfofieldset().show();
        this.loadData(recObj);
      }
    } else if (mode == 'edit') {
      this.setEntryFormMode('edit');
      this.makeFormAsNewEntry(true);
      if (Ext.getCmp(this.getForm().id).getRecord() != null) {
        var recObj = Ext.getCmp(this.getForm().id).getRecord().getData();
        this.getLocationid().setValue(recObj.cmlLocationID);
        this.setCusLocationInfo(recObj);
        this.setContactInfo(recObj);
        this.getSourceinfoexpandbtn().hide();
        this.getSourceinfofieldset().hide();
        this.setPartInfo(recObj);
      }
    } else if (mode == 'addnew') {
      this.setEntryFormMode('addnew');
      this.makeFormAsNewEntry(true);
      this.getCallid().setValue('');
      if (Ext.getCmp(this.getForm().id).getRecord() != null) {
        var recObj = Ext.getCmp(this.getForm().id).getRecord().getData();
        this.setCustomerInfo(recObj);
        this.setCusLocationInfo(recObj);
        this.setContactInfo(recObj);
        this.getCustinfo().hide();
        this.getLookuplocationid().show();
        this.getLocationinfo().hide();
        this.getLookupcustid().enable();
        Ext.getCmp(this.getContact().id + '_lookupbtn').enable();
        this.setPartInfo(recObj);
        this.getSourceinfoexpandbtn().hide();
        this.getSourceinfofieldset().hide();
        this.getSubtitle().setHtml('');
        if (recObj.QuoteID != null) {
          this.getQuoteid().setValue(recObj.QuoteID);
          recObj = Ext.getCmp('addnewquote').getRecord().getData();


          this.setCustomerInfo(recObj);
          this.getLookupcustid().disable();
          var tObj = Ext.create('M1CRM.model.crmLocationInfo', {
            LocationID: Ext.getCmp('addnewquote_quotelocationid').getValue(),
            LocationName: Ext.getCmp('addnewquote_quotelocation').getValue()
          }).getData();
          this.setCusLocationInfo(tObj);
          this.getLookuplocationid().disable();

          var tObj = Ext.create('M1CRM.model.crmLocationInfo', {
            ContactID: Ext.getCmp('addnewquote_quotecontactsel').getValue(),
            ContactName: Ext.getCmp('addnewquote_quotecontact').getValue()
          }).getData();
          this.setContactInfo(tObj);
          Ext.getCmp(this.getContact().id + '_lookupbtn').disable();

        }
        if (recObj.LeadID != null) {
          this.getLeadid().setValue(recObj.LeadID);
          recObj = Ext.getCmp('addeditlead').getRecord().getData();
          this.setCustomerInfo(recObj);
          this.getLookupcustid().disable();
          this.setCusLocationInfo(recObj.LeadLocationInfo);
          this.getLookuplocationid().disable();
          var tObj = Ext.create('M1CRM.model.crmLocationInfo', {
            ContactID: (recObj.LeadLocationInfo || {}).ContactID || '',
            ContactName: recObj.ContactName || ''
          }).getData();

          this.setContactInfo(tObj);
          Ext.getCmp(this.getContact().id + '_lookupbtn').disable();
          //this.getLeadPartItem();
        }
      } else {
        this.hideorshowContactField(this.getContact(), true);
        this.manageLocationAndContactLookup();
        this.getCustinfo().hide();
        this.getLookupcustid().show();
        this.getLookupcustid().enable();
        this.getLookuplocationid().disable();
        this.getSourceinfoexpandbtn().hide();
        this.getSourceinfofieldset().hide();
        this.getSubtitle().setHtml('');

        this.getCustomerid().setValue('');
        this.getCustomer().setValue('');
        this.getLocationid().setValue('');
        this.getLocation().setValue('');
        this.getContact().setValue('');
        this.getContactid().setValue('');
      }
    }
  },
  getLeadPartItem: function () {
    /*
     Ext.create('M1CRM.model.crmLeadLines', {
     LeadID :  this.getLeadid().getValue()
     }).getLeadPartItem({
     scope : this,
     onReturn : function (result){
     this.setPartInfo(result);
     }
     }); */

    /*
     Ext.create('M1CRM.model.crmAddEditCall', {
     CallID : this.getCallid().getValue()
     }).getIt({
     RequestID : 'data',
     scope : this,
     onReturn : function (result){
     */

  },
  loadData: function (rec) {
    this.loadCallDetails();
  },
  setDefaultValues: function () {
    this.getCalltypesel().setValue(this.config._prodempDefCallType);
    this.getCalltypesel().isDirty = false;
    this.calltypeselChange();
    this.getContactmethodsel().setValue('PHONE');
    this.getContactmethodsel().isDirty = false;
    this.getPrioritysel().setValue('2');
    this.getPrioritysel().isDirty = false;
    this.getAssigntosel().setValue(SessionObj.getEmpId());
    this.getAssigntosel().isDirty = false;
    this.getOpenbysel().setValue(SessionObj.getEmpId());
    this.getOpenbysel().isDirty = false;
    this.getClosedbysel().setValue(SessionObj.getEmpId());
    this.getClosedbysel().isDirty = false;
    this.getOpendatesel().setValue(new Date());
    this.opendateselChange()
    this.getOpendatesel().isDirty = false;
    this.getAssigndatesel().setValue(new Date());
    this.assigndateselChange()
    this.getAssigndatesel().isDirty = false;
    this.getAccepteddatesel().setValue(new Date());
    this.accepteddateselChange();
    this.getAccepteddatesel().isDirty = false;
    this.getCloseddatesel().setValue(new Date());
    this.closeddateselChange();
    this.getCloseddatesel().isDirty = false;
    this.getReasonsel().setValue('');
    this.getReasonsel().isDirty = false;
  },
  makeFormAsNewEntry: function (flag) {
    this.pairControlsShowAndHide(!flag, this.getCalltype().id);
    this.pairControlsShowAndHide(!flag, this.getContactmethod().id);
    this.pairControlsShowAndHide(!flag, this.getPriority().id);
    this.pairControlsShowAndHide(!flag, this.getStatus().id);
    this.pairControlsShowAndHide(!flag, this.getReason().id);
    this.pairControlsShowAndHide(!flag, this.getOpenby().id);
    this.pairControlsShowAndHide(!flag, this.getOpendate().id);
    this.pairControlsShowAndHide(!flag, this.getAssignto().id);
    this.pairControlsShowAndHide(!flag, this.getAssigndate().id);
    this.pairControlsShowAndHide(!flag, this.getAccepteddate().id);
    this.pairControlsShowAndHide(!flag, this.getClosedby().id);
    this.pairControlsShowAndHide(!flag, this.getCloseddate().id);
    this.pairControlsShowAndHide(!flag, this.getCurrency().id);
    this.getPartid().setReadOnly(!flag);
    this.getPartrevision().setReadOnly(!flag);
    this.getDesc().setReadOnly(!flag);
    this.getNotes().setReadOnly(!flag);
    if (flag) {
      this.hideorshowContactField(this.getContact(), true);
      this.hideControls([this.getCustinfo().id, this.getLocationinfo().id, this.getPartidinfo().id, this.getClosedbysel().getParent().getParent().id,
        this.getClosedbysel().getParent().id, this.getCloseddatesel().getParent().id, this.getBtncalllines().getParent().id]);
      if (this.getCustomerid().getValue() == null || this.getCustomerid().getValue() == '') {
        this.setDefaultValues();
        this.showControls([this.getLookupcustid().id, this.getLookuplocationid().id, this.getLookuppartid().id]);
      } else if (this.getCustomerid().getValue() != null || this.getCustomerid().getValue() != '') {
        this.showControls([this.getLookupcustid().id, this.getLookuplocationid().id, this.getLookuppartid().id]);
        this.getLookuplocationid().enable();
      } else {
        if (this.getLocationid().getValue() == null || this.getLocation().getValue() == '') {
          this.getLookuplocationid().disable();
        } else {
          this.getLookuplocationid().enable();
        }
      }
      if ((this.getLeadid().getValue() != null && this.getLeadid().getValue() != '') || (this.getQuoteid().getValue() != null && this.getQuoteid().getValue() != '')) {
        this.disableControls([this.getLookupcustid().id, this.getLookuplocationid().id, Ext.getCmp(this.getContact().id + '_lookupbtn').id]);
      }

      this.enableControls([this.getInternalonly().id]);
    } else {
      this.hideorshowContactField(this.getContact(), false);
      this.showControls([this.getCustinfo().id, this.getLocationinfo().id, this.getPartidinfo().id, this.getBtncalllines().getParent().id]);
      this.hideControls([this.getLookupcustid().id, this.getLookuplocationid().id, this.getLookuppartid().id]);
      this.disableControls([this.getInternalonly().id]);
    }
    this.setControlsReadOnly(!flag, [this.getDesc().id]);
  },
  clearForm: function (allfields) {
    if (allfields) {
      this.getCustomerid().setValue('');
      this.getCustomer().setValue('');
      this.getLocationid().setValue('');
      this.getLocation().setValue('');
      this.getContact().setValue('');
      this.getContactid().setValue('');
    }
    this.getCallid().getValue('');
    this.getPartid().setValue('');
    this.getPartdesc().setValue('');
    this.getPartrevision().setValue('');
    this.getDesc().setValue('');
    this.getNotes().setValue('');
    this.setDefaultValues();
    this.getLeadid().setValue('');
    this.getQuoteid().setValue('');
    this.getOrderid().setValue('');
  },
  custLookupTap: function () {
    this.openOrganisation();
  },
  openOrganisation: function () {
    var view = M1CRM.util.crmViews.getCustomers();
    view.config._parentformid = this.getForm().id;
    this.slideLeft(view);
  },
  setCustomerInfo: function (rec) {
    var orgid = '';
    var orgname = '';
    var flag = false;
    if (Ext.isDefined(rec.OrganizationID) && rec.OrganizationID != '') {
      orgid = rec.OrganizationID;
      orgname = rec.OrganizationName;
      flag = true;
    }
    if (flag) {
      if (Ext.getCmp(this.getForm().id).config._mode != 'view') {
        this.enableControls([this.getLookuplocationid().id, this.getLookuppartid().id]);
      }
      if (this.getCustomerid().getValue() != orgid) {
        this.getCustomerid().setValue(orgid);
        this.getCustomer().setValue(orgname);
        this.getCustomer().isDirty = true;
        this.setCusLocationInfo(null);
        this.setContactInfo(null);
      }

      this.manageLocationAndContactLookup();
      this.setControlsReadOnly(false, [this.getDesc().id, this.getNotes().id]);
    } else {
      if (Ext.getCmp(this.getForm().config._parentformid).config._parentformid == 'customeraddedit') {
        this.getCustinfo().disable();
      } else {
        this.getCustinfo().enable();
      }
    }
  },
  manageLocationAndContactLookup: function () {
    if (this.getCustomerid().getValue() != null && this.getCustomerid().getValue() != '') {
      this.getLookuplocationid().enable();
      this.disableorenableCallLookup(this.getContact(), false);
    } else {
      this.getLookuplocationid().disable();
      this.disableorenableCallLookup(this.getContact(), true);
    }
  },
  lookuplocationTap: function () {
    if (this.getCustomerid().getValue() != null && this.getCustomerid().getValue() != '') {
      var view = M1CRM.util.crmViews.getCustomerLocations();
      view.config._parentformid = this.getForm().id;
      this.slideLeft(view);
    }
  },
  setCusLocationInfo: function (rec) {
    var locid = '';
    var locname = '';
    var flag = false;
    if (rec == null) {
      this.getLocationid().setValue(null);
      this.getLocation().setValue('');
    } else if (this.getForm().config._parentformid == 'customercontactaddedit' || this.getForm().config._parentformid == 'allcallsoncontactlist' || this.getForm().config._parentformid == 'customeropencallslist') {
      locid = rec.LocationID;
      locname = rec.LocationName != null ? rec.LocationName : rec.cmlName;
      flag = true;
    } else if (rec.cmlLocationID != null) {
      locid = rec.cmlLocationID;
      locname = rec.cmlName;
      flag = true;
    } else if (rec.LocationID != null) {
      locid = rec.LocationID;
      locname = rec.LocationName;
      flag = true;
    }
    if (flag) {
      if (locid != null)
        this.getLocationid().setValue(locid);
      if (locname != null)
        this.getLocation().setValue(locname);

      if (locid != null && locname != null) {
        this.getContact().setValue('');
        this.getContactid().setValue('');
      }

      if (Ext.getCmp(this.getForm().id).config._mode != 'view') {
        this.getLocation().isDirty = true;
      }
    }
    if (Ext.getCmp(this.getForm().id).config._mode == 'view' && (this.getLocationid().getValue() == '' || this.getLocationid().getValue() == null)) {
      this.getLocation().setValue('');
      this.getLocationinfo().disable();
    } else {
      this.getLocationinfo().enable();
    }
  },
  setContactInfo: function (rec) {
    if (rec == null) {
      this.getContactid().setValue(null);
      this.getContact().setValue('');
    } else if (this.getForm().config._parentformid == 'customercontactaddedit') {
      this.getContactid().setValue(rec.ContactID);
      this.getContact().setValue(rec.ContactName);
    } else if (this.getForm().config._parentformid == 'customeropencallslist' && rec.ContactID != null && rec.ContactName != null) {
      this.getContactid().setValue(rec.ContactID);
      this.getContact().setValue(rec.ContactName);
    } else if (rec.cmcContactID != null) {
      this.getContactid().setValue(rec.cmcContactID);
      this.getContact().setValue(rec.cmcName);
    } else if (rec.ContactID != null) {
      this.getContactid().setValue(rec.ContactID);
      this.getContact().setValue(rec.Name);
    }
    if (Ext.getCmp(this.getForm().id).config._mode != 'view') {
      this.getContact().isDirty = true;
    }
    this.hideorshowContactField(this.getContact(), false);
    var flag = ((this.getContactid().getValue() == null || this.getContactid().getValue() == '') || (this.getContact().getValue() == null || this.getContact().getValue() == '')) ? true : false;
    this.disableorenableCallDialBtn(this.getContact(), flag);
  },
  lookuppartTap: function () {
    var view = M1CRM.util.crmViews.getParts();
    view.config._parentformid = this.getForm().id;
    this.slideLeft(view);
  },
  setPartInfo: function (rec) {
    if (rec != null && rec.PartID != null && rec.PartID != '') {
      this.getPartid().setValue(rec.PartID);
      this.getPartrevision().setValue(rec.RevisionID);
      this.getPartdesc().setValue(rec.Description != null ? rec.Description : rec.ShortDescription != null ? rec.ShortDescription : '');
      this.getPartid().isDirty = false;
      this.getPartidinfo().enable();
    } else {
      this.getPartidinfo().disable();
      this.getPartid().setValue('');
      this.getPartrevision().setValue('');
      this.getPartdesc().setValue('');
      this.getPartid().isDirty = false;
      this.getPartidinfo().disable();
    }
  },
  statusselChange: function () {
    if (this.getStatussel().getValue() == 'C') {
      this.getCloseddatesel().parent.parent.show();
      this.getClosedbysel().parent.show();
      this.getCloseddatesel().setValue(new Date());
      this.getClosedbysel().setValue(SessionObj.getEmpId());
      this.getCloseddatesel().parent.show();
    } else {
      if (this.getStatussel().getValue() == 'W') {
        this.getAccepteddatesel().parent.hide();
      } else {
        this.getAccepteddatesel().parent.show();
      }
      this.getClosedbysel().parent.hide();
      this.getCloseddatesel().parent.hide();
      this.getCloseddatesel().parent.parent.hide();
    }
    this.getStatus().setValue(this.getSelectFieldDisplayValue(this.getStatussel()));
  },
  calltypeselChange: function () {
    var tempCallType = this.getCalltypesel().getValue();
    switch (tempCallType) {
      case 'FIN': {
        this.getInternalonly().check();
        this.getStatussel().setValue(this.config._prodCallTypeStatus.FIN);
        break;
      }
      case 'SALES': {
        this.getInternalonly().check();
        this.getStatussel().setValue(this.config._prodCallTypeStatus.SALES);
        break;
      }
      case 'QUAL': {
        this.getInternalonly().check();
        this.getStatussel().setValue(this.config._prodCallTypeStatus.QUAL);
        break;
      }
      case 'SUPP': {
        this.getStatussel().setValue(this.config._prodCallTypeStatus.SUPP);
        this.getInternalonly().uncheck();
        break;
      }
    }
    this.getCalltype().setValue(this.getSelectFieldDisplayValue(this.getCalltypesel()));
  },
  contactmethodselChange: function () {
    this.getContactmethod().setValue(this.getSelectFieldDisplayValue(this.getContactmethodsel()));
  },
  priorityselChange: function () {
    this.getPriority().setValue(this.getSelectFieldDisplayValue(this.getPrioritysel()));
  },
  reasonselChange: function () {
    this.getReason().setValue(this.getSelectFieldDisplayValue(this.getReasonsel()));
  },
  openbyselChange: function () {
    this.getOpenby().setValue(this.getSelectFieldDisplayValue(this.getOpenbysel()));
  },
  assigntoselChange: function () {
    this.getAssignto().setValue(this.getSelectFieldDisplayValue(this.getAssigntosel()));
  },
  currencyselChange: function () {
    this.getCurrency().setValue(this.getSelectFieldDisplayValue(this.getCurrencysel()));
  },
  opendateselChange: function () {
    this.getOpendate().setValue(formatDate(this.getOpendatesel().getValue(), SessionObj.getSimpleDateFormat()));
  },
  assigndateselChange: function () {
    this.getAssigndate().setValue(formatDate(this.getAssigndatesel().getValue(), SessionObj.getSimpleDateFormat()));
  },
  accepteddateselChange: function () {
    this.getAccepteddate().setValue(formatDate(this.getAccepteddatesel().getValue(), SessionObj.getSimpleDateFormat()));
  },
  closeddateselChange: function () {
    this.getCloseddate().setValue(formatDate(this.getCloseddatesel().getValue(), SessionObj.getSimpleDateFormat()));
  },
  cancelbtnTap: function () {
    if (this.getForm().getRecord() == null) {
      this.clearForm(true);
      this.makeFormAsNewEntry(true);
      this.getCallid().setValue(null);
      this.getSourceinfoexpandbtn().hide();
      this.getSourceinfofieldset().hide();
      this.resetEditableForms(this.getEditableFormFields());
    } else {
      if (Ext.getCmp(this.getForm().id).config._orgrec != null) {
        Ext.getCmp(this.getForm().id).config._mode = 'view';
        this.loadData(Ext.getCmp(this.getForm().id).config._orgrec);
        this.makeFormAsNewEntry(false);
        this.setEntryFormMode('view');
        this.hideorshowContactField(this.getContact(), false);
        this.getSubtitle().setHtml('ID: ' + this.getCallid().getValue());
        this.getSourceinfoexpandbtn().show();
        this.getSourceinfofieldset().show();
      } else {
        this.clearForm(true);
        this.getCallid().setValue(null);
        this.makeFormAsNewEntry(true);
        this.getSourceinfoexpandbtn().hide();
        this.getSourceinfofieldset().hide();
        //this.loadTotals();
        this.resetEditableForms(this.getEditableFormFields());
      }
    }
    this.scrollFormToTop();
  },
  btnSaveTap: function () {
    var saveDataObj = this.getSaveObject();
    var error = saveDataObj.validate();
    if (saveDataObj.isValid()) {
      saveDataObj.saveData({
        scope: this,
        onReturn: function (result) {
          if (result.SaveStatus == true) {
            if (this.getCallid().getValue() != result.ResultObject.CallID) {
              this.getCallid().setValue(result.ResultObject.CallID);
              this.getSubtitle().setHtml('ID: ' + this.getCallid().getValue());
            }
            Ext.getCmp(this.getForm().id).config._orgrec = saveDataObj;
            Ext.getCmp(this.getForm().id).config._mode = 'view';
            this.makeFormAsNewEntry(false);
            this.setEntryFormMode('view');
            this.getSourceinfoexpandbtn().show();
            this.getSourceinfofieldset().show();
            //this.loadTotals();
            this.resetEditableForms(this.getEditableFormFields());
          } else {
            this.setEntryFormMode('addnew');
            Ext.Msg.alert('Error', 'Error saving call info');
          }
        }

      });
    }
    else {
      this.displayValidationMessage(error);
    }
  },

  processAutoSave: function (type) {
    if (this.isFormDirty(this.getEditableFormFields())) {
      var saveDataObj = this.getSaveObject();
      saveDataObj.validate();
      if (saveDataObj.isValid()) {
        saveDataObj.saveData({
          scope: this,
          onReturn: function (result) {
            if (result.SaveStatus == true) {
              this.clearForm(true);
              this.processAfterAutoSave(type, this.getForm().id);
            } else {
              this.displaySaveErrorMessage('customer call');
            }
          }
        });
      } else {
        Ext.Msg.confirm("", this.getRecordInCompleteMessage(), function (btn, type) {
          if (btn == 'yes') {
            this.clearForm(true);
            this.processAfterAutoSave(type, this.getForm().id);
          }
        }, this);
      }
    }
    else {
      this.clearForm(true);
      Ext.getCmp(this.getForm().id).config._mode = 'view';
      this.processAfterAutoSave(type, this.getForm().id);
    }
  },

  opaddnewbtnTap: function () {
    Ext.getCmp(this.getForm().id).config._mode = 'addnew';
    this.setEntryFormMode('addnew');
    this.getCallid().setValue(null);
    this.getSubtitle().setHtml('');
    this.getForm().setRecord(null);
    this.clearForm(false);
    this.getCallid().setValue(null);
    this.makeFormAsNewEntry(true);
    this.manageLocationAndContactLookup();
    this.getSourceinfoexpandbtn().hide();
    this.getSourceinfofieldset().hide();
    this.scrollFormToTop();
  },
  btnEditTap: function () {
    if (this.getForm().getRecord() != null) {
      Ext.getCmp(this.getForm().id).config._mode = 'edit';
      this.makeFormAsNewEntry(true);
      this.hideorshowContactField(this.getContact(), true);
      this.getSourceinfoexpandbtn().hide();
      this.getSourceinfofieldset().hide();
      this.getSubtitle().setHtml('ID: ' + this.getCallid().getValue());
    }
  },

  loadCallDetails: function () {
    Ext.create('M1CRM.model.crmAddEditCall', {
      CallID: this.getCallid().getValue()
    }).getIt({
      RequestID: 'data',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          recObj = result.ResultObject;
          this.getCalltypesel().setValue(recObj.CallTypeID);
          this.getCustomerid().setValue(recObj.OrganizationID);
          this.getCustomer().setValue(recObj.OrganizationName);
          this.getLocationid().setValue(recObj.LocationID);
          this.getLocation().setValue(recObj.LocationName);

          this.getContactid().setValue(recObj.ContactID);
          this.getContact().setValue(recObj.ContactName);

          if (recObj.PartID != null && recObj.PartID != '') {
            this.getPartid().setValue(recObj.PartID);
            this.getPartrevision().setValue(recObj.PartRevisionID);
            this.getPartdesc().setValue(recObj.PartShortDescription);
            this.getPartidinfo().enable();
          } else {
            this.getPartid().setValue('');
            this.getPartrevision().setValue('');
            this.getPartdesc().setValue('');
            this.getPartidinfo().disable();
          }

          this.getDesc().setValue(recObj.ShortDescription);
          this.getNotes().setValue(recObj.LongDescriptionText);
          this.getDesc().setReadOnly(true);
          this.getNotes().setReadOnly(true);
          var tempCallType = recObj.CallTypeID != '' ? recObj.CallTypeID : this.config._prodcalltypeID;
          this.getCalltypesel().setValue(tempCallType);
          this.getCalltype().setValue(this.getSelectFieldDisplayValue(this.getCalltypesel()));
          var tempContactMethod = recObj.ContactMethodID != null && recObj.ContactMethodID != '' ? recObj.ContactMethodID : this.config._prodContactMethodID;
          this.getContactmethodsel().setValue(tempContactMethod);
          this.getContactmethod().setValue(this.getSelectFieldDisplayValue(this.getContactmethodsel()));
          this.getAssignto().setValue(recObj.AssignedTo);
          this.getStatussel().setValue(recObj.CallStatus);
          this.getStatus().setValue(this.getSelectFieldDisplayValue(this.getStatussel()));
          if (parseInt(recObj.InternalOnly) == 1) {
            this.getInternalonly().check();
          } else {
            this.getInternalonly().uncheck();
          }

          this.disableControls([this.getInternalonly().id]);
          this.getPrioritysel().setValue(recObj.PriorityID);
          this.getPriority().setValue(this.getSelectFieldDisplayValue(this.getPrioritysel()));
          this.getReasonsel().setValue(recObj.ReasonID);
          this.getReason().setValue(this.getSelectFieldDisplayValue(this.getReasonsel()));
          this.getOpenbysel().setValue(recObj.OpenedBy);
          this.getOpenby().setValue(this.getSelectFieldDisplayValue(this.getOpenbysel()));
          this.getAssigntosel().setValue(recObj.AssignedTo);
          this.getAssignto().setValue(this.getSelectFieldDisplayValue(this.getAssigntosel()));

          var tdate = this.getCorrectDate(recObj.AssignedDate);

          this.getAssigndate().setValue(formatDate(tdate, SessionObj.getSimpleDateFormat()));
          this.getAssigndatesel().setValue(new Date(tdate));

          if (recObj.OpenedDate != null) {
            var tdate = this.getCorrectDate(recObj.OpenedDate);
            this.getOpendate().setValue(formatDate(tdate, SessionObj.getSimpleDateFormat()));
            this.getOpendatesel().setValue(new Date(tdate));
          }
          if (recObj.AcceptedDate != null) {
            var tdate = this.getCorrectDate(recObj.AcceptedDate);
            this.getAccepteddate().setValue(formatDate(tdate, SessionObj.getSimpleDateFormat()));
            this.getAccepteddatesel().setValue(new Date(tdate));

          }
          this.getClosedbysel().setValue(recObj.ClosedBy);
          if (recObj.ClosedDate != null) {
            var tdate = this.getCorrectDate(recObj.AcceptedDate);
            this.getCloseddate().setValue(new Date(tdate, SessionObj.getSimpleDateFormat()));
            this.getCloseddatesel().setValue(new Date(tdate));
          } else {
            this.getCloseddate().setValue(this.getOpendate().getValue());
            this.getCloseddatesel().setValue(new Date(this.getOpendate().getValue()));
          }
          this.getCurrencysel().setValue(recObj.CurrencyRateID);
          this.getCurrency().setValue(this.getSelectFieldDisplayValue(this.getCurrencysel()));
          this.getLeadid().setValue(recObj.LeadID);
          this.getQuoteid().setValue(recObj.QuoteID);
          this.getOrderid().setValue(recObj.SalesOrderID);
          this.resetEditableForms(this.getEditableFormFields());

          var total = parseInt(result.ResultObject.Summary.CallLines);
          this.getBtncalllines().setBadgeText(total > 0 ? total : null);
          total = parseInt(result.ResultObject.Summary.CallFollowups);
          this.getBtnlinkedfollowups().setBadgeText(total > 0 ? total : null);
        } else {

        }
      }
    });
  },
  getSaveObject: function () {
    return Ext.create('M1CRM.model.crmAddEditCall', {
      CallID: this.getCallid().getValue() != null ? this.getCallid().getValue() : '',
      OrganizationID: this.getCustomerid().getValue(),
      OrganizationName: this.getCustomer().getValue(),
      CallTypeID: this.getCalltypesel().getValue() != null ? this.getCalltypesel().getValue() : '',
      LocationID: this.getLocationid().getValue() != null ? this.getLocationid().getValue() : '',
      LocationName: this.getLocation().getValue(),
      ContactID: this.getContactid().getValue() != null ? this.getContactid().getValue() : '',
      Contact: this.getContact().getValue(),
      PartID: this.getPartid().getValue(),
      PartRevisionID: this.getPartrevision().getValue(),
      PartShortDescription: this.getPartdesc().getValue(),
      ShortDescription: this.getDesc().getValue(),
      LongDescriptionText: this.getNotes().getValue(),
      ContactMethodID: this.getContactmethodsel().getValue() != null ? this.getContactmethodsel().getValue() : '',
      InternalOnly: this.getInternalonly().isChecked(),
      PriorityID: this.getPrioritysel().getValue(),
      CallStatus: this.getStatussel().getValue(),
      ReasonID: this.getReasonsel().getValue() != null ? this.getReasonsel().getValue() : '',
      OpenedBy: this.getOpenbysel().getValue(),
      OpenedDate: this.getOpendatesel().getValue(),
      AssignedTo: this.getAssigntosel().getValue(),
      AssignedDate: this.getAssigndatesel().getValue(),
      AcceptedDate: this.getAccepteddatesel().getValue(),
      ClosedBy: this.getClosedbysel().getValue(),
      ClosedDate: this.getCloseddatesel().getValue(),
      CurrencyRateID: this.getCurrencysel().getValue(),
      LeadID: this.getLeadid().getValue() != null ? this.getLeadid().getValue() : '',
      QuoteID: this.getQuoteid().getValue() != null ? this.getQuoteid().getValue() : '',
      SalesOrderID: this.getOrderid().getValue() != null ? this.getOrderid().getValue() : ''
    });
  },
  btncalllinesTap: function () {
    this.getForm().setRecord(this.getSaveObject());
    var view = M1CRM.util.crmViews.getCallLines();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'calllineaddedit';
    this.slideLeft(view);
  },
  btnlinkedfollowupsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerFollowups();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'followupaddedit';
    this.slideLeft(view);
  },
  clearPartInfo: function (flag) {
    this.getPartrevision().setValue('');
    this.getPartrevision().setReadOnly(flag);
    this.getPartdesc().setValue('');
    this.getPartid().focus();
  },
  checkPartID: function () {
    this.config._tmpitemCount = 0;
    if (this.getPartid().getValue() != '') {

      Ext.create('M1CRM.model.crmAddEditCall', {
        PartID: this.getPartid().getValue()
      }).getIt({
        RequestID: 'checkpart',
        scope: this,
        onReturn: function (result) {
          if (result != null) {
            if (this.config._propPartsMustExist == true && result.RecordFound == false) {
              Ext.Msg.alert("Warning", "Part does not exist in the database",
                function (btn) {
                  if (btn == 'ok') {
                    this.getPartid().setValue('');
                    this.getPartid().focus();
                  }
                }, this);
            }

            else if (this.config._propPartsMustExist == false && parseInt(result.ResultObject.PartRevisionCount) <= 0 && Ext.getCmp(this.getForm().id).config._mode != 'view') {
              this.getPartrevision().setReadOnly(false);
              this.getPartdesc().setReadOnly(false);
              this.getPartrevision().focus();
            } else if (parseInt(result.ResultObject.PartRevisionCount) > 1) {
              this.config._tmpitemCount = parseInt(result.ResultObject.PartRevisionCount);
              this.getPartrevision().setReadOnly(false);
              this.displayPartRevisions();
            } else if (parseInt(result.ResultObject.PartRevisionCount) == 1) {
              this.config._tmpitemCount = 1;
              this.getPartrevision().setValue(result.ResultObject.PartRevision.RevisionID);
              this.getPartrevision().setReadOnly(true);
              this.getPartdesc().setValue(result.ResultObject.PartRevision.ShortDescription);
            }
          }
        }
      });
    }
  },
  displayPartRevisions: function () {
    var view = M1CRM.util.crmViews.getPartRevisions();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = null;
    this.getForm().setRecord(this.getPartRevisionDataObject());
    this.slideLeft(view);
  },
  getPartRevisionDataObject: function () {
    var dataObj = Ext.create('M1CRM.model.crmPartRevision', {});
    dataObj.data.user = SessionObj.getEmpId();
    dataObj.data.PartID = this.getPartid().getValue();
    dataObj.data.ParentId = this.getForm().id;
    return dataObj;
  },
  partRevisionBlur: function () {
    if (this.getPartid().getValue() != '' && !this.getPartrevision().getReadOnly() && this.getPartrevision().getValue() == '' && this.config._tmpitemCount > 0 && this.config._propPartsMustExist != 0) {
      Ext.Msg.alert('', 'There are multiple revisions for this part, enter required revision id.',
        function (btn) {
          if (btn == 'ok') {
            this.getPartrevision().focus();
          }
        }, this);
      this.getPartrevision().focus();
    }
  },
  partRevisionChange: function () {
    if (this.getPartid().getValue() != '' && !this.getPartrevision().getReadOnly() && this.config._tmpitemCount > 0) {
      this.getPartInfo(true);
    }
  },
  getPartInfo: function (flag) {
    Ext.create('M1CRM.model.crmPartRevision', {
      PartID: this.getPartid().getValue(),
      RevisionID: this.getPartrevision().getValue(),
      ParentId: this.getForm().id
    }).loadPartInfo({
      scope: this,
      onReturn: function (result) {
        if (result != null && parseInt(result.RecCount) > 0) {
          if (flag) {
            this.getPartdesc().setValue(result.RevisionObj.ShortDescription);
          } else {
            this.viewInfo('part', 'addeditquotepart', null, null, null, null, null, null, this.getPartid().getValue(), this.getPartrevision().getValue(), null);
          }
        } else if (this.config._propPartsMustExist != 0) {
          msg = flag ? 'Part Revision does not exist in the database.' : 'Part does not exist in the database.';
          Ext.Msg.alert('', msg,
            function (btn) {
              if (btn == 'ok') {
                if (flag) {
                  this.clearPartInfo(false);
                  this.getPartrevision().focus();
                }
              }
            }, this);
        }
      }
    });
  },
  quoteinfoTap: function () {
    if (this.getQuoteid().getValue() != null && this.getQuoteid().getValue() != '') {
      var view = M1CRM.util.crmViews.getAddNewQuote();
      view.config._parentformid = this.getForm().id;
      view.config._detailformid = null;
      this.slideRight(view);
    }
  }
});

