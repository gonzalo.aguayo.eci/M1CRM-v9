Ext.define('M1CRM.controller.crmCustomerOpenCalls', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmCalls',
    'M1CRM.store.crmCalls'
  ],
  config: {
    _listid: 'customeropencallslist',
    _seardfieldid: 'customeropencall_search',
    control: {
      customeropencalls: {
        initialize: 'initializeList',
        show: 'showCustomerOpenCalls',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.viewDetail(this.config._listid, item);
        },
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },
  init: function () {
    this.control({
      '#customeropencallslist_addnew': {
        scope: this,
        tap: this.addNewCustomerCallTap
      }
    });
  },

  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labCustOpenCallsSummary';
    Ext.getCmp('customeropencallslist_toolbarid').setTitle('Linked Open Calls');
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmCalls', {}).getMyOpenCallsListLayout());

  },
  showCustomerOpenCalls: function (obj, e) {
    this.initilize();
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  getDataObject: function () {
    var parentviewid = this.getParetViewId();
    if (parentviewid == 'customeraddedit') {
      var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
        OrganizationID: Ext.getCmp(parentviewid + '_custid').getValue(),
        OrganizationName: Ext.getCmp(parentviewid + '_customer').getValue(),
        LocationID: '',
        ListType: 'CUSTOMEROPENCALLS'
      });
      this.setSummary('Org:' + Ext.getCmp(parentviewid + '_custid').getValue());
    } else if (parentviewid == "customerlocationaddedit" || parentviewid == 'customercontactaddedit') {

      var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
        OrganizationID: Ext.getCmp(parentviewid + '_custid').getValue(),
        OrganizationName: Ext.getCmp(parentviewid + '_customer').getValue(),
        LocationID: Ext.getCmp(parentviewid + '_locationid').getValue(),
        LocationName: Ext.getCmp(parentviewid + '_location').getValue(),
        ListType: 'CUSTOMEROPENCALLSONLOC'
      });
      this.setSummary('Org:' + Ext.getCmp(parentviewid + '_custid').getValue() + ' Loc :' + Ext.getCmp(parentviewid + '_locationid').getValue());

    } else if (parentviewid == 'addnewquote') {

      var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
        QuoteID: Ext.getCmp(parentviewid + '_quoteid').getValue(),
        ListType: 'CUSTOMEROPENCALLSQUOTE'
      });
      this.setSummary('QuoteID:' + Ext.getCmp(parentviewid + '_quoteid').getValue());


    } else if (parentviewid == 'addeditlead') {
      var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
        LeadID: Ext.getCmp(parentviewid + '_leadid').getValue(),
        ListType: 'CUSTOMEROPENCALLSLEAD'
      });
      this.setSummary('LeadID:' + Ext.getCmp(parentviewid + '_leadid').getValue());
    }
    return dataObj;
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmCalls', this.getDataObject());
  },
  addNewCustomerCallTap: function () {
    var view = M1CRM.util.crmViews.getAddEditCall();
    view.config._parentformid = this.config._listid;
    view.setRecord(this.getDataObject());
    Ext.getCmp(view.id).config._mode = 'addnew';
    M1CRM.util.crmViews.slideLeft(view);
  }


});
