Ext.define('M1CRM.controller.crmHome', {
  extend: 'M1CRM.util.crmControllerBase',
  config: {
    refs: {
      'form': '#crmhome',
      'myFollowUp': '#crmhome_myopenfollowups',
      'addnewfollowup': '#crmhome_addnewfollowup',
      'allfollowups': '#crmhome_searchfollowups',
      'myCalls': '#crmhome_myopencalls',
      'addnewcall': '#crmhome_addnewcall',
      'allopencalls': '#crmhome_searchcalls',
      'calls': '#btnHomeCalls',
      'contacts': '#crmhome_contacts',
      'followups': '#btnHome_Followups',
      'organisations': '#crmhome_searchorg',
      'btnLogout': '#crmhome_logout',
      'addnewcontact': '#crmhome_addnewcontact',
      'myopenquotes': '#crmhome_myopenquotes',
      'addnewquote': '#crmhome_addnewquote',
      'allopenquotes': '#crmhome_searchquote',
      'searchparts': '#crmhome_searchparts',
      'myopenleads': '#crmhome_myopenleads',
      'addnewlead': '#crmhome_addnewlead',
      'searchleads': '#crmhome_searchleads'
    },
    control: {
      form: {
        initialize: 'initializeForm',
        show: 'showForm'
      },
      myFollowUp: {
        tap: 'myFollowUpTap'
      },
      myCalls: {
        tap: 'myCallsTap'
      },
      followups: {
        tap: 'followupsTap'
      },
      organisations: {
        tap: 'organisationsTap'
      },
      'button[itemId= crmhome_addneworg]': {
        tap: 'addneworganisationsTap'
      },
      contacts: {
        tap: 'contactsTap'
      },
      calls: {
        tap: 'allCallsTap'
      },
      btnLogout: {
        tap: 'logoutTap'
      },
      allfollowups: {
        tap: 'allfollowupsTap'
      },
      addnewfollowup: {
        tap: 'addnewfollowupTap'
      },
      allopencalls: {
        tap: 'allopencallsTap'
      },
      addnewcall: {
        tap: 'addnewcallTap'
      },
      addnewcontact: {
        tap: 'addnewcontactTap'
      },
      myopenquotes: {
        tap: 'myopenquotesTap'
      },
      addnewquote: {
        tap: 'addnewquoteTap'
      },
      allopenquotes: {
        tap: 'allopenquotesTap'
      },
      searchparts: {
        tap: 'searchpartsTap'
      },
      addnewlead: {
        tap: 'addnewleadTap'
      },
      myopenleads: {
        tap: 'myopenleadsTap'
      },
      searchleads: {
        tap: 'searchleadsTap'
      }
    }
  },

  initializeForm: function () {
    this.setLabels();
  },
  setLabels: function () {
    Ext.getCmp('crmhome_searchorg').setText(SessionObj.getOrgLabel() + 's');
    this.getForm().query('#crmhome_toolbartitle')[0].getTitle().setTitle(SessionObj.getOrgName());
  },

  getFormButtons: function () {
    return ['crmhome_myopenfollowups',
      'crmhome_addnewfollowup',
      'crmhome_myopencalls',
      'crmhome_addnewcall',
      'crmhome_contacts',
      'crmhome_addnewcontact',
      'crmhome_searchorg',
      'crmhome_myopenquotes',
      'crmhome_addnewquote',
      'crmhome_myopenleads',
      'crmhome_addnewlead'
    ];
  },
  checkComponentSecurity: function (done) {
    var that = this;
    var userSecurity = Ext.create('M1CRM.model.crmUserSecurity', {});
    userSecurity.getComponentSecurity({
      module: 'CRM',
      scope: this,
      onReturn: function (result) {
        var decodedResult = Ext.decode(result) || {};
        var message = '';

        if (+decodedResult.Status === 200) {
          var dataObj = decodedResult.DataObject || {};

          if (!dataObj.Basic) {
            message = 'Component security not defined for basic CRM';
            return Ext.Msg.alert('Warning', message, function (btn) {
              if (btn === 'ok') {
                Ext.create('M1CRM.model.crmLogin', {}).logOut({});
                var view = M1CRM.util.crmViews.getCRMLogin();
                Ext.Viewport.animateActiveItem(view, {type: 'slide', direction: 'right'});
              }
            });
          } else {
            SessionObj.setCRMBasic(dataObj.Basic);
          }
          
          if (dataObj.Advance) {
            userSecurity.checkCRMAdvance({
              scope: this,
              onReturn: function (result) {
                dataObj = Ext.decode(result).DataObject || {};
                SessionObj.setCRMAdvace(dataObj.EnableCRMAdv);
                SessionObj.setQuotationManagement(dataObj.QuoteManagement);
                SessionObj.setLeadManagement(dataObj.LeadManagement);
                that.enableControls(that.getFormButtons());
              }
            })
          } else {
            SessionObj.setQuotationManagement(false);
            SessionObj.setLeadManagement(false);
            SessionObj.setCRMAdvace(false);
            that.enableControls(that.getFormButtons());
          }
        }

        done();
      }
    });
  },

  showForm: function () {
    var that = this;
    var done = function () {
      var buttons = that.getFormButtons();
      var applyAccessOnControls = function (rec) {
        that.applyUserSecurityOnControl(rec.Followups, {'View': 'crmhome_myopenfollowups', 'Add': 'crmhome_addnewfollowup'});
        that.applyUserSecurityOnControl(rec.Calls, {'View': 'crmhome_myopencalls', 'Add': 'crmhome_addnewcall'});
        that.applyUserSecurityOnControl(rec.Contacts, {'View': 'crmhome_contacts', 'Add': 'crmhome_addnewcontact'});
        that.applyUserSecurityOnControl(rec.Organizations, {'View': 'crmhome_searchorg', 'Add': 'crmhome_addneworg'});
        that.applyUserSecurityOnControl(rec.Quotes, {'View': 'crmhome_myopenquotes', 'Add': 'crmhome_addnewquote'});
        that.applyUserSecurityOnControl(rec.Leads, {'View': 'crmhome_myopenleads', 'Add': 'crmhome_addnewlead'});
        that.applyUserSecurityOnControl(rec.Parts, {'View': 'crmhome_searchparts'});
      };
      that.enableControls(buttons);
      
      if (_.isEmpty(that.config._userkey)) {
        Ext.create('M1CRM.model.crmUserSecurity', {}).getUserSecurityLevel({
          ID: that.getForm().id,
          scope: that,
          onReturn: function (rec) {
            if (rec) {
              var setAccessLevel = SessionObj.setAccessLevel();
              applyAccessOnControls(rec);
              setAccessLevel(_.clone(rec));
              that.processShowForm();
            } else {
              Ext.Msg.alert('Warning', 'User security not defined for this view, unable to proceed.', function (btn) {
                if (btn === 'ok') {
                  that.logoutTap();
                }
              }, that);
            }
          }
        });
      } else {
        var rec = SessionObj.getAccessLevel();
        applyAccessOnControls(rec);
        that.processShowForm();
      }
    };

    this.checkComponentSecurity(done);
  },
  processShowForm: function () {
    this.setComponentSecurity(this.getFormButtons());
    this.loadTotals();
  },
  loadTotals: function () {
    Ext.create('M1CRM.model.crmHome', {EmployeeID: SessionObj.getEmpId()}
    ).getTotals({
      ID: this.getForm().id,
      scope: this,
      onReturn: function (result) {
        if (result) {
          if (result.MyOpenFollowUps > 0) {
            var infoFollowups = 'Follow-Ups <br/><font size="2">Count :' + result.MyOpenFollowUps;
            Ext.getCmp('crmhome_myopenfollowups').setText(infoFollowups);
          } else {
            Ext.getCmp('crmhome_myopenfollowups').setText('Follow-Ups');
          }

          if (SessionObj.getCallManagement()) {
            if (result.MyOpenCalls > 0) {
              var infoCalls = 'Calls <br/><font size="2">Count :' + result.MyOpenCalls + '</font>';
              Ext.getCmp('crmhome_myopencalls').setText(infoCalls);
            } else {
              Ext.getCmp('crmhome_myopencalls').setText('Calls');
            }
          } else {
            Ext.getCmp('crmhome_myopencalls').setText('Calls');
          }
          if (SessionObj.getCRMAdvace()) {
            if (result.MyOpenQuotes > 0) {
              var infoQuotes = 'Quotes <br/><font size="2">Count :' + result.MyOpenQuotes;
              Ext.getCmp('crmhome_myopenquotes').setText(infoQuotes);
            } else {
              Ext.getCmp('crmhome_myopenquotes').setText('Quotes');
            }
            if (result.MyOpenLeads > 0) {
              var infoLeads = 'Leads <br/><font size="2">Count :' + result.MyOpenLeads + '</font>';
              Ext.getCmp('crmhome_myopenleads').setText(infoLeads);
            } else {
              Ext.getCmp('crmhome_myopenleads').setText('Leads');
            }
          } else {
            Ext.getCmp('crmhome_myopenquotes').setText('Quotes');
            Ext.getCmp('crmhome_myopenleads').setText('Leads');

          }
        } else {
          this.setButtonBadgeText('crmhome_myopencalls', 0);
          this.setButtonBadgeText('crmhome_myopencalls', 0);
          this.setButtonBadgeText('crmhome_myopenquotes', 0);
          this.setButtonBadgeText('crmhome_myopenleads', 0);
        }
      }
    });
  },

  myFollowUpTap: function () {
    var view = M1CRM.util.crmViews.getMyFollowups();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'followupdetail';
    M1CRM.util.crmViews.slideLeft(view);
  },
  allfollowupsTap: function () {
    var view = M1CRM.util.crmViews.getAllFollowups();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'followupdetail';
    M1CRM.util.crmViews.slideLeft(view);
  },
  addnewfollowupTap: function (button) {
    var view = M1CRM.util.crmViews.getFollowupAddedit();
    view.setRecord(null);
    view.config._parentformid = this.getForm().id;
    Ext.getCmp('followupaddedit').config._mode = 'addnew';
    this.slideLeft(view);
  },

  myCallsTap: function () {
    var view = M1CRM.util.crmViews.getMyOpenCalls();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addeditcall';
    this.slideLeft(view);
  },
  addnewcallTap: function () {
    var view = M1CRM.util.crmViews.getAddEditCall();
    view.setRecord(null);
    view.config._parentformid = this.getForm().id;
    Ext.getCmp(view.id).config._mode = 'addnew';
    this.slideLeft(view);
  },

  allopencallsTap: function () {
    var view = M1CRM.util.crmViews.getAllOpenCalls();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addeditcall';
    this.slideLeft(view);
  },

  contactsTap: function () {
    var view = M1CRM.util.crmViews.getContacts();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'custcontactaddedit';
    this.slideLeft(view);
  },

  followupsTap: function () {
    var view = M1CRM.util.crmViews.getMyFollowups();
    this.slideLeft(view);
  },

  organisationsTap: function () {
    var view = M1CRM.util.crmViews.getCustomers();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'customeraddedit';
    this.slideLeft(view);
  },
  addneworganisationsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerAddEdit();
    view.config._parentformid = this.getForm().id;
    if (Ext.getCmp('customeraddedit').getRecord() !== null)
      Ext.getCmp('customeraddedit').setRecord(null);
    this.slideLeft(view);
  },
  addnewcontactTap: function () {
    var view = M1CRM.util.crmViews.getCustomerContactAddEdit();
    view.config._parentformid = this.getForm().id;
    Ext.getCmp(view.id).config._mode = 'addnew';
    this.slideLeft(view);
  },
  myopenquotesTap: function () {
    var view = M1CRM.util.crmViews.getMyOpenQuotes();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addnewquote';
    this.slideLeft(view);
  },
  addnewquoteTap: function () {
    var view = M1CRM.util.crmViews.getAddNewQuote();
    view.config._parentformid = this.getForm().id;
    Ext.getCmp('addnewquote').setRecord(null);
    Ext.getCmp('addnewquote').config._mode = 'addnew';
    this.slideLeft(view);
  },
  allopenquotesTap: function () {
    var view = M1CRM.util.crmViews.getAllOpenQuotes();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addnewquote';
    this.slideLeft(view);
  },
  searchpartsTap: function () {
    var view = crmViews.self.getParts();
    var setRecord = function () {};
    var crmcustomerdateil = Ext.getCmp('crmcustomerdateil') || {setRecord};
    
    crmcustomerdateil.setRecord(null);
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'crmpartdateil';
    this.slideLeft(view);
  },
  myopenleadsTap: function () {
    var view = M1CRM.util.crmViews.getMyOpenLeads();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addeditlead';
    this.slideLeft(view);
  },
  addnewleadTap: function () {
    var view = M1CRM.util.crmViews.getAddEditLead();
    view.config._parentformid = this.getForm().id;
    Ext.getCmp('addeditlead').setRecord(null);
    Ext.getCmp('addeditlead').config._mode = 'addnew';
    this.slideLeft(view);
  },
  searchleadsTap: function () {
    var view = M1CRM.util.crmViews.getOpenLeads();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addeditlead';
    this.slideLeft(view);
  },
  logoutTap: function () {
    var user = Ext.create('M1CRM.model.crmLogin', {});
    user.logOut({
      scope: this,
      onReturn: function (result) {
        if (result) {
          var view = crmViews.self.getCRMLogin();
          this.slideRight(view);
        }
      }
    });
  }
});
