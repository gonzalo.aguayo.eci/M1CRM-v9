Ext.define('M1CRM.controller.crmCustomerLocationAddEdit', {
  extend: 'M1CRM.util.crmFormControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmListItem',
    'M1CRM.store.crmListItem',
    'M1CRM.model.crmTaxCodes',
    'M1CRM.store.crmTaxCodes',
    'M1CRM.model.crmCustomerLocationAddEdit',
    'M1CRM.model.crmCustomerAddEdit',
    'M1CRM.model.crmCustomerContacts',
    'M1CRM.store.crmCustomerContacts',
    'M1CRM.model.crmAddress',
    'M1CRM.model.crmContactDetail'
  ],
  config: {
    _totalcontacts: null,
    _totalcalls: null,
    _totalfollowups: null,
    _totalquotes: null,
    _custStatus: 0,
    _viewmodeTitle: '',
    _addmodeTitle: '',
    _editmodeTitle: '',

    refs: {
      'form': '#customerlocationaddedit',
      'toolbarid': '#customerlocationaddedit_toolbarid',
      'subtitle': '#addeditcustlocat_locationidtag',
      'addnewbtn': '#customerlocationaddedit_addnew',
      'cancelbtn': '#customerlocationaddedit_cancelbtn',
      'savebtn': '#customerlocationaddedit_savebtn',
      'editbtn': '#customerlocationaddedit_editbtn',

      'customerid': '#customerlocationaddedit_custid',
      'customer': '#customerlocationaddedit_customer',
      'locationid': '#customerlocationaddedit_locationid',
      'locationname': '#customerlocationaddedit_location',

      'editLocation': '#customerlocationaddeditop_editbtn',
      'addNewLocation': '#customerlocationaddeditop_addbtn',

      'custinfoset': '#addeditcustloc_custinfoset',
      'country': '#addeditloc_country',
      'quoteloc': '#addeditloc_quoteloc',
      'shiploc': '#addeditloc_shiploc',
      'ARInvoiceLoc': '#addeditloc_ARInvoiceloc',
      'shipmethod': '#addeditloc_shipmethod',
      'shipmethodsel': '#addeditloc_shipmethodsel',
      'taxable': '#addeditloc_taxable',
      'taxid': '#addeditloc_taxid',
      'taxidsel': '#addeditloc_taxidsel',
      'secondtaxidsel': '#addeditloc_secondtaxidsel',
      'secondtaxid': '#addeditloc_secondtaxid',
      'contacts': '#addeditloc_contacts',
      'calls': '#addeditloc_calls',
      'followups': '#addeditloc_followups',
      'quotes': '#addeditloc_quotes'
    },
    control: {
      form: {
        initialize: 'initializeForm',
        show: 'showForm',
        hide: function () {
          this.scrollFormToTop();
        }
      },
      locationid: {
        change: 'verifyLocationID'
      },
      cancelbtn: {
        tap: 'btnCancelLocationTap'
      },
      addnewbtn: {
        tap: 'addNewLocationTap'
      },
      countrysel: {
        change: 'countryselChange'
      },
      shipmethodsel: {
        change: 'shipmethodselChange'
      },
      taxidsel: {
        change: 'taxidselChange'
      },
      secondtaxidsel: {
        change: 'secondtaxidselChange'
      },
      contacts: {
        tap: 'contactsTap'
      },
      calls: {
        tap: 'callsTap'
      },
      followups: {
        tap: 'followupsTap'
      },
      quotes: {
        tap: 'quotesTap'
      }

    }
  },

  initializeForm: function () {
    this.formInitilize();
    this.config._viewmodeTitle = SessionObj.statics.OrgLabel + ' Location';
    this.config._addmodeTitle = ' Add  Location';
    this.config._editmodeTitle = 'Edit Location';
    this.initializeFields();
    this.loadFormProperties();
  },


  loadFormProperties: function () {
    Ext.create('M1CRM.model.crmCustomerLocationAddEdit', {}).getData({
      RequestID: 'customerlocaddeditprop',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {

          this.getTaxidsel().suspendEvents();
          this.getTaxidsel().setStore(this.getStoreFromListForDropDown(result.ResultObject.TaxCodeList, 'crmTaxCodes', true));
          this.getTaxidsel().isDirty = false;
          this.getTaxidsel().resumeEvents(true);

          this.getSecondtaxidsel().suspendEvents();
          this.getSecondtaxidsel().setStore(this.getStoreFromListForDropDown(result.ResultObject.SecondTaxCodeList, 'crmTaxCodes', true));
          this.getSecondtaxidsel().isDirty = false;
          this.getSecondtaxidsel().resumeEvents(true);

          this.getShipmethodsel().setStore(this.getStoreFromListForDropDown(result.ResultObject.ShippingMethodList, 'crmListItem', true));
          this.getShipmethodsel().setValue('');
          this.getShipmethodsel().isDirty = false;
        }
      }
    });
  },


  initializeFields: function () {
    this.getCustomerid().hide();
    this.getLocationid().setMaxLength(5);
    Ext.getCmp('addeditloc_state').setMaxLength(3);
    Ext.getCmp('addeditloc_city').setMaxLength(30);
    Ext.getCmp('addeditloc_postcode').setMaxLength(10);
    Ext.getCmp('addeditloc_line1').setMaxLength(50);
    Ext.getCmp('addeditloc_line2').setMaxLength(50);
    Ext.getCmp('addeditloc_line3').setMaxLength(50);
    Ext.getCmp('addeditloc_phone1').setMaxLength(20);
    Ext.getCmp('addeditloc_phone2').setMaxLength(20);
    Ext.getCmp('addeditloc_fax').setMaxLength(20);
    Ext.getCmp('addeditloc_postcode').setLabel(SessionObj.statics.PostCodeLabel);
    if (SessionObj.getShowSecondTax() == true) {
      this.getSecondtaxid().getParent().show();
    } else {
      this.getSecondtaxid().getParent().hide();
    }

  },
  getFormButtons: function () {
    return [this.getCancelbtn().id, this.getSavebtn().id];
  },
  getEditableFormFields: function () {
    return [
      this.getLocationid().id,
      this.getLocationname().id,
      Ext.getCmp('addeditloc_state').id,
      Ext.getCmp('addeditloc_city').id,
      Ext.getCmp('addeditloc_postcode').id,
      Ext.getCmp('addeditloc_line1').id,
      Ext.getCmp('addeditloc_line2').id,
      Ext.getCmp('addeditloc_line3').id,
      Ext.getCmp('addeditloc_phone1').id,
      Ext.getCmp('addeditloc_phone2').id,
      Ext.getCmp('addeditloc_fax').id,
      Ext.getCmp('addeditloc_email').id,
      this.getQuoteloc().id,
      this.getShiploc().id,
      this.getARInvoiceLoc().id,
      this.getShipmethodsel().id,
      this.getTaxable().id,
      this.getTaxidsel().id,
      this.getSecondtaxid().id,
      this.getSecondtaxidsel().id
    ]
  },
  showForm: function () {
    Ext.create('M1CRM.model.crmUserSecurity', {}).getUserSecurityLevel({
      ID: 'CRMCUSTOMERLOCADDEDIT',
      scope: this,
      onReturn: function (rec) {
        if (rec != null) {
          if (this.applyUserSecuritySettings(this.getForm().id, parseInt(rec.PAccessLevel), parseInt(rec.SAccessLevel),
              this.getFormButtons())) {
            this.loadData();
          }
        } else {
          this.toBackTap(this.getForm().id);
        }
      }
    });
  },
  loadCustomerTaxInfo: function () {
    Ext.create('M1CRM.model.crmCustomerLocationAddEdit', {
      LocationID: this.getLocationid().getValue(),
      OrganizationID: this.getCustomerid().getValue()
    }).getData({
      RequestID: 'customerlocdata',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          //  this.populateData(result.ResultObject);
          if (result.ResultObject.CustomerTaxable == true) {
            this.getTaxable().check();
          } else {
            this.getTaxable().uncheck();
          }
          this.getTaxable().isDirty = false;
          this.getTaxidsel().setValue(result.ResultObject.CustomerTaxCodeID);
          this.getTaxidsel().isDirty = false;
          this.getTaxid().setValue(this.getSelectFieldDisplayValue(this.getTaxidsel()));

          this.getSecondtaxidsel().setValue(result.ResultObject.CustomerSecondTaxCodeID);
          this.getSecondtaxidsel().isDirty = false;
          this.getSecondtaxid().setValue(this.getSelectFieldDisplayValue(this.getSecondtaxidsel()));
        } else {
          this.getTaxable().uncheck();
          this.getTaxable().isDirty = false;

          this.getTaxidsel().setValue('');
          this.getTaxidsel().isDirty = false;
          this.getTaxid().setValue(this.getSelectFieldDisplayValue(this.getTaxidsel()));

          this.getSecondtaxidsel().setValue('');
          this.getSecondtaxidsel().isDirty = false;
          this.getSecondtaxid().setValue(this.getSelectFieldDisplayValue(this.getSecondtaxidsel()));
        }
      }
    });


  },

  loadData: function () {
    this.clearForm();
    this.loadCustomerName();
    if (Ext.getCmp('customerlocationaddedit').getRecord() != null) {
      var mode = this.getForm().config._mode != null ? this.getForm().config._mode : 'view';
      this.setEntryFormMode(mode);
      if (mode == 'addnew' || mode == 'edit') {
        this.makeFormReadOnly(false);
        this.loadCustomerTaxInfo();
      } else {
        this.makeFormReadOnly(true);
      }
      if (mode != 'addnew') {
        Ext.create('M1CRM.model.crmCustomerLocationAddEdit', {
          LocationID: this.getLocationid().getValue(),
          OrganizationID: this.getCustomerid().getValue()
        }).getData({
          RequestID: 'customerlocdata',
          scope: this,
          onReturn: function (result) {
            if (result != null && result.RecordFound == true) {
              this.populateData(result.ResultObject);
              this.setLocationSubTitle();
              this.resetEditableForms(this.getEditableFormFields());
              this.setEntryFormMode('view');
            } else {
              this.makeNewEntry();
            }
          }

        });
      } else {
        this.setLocationSubTitle(true);
        this.getLocationid().setValue('');
        this.resetEditableForms(this.getEditableFormFields());
        this.setEntryFormMode('addnew');
        Ext.getCmp('addeditloc_btnop1').hide();
        Ext.getCmp('addeditloc_btnop2').hide();
      }
    } else {
      this.makeNewEntry();
      Ext.getCmp('addeditloc_btnop1').hide();
      Ext.getCmp('addeditloc_btnop2').hide();
      this.setLocationSubTitle(true);
    }

  },
  makeNewEntry: function () {
    this.makeFormReadOnly(false);
    this.getLocationname().setValue('');
    this.getQuoteloc().check();
    this.setLocationSubTitle();
    this.resetEditableForms(this.getEditableFormFields());
    this.setEntryFormMode('addnew');
  },
  clearForm: function () {
    this.clearFormFields();
    this.getLocationid().setValue(null);
  },
  makeFormReadOnly: function (flag) {
    this.getLocationname().setReadOnly(flag);
    Ext.getCmp('addeditloc_state').setReadOnly(flag);
    Ext.getCmp('addeditloc_city').setReadOnly(flag);
    Ext.getCmp('addeditloc_postcode').setReadOnly(flag);
    Ext.getCmp('addeditloc_line1').setReadOnly(flag);
    Ext.getCmp('addeditloc_line2').setReadOnly(flag);
    Ext.getCmp('addeditloc_line3').setReadOnly(flag);
    Ext.getCmp('addeditloc_phone1').setReadOnly(flag);
    Ext.getCmp('addeditloc_phone2').setReadOnly(flag);
    Ext.getCmp('addeditloc_fax').setReadOnly(flag);
    Ext.getCmp('addeditloc_email').setReadOnly(flag);
    this.getCountry().setReadOnly(flag);
    //this.pairControlsShowAndHide(flag, this.getCountry().id );
    this.pairControlsShowAndHide(flag, this.getShipmethod().id);
    this.pairControlsShowAndHide(flag, this.getTaxid().id);
    this.pairControlsShowAndHide(flag, this.getSecondtaxid().id);
    if (flag)
      this.disableControls([this.getQuoteloc().id, this.getShiploc().id, this.getARInvoiceLoc().id, this.getTaxable().id]);
    else
      this.enableControls([this.getQuoteloc().id, this.getShiploc().id, this.getARInvoiceLoc().id, this.getTaxable().id]);
    this.getSavebtn().setHidden(flag);
    this.getCancelbtn().setHidden(flag);

  },
  loadCustomerName: function (args) {
    var rec = null;
    var custid = null;
    var custname = null;

    if (this.getForm().getRecord() != null) {
      rec = this.getForm().getRecord().getData();

      this.getCustomerid().setValue(rec.OrganizationID);
      this.getCustomerid().setReadOnly(true);

      this.getCustomer().setValue(rec.OrganizationName);
      this.getCustomer().setReadOnly(true);

      this.getLocationid().suspendEvents();
      this.getLocationid().setValue(rec.LocationID);
      this.getLocationname().setValue(rec.LocationName);
      this.getLocationid().resumeEvents(true);


      Ext.create('M1CRM.model.crmCustomerAddEdit', {
        OrganizationID: this.getCustomerid().getValue()
      }).getData({
        RequestID: 'customerdata',
        scope: this,
        onReturn: function (result) {
          if (result && result.RecordFound) {

            if (this.getCustomer().getValue() == null || this.getCustomer().getValue() == '') {
              this.getCustomer().setValue(result.ResultObject.OrganizationName);
            }

            this.config._custStatus = parseInt(result.ResultObject.CustomerStatus);
            if (+result.ResultObject.CustomerStatus == 1 || +result.ResultObject.CustomerStatus == 2) {
              this.getQuoteloc().check();
              this.getShiploc().check();
            } else {
              this.getQuoteloc().uncheck();
              this.getShiploc().uncheck();
            }
          } else {
            this.getQuoteloc().uncheck();
            this.getShiploc().uncheck();
          }

          if (this.getForm().config._loctype === 'ARINV') {
            this.getARInvoiceLoc().check();
          } else {
            this.getARInvoiceLoc().uncheck();
          }
        }
      });
    }
  },
  getSaveObject: function () {
    var addressobj = Ext.create('M1CRM.model.crmAddress', {
      AddressLine1: Ext.getCmp('addeditloc_line1').getValue(),
      AddressLine2: Ext.getCmp('addeditloc_line2').getValue(),
      AddressLine3: Ext.getCmp('addeditloc_line3').getValue(),
      City: Ext.getCmp('addeditloc_city').getValue(),
      State: Ext.getCmp('addeditloc_state').getValue(),
      PostCode: Ext.getCmp('addeditloc_postcode').getValue(),
      Country: this.getCountry().getValue()
    });

    var contactObj = Ext.create('M1CRM.model.crmContactDetail', {
      PhoneNumber: Ext.getCmp('addeditloc_phone1').getValue(),
      AlternatePhoneNumber: Ext.getCmp('addeditloc_phone2').getValue(),
      EMailAddress: Ext.getCmp('addeditloc_email').getValue(),
      Fax: Ext.getCmp('addeditloc_fax').getValue()
    });

    return Ext.create('M1CRM.model.crmCustomerLocationAddEdit', {
      LocationID: this.getLocationid().getValue(), //locationid,
      OrganizationID: this.getCustomerid().getValue(),
      LocationName: this.getLocationname().getValue(),
      Address: addressobj.getData(),
      ContactDetail: contactObj.getData(),
      QuoteLocation: this.getQuoteloc().isChecked(),
      ShipLocation: this.getShiploc().isChecked(),
      ARInvoiceLocation: this.getARInvoiceLoc().isChecked(),
      CustomerShippingMethodID: this.getShipmethodsel().getValue(),
      CustomerTaxable: this.getTaxable().isChecked(),
      CustomerTaxCodeID: this.getTaxidsel().getValue(),
      CustomerSecondTaxCodeID: this.getSecondtaxidsel().getValue()
    });
  },
  btnSaveTap: function () {
    this.saveData();
  },
  saveData: function () {
    var custLocation = this.getSaveObject();
    var error = custLocation.validate();
    if (custLocation.isValid()) {
      custLocation.saveData({
        scope: this,
        onReturn: function (result) {
          if (result.SaveStatus == true) {
            this.makeFormReadOnly(true);
            this.setLocationSubTitle();
            this.setEntryFormMode('view');
          }
          else {
            this.setEntryFormMode('addnew');
            Ext.Msg.alert('Error', 'Error saving customer location info');
          }
        }
      });
    }
    else {
      this.displayValidationMessage(error);
    }
  },
  processAutoSave: function (type) {
    if (this.isFormDirty(this.getEditableFormFields())) {
      var customerLoc = this.getSaveObject();
      customerLoc.validate();
      if (customerLoc.isValid()) {
        customerLoc.saveData({
          scope: this,
          onReturn: function (result) {
            if (result.SaveStatus == true) {
              this.makeFormReadOnly(true);
              this.setEntryFormMode('view');
              this.processAfterAutoSave(type, this.getForm().id);
            } else {
              this.setEntryFormMode('addnew');
              Ext.Msg.alert('Error', 'Error saving customer location info');
            }
          }
        });
      } else {
        Ext.Msg.confirm("", this.getRecordInCompleteMessage(), function (btn, type) {
          if (btn == 'yes') {
            this.makeFormReadOnly(true);
            this.processAfterAutoSave(type, this.getForm().id);
          }
        }, this);
      }
    } else {
      this.processAfterAutoSave(type, this.getForm().id);
    }
  },
  btnEditTap: function () {
    this.makeFormReadOnly(false);
    this.setEntryFormMode('edit');
    this.getCustomer().disable();
    Ext.getCmp('addeditloc_btnop1').hide();
    Ext.getCmp('addeditloc_btnop2').hide();

    this.scrollFormToTop()
  },
  btnCancelLocationTap: function () {
    if (Ext.getCmp('customerlocationaddedit').getRecord() != null && this.getLocationid().getValue() != '') {
      this.loadData();
      this.scrollFormToTop();
      this.getCustomer().enable();
      Ext.getCmp('addeditloc_btnop1').show();
      Ext.getCmp('addeditloc_btnop2').show();
    } else {
      this.toBackTap();
    }
  },
  addNewLocationTap: function () {
    this.makeFormReadOnly(false);
    this.getLocationid().show();
    this.clearForm();
    this.loadCustomerName();
    this.getLocationid().setValue('');
    this.setEntryFormMode('addnew');
    this.setLocationSubTitle();
    this.scrollFormToTop();
    //this.getOptions().hide();
    Ext.getCmp('addeditloc_btnop1').hide();
    Ext.getCmp('addeditloc_btnop2').hide();
  },
  shipmethodselChange: function () {
    this.getShipmethod().setValue(this.getSelectFieldDisplayValue(this.getShipmethodsel()));
  },
  taxidselChange: function () {
    this.getTaxid().setValue(this.getSelectFieldDisplayValue(this.getTaxidsel()));
  },
  secondtaxidselChange: function () {
    this.getSecondtaxid().setValue(this.getSelectFieldDisplayValue(this.getSecondtaxidsel()));
  },
  verifyLocationID: function () {
    if (this.getLocationid().getValue() != '') {
      this.getCustomerLocationData();
    }
  },
  clearForm: function () {
    Ext.getCmp('addeditloc_state').setValue('');
    Ext.getCmp('addeditloc_city').setValue('');
    Ext.getCmp('addeditloc_postcode').setValue('');
    Ext.getCmp('addeditloc_line1').setValue('');
    Ext.getCmp('addeditloc_line2').setValue('');
    Ext.getCmp('addeditloc_line3').setValue('');
    Ext.getCmp('addeditloc_phone1').setValue('');
    Ext.getCmp('addeditloc_phone2').setValue('');
    Ext.getCmp('addeditloc_fax').setValue('');
    Ext.getCmp('addeditloc_email').setValue('');
    this.getQuoteloc().uncheck();
    this.getShiploc().uncheck();
    this.getARInvoiceLoc().uncheck();
    this.getShipmethod().setValue('');
    this.getShipmethodsel().setValue('');
    this.getShipmethod().setValue('');
    this.getTaxable().uncheck();
    this.getTaxidsel().setValue('');
    this.getTaxid().setValue('');
    this.getSecondtaxidsel().setValue('');
    this.getSecondtaxid().setValue('');
    this.getCountry().setValue('');
    this.getShipmethodsel().isDirty = false;
    this.getTaxidsel().isDirty = false;
    this.getSecondtaxidsel().isDirty = false;
    this.resetEditableForms(this.getEditableFormFields());
  },
  getCustomerLocationData: function () {
    Ext.create('M1CRM.model.crmCustomerLocationAddEdit', {
      LocationID: this.getLocationid().getValue(),
      OrganizationID: this.getCustomerid().getValue()
    }).getData({
      RequestID: 'customerlocdata',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true && this.getLocationid().getValue() != '') {
          var msg = 'There is already a Location with this ID. Do you wish to view the record?';
          Ext.Msg.confirm("", msg, function (btn) {
            if (btn == 'yes') {
              this.populateData(result.ResultObject);
              this.makeFormReadOnly(true);
              this.setLocationSubTitle();
              this.resetEditableForms(this.getEditableFormFields());
              this.setEntryFormMode('view');
            } else {
              this.makeFormReadOnly(false);
              this.getQuoteloc().check();
              this.setLocationSubTitle(true);
              this.getLocationid().setValue('');
              this.resetEditableForms(this.getEditableFormFields());
              this.setEntryFormMode('addnew');
            }
          }, this);
        } else {
          this.makeNewEntry();
          this.setLocationSubTitle(true);
        }
      }
    });
  },
  resetButtonCounts: function () {
    this.getCalls().setBadgeText
    this.getContacts().setBadgeText(0);
    this.config._totalcalls = 0;
    this.getFollowups().setBadgeText(0);
    this.config._totalquotes = 0;
    this.getQuotes().setBadgeText;
  },
  populateData: function (result) {
    this.resetButtonCounts();
    Ext.getCmp('addeditloc_line1').setValue(result.Address.AddressLine1);
    Ext.getCmp('addeditloc_line2').setValue(result.Address.AddressLine2);
    Ext.getCmp('addeditloc_line3').setValue(result.Address.AddressLine3);
    Ext.getCmp('addeditloc_state').setValue(result.Address.State);
    Ext.getCmp('addeditloc_city').setValue(result.Address.City);
    Ext.getCmp('addeditloc_postcode').setValue(result.Address.PostCode);
    this.getCountry().setValue(result.Address.Country);

    Ext.getCmp('addeditloc_phone1').setValue(result.ContactDetail.PhoneNumber);
    Ext.getCmp('addeditloc_phone2').setValue(result.ContactDetail.AlternatePhoneNumber);
    Ext.getCmp('addeditloc_fax').setValue(result.ContactDetail.Fax);
    Ext.getCmp('addeditloc_email').setValue(result.ContactDetail.EMailAddress);
    if (result.QuoteLocation == true) {
      this.getQuoteloc().check();
    } else {
      this.getQuoteloc().uncheck();
    }
    if (result.ShipLocation == true) {
      this.getShiploc().check();
    } else {
      this.getShiploc().uncheck();
    }
    this.getARInvoiceLoc()[result.ARInvoiceLocation ? 'check' : 'uncheck']();
    this.getShipmethodsel().setValue(result.CustomerShippingMethodID);
    this.getShipmethodsel().isDirty = false;
    this.getShipmethod().setValue(this.getSelectFieldDisplayValue(this.getShipmethodsel()));

    if (result.CustomerTaxable == true) {
      this.getTaxable().check();
    } else {
      this.getTaxable().uncheck();
    }
    this.getTaxidsel().setValue(result.CustomerTaxCodeID);
    this.getTaxidsel().isDirty = false;
    this.getTaxid().setValue(this.getSelectFieldDisplayValue(this.getTaxidsel()));

    this.getSecondtaxidsel().setValue(result.CustomerSecondTaxCodeID);
    this.getSecondtaxidsel().isDirty = false;
    this.getSecondtaxid().setValue(this.getSelectFieldDisplayValue(this.getSecondtaxidsel()));

    Ext.getCmp('addeditloc_btnop1').show();
    Ext.getCmp('addeditloc_btnop2').show();

    if (result.Summary != null) {
      // this.getContacts().setBadgeText(result.Summary.OrgContact);
      this.config._totalcalls = parseInt(result.Summary.OrgCalls);
      this.getFollowups().setBadgeText(parseInt(result.Summary.OrgFollowups));
      this.config._totalquotes = parseInt(result.Summary.OrgQuotes);
    }
    if (SessionObj.getCallManagement() == true) {
      this.getCalls().enable();
      this.getCalls().setBadgeText(this.config._totalcalls);
    } else {
      this.getCalls().disable();
      this.getCalls().setBadgeText(0);
    }

    if (this.isCRMAdvanceAvailable() == true && (this.config._custStatus == 1 || this.config._custStatus == 2) && this.getQuoteloc().isChecked()) {
      this.getQuotes().enable(true);
      this.getQuotes().setBadgeText(this.config._totalquotes);
    } else {
      this.getQuotes().disable();
      this.getQuotes().setBadgeText(0);
    }
  },
  setLocationSubTitle: function (flag) {
    if (flag == null || flag == false) {
      if (this.getLocationid().getValue() != null && this.getLocationid().getValue() != '') {
        this.getSubtitle().setHtml(' Loc ID: ' + this.getLocationid().getValue());
        this.getSubtitle().show();
        this.getLocationid().hide();
      } else {
        this.getSubtitle().setHtml('');
        this.getSubtitle().hide();
        this.getLocationid().show();
      }
    } else {
      this.getSubtitle().setHtml('');
      this.getSubtitle().hide();
      this.getLocationid().show();
    }
  },
  contactsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerContacts(); // getLinkedContacts();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'custcontactaddedit';
    Ext.getCmp(view.id).config._paramObj = Ext.create('M1CRM.model.crmCustomerContacts', {
      OrganizationID: this.getCustomerid().getValue(),
      Organization: this.getCustomer().getValue(),
      LocationID: this.getLocationid().getValue(),
      LocationName: this.getLocationname().getValue()
    });
    this.slideLeft(view);
  },
  callsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerOpenCalls();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addeditcall';
    this.slideLeft(view);
  },

  followupsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerFollowups();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'followupaddedit';
    this.slideLeft(view);
  },

  quotesTap: function () {
    var view = M1CRM.util.crmViews.getAllQuotesOnContact();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addnewquote';
    Ext.getCmp(view.id).config._paramObj = Ext.create('M1CRM.model.crmCustomerContacts', {
      OrganizationID: this.getCustomerid().getValue(),
      Organization: this.getCustomer().getValue(),
      LocationID: this.getLocationid().getValue(),
      LocationName: this.getLocationname().getValue()
    });
    this.slideLeft(view);
  }

});
  
