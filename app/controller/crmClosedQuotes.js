Ext.define('M1CRM.controller.crmClosedQuotes', {
  extend: 'M1CRM.util.crmQuotesListControllerBase',
  config: {
    _listid: 'closedquoteslist',
    _seardfieldid: 'closedquotes_search',
    control: {
      closedquotes: {
        initialize: 'initializeList',
        show: 'onShow',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.viewDetail('closedquoteslist', item);
        },

        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },

  init: function () {
    this.control({
      '#closedquoteslist_addnew': {
        scope: this,
        tap: this.addNewQuoteTap
      }
    });
  },

  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labClosedQuotesSummary';
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmQuotations', {}).getAllOpenQuoteLayout());
  },
  onShow: function (obj, e) {
    this.initilize();
    this.createOptionMenu();
    this.loadData(obj);
  },
  createOptionMenu: function () {
    Ext.Viewport.setMenu(this.createMenu(this.getOptionMenuItems(this)), {
      side: 'right',
      reveal: true
    });
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  getDataObject: function () {
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {});
    return dataObj;
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmQuotations', this.getDataObject());
  },
  addNewQuoteTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getAddNewQuote();
    view.config._parentformid = this.config._listid;
    Ext.getCmp('addnewquote').setRecord(null);
    M1CRM.util.crmViews.slideLeft(view);
  }


});
