Ext.define('M1CRM.controller.crmLogin', {
  extend: 'M1CRM.util.crmFormControllerBase',
  requires: [
    'M1CRM.model.crmHome',
    'M1CRM.store.crmDataSet',
    'M1CRM.model.crmEmployee',
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmUtility',
    'M1CRM.view.crmHome',
    'M1CRM.util.crmCustomerControls'
  ],
  config: {
    refs: {
      'form': '#loginform',
      'dataset': '#dataset',
      'loginid': '#loginid',
      'loginbtn': '#btnlogin'
    },
    control: {
      form: {
        initialize: 'initializeForm',
        show: 'showForm'
      },
      loginbtn: {
        tap: 'loginTap'
      }
    }
  },
  initializeForm: function () {
  },
  showForm: function () {
    var dataSetLocalStore = Ext.create('M1CRM.store.crmDataSet').load();
    if (dataSetLocalStore.getCount() > 0) {
      this.getDataset().setValue(dataSetLocalStore.last().get('dataset'));
      this.getLoginid().setValue(dataSetLocalStore.last().get('loginid'));
      Ext.getCmp('password').setValue('');
    }
  },
  loginTap: function () {
    var user = Ext.create('M1CRM.model.crmLogin', {
      Database: this.getDataset().getValue(),
      UserName: this.getLoginid().getValue(),
      Module: 'CRM',
      Password: Ext.getCmp('password').getValue()
    });
    var error = user.validate();
    if (user.isValid()) {
      user.userLogin({
        scope: this,
        onReturn: function (result) {
          if (!result) {
            Ext.Msg.alert('Error', 'Login Error');
            return;
          }

          result = +result || 0;
          if (result === 200) {
            var view = Ext.getCmp('crmhome') || Ext.create('M1CRM.view.crmHome');
            Ext.Viewport.animateActiveItem(view, {type: 'slide', direction: 'left'});
          } else if (result === 0) {
            Ext.create('M1CRM.model.crmLogin', {}).logOut({
              scope: this,
              onReturn: function (result) {
                if (!result) {
                  Ext.Msg.alert('Warning', 'Connection Failure', Ext.emptyFn, this);
                }
              }
            });
          }
        }
      });
    } else {
      this.displayValidationMessage(error);
    }
  }
});
