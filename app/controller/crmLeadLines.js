Ext.define('M1CRM.controller.crmLeadLines', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'M1CRM.model.crmLeadLines',
    'M1CRM.store.crmLeadLines'
  ],
  config: {
    _listid: 'leadlineslist',
    _seardfieldid: 'leadlines_search',
    control: {
      leadlines: {
        initialize: 'initializeList',
        show: 'onShow',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.viewDetail('leadlineslist', item);
        },
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },
  init: function () {
    this.control({
      '#leadlineslist_addnew': {
        scope: this,
        tap: function () {
          Ext.Viewport.getActiveItem().setRecord(null);
          this.addNewTap();
        }
      }
    });
  },
  initializeList: function () {
    this.initilize();
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labLeadLinesSummary';
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmLeadLines', {}).getLeadLinesLayout());
  },


  onShow: function (obj, e) {
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  loadDataAfterSecurityCheck: function (obj, pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  getDataObject: function () {
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
      LeadID: Ext.getCmp('addeditlead_leadid').getValue(),
      AssignedToEmployeeID: SessionObj.getEmpId()
    });

    return dataObj;
  },
  loadFromDB: function () {
    this.setSummary('Lead:' + Ext.getCmp('addeditlead_leadid').getValue());
    this.loadDatafromDB('crmLeadLines', this.getDataObject());
  },
  addNewTap: function () {
    var view = M1CRM.util.crmViews.getLeadLineAddEdit();
    Ext.getCmp('leadlineaddedit').setRecord(null);
    view.config._parentformid = this.config._listid;
    Ext.getCmp(view.id).config._mode = 'addnew';
    this.slideLeft(view);
  }


});
