Ext.define('M1CRM.controller.crmQuoteParts', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'M1CRM.model.crmQuoteParts',
    'M1CRM.store.crmQuoteParts',
    'M1CRM.model.crmUserSecurity'
  ],
  config: {
    _listid: 'quotepartslist',
    _seardfieldid: 'quoteparts_search',
    control: {
      quoteparts: {
        initialize: 'initializeList',
        show: 'onShow',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          if (AddEditQuote != null) {
          }
          this.viewDetail('quotepartslist', item);
        },
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },
  init: function () {
    this.control({
      '#quotepartslist_addnew': {
        scope: this,
        tap: function () {
          Ext.Viewport.getActiveItem().setRecord(null);
          this.addNewPartTap();
        }
      }
    });
  },
  initializeList: function () {
    this.initilize();
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labQuoteLinesSummary';
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmQuoteParts', {}).getQuotePartsLayout());
  },
  onShow: function (obj, e) {
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  loadDataAfterSecurityCheck: function (obj, pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
      QuoteID: Ext.getCmp('addnewquote_quoteid').getValue() != '' ? Ext.getCmp('addnewquote_quoteid').getValue() : AddEditQuote.getData().QuoteID,
      AssignedToEmployeeID: SessionObj.getEmpId()
    });

    this.setSummary('Quote:' + dataObj.data.QuoteID);
    this.loadDatafromDB('crmQuoteParts', dataObj);
    if (Ext.getCmp('addnewquote_closed').getValue() == true) {
      Ext.getCmp('quotepartslist_addnew').hide();
    } else {
      Ext.getCmp('quotepartslist_addnew').show();
    }

  },
  addNewPartTap: function () {
    var view = M1CRM.util.crmViews.getQuotePartAddEdit();
    Ext.getCmp('addeditquotepart').setRecord(null);
    view.config._parentformid = this.config._listid;
    Ext.getCmp(view.id).config._mode = 'addnew';
    this.slideLeft(view);
  }

});
