Ext.define('Ext.overrides.field.Select', {
  override: 'Ext.field.Select',

  updateStore: function (newStore) {
    if (newStore) {
      this.onStoreDataChanged(newStore);
    }

    if (this.getUsePicker() && this.picker) {
      this.picker.down('pickerslot').setStore(newStore);
    } else if (this.listPanel) {
      this.listPanel.down('dataview').setStore(newStore);
    }
  }
});


Ext.define('M1CRM.controller.crmCustomerAddEdit', {
  extend: 'M1CRM.util.crmFormControllerBase',
  id: 'customeraddedit',
  requires: [
    'M1CRM.model.crmCountry',
    'M1CRM.store.crmCountry',
    'M1CRM.model.crmCustomerDetail',
    'M1CRM.model.crmTaxCodes',
    'M1CRM.store.crmTaxCodes',
    'M1CRM.model.crmProductionProperties',
    'M1CRM.model.crmCustomerAddEdit',
    'M1CRM.model.crmExceptionHandler',
    'M1CRM.store.crmExceptionHandler'
  ],

  config: {
    _totalLocations: 0,
    _totalContacts: 0,
    _totalOpenCalls: 0,
    _totalOpenFollowups: 0,
    _totalLinkedQuotes: 0,
    _viewmodeTitle: '',
    _addmodeTitle: '',
    _editmodeTitle: '',
    _propCustomerTaxable: 0,
    refs: {
      'form': '#customeraddedit',
      'toolbarid': '#customeraddedit_toolbarid',
      'subtitle': '#customeraddeditsubtitle',

      'addnewbtn': '#customeraddedit_addnew',
      'cancelbtn': '#customeraddedit_cancelbtn',
      'savebtn': '#customeraddedit_savebtn',
      'editbtn': '#customeraddedit_editbtn',
      'toback': '#customeraddedit_back',
      'tohome': '#customeraddedit_home',

      'customerid': '#customeraddedit_custid',
      'customerName': '#customeraddedit_customer',
      'custAddLine1': '#customeraddedit_addline1',
      'custAddLine2': '#customeraddedit_addline2',
      'custAddLine3': '#customeraddedit_addline3',
      'custCity': '#customeraddedit_city',
      'custState': '#customeraddedit_state',
      'custPostCode': '#customeraddedit_postcode',
      'custCountry': '#customeraddedit_country',
      'custPhone1': '#customeraddedit_phone1',
      'custPhone2': '#customeraddedit_phone2',
      'custEmail': '#customeraddedit_email',
      'custWeb': '#customeraddedit_web',
      'custStatus': '#customeraddedit_custstatus',
      'custSupStatus': '#customeraddedit_supplierstatus',
      'custNotes': '#customeraddedit_notes',
      'taxable': '#customeraddedit_taxable',
      'taxid': '#customeraddedit_taxid',
      'taxidsel': '#customeraddedit_taxidsel',
      'secondtaxidsel': '#customeraddedit_secondtaxidsel',
      'secondtaxid': '#customeraddedit_secondtaxid',
      'locations': '#customeraddedit_locations',
      'contacts': '#customeraddedit_contacts',
      'calls': '#customeraddedit_calls',
      'followups': '#customeraddedit_followups',
      'quotes': '#customeraddedit_quotes'
    },
    control: {
      form: {
        initialize: 'initializeForm',
        show: 'showForm',
        hide: function () {
          this.scrollFormToTop();
        }
      },
      cancelbtn: {
        tap: 'cancelbtnTap'
      },
      addnewbtn: {
        tap: 'addnewbtnTap'
      },
      customerid: {
        blur: 'verifyCustomerID',
        clearicontap: 'clearForm'
      },
      locations: {
        tap: 'locationsTap'
      },
      contacts: {
        tap: 'contactsTap'
      },
      calls: {
        tap: 'callsTap'
      },
      followups: {
        tap: 'followupsTap'
      },
      quotes: {
        tap: 'quotesTap'
      },
      taxidsel: {
        change: 'taxidselChange'
      },
      secondtaxidsel: {
        change: 'secondtaxidselChange'
      },
      uncheck: 'taxableUncheck',
      check: 'taxableCheck'

    }
  },

  initializeForm: function () {
    this.formInitilize();
    this.config._viewmodeTitle = SessionObj.getOrgLabel();  //+ ' Details';
    this.config._addmodeTitle = 'Add ' + SessionObj.getOrgLabel();
    this.config._editmodeTitle = 'Edit ' + SessionObj.getOrgLabel();
    this.initializeFields();
    this.loadFormProperties();
    Ext.getCmp(this.getForm().id).getScrollable().getScroller().on({
      scope: this,
      scrollend: function (scroller, x, y, eOpts) {
        this.setFocusOnField();
      }
    });

  },

  setFocusOnField: function () {
    if (this.getCustomerid().getValue() == '') {
      this.getCustomerid().focus();
    } else if (this.getCustomerName().getValue() == '') {
      this.getCustomerName().focus();
    }
  },

  initializeFields: function () {
    this.getCustomerid().setMaxLength(10);
    this.getCustomerName().setMaxLength(50);
    this.getCustState().setMaxLength(3);
    this.getCustCity().setMaxLength(30);
    this.getCustPostCode().setMaxLength(10);
    this.getCustPostCode().setLabel(SessionObj.getPostCodeLabel());
    this.getCustAddLine1().setMaxLength(50);
    this.getCustAddLine2().setMaxLength(50);
    this.getCustAddLine3().setMaxLength(50);
    this.getCustPhone1().setMaxLength(20);
    this.getCustPhone2().setMaxLength(20);
    this.getCustWeb().setMaxLength(50);
    if (SessionObj.getShowSecondTax() == true) {
      this.getSecondtaxid().getParent().show();
    } else {
      this.getSecondtaxid().getParent().hide();
    }
  },

  getFormButtons: function () {
    return [this.getSavebtn().id, this.getCancelbtn().id];
  },

  getEditableFormFields: function () {
    return [this.getCustomerid().id, this.getCustomerName().id,
      this.getCustAddLine1().id, this.getCustAddLine2().id, this.getCustAddLine3().id,
      this.getCustCity().id, this.getCustState().id, this.getCustPostCode().id, this.getCustCountry().id,
      this.getCustPhone1().id, this.getCustPhone2().id, this.getCustEmail().id, this.getCustWeb().id,
      this.getTaxable().id, this.getTaxidsel().id, this.getSecondtaxid().id, this.getSecondtaxidsel().id
    ];
  },

  loadFormProperties: function () {
    Ext.create('M1CRM.model.crmCustomerAddEdit', {}).getData({
      RequestID: 'customeraddeditprop',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          this.config._propCustomerTaxable = result.ResultObject.CMCustomerTaxable;
          this.config._propCustomerTaxable != 0 ? this.getTaxable().check() : this.getTaxable().uncheck();

          this.getTaxidsel().suspendEvents();
          this.getTaxidsel().setStore(this.getStoreFromListForDropDown(result.ResultObject.TaxCodeList, 'crmTaxCodes', true));
          this.getTaxidsel().isDirty = false;
          this.getTaxidsel().resumeEvents(true);

          this.getSecondtaxidsel().suspendEvents();
          this.getSecondtaxidsel().setStore(this.getStoreFromListForDropDown(result.ResultObject.SecondTaxCodeList, 'crmTaxCodes', true));
          this.getSecondtaxidsel().isDirty = false;
          this.getSecondtaxidsel().resumeEvents(true);
        }
      }
    });
  },

  showForm: function () {
    if (SessionObj.getCustomerAddEditTSecurity() == null || SessionObj.getCustomerAddEditTSecurity() == '') {
      Ext.create('M1CRM.model.crmUserSecurity', {}).getUserSecurityLevel({
        ID: 'CRMCUSTOMERADDEDIT',
        scope: this,
        onReturn: function (rec) {
          if (rec != null) {
            SessionObj.setCustomerAddEditTSecurity(rec.PAccessLevel + ',' + rec.SAccessLevel);
            if (this.applyUserSecuritySettings(this.getForm().id, parseInt(rec.PAccessLevel), parseInt(rec.SAccessLevel),
                this.getFormButtons())) {
              this.processShowForm();
            }
          } else {
            this.toBackTap(this.getForm().id);
          }
        }
      });
    } else {
      if (this.applyUserSecuritySettings(this.getForm().id,
          parseInt(this.getPKeyFromSessionObj(SessionObj.getCustomerAddEditTSecurity())), parseInt(this.getSKeyFromSessionObj(SessionObj.getCustomerAddEditTSecurity())), this.getFormButtons())) {
        this.processShowForm();
      } else {
        this.toBackTap(this.getForm().id);
      }
    }
  },
  processShowForm: function () {
    this.loadData();
  },
  clearForm: function () {
    this.getTaxidsel().suspendEvents();
    this.getSecondtaxidsel().suspendEvents();

    this.clearFormFields();
    this.config._propCustomerTaxable != 0 ? this.getTaxable().check() : this.getTaxable().uncheck();
    this.getTaxidsel().setValue('');
    this.getTaxid().setValue('');
    this.getSecondtaxidsel().setValue('');
    this.getSecondtaxid().setValue('');
    this.getTaxable().isDirty = false;
    this.getTaxidsel().isDirty = false;
    this.getSecondtaxidsel().isDirty = false;

    this.getTaxidsel().resumeEvents(true);
    this.getSecondtaxidsel().resumeEvents(true);

  },

  loadData: function () {
    var mode = Ext.getCmp(this.getForm().id).config._mode;
    if (this.getForm().getRecord() != null) {
      if (this.getForm().getRecord().OrganizationID != null && this.getForm().getRecord().OrganizationID != '') {
        if (Ext.getCmp(this.getForm().id).config._mode == 'view') {
          this.makeFormReadOnly(true);
          this.setEntryFormMode('view');
        } else {
          this.makeFormReadOnly(false);
          this.setEntryFormMode('addnew');
        }
      } else {
        Ext.create('M1CRM.model.crmCustomerAddEdit', {
          OrganizationID: this.getForm().getRecord().getData().OrganizationID
        }).getData({
          RequestID: 'customerdata',
          scope: this,
          onReturn: function (result) {
            if (result != null && result.RecordFound == true) {
              this.getForm().setRecord(result.ResultObject);
              this.getCustomerid().suspendEvents();
              this.populateData();
              this.getCustomerid().resumeEvents(true);
              this.makeFormReadOnly(true);
              this.setEntryFormMode('view');
              Ext.getCmp(this.getForm().id).config._mode = 'view';
            } else {
              this.makeFormToCreateNewEntry(true);
              this.setEntryFormMode('view');
              Ext.getCmp(this.getForm().id).config._mode = 'view';
            }
            Ext.getCmp('customeraddedit_btnop1').show();
            Ext.getCmp('customeraddedit_btnop2').show();
          }
        });

      }
    } else {
      this.clearForm();
      this.makeFormReadOnly(false);
      this.getSubtitle().setHtml('');
      Ext.getCmp('customeraddedit_btnop1').hide();
      Ext.getCmp('customeraddedit_btnop2').hide();
      this.setEntryFormMode('addnew');
      Ext.getCmp(this.getForm().id).config._mode = 'addnew';
      this.getCustStatus().setValue('<None>');
      this.getCustSupStatus().setValue('<None>');
    }
    this.resetEditableForms(this.getEditableFormFields());
  },

  populateData: function () {
    rec = this.getForm().getRecord();
    if (rec != null) {
      this.getCustomerid().setValue(rec.OrganizationID);
      this.getCustomerName().setValue(rec.OrganizationName);
      this.getCustCountry().setValue(rec.Address.Country);
      this.getCustState().setValue(rec.Address.State);
      this.getCustCity().setValue(rec.Address.City);
      this.getCustPostCode().setValue(rec.Address.PostCode);
      this.getCustAddLine1().setValue(rec.Address.AddressLine1);
      this.hideOrShowControlerParent(this.getCustAddLine2(), rec.Address.AddressLine2);
      this.hideOrShowControlerParent(this.getCustAddLine3(), rec.Address.AddressLine3);
      this.hideOrShowControlerParent(this.getCustPhone1(), rec.ContactDetail.PhoneNumber);
      this.hideOrShowControlerParent(this.getCustPhone2(), rec.ContactDetail.AlternatePhoneNumber);
      this.hideOrShowControlerParent(this.getCustEmail(), rec.ContactDetail.EMailAddress);
      this.hideOrShowControlerParent(this.getCustWeb(), rec.ContactDetail.WebAddress);
      if (rec.CustomerTaxable == true) {
        this.getTaxable().check();
      } else {
        this.getTaxable().uncheck();
      }

      this.getTaxidsel().setValue(rec.CustomerTaxCodeID);
      this.getTaxidsel().isDirty = false;
      this.getTaxid().setValue(this.getSelectFieldDisplayValue(this.getTaxidsel()));

      this.getSecondtaxidsel().setValue(rec.CustomerSecondTaxCodeID);
      this.getSecondtaxidsel().isDirty = false;
      this.getSecondtaxid().setValue(this.getSelectFieldDisplayValue(this.getSecondtaxidsel()));

      var custDetail = Ext.create('M1CRM.model.crmCustomerAddEdit', {});
      this.getCustStatus().setValue(custDetail.getStatusCodes(rec.CustomerStatus));
      this.getCustSupStatus().setValue(custDetail.getStatusCodes(rec.SupplierStatus));
      this.getCustNotes().setValue(rec.LongDescriptionText);

      if (SessionObj.getCallManagement()) {
        this.getCalls().setBadgeText(parseInt(rec.Summary.OrgCalls));
        // this.getCalls().setBadgeText(0);

      } else {
        this.getCalls().setBadgeText(0);
        this.getCalls().disable();
      }


      this.getFollowups().setBadgeText(parseInt(rec.Summary.OrgFollowups));
      var custStatusCodeID = custDetail.getStatusCodeID(this.getCustStatus().getValue());
      if (this.isCRMAdvanceAvailable() == true && SessionObj.getQuotationManagement() == true && (custStatusCodeID == 1 || custStatusCodeID == 2)) {
        this.getQuotes().enable();
        this.getQuotes().setBadgeText(parseInt(rec.Summary.OrgQuotes));
        //  this.getQuotes().setBadgeText(0);

      } else {
        this.getQuotes().setBadgeText(0);
        this.getQuotes().disable();
      }

    } else {
      if (!SessionObj.getCallManagement()) {
        this.getCalls().disable();
      }
      this.getCalls().setBadgeText(0);
      this.getFollowups().setBadgeText(0);
      var custDetail = Ext.create('M1CRM.model.crmCustomerAddEdit', {});
      var custStatusCodeID = custDetail.getStatusCodeID(this.getCustStatus().getValue());
      if (this.isCRMAdvanceAvailable() == true && (custStatusCodeID == 1 || custStatusCodeID == 2)) {
        this.getQuotes().enable();
      } else {
        this.getQuotes().disable();
      }
      this.getQuotes().setBadgeText(0);

    }
  },

  makeFormReadOnly: function (flag) {
    this.getCustomerid().setReadOnly(flag);
    this.getSubtitle().setHtml('ID : ' + this.getCustomerid().getValue());
    this.makeFormReadOnlyOrCreateNew(flag);

    this.pairControlsShowAndHide(flag, this.getTaxid().id);
    this.pairControlsShowAndHide(flag, this.getSecondtaxid().id);

    if (flag) {
      this.getCustomerid().getParent().hide();
      this.resetEditableForms(this.getEditableFormFields());
      this.disableControls([this.getTaxable().id]);
    } else {
      this.enableControls([this.getTaxable().id]);
      this.getCustomerid().getParent().show();
      this.showorhideParentControl([this.getCustAddLine2(), this.getCustAddLine3(), this.getCustPhone1(), this.getCustPhone1(), this.getCustEmail(), this.getCustWeb()], flag);
    }
  },

  makeFormToCreateNewEntry: function (flag) {
    this.getCustomerid().setReadOnly(!flag);
    this.showorhideParentControl([this.getCustomerid()], flag);
    this.getSubtitle().setHtml('');
    this.makeFormReadOnlyOrCreateNew(false);
    this.showorhideParentControl([this.getCustAddLine2(), this.getCustAddLine3(), this.getCustPhone1(), this.getCustPhone1(), this.getCustEmail(), this.getCustWeb()], true);
    this.config._propCustomerTaxable != 0 ? this.getTaxable().check() : this.getTaxable().uncheck();
    //this.getTaxable().checked = this.config._propCustomerTaxable != 0 ? true : false;

    if (flag) {
      Ext.getCmp('customeraddedit_btnop1').hide();
      Ext.getCmp('customeraddedit_btnop2').hide();
      this.resetEditableForms(this.getEditableFormFields());
    } else {
      Ext.getCmp('customeraddedit_btnop1').show();
      Ext.getCmp('customeraddedit_btnop2').show();
    }
  },

  makeFormReadOnlyOrCreateNew: function (flag) {
    this.getCustomerName().setReadOnly(flag);
    flag == true ? this.getCustCountry().disable : this.getCustCountry().enable();
    this.getCustState().setReadOnly(flag);
    this.getCustCity().setReadOnly(flag);
    this.getCustPostCode().setReadOnly(flag);
    this.getCustAddLine1().setReadOnly(flag);
    this.getCustAddLine2().setReadOnly(flag);
    this.getCustAddLine3().setReadOnly(flag);
    this.getCustPhone1().setReadOnly(flag);
    this.getCustPhone2().setReadOnly(flag);
    this.getCustEmail().setReadOnly(flag);
    this.getCustWeb().setReadOnly(flag);
    this.getCustNotes().setReadOnly(flag);
    this.getCustCountry().setReadOnly(flag);
    this.showorhideParentControl([this.getCustStatus()], flag);
    this.pairControlsShowAndHide(flag, this.getTaxid().id);
    this.pairControlsShowAndHide(flag, this.getSecondtaxid().id);
  },

  vfcustidTap: function () {
    this.verifyCustomerID();
  },

  verifyCustomerID: function () {
    if (this.getCustomerid().getValue() != '') {
      this.checkCustIDandPopulate();
    }
  },

  checkCustIDandPopulate: function () {
    Ext.create('M1CRM.model.crmCustomerAddEdit', {
      OrganizationID: this.getCustomerid().getValue()
    }).getData({
      RequestID: 'customerdata',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == false) {
          /*
           Ext.Msg.confirm("New Customer", "Do you wish to create a new " + SessionObj.statics.OrgLabel  + " with this id? ",
           function(btn){
           if (btn == 'yes'){
           this.makeFormToCreateNewEntry(true);
           this.getCustStatus().setValue('Prospective');
           this.getCustSupStatus().setValue('<None>');
           }else{
           this.getCustomerid().suspendEvents();
           this.getCustomerid().setValue('');
           this.getCustomerid().resumeEvents(true);
           this.makeFormToCreateNewEntry(true);
           this.setEntryFormMode('addnew');
           this.getCustStatus().setValue('<None>');
           this.getCustSupStatus().setValue('<None>');
           }
           }, this);
           */

          this.makeFormToCreateNewEntry(true);
          this.getCustStatus().setValue('Prospective');
          this.getCustSupStatus().setValue('<None>');

        } else if (result != null && result.RecordFound == true) {
          /*
           var msg = 'There is already an ' + SessionObj.statics.OrgLabel + ' with this ID.  Do you wish to view the record?';
           Ext.Msg.confirm("", msg, function(btn){
           if (btn == 'yes'){
           this.getForm().setRecord(result.ResultObject);
           this.populateData();
           this.makeFormReadOnly(true);
           Ext.getCmp('customeraddedit_btnop1').show();
           Ext.getCmp('customeraddedit_btnop2').show();
           this.setEntryFormMode('view');
           }else{
           this.getCustomerid().suspendEvents();
           this.getCustomerid().setValue('');
           this.getCustomerid().resumeEvents(true);
           this.makeFormToCreateNewEntry(true);
           this.setEntryFormMode('addnew');
           this.getCustStatus().setValue('<None>');
           this.getCustSupStatus().setValue('<None>');
           }
           }, this);
           */

          this.getForm().setRecord(result.ResultObject);
          this.populateData();
          this.makeFormReadOnly(true);
          Ext.getCmp('customeraddedit_btnop1').show();
          Ext.getCmp('customeraddedit_btnop2').show();
          this.setEntryFormMode('view');

        }
      }
    });
  },

  getSaveObject: function () {
    var addressobj = Ext.create('M1CRM.model.crmAddress', {
      AddressLine1: this.getCustAddLine1().getValue(),
      AddressLine2: this.getCustAddLine2().getValue(),
      AddressLine3: this.getCustAddLine3().getValue(),
      City: this.getCustCity().getValue(),
      State: this.getCustState().getValue(),
      PostCode: this.getCustPostCode().getValue(),
      Country: this.getCustCountry().getValue()
    });

    var contactObj = Ext.create('M1CRM.model.crmContactDetail', {
      PhoneNumber: this.getCustPhone1().getValue(),
      AlternatePhoneNumber: this.getCustPhone2().getValue(),
      EMailAddress: this.getCustEmail().getValue(),
      WebAddress: this.getCustWeb().getValue()
    });
    return Ext.create('M1CRM.model.crmCustomerAddEdit', {
      OrganizationID: this.getCustomerid().getValue(),
      OrganizationName: this.getCustomerName().getValue(),
      Address: addressobj.getData(),
      ContactDetail: contactObj.getData(),
      LongDescriptionText: this.getCustNotes().getValue(),
      CustomerTaxable: this.getTaxable().isChecked(),
      CustomerTaxCodeID: this.getTaxidsel().getValue(),
      CustomerSecondTaxCodeID: this.getSecondtaxidsel().getValue(),
      EmployeeID: SessionObj.getEmpId()
    });
  },

  btnSaveTap: function () {
    var newCustomer = this.getSaveObject();
    var error = newCustomer.validate();
    var customValidate = true;
    if (newCustomer.isValid()) {
      newCustomer.saveData({
        scope: this,
        onReturn: function (result) {
          if (result.SaveStatus == true) {
            this.makeFormReadOnly(true);
            this.setEntryFormMode('view');
            Ext.getCmp('customeraddedit_btnop1').show();
            Ext.getCmp('customeraddedit_btnop2').show();
          } else {
            this.setEntryFormMode('addnew');
            Ext.Msg.alert('Error', 'Error saving customer info');
          }
        }

      });
    }
    else {
      this.displayValidationMessage(error);
    }
  },

  processAutoSave: function (type) {
    if (this.isFormDirty(this.getEditableFormFields())) {
      var newCustomer = this.getSaveObject();
      newCustomer.validate();
      if (newCustomer.isValid()) {
        newCustomer.saveData({
          scope: this,
          onReturn: function (result) {
            if (result.SaveStatus == true) {
              this.cancelbtnTap();
              this.processAfterAutoSave(type, this.getForm().id);
            } else {
              Ext.Msg.alert('Error', 'Error saving customer info');
            }
          }
        });
      } else {
        Ext.Msg.confirm("", this.getRecordInCompleteMessage(), function (btn, type) {
          if (btn == 'yes') {
            this.cancelbtnTap();
            this.processAfterAutoSave(type, this.getForm().id);
          }
        }, this);
      }
    }
    else {
      this.cancelbtnTap();
      this.processAfterAutoSave(type, this.getForm().id);
    }
  },

  btnEditTap: function () {
    Ext.getCmp('customeraddedit_btnop1').hide();
    Ext.getCmp('customeraddedit_btnop2').hide();
    this.makeFormReadOnly(false);
    this.setEntryFormMode('edit');
    Ext.getCmp(this.getForm().id).config._mode = 'edit';
  },

  cancelbtnTap: function (args) {
    if (this.getForm().getRecord() != null) {
      this.populateData();
      Ext.getCmp('customeraddedit_btnop1').show();
      Ext.getCmp('customeraddedit_btnop2').show();
      this.makeFormReadOnly(true);
      this.resetEditableForms(this.getEditableFormFields());
      this.setEntryFormMode('view');
      Ext.getCmp(this.getForm().id).config._mode = 'view';
    } else {
      this.addnewbtnTap();
    }
  },

  addnewbtnTap: function () {
    this.clearForm();
    this.getForm().setRecord(null);
    this.makeFormToCreateNewEntry(true);
    this.resetEditableForms(this.getEditableFormFields());
    this.setEntryFormMode('addnew');
    Ext.getCmp(this.getForm().id).config._mode = 'addnew';
  },

  getDataObject: function () {
    var dataObj = Ext.create('M1CRM.model.crmCustomerAddEdit', {});
    dataObj.data.OrganizationID = this.getCustomerid().getValue();
    dataObj.data.Organization = this.getCustomerName().getValue();
    return dataObj;
  },

  taxidselChange: function () {
    this.getTaxid().setValue(this.getSelectFieldDisplayValue(this.getTaxidsel()));
  },

  secondtaxidselChange: function () {
    this.getSecondtaxid().setValue(this.getSelectFieldDisplayValue(this.getSecondtaxidsel()));
  },

  locationsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerLocations();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'customerlocationaddedit';
    this.slideLeft(view);
  },

  contactsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerContacts();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'custcontactaddedit';
    this.slideLeft(view);
  },

  callsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerOpenCalls();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addeditcall';
    this.slideLeft(view);
  },

  followupsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerFollowups();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'followupaddedit';
    this.slideLeft(view);
  },

  quotesTap: function () {
    var view = M1CRM.util.crmViews.getAllQuotesOnContact();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addnewquote';
    Ext.getCmp(view.id).config._paramObj = this.getDataObject();
    this.slideLeft(view);
  },
  taxableUncheck: function () {
    this.getTaxidsel().setValue('');
    this.getTaxidsel().disable();
    if (SessionObj.getShowSecondTax() == true) {
      this.getSecondtaxidsel().setValue('');
      this.getSecondtaxidsel().disable();
    }
  },
  taxableCheck: function () {
    this.getTaxidsel().setValue('');
    this.getTaxidsel().enable();
    if (SessionObj.getShowSecondTax() == true) {
      this.getSecondtaxidsel().setValue('');
      this.getSecondtaxidsel().enable();
    }
  }
});	