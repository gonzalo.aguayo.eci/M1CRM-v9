Ext.define('Ext.overrides.field.Select', {
  override: 'Ext.field.Select',

  updateStore: function (newStore) {
    if (newStore) {
      this.onStoreDataChanged(newStore);
    }

    if (this.getUsePicker() && this.picker) {
      this.picker.down('pickerslot').setStore(newStore);
    } else if (this.listPanel) {
      this.listPanel.down('dataview').setStore(newStore);
    }
  }
});


Ext.define('M1CRM.controller.crmAddEditLead', {
  extend: 'M1CRM.util.crmFormControllerBase',
  requires: [
    'M1CRM.model.crmAddEditLead',

    // 'M1CRM.model.crmLeadProperties',
    'M1CRM.model.crmContactMethod',
    'M1CRM.store.crmContactMethod',
    'M1CRM.model.crmMilestone',
    'M1CRM.store.crmMilestone',
    'M1CRM.model.crmStatus',
    'M1CRM.store.crmStatus',
    'M1CRM.model.crmMarketingProgram',
    'M1CRM.store.crmMarketingProgram',
    'M1CRM.model.crmReason',
    'M1CRM.store.crmReason',
    'M1CRM.model.crmEmployee',
    'M1CRM.store.crmEmployee',
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmLeads',
    'M1CRM.model.crmCustomerDefaults',
    'M1CRM.store.crmCustomerDefaults',
    'M1CRM.model.crmCustomerDetail',
    'M1CRM.model.crmCustomerContacts',
    'M1CRM.store.crmCustomerContacts'
  ],
  config: {
    _shiporglookup: false,
    _quoteexpdate: null,
    _totalquotelines: null,
    _defleadproperties: null,
    _tempmsdate: null,
    _viewmodeTitle: 'Lead',
    _addmodeTitle: 'Add Lead',
    _editmodeTitle: 'Edit Lead',
    refs: {
      'form': '#addeditlead',
      'toolbarid': '#addeditlead_toolbarid',
      'subtitle': '#addeditleadsubtitle',
      'toback': '#addeditlead_back',
      'tohome': '#addeditlead_home',
      'addnewbtn': '#addeditlead_addnew',
      'cancelbtn': '#addeditlead_cancelbtn',
      'savebtn': '#addeditlead_savebtn',
      'editbtn': '#addeditlead_editbtn',

      'leadid': '#addeditlead_leadid',
      'custid': '#addeditlead_custid',
      'customer': '#addeditlead_customer',
      'customerlookup': '#addeditlead_customer_lookup',
      'invlocationid': '#addeditlead_invlocationid',
      'invlocation': '#addeditlead_invlocation',
      'invlocationlookup': '#addeditlead_invlocation_lookup',
      'leadcontactid': '#addeditlead_leadcontactid',
      'leadcontact': '#addeditlead_leadcontact',
      'leadcontactsel': '#addeditlead_leadcontactsel',
      'leadcontactinfo': '#addeditlead_leadcontactinfo',
      'quotelocation': '#addeditlead_quotelocation',
      'quotelocationid': '#addeditlead_quotelocationid',
      'quotelocationlookup': '#addeditlead_quotelocation_lookup',
      'quotecontact': '#addeditlead_quotecontact',
      'quotecontactid': '#addeditlead_quotecontactid',
      'quotecontactsel': '#addeditlead_quotecontactsel',
      'quotecontactinfo': '#addeditlead_quotecontactinfo',
      'shipcustid': '#addeditlead_shipcustid',
      'shipcustomer': '#addeditlead_shipcustomer',
      'shipcustomerlookup': '#addeditlead_shipcustomer_lookup',
      'shiplocationid': '#addeditlead_shiplocationid',
      'shiplocation': '#addeditlead_shiplocation',
      'shiplocationlookup': '#addeditlead_shiplocation_lookup',
      'shipcontactid': '#addeditlead_shipcontactid',
      'shipcontact': '#addeditlead_shipcontact',
      'shipcontactsel': '#addeditlead_shipcontactsel',
      'shipcontactinfo': '#addeditlead_shipcontactinfo',
      'shortdesc': '#addeditlead_shortdesc',
      'ldesc': '#addeditlead_ldesc',
      'responsemethodsel': '#addeditlead_responsemethodsel',
      'responsemethod': '#addeditlead_responsemethod',
      'marketingprogsel': '#addeditlead_marketingprogsel',
      'marketingprog': '#addeditlead_marketingprog',
      'referedby': '#addeditlead_referedby',
      'quotersel': '#addeditlead_quotersel',
      'quoter': '#addeditlead_quoter',
      'leaddate': '#addeditlead_leaddate',
      'leaddatesel': '#addeditlead_leaddatesel',
      'expecteddate': '#addeditlead_expecteddate',
      'expecteddatesel': '#addeditlead_expecteddatesel',
      'expdate': '#addeditlead_expdate',
      'expdatesel': '#addeditlead_expdatesel',
      'milestone': '#addeditlead_milestone',
      'milestonesel': '#addeditlead_milestonesel',
      'milestoneddatesel': '#addeditlead_milestoneddatesel',
      'milestoneddate': '#addeditlead_milestoneddate',
      'leadtotal': '#addeditlead_leadtotal',
      'confidence': '#addeditlead_confidence',
      'expectrevenue': '#addeditlead_expectrevenue',
      'status': '#addeditlead_status',
      'statussel': '#addeditlead_statussel',
      'closedreason': '#addeditlead_closedreason',
      'closedreasonsel': '#addeditlead_closedreasonsel',
      'closedby': '#addeditlead_closedby',
      'closedbysel': '#addeditlead_closedbysel',
      'closeddate': '#addeditlead_closeddate',
      'closeddatesel': '#addeditlead_closeddatesel',
      'leadlines': '#addeditlead_leadlines',
      'leadfollowups': '#addeditlead_leadfollowups',
      'leadcalls': '#addeditlead_leadcalls',
      'btnop1': '#addeditlead_btnop1',
      'btnop2': '#addeditlead_btnop2',
      'createquote': '#addeditlead_createquote'
    },
    control: {
      form: {
        initialize: 'initializeForm',
        show: 'showForm',
        hide: function () {
          this.scrollFormToTop();
        }
      },

      customerlookup: {
        tap: 'custLookupTap'
      },
      customer: {
        blur: 'verifyCustomerID',
        clearicontap: 'clearCustomerTap'
      },
      invlocationlookup: {
        tap: 'invlocationLookupTap'
      },
      invlocation: {
        clearicontap: 'clearInvlocationTap'
      },
      quotelocationlookup: {
        tap: 'quotelocationLookupTap'
      },
      quotelocation: {
        clearicontap: 'clearQuotelocationTap'
      },
      leadcontactsel: {
        change: 'leadcontactselChange'
      },
      quotecontactsel: {
        change: 'quotecontactselChange'
      },
      shipcustomer: {
        clearicontap: 'clearShipcustomerTap'
      },
      shipcustomerlookup: {
        tap: 'shipcustomerLookupTap'
      },
      shiplocation: {
        clearicontap: 'clearShiplocationTap'
      },
      shiplocationlookup: {
        tap: 'shiplocationLookupTap'
      },
      shipcontactsel: {
        change: 'shipcontactselChange'
      },
      responsemethodsel: {
        change: 'responsemethodChange'
      },
      marketingprogsel: {
        change: 'marketingprogselChange'
      },
      quotersel: {
        change: 'quoterselChange'
      },
      leaddatesel: {
        change: 'leaddateselChange'
      },
      expecteddatesel: {
        change: 'expecteddateselChange'
      },
      expdatesel: {
        change: 'expdateselChange'
      },
      milestonesel: {
        change: 'milestoneChange'
      },
      statussel: {
        change: 'statusChange'
      },
      closedbysel: {
        change: 'closedbyselChange'
      },
      closeddatesel: {
        change: 'closeddateselChange'
      },
      cancelbtn: {
        tap: 'btnCancelTap'
      },
      addnewbtn: {
        tap: 'btnAddNewTap'
      },
      leadfollowups: {
        tap: 'leadfollowupsTap'
      },
      leadcalls: {
        tap: 'leadopencallsTap'
      },
      leadlines: {
        tap: 'leadlinesTap'
      },
      createquote: {
        tap: 'createquoteTap'
      }
    }
  },
  initializeForm: function () {
    this.formInitilize();
    Ext.create('M1CRM.model.crmAddEditLead', {}).getData({
      RequestID: 'prop',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          this.config._defleadproperties = result.ResultObject.ResponseMethodID;

          this.getResponsemethodsel().setStore(this.getStoreFromListForDropDown(result.ResultObject.ContactMethodList, 'crmContactMethod', true));
          this.getResponsemethodsel().isDirty = false;
          if (result.ResultObject.ResponseMethodID != null) {
            this.getResponsemethodsel().setValue(result.ResultObject.ResponseMethodID);
            this.getResponsemethodsel().isDirty = false;
          }
          this.initializeMilestone(result.ResultObject.MileStoneList);


          this.getMarketingprogsel().setStore(this.getStoreFromListForDropDown(result.ResultObject.MarketingProgramList, 'crmMarketingProgram', true));
          this.getMarketingprogsel().isDirty = false;

          this.getStatussel().suspendEvents();
          this.getStatussel().setStore(Ext.create('M1CRM.model.crmStatus').getStoreFromValueList(result.ResultObject.LeadStatus));
          this.getStatussel().isDirty = false;
          this.getStatussel().setValue('O');
          this.getStatussel().resumeEvents(true);

          this.getClosedreasonsel().setStore(this.getStoreFromListForDropDown(result.ResultObject.ReasonList, 'crmReason', true));
          this.getClosedreasonsel().isDirty = false;


          this.getQuotersel().setStore(this.getStoreFromListForDropDown(result.ResultObject.EmpAsQuoterList, 'crmEmployee', true));
          this.setDefaultQuoter();
          this.getQuotersel().isDirty = false;


          this.getClosedbysel().setStore(this.getStoreFromListForDropDown(result.ResultObject.EmpAsSalesList, 'crmEmployee', true));
          this.getClosedbysel().isDirty = false;
        }
      }
    });
  },
  initializeMilestone: function (data) {
    this.getMilestonesel().suspendEvents();
    if (data == null) {
      this.getMilestonesel().setValue('');
    } else {
      this.getMilestonesel().setStore(this.getStoreFromListForDropDown(data, 'crmMilestone', true));
    }
    this.getMilestonesel().isDirty = false;
    this.getMilestonesel().resumeEvents(true);
  },

  setDefaultQuoter: function () {
    var empid = '';
    if (parseInt(SessionObj.getEmpIsAQuoter()) != 0) {
      empid = SessionObj.getEmpId();
      this.getQuotersel().setValue(empid);
      this.getQuoter().setValue(SessionObj.getEmpName());
    }
  },
  setMileStoneDate: function (flag, date) {
    if (date == null) {
      if (flag) {
        this.getMilestoneddate().setValue(formatDate(new Date(), SessionObj.getSimpleDateFormat()));
      } else {
        this.getMilestoneddate().setValue('');
      }
    } else {
      this.getMilestoneddate().setValue(formatDate(parseDate(date), SessionObj.getSimpleDateFormat()));
    }
  },
  getFormButtons: function () {
    return [this.getSavebtn().id, this.getCancelbtn().id];
  },
  getEditableFormFields: function () {
    return [this.getCustomer().id, this.getInvlocation().id, this.getLeadcontactsel().id, this.getQuotelocation().id, this.getQuotecontact().id, this.getQuotecontactsel().id, this.getShipcustomer().id,
      this.getShiplocation().id, this.getShipcontact().id, this.getShipcontactsel().id, this.getShortdesc().id, this.getLdesc().id, this.getResponsemethodsel().id, this.getMarketingprogsel().id,
      this.getReferedby().id, this.getQuotersel().id, this.getLeaddatesel().id, this.getExpecteddatesel().id, this.getExpdatesel().id, this.getMilestonesel().id, this.getLeadtotal().id,
      this.getConfidence().id, this.getExpectrevenue().id, this.getStatussel().id, this.getClosedreasonsel().id, this.getClosedbysel().id, this.getCloseddatesel().id
    ]
  },
  getAllLookupButtons: function () {
    return [this.getCustomerlookup().id, this.getInvlocationlookup().id, this.getQuotelocationlookup().id, this.getShipcustomerlookup().id, this.getShiplocationlookup().id];
  },
  getAllInfoButtons: function () {
    return [this.getLeadcontactinfo().id, this.getQuotecontactinfo().id, this.getShipcontactinfo().id];
  },
  showForm: function () {
    if (SessionObj.getLeadAddEditTSecurity() == null || SessionObj.getLeadAddEditTSecurity() == '') {
      Ext.create('M1CRM.model.crmUserSecurity', {}).getUserSecurityLevel({
        ID: 'CRMADDEDITLEAD',
        scope: this,
        onReturn: function (rec) {
          if (rec != null) {
            SessionObj.setLeadAddEditTSecurity(rec.PAccessLevel + ',' + rec.SAccessLevel);
            if (this.applyUserSecuritySettings(this.getForm().id, parseInt(rec.PAccessLevel), parseInt(rec.SAccessLevel), this.getFormButtons())) {
              this.processShowForm();
            }
          } else {
            this.toBackTap(this.getForm().id);
          }
        }
      });
    } else {
      if (this.applyUserSecuritySettings(this.getForm().id,
          parseInt(this.getPKeyFromSessionObj(SessionObj.getLeadAddEditTSecurity())), parseInt(this.getSKeyFromSessionObj(SessionObj.getLeadAddEditTSecurity())), this.getFormButtons())) {
        this.processShowForm();
      } else {
        this.toBackTap(this.getForm().id);
      }
    }
  },
  processShowForm: function () {
    var mode = Ext.getCmp(this.getForm().id).config._mode;
    if (this.getForm().getRecord() != null) {
      var rec = this.getForm().getRecord().getData();
      if (mode == 'view') {
        this.setEntryFormMode('view');
        this.makeFormEditable(false);
        this.populateLeadData(rec);
      } else if (mode == 'edit') {
        this.setEntryFormMode('edit');
        if (this.getMilestoneddate().getValue() != '')
          Ext.getCmp(this.getForm().id).config._tempmsdate = this.getMilestoneddate().getValue();
        this.makeFormEditable(true);
        this.getMilestoneddate().setValue(Ext.getCmp(this.getForm().id).config._tempmsdate);
        this.populateLeadData(rec);

      } else if (mode == 'addnew') {
        this.setEntryFormMode('addnew');
        this.makeFormEditable(true);
        if (rec != null) {
          this.populateLeadData(rec);
        }
      }
    } else {
      this.getLeadid().setValue('');
      this.leadidChange();
      this.setEntryFormMode('addnew');
      this.makeFormEditable(true);
      this.populateCustomerandShipOrg(null);
      this.makeFormsAsNewEntry();
    }
  },
  makeFormEditable: function (flag) {
    this.hideorshowTextFieldLookupInfo(this.getCustomer());
    this.hideorshowTextFieldLookupInfo(this.getInvlocation());
    this.pairControlsShowAndHide(!flag, this.getLeadcontact().id);
    this.hideorshowTextFieldLookupInfo(this.getQuotelocation());
    this.pairControlsShowAndHide(!flag, this.getQuotecontact().id);
    this.hideorshowTextFieldLookupInfo(this.getShipcustomer());
    this.hideorshowTextFieldLookupInfo(this.getShiplocation());
    this.pairControlsShowAndHide(!flag, this.getShipcontact().id);
    this.pairControlsShowAndHide(!flag, this.getResponsemethod().id);
    this.pairControlsShowAndHide(!flag, this.getMarketingprog().id);
    this.pairControlsShowAndHide(!flag, this.getQuoter().id);
    this.pairControlsShowAndHide(!flag, this.getLeaddate().id);
    this.pairControlsShowAndHide(!flag, this.getExpecteddate().id);
    this.pairControlsShowAndHide(!flag, this.getExpdate().id);
    this.pairControlsShowAndHide(!flag, this.getMilestone().id);
    this.getShortdesc().setReadOnly(!flag);
    this.getLdesc().setReadOnly(!flag);
    this.getReferedby().setReadOnly(!flag);
    this.pairControlsShowAndHide(!flag, this.getStatus().id);
    this.pairControlsShowAndHide(!flag, this.getClosedreason().id);
    this.pairControlsShowAndHide(!flag, this.getClosedby().id);
    this.pairControlsShowAndHide(!flag, this.getCloseddate().id);
    if (flag) {
      this.disableControls(this.getAllInfoButtons());
      this.getBtnop1().hide();
      this.getBtnop2().hide();

    } else {
      this.enableControls(this.getAllInfoButtons());
      this.getBtnop1().show();
      this.getBtnop2().show();
    }
    this.setMileStoneDate(false, null);
  },
  custLookupTap: function () {
    this.config._shiporglookup = false;
    var view = M1CRM.util.crmViews.getCustomers();
    view.config._parentformid = this.getForm().id;
    this.slideLeft(view);
  },
  invlocationLookupTap: function () {
    var view = M1CRM.util.crmViews.getQuoteIQSLocations();
    view.config._parentformid = this.getForm().id;
    view.config._custid = this.getCustid().getValue();
    view.config._loctype = 'ARINV';
    this.slideLeft(view);
  },
  clearInvlocationTap: function () {
    this.setInvLocationData(null);
  },
  setInvLocationData: function (rec) {
    if (rec != null) {
      this.getInvlocationid().setValue(rec.LocationID != null ? rec.LocationID : rec.InvLocationID);
      this.getInvlocation().setValue(rec.LeadLocation != null ? rec.LeadLocation : rec.LocationName);
    } else {
      this.getInvlocationid().setValue('');
      this.getInvlocation().setValue('');
    }
    this.getInvlocationid().isDirty = true;
    if (rec && rec.LocationID == null) {
      this.getInvlocation().setReadOnly(this.getInvlocation().getValue() != null && this.getInvlocation().getValue() != '' ? false : true);
    } else {
      this.getInvlocation().setReadOnly(true);
    }

    if (rec.ContactID != null)
      this.loadInvContact(rec.ContactID);
    else
      this.loadInvContact(null);
  },
  quotelocationLookupTap: function () {
    var view = M1CRM.util.crmViews.getQuoteIQSLocations();
    view.config._parentformid = this.getForm().id;
    view.config._custid = this.getCustid().getValue();
    view.config._loctype = 'QTLOC';
    M1CRM.util.crmViews.slideLeft(view);
  },
  clearQuotelocationTap: function () {
    this.getQuotelocationid().setValue('');
    this.getQuotelocationid().isDirty = true;
    this.setQuoteLocationData(null);
  },
  setQuoteLocationData: function (rec) {
    if (rec != null) {
      this.getQuotelocationid().setValue(rec.QuoteLocationID != null ? rec.QuoteLocationID : rec.LocationID != null ? rec.LocationID : rec.quoteLocationID);
      this.getQuotelocation().setValue(rec.QuoteLocation != null ? rec.QuoteLocation : rec.LocationName);
    } else {
      this.getQuotelocationid().setValue('');
      this.getQuotelocation().setValue('');
    }
    this.getQuotelocationid().isDirty = true;
    if (this.getQuotelocationid().getValue() != null && this.getQuotelocationid().getValue() != '') {
      this.getQuotelocation().setReadOnly(this.getQuotelocation().getValue() != null && this.getQuotelocation().getValue() != '' ? false : true);
    } else {
      this.getQuotelocation().setReadOnly(true);
    }

    if (rec.ContactID != null)
      this.loadQuoteContact(rec.ContactID);
    else
      this.loadQuoteContact(null);

  },
  clearShipcustomerTap: function () {
    this.getShipcustid().setValue('');
    this.getShipcustid().isDirty = true;
    this.setShipLocationData(null);
  },
  shipcustomerLookupTap: function () {
    this.config._shiporglookup = true;
    var view = M1CRM.util.crmViews.getCustomers();
    view.config._parentformid = this.getForm().id;
    M1CRM.util.crmViews.slideLeft(view);
  },
  shiplocationLookupTap: function () {
    var view = M1CRM.util.crmViews.getQuoteIQSLocations();
    view.config._parentformid = this.getForm().id;
    view.config._custid = this.getShipcustid().getValue() != '' ? this.getShipcustid().getValue() : this.getCustid().getValue();
    view.config._loctype = 'SHIPLOC';
    M1CRM.util.crmViews.slideLeft(view);
  },
  setShipLocationData: function (rec) {
    if (rec != null) {
      this.getShiplocationid().setValue(rec.ShipLocationID);
      this.getShiplocation().setValue(rec.LocationName);
    } else {
      this.getShiplocationid().setValue('');
      this.getShiplocation().setValue('');
    }
    this.getShiplocationid().isDirty = true;
    this.loadShipContact(null);
  },
  clearShiplocationTap: function () {
    this.loadShipContact(null);
  },
  populateLeadData: function (rec) {
    if (rec.LeadID != null || this.getLeadid().getValue() != '') {
      if (rec.LeadID != null && rec.LeadID != '') {
        this.getLeadid().setValue(rec.LeadID);
      }
      this.leadidChange();
      this.loadLeadHeaderInformation();
    } else if (Ext.getCmp(this.getForm().id).config._mode != 'view') {

      if (rec.OrganizationID != null && (rec.InvLocationID == null || rec.InvLocationID == '') && (rec.QuoteLocationID == null || rec.QuoteLocationID == '' ) && (rec.ShipLocationID == null || rec.ShipLocationID == '')) {
        this.populateCustomerandShipOrg(rec);
      } else if (rec.OrganizationID != null && rec.InvLocationID != null) {
        this.setInvLocationData(rec);
      } else if (rec.OrganizationID != null && rec.QuoteLocationID != null) {
        this.setQuoteLocationData(rec);
      } else if (rec.OrganizationID != null && rec.ShipLocationID != null) {
        this.setShipLocationData(rec);
      }
    }
  },
  populateShipData: function (rec) {
    this.getShipcustid().setValue(rec.ShipOrganizationID == null ? rec.OrganizationID : rec.ShipOrganizationID);
    this.getShipcustomer().setValue(rec.ShipOrganizationName == null ? rec.OrganizationName : rec.ShipOrganizationName);
    this.getShipcustid().isDirty = true;

    if (rec.LocationID == null) {
      this.getShiplocationid().setValue('');
      this.getShiplocation().setValue('');
    } else {
      this.getShiplocationid().setValue(rec.LocationID);
      this.getShiplocation().setValue(rec.LocationName);
    }

    if (rec.ContactID == null)
      this.loadShipContact(null);
    else
      this.loadShipContact(rec.ContactID);


  },
  loadLeadHeaderInformation: function () {
    Ext.create('M1CRM.model.crmAddEditLead', {
      LeadID: this.getLeadid().getValue()
    }).getData({
      RequestID: 'data',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          var rec = result.ResultObject;

          this.getCustid().setValue(rec.OrganizationID);
          this.getCustomer().setValue(rec.OrganizationName);
          this.getLeadcontactid().setValue(rec.LeadLocationInfo.ContactID);
          this.setInvLocationData(rec.LeadLocationInfo);

          this.getQuotecontactid().setValue(rec.QuoteLocationInfo.ContactID);
          this.setQuoteLocationData(rec.QuoteLocationInfo);

          this.getShipcontactid().setValue(rec.ShipLocationInfo.ContactID);
          this.populateShipData(rec.ShipLocationInfo);

          this.getShortdesc().setValue(rec.ShortDescription);
          this.getLdesc().setValue(rec.LongDescription);

          this.getLeadtotal().setValue(this.getFormatNumberAsCurrency(rec.LeadTotal, true));
          this.getResponsemethodsel().setValue(rec.ResponseMethodID);
          this.getMarketingprogsel().setValue(rec.MarketingProgramID);
          this.getReferedby().setValue(rec.ReferedBy);
          this.getQuotersel().setValue(rec.Quoter);
          if (rec.LeadDate != null) {
            var tdate = this.getCorrectDate(rec.LeadDate);
            this.getLeaddate().setValue(formatDate(tdate, SessionObj.getSimpleDateFormat()));
            this.getLeaddatesel().setValue(new Date(tdate));
          }

          if (rec.ExpectedDate != null) {
            var tdate = this.getCorrectDate(rec.ExpectedDate);
            this.getExpecteddate().setValue(formatDate(tdate, SessionObj.getSimpleDateFormat()));
            this.getExpecteddatesel().setValue(new Date(tdate));
          }

          if (rec.ExpireDate != null) {
            var tdate = this.getCorrectDate(rec.ExpectedDate);
            this.getExpdate().setValue(formatDate(tdate, SessionObj.getSimpleDateFormat()));
            this.getExpdatesel().setValue(new Date(tdate));
          }

          this.getMilestonesel().setValue(rec.MilestoneID);
          this.calculateExpenceRevenue();
          var tdate = this.getCorrectDate(rec.MilestoneDate);
          this.getMilestoneddate().setValue(formatDate(tdate, SessionObj.getSimpleDateFormat()));

          this.getStatussel().suspendEvents();
          this.getStatussel(rec.Status);
          this.getStatussel().resumeEvents(true);
          this.getStatus().setValue(this.getSelectFieldDisplayValue(this.getStatussel()));
          this.getClosedreasonsel(rec.ClosedReasonID);
          this.getClosedbysel(rec.ClosedBy);

          this.loadCounts(rec.Summary);
        }
      }
    });

  },

  loadCounts: function (recObj) {
    this.setButtonBadgeText(this.getLeadfollowups().id, parseInt(recObj.LeadFollowUps));
    this.setButtonBadgeText(this.getLeadlines().id, parseInt(recObj.LeadLines));
    this.setButtonBadgeText(this.getLeadcalls().id, parseInt(recObj.LeadCalls));
    
    if (parseInt(recObj.LeadLines) > 0) {
      this.getCreatequote().enable();
    } else {
      this.getCreatequote().disable();
    }
  },
  leadidChange: function () {
    if (this.getLeadid().getValue() != '') {
      this.getSubtitle().setHtml('ID : ' + this.getLeadid().getValue());
    } else {
      this.getSubtitle().setHtml('');
    }
  },
  leadcontactselChange: function () {
    this.getLeadcontact().setValue(this.getSelectFieldDisplayValue(this.getLeadcontactsel()));
  },
  quotecontactselChange: function () {
    this.getQuotecontact().setValue(this.getSelectFieldDisplayValue(this.getQuotecontactsel()));
  },
  responsemethodChange: function () {
    this.getResponsemethod().setValue(this.getSelectFieldDisplayValue(this.getResponsemethodsel()));
  },
  marketingprogselChange: function () {
    this.getMarketingprog().setValue(this.getSelectFieldDisplayValue(this.getMarketingprogsel()));
  },
  shipcontactselChange: function () {
    this.getShipcontact().setValue(this.getSelectFieldDisplayValue(this.getShipcontactsel()));
  },
  quoterselChange: function () {
    this.getQuoter().setValue(this.getSelectFieldDisplayValue(this.getQuotersel()));
  },
  leaddateselChange: function () {
    this.getLeaddate().setValue(formatDate(this.getLeaddatesel().getValue(), SessionObj.getSimpleDateFormat()));
  },
  expecteddateselChange: function () {
    this.getExpecteddate().setValue(formatDate(this.getExpecteddatesel().getValue(), SessionObj.getSimpleDateFormat()));
  },
  expdateselChange: function () {
    this.getExpdate().setValue(formatDate(this.getExpdatesel().getValue(), SessionObj.getSimpleDateFormat()));
  },
  closedbyselChange: function () {
    this.getClosedby().setValue(this.getSelectFieldDisplayValue(this.getClosedbysel()));
  },
  closeddateselChange: function () {
    this.getCloseddate().setValue(formatDate(this.getSelectFieldDisplayValue(this.getCloseddatesel()), SessionObj.getSimpleDateFormat()));
  },
  populateCustomerandShipOrg: function (rec) {
    if (this.config._shiporglookup == false) {
      if (rec != null) {
        var orgid = rec.OrganizationID;
        var name = rec.OrganizationName;
        this.getCustid().setValue(orgid);
        this.getCustomer().setValue(name);
        this.getCustid().isDirty = true;
        this.getShipcustid().setValue(orgid);
        this.getShipcustid().isDirty = true;
        this.getShipcustomer().setValue(this.getCustomer().getValue());
        this.loadDefaultCustomerInfo();
        this.enableControls(this.getAllLookupButtons());
      } else {
        this.getCustid().setValue('');
        this.getCustomer().setValue('');
        this.getCustid().isDirty = false;
        this.getShipcustid().setValue('');
        this.getShipcustid().isDirty = true;
        this.getShipcustomer().setValue('');
        this.loadDefaultCustomerInfo();
      }
    } else {
      this.populateShipData(rec);
    }
  },
  loadDefaultCustomerInfo: function () {
    if (this.getCustid().getValue() != '') {
      Ext.create('M1CRM.model.crmAddEditLead', {
        OrganizationID: this.getCustid().getValue()
      }).getData({
        RequestID: 'custdefaults',
        scope: this,
        onReturn: function (result) {
          if (result != null && result.RecordFound == true) {
            var defCustObj = result.ResultObject.CustomerDefaults;

            this.getInvlocationid().setValue(defCustObj.ARInvoiceLocationID);
            if (defCustObj.ARInvoiceLocationID != '') {
              this.getInvlocation().setValue(defCustObj.InvLocationName);
            } else {
              this.getInvlocation().setValue(this.getCustomer().getValue());
            }

            this.getInvlocation().setReadOnly(this.getInvlocation().getValue() != null && this.getInvlocation().getValue() != '' ? false : true);
            this.loadInvContact(defCustObj.ARInvoiceContactID != '' ? defCustObj.ARInvoiceContactID : null);

            this.getQuotelocationid().setValue(defCustObj.QuoteLocationID);
            if (defCustObj.QuoteLocationID != '') {
              this.getQuotelocation().setValue(defCustObj.QuoteLocationName);
            } else {
              this.getQuotelocation().setValue(this.getCustomer().getValue());
            }
            this.loadQuoteContact(defCustObj.QuoteContactID != '' ? defCustObj.QuoteContactID : null);

            this.getShiplocationid().setValue(defCustObj.ShipLocationID);
            if (defCustObj.ShipLocationID != '') {
              this.getShiplocation().setValue(defCustObj.ShipLocationName);
            } else {
              this.getShiplocation().setValue(this.getCustomer().getValue());
            }
            this.loadShipContact(defCustObj.ShipContactID != '' ? defCustObj.ShipContactID : null);
          }
        }
      });
    } else {
      this.getInvlocationid().setValue('');
      this.getInvlocation().setValue('');
      this.getInvlocation().setReadOnly(true);
      this.getLeadcontactsel().setValue('');
      this.getLeadcontactsel().setStore(null);
      this.getQuotelocationid().setValue('');
      this.getQuotelocation().setValue('');
      this.getQuotecontact().setReadOnly(true);
      this.getQuotecontactsel().setValue('');
      this.getQuotecontactsel().setStore(null);
      this.getShiplocationid().setValue('');
      this.getShiplocation().setValue('');
      this.getShiplocation().setReadOnly(true);
      this.getShipcontactsel().setValue('');
      this.getShipcontactsel().setStore(null);
      this.makeFormsAsNewEntry();
    }
  },
  loadInvContact: function (sellid) {
    this.loadContacts(this.getLeadcontactsel(), this.getCustid().getValue(), this.getInvlocationid().getValue(), sellid);
  },
  loadQuoteContact: function (sellid) {
    this.loadContacts(this.getQuotecontactsel(), this.getCustid().getValue(), this.getQuotelocationid().getValue(), sellid);
  },
  loadShipContact: function (sellid) {
    this.loadContacts(this.getShipcontactsel(), this.getShipcustid().getValue(), this.getShiplocationid().getValue(), sellid);
  },
  milestoneChange: function () {
    if (this.getMilestonesel().getValue() != '') {
      this.setMileStoneDate(true, null);
      this.getConfidence().setValue(this.getMilestonesel().getRecord().getData().ConfidenceFactor);
    } else {
      this.getMilestonesel().setValue(this.getMilestonesel().previousRecord.getData().MilestoneID);
    }
    this.getMilestone().setValue(this.getSelectFieldDisplayValue(this.getMilestonesel()));
    this.calculateExpenceRevenue();
  },
  calculateExpenceRevenue: function () {
    this.getExpectrevenue().setValue(this.getFormatNumberAsCurrency(this.getFormatCurrencyAsNumber(this.getLeadtotal().getValue() == null || this.getLeadtotal().getValue() == '' ? 0 : this.getLeadtotal().getValue()) * this.getConfidence().getValue(), true));
  },
  statusChange: function () {
    if (this.getStatussel().getValue() == 'C') {
      this.pairControlsShowAndHide(false, this.getCloseddate().id);

      this.getCloseddatesel().setValue(new Date());
      this.pairControlsShowAndHide(false, this.getClosedreason().id);
      this.pairControlsShowAndHide(false, this.getClosedby().id);
    } else {
      this.pairControlsShowAndHide(true, this.getCloseddate().id);
      this.getCloseddate().getValue('');
      this.pairControlsShowAndHide(true, this.getClosedreason().id);
      this.pairControlsShowAndHide(true, this.getClosedby().id);
    }
    this.getStatus().setValue(this.getSelectFieldDisplayValue(this.getStatussel()));
    this.getClosedbysel().setValue('');
  },
  verifyCustomerID: function () {
    if (this.getCustomer().getValue() != '') {
      Ext.create('M1CRM.model.crmAddEditLead', {
        OrganizationID: this.getCustid().getValue()
      }).getData({
        RequestID: 'custdata',
        scope: this,
        onReturn: function (result) {
          if (result != null) {
            if (result.RecordFound == false) {
              Ext.Msg.alert("Warning", 'Not a valid ' + SessionObj.getOrgLabel() + ' ID',
                function (btn) {
                  if (btn == 'ok') {
                    this.populateCustomerandShipOrg(null);
                  }
                }, this);
            } else {
              this.getCustomer().suspendEvents();
              this.populateCustomerandShipOrg(result.ResultObject);
              this.getCustomer().resumeEvents(true);
            }
          }
        }
      });
    }
  },
  clearCustomerTap: function () {
    this.populateCustomerandShipOrg(null);
  },
  makeFormsAsNewEntry: function () {
    this.disableControls(this.getAllLookupButtons());
    this.getCustomerlookup().enable();
    this.getLeaddatesel().setValue(new Date()); //, SessionObj.statics.SimpleDateFormat));
    this.getLeaddate().setValue(formatDate(this.getLeaddatesel().getValue(), SessionObj.statics.SimpleDateFormat));
    this.getShortdesc().setValue();
    this.getLdesc().setValue();
    this.getStatussel().setValue('O');
    this.setControlsReadOnly(true, []);
    this.initializeMilestone(null);
    this.getLeadtotal().setValue(0);
    this.setMileStoneDate(false, null);
    Ext.getCmp(this.getForm().id).config._tempmsdate = null;
    this.resetEditableForms(this.getEditableFormFields());
  },
  getSaveObject: function () {

    var leadLocObj = Ext.create('M1CRM.model.crmLocationInfo', {
      LocationID: this.getInvlocationid().getValue(),
      ContactID: this.getLeadcontactsel().getValue()
    });

    var leadQuoteLocObj = Ext.create('M1CRM.model.crmLocationInfo', {
      LocationID: this.getQuotelocationid().getValue(),
      ContactID: this.getQuotecontactsel().getValue()
    });

    var leadShipLocObj = Ext.create('M1CRM.model.crmShipInfo', {
      ShipOrganizationID: this.getShipcustid().getValue(),
      LocationID: this.getShiplocationid().getValue(),
      ContactID: this.getShipcontactsel().getValue()
    });

    return Ext.create('M1CRM.model.crmAddEditLead', {
      LeadID: this.getLeadid().getValue(),
      OrganizationID: this.getCustid().getValue(),
      LeadLocationInfo: leadLocObj.getData(),
      QuoteLocationInfo: leadQuoteLocObj.getData(),
      ShipLocationInfo: leadShipLocObj.getData(),
      ShortDescription: this.getShortdesc().getValue(),
      LongDescription: this.getLdesc().getValue(),
      ResponseMethodID: this.getResponsemethodsel().getValue(),
      MarketingProgramID: this.getMarketingprogsel().getValue(),
      ReferedBy: this.getReferedby().getValue(),
      Quoter: this.getQuotersel().getValue(),
      LeadDate: this.getLeaddatesel().getValue(),
      ExpectedDate: this.getExpecteddatesel().getValue(),
      ExpireDate: this.getExpdatesel().getValue(),
      MilestoneID: this.getMilestonesel().getValue(),
      MilestoneDate: this.getMilestoneddate().getValue() != '' ? parseDate(this.getMilestoneddate().getValue()) : new Date(),    //this.getMilestoneddate().getValue(), //this.changeDisplayDateToCorrectDateTime(this.getMilestoneddate().getValue()),
      LeadTotal: this.getFormatCurrencyAsNumber(this.getLeadtotal().getValue() == null || this.getLeadtotal().getValue() == '' ? 0 : this.getLeadtotal().getValue()),
      LeadStatus: this.getStatussel().getValue(),
      ClosedReasonID: this.getClosedreasonsel().getValue(),
      ClosedBy: this.getClosedbysel().getValue(),
      ClosedDate: this.getCloseddatesel().getValue(),
      EmployeeID: SessionObj.getEmpId()
    });
  },
  btnEditTap: function () {
    this.setEntryFormMode('edit');
    Ext.getCmp(this.getForm().id).config._mode = 'edit';
    if (this.getMilestoneddate().getValue() != '')
      Ext.getCmp(this.getForm().id).config._tempmsdate = this.getMilestoneddatesel().getValue();
    this.makeFormEditable(true);
    var x = formatDate(new Date(this.changeDisplayDateToCorrectDateTime(Ext.getCmp(this.getForm().id).config._tempmsdate)), SessionObj.getSimpleDateFormat());
    this.getMilestoneddate().setValue(x);
    this.enableControls(this.getAllLookupButtons());
  },
  btnSaveTap: function () {
    var saveObj = this.getSaveObject();
    var error = saveObj.validate();
    if (saveObj.isValid()) {
      saveObj.saveData({
        scope: this,
        onReturn: function (result) {
          if (result && result.SaveStatus) {
            this.getLeadid().setValue(result.ResultObject.LeadID);
            this.leadidChange();
            this.setEntryFormMode('view');
            Ext.getCmp(this.getForm().id).config._mode = 'view';
            if (this.getMilestoneddate().getValue() != '')
              Ext.getCmp(this.getForm().id).config._tempmsdate = this.getMilestoneddatesel().getValue();
            this.makeFormEditable(false);
            var x = formatDate(new Date(this.changeDisplayDateToCorrectDateTime(Ext.getCmp(this.getForm().id).config._tempmsdate)), SessionObj.getSimpleDateFormat());
            this.getMilestoneddate().setValue(x);
            this.loadCounts(result.ResultObject.Summary);
          } else {
            this.setEntryFormMode('addnew');
            Ext.Msg.alert('Error', 'Error saving quotation');
          }
        }
      });
    } else {
      M1CRM.util.crmViews.displayValidationMessage(error);
    }
  },
  btnCancelTap: function () {
    if (this.getForm().getRecord() == null) {
      Ext.getCmp(this.getForm().id).config._mode = 'addnew';
      this.setEntryFormMode('addnew');
      this.populateCustomerandShipOrg(null);
      this.makeFormsAsNewEntry();
    } else if (Ext.getCmp(this.getForm().id).config._mode == 'addnew') {
      this.setEntryFormMode('addnew');
      this.getForm().setRecord(null);
      this.populateCustomerandShipOrg(null);
      this.makeFormsAsNewEntry();
    } else {
      Ext.getCmp(this.getForm().id).config._mode = 'view';
      this.setEntryFormMode('view');
      this.makeFormEditable(false);
      if (Ext.getCmp(this.getForm().id).config._tempmsdate != null) {
        var x = formatDate(new Date(this.changeDisplayDateToCorrectDateTime(Ext.getCmp(this.getForm().id).config._tempmsdate)), SessionObj.getSimpleDateFormat());
        this.getMilestoneddate().setValue(x);
      }
      this.resetEditableForms(this.getEditableFormFields());
    }
  },
  btnAddNewTap: function () {
    Ext.getCmp(this.getForm().id).config._mode = 'addnew';
    this.setEntryFormMode('addnew');
    if (this.getLeadid().getValue() == '') {
      this.getForm().setRecord(null);
      this.makeFormsAsNewEntry();
    } else {
      this.getLeadid().setValue('');
      this.leadidChange();
      var tempcustid = this.getCustid().getValue();
      var tempcustomer = this.getCustomer().getValue();
      this.makeFormEditable(true);
      this.makeFormsAsNewEntry();
      this.getCustid().setValue(tempcustid);
      this.getCustid().isDirty = true;
      this.getCustomer().setValue(tempcustomer);
      this.getShipcustid().setValue(this.getCustid().getValue());
      this.getShipcustid().isDirty = true;
      this.getShipcustomer().setValue(this.getCustomer().getValue());
      this.loadDefaultCustomerInfo();
    }
  },
  processAutoSave: function (type) {
    if (Ext.getCmp(this.getForm().id).config._mode != 'view' && this.isFormDirty(this.getEditableFormFields())) {
      var isnewRec = false;
      if (( this.getLeadid().getValue() == null || this.getLeadid().getValue() == '') && this.getCustid().getValue() != '') {

        var msg = "This lead has no lines Do you wish to save anyway?";
        Ext.Msg.confirm("Save Data", msg, function (btn, type) {
          if (btn == 'yes') {
            this.invokeprocessAutoSave(type);
          } else {
            this.processAfterAutoSave(type, this.getForm().id);
          }
        }, this);
      } else {
        var msg = "This lead has no lines.  Do you wish to exit anyway?";
        this.checkLeadLines(type, msg);
      }
    } else {
      if (Ext.getCmp(this.getForm().id).config._mode != 'view' && this.getLeadid().getValue() != null && this.getLeadid().getValue() != '') {
        var msg = "This lead has no lines. Do you wish to exit anyway?"
        this.checkLeadLines(type, msg);
      } else {
        this.processAfterAutoSave(type, this.getForm().id);
      }
    }
  },
  checkLeadLines: function (type, msg) {
    Ext.create('M1CRM.model.crmAddEditLead', {
      LeadID: this.getLeadid().getValue()
    }).getData({
      RequestID: 'checklines',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          if (parseInt(result.ResultObject.Summary.LeadLines) <= 0) {

            if (type == 'back') {
              Ext.Msg.confirm("Save Data", msg, function (btn, type) {
                if (btn == 'yes') {
                  this.makeFormEditable(false);
                  this.setEntryFormMode('view');
                  this.invokeprocessAutoSave('back');
                }
              }, this);
            } else {
              Ext.Msg.confirm("Save Data", msg, function (btn, type) {
                if (btn == 'yes') {
                  this.makeFormEditable(false);
                  this.makeFormsAsNewEntry();
                  this.invokeprocessAutoSave('home');
                }
              }, this);
            }
          } else {
            this.invokeprocessAutoSave(type);
          }
        } else {
          this.populateCustomerandShipOrg(null);
          this.makeFormsAsNewEntry();
          this.invokeprocessAutoSave(type);
        }
      }
    });
  },
  invokeprocessAutoSave: function (type) {
    var paramObj = this.getSaveObject();
    var error = paramObj.validate();
    if (paramObj.isValid()) {
      paramObj.saveData({
        scope: this,
        onReturn: function (result) {
          if (result.SaveStatus == true) {
            this.makeFormEditable(false);
            this.processAfterAutoSave(type, this.getForm().id);
          } else {
            Ext.Msg.alert('Error', 'Error saving customer lead');
          }
        }
      });
    } else {
      Ext.Msg.confirm("", this.getRecordInCompleteMessage(), function (btn, type) {
        if (btn == 'yes') {
          this.processAfterAutoSave(type, this.getForm().id);
        }
      }, this);
    }
  },
  leadfollowupsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerFollowups();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'followupaddedit';
    this.slideLeft(view);
  },
  leadopencallsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerOpenCalls();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addeditcall';
    this.slideLeft(view);
  },
  leadlinesTap: function () {
    var view = M1CRM.util.crmViews.getLeadLines();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'leadlineaddedit';
    this.slideLeft(view);
  },
  createquoteTap: function () {
    Ext.create('M1CRM.model.crmAddEditLead', {
      LeadID: this.getLeadid().getValue()
    }).saveData({
      RequestID: 'createquote',
      scope: this,
      onReturn: function (result) {
        if (!result.SaveStatus) {
          Ext.Msg.alert('Error', 'Error Create Quote from Leads');
        } else if (result.SaveStatus && result.ValidationFail) {
          M1CRM.util.crmViews.displayValidationMessage(result.Messages);
        } else if (result.SaveStatus && !result.ValidationFail) {
          var view = M1CRM.util.crmViews.getAddNewQuote();
          view.config._parentformid = this.getForm().id;
          var rec = Ext.create('M1CRM.model.crmAddEditQuotation', {
            QuoteID: result.ResultObject.QuoteID,
            OrganizationID: this.getCustid().getValue(),
            OrganizationName: this.getCustomer().getValue()
          });
          Ext.getCmp('addnewquote').setRecord(rec);
          M1CRM.util.crmViews.slideLeft(view);
        }
      }
    });

  }
});
