Ext.define('Ext.overrides.field.Select', {
  requires: [
    'Ext.field.Select'
  ],
  override: 'Ext.field.Select',

  updateStore: function (newStore) {
    if (newStore) {
      this.onStoreDataChanged(newStore);
    }

    if (this.getUsePicker() && this.picker) {
      this.picker.down('pickerslot').setStore(newStore);
    } else if (this.listPanel) {
      this.listPanel.down('dataview').setStore(newStore);
    }
  }
});


Ext.define('M1CRM.controller.crmAddNewQuote', {
  extend: 'M1CRM.util.crmFormControllerBase',
  requires: [
    'M1CRM.model.crmStandardMessages',
    'M1CRM.store.crmStandardMessages',
    'M1CRM.model.crmQuoteProperties',
    'M1CRM.store.crmQuoteProperties',
    'M1CRM.model.crmQuotations',
    'M1CRM.model.crmEmployee',
    'M1CRM.store.crmEmployee',
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmCustomerContacts',
    'Ext.ux.M1.M1MenuButton'
  ],
  config: {
    _shiporglookup: false,
    _quoteexpdate: null,
    _totalquotelines: null,
    _viewmodeTitle: 'Quote',
    _addmodeTitle: 'Add Quote',
    _editmodeTitle: 'Edit Quote',
    _quotationPropeties: null,
    refs: {
      'form': '#addnewquote',
      'toolbarid': '#addnewquote_toolbarid',
      'subtitle': '#addnewquotesubtitle',
      'toback': '#addnewquote_back',
      'tohome': '#addnewquote_home',
      'addnewbtn': '#addnewquote_addnew',
      'cancelbtn': '#addnewquote_cancelbtn',
      'savebtn': '#addnewquote_savebtn',
      'editbtn': '#addnewquote_editbtn',
      'quoteid': '#addnewquote_quoteid',
      'customerid': '#addnewquote_custid',
      'customer': '#addnewquote_customer',
      'customerlookup': '#addnewquote_customer_lookup',
      'custinfo': '#addnewquote_customer_info',
      'quotelocationid': '#addnewquote_quotelocationid',
      'quotelocation': '#addnewquote_quotelocation',
      'quotelocationlookup': '#addnewquote_quotelocation_lookup',
      'quotelocationinfo': '#addnewquote_quotelocation_info',
      'quotecontactid': '#addnewquote_quotecontactid',
      'quotecontact': '#addnewquote_quotecontact',
      'quotecontactsel': '#addnewquote_quotecontactsel',
      'quotecontactinfo': '#addnewquote_quotecontactinfo',
      'invlocationid': '#addnewquote_invlocationid',
      'invlocation': '#addnewquote_invlocation',
      'quoteinvlocationlookup': '#addnewquote_invlocation_lookup',
      'invlocationinfo': '#addnewquote_invlocation_info',
      'quoteactcontact': '#addnewquote_actcontact',
      'actcontactid': '#addnewquote_actcontactid',
      'quoteactcontactsel': '#addnewquote_actcontactsel',
      'contactinfo': '#addnewquote_contactinfo',
      'shipcustomerid': '#addnewquote_shipcustomerid',
      'shipcustomer': '#addnewquote_shipcustomer',
      'shipcustlookup': '#addnewquote_shipcustomer_lookup',
      'shipcustinfo': '#addnewquote_shipcustomer_info',
      'shiplocation': '#addnewquote_shiplocation',
      'shiplocationlookup': '#addnewquote_shiplocation_lookup',
      'shiplocationinfo': '#addnewquote_shiplocation_info',
      'shiplocationid': '#addnewquote_shiplocationid',
      'shipcontactid': '#addnewquote_shipcontactid',
      'shipcontactsel': '#addnewquote_shipcontactsel',
      'shipcontact': '#addnewquote_shipcontact',
      'shipcontactinfo': '#addnewquote_shipcontactinfo',
      'quoter': '#addnewquote_quoter',
      'quotersel': '#addnewquote_quotersel',
      'quotedate': '#addnewquote_quotedate',
      'quotedatesel': '#addnewquote_quotedatesel',
      'quoteduedate': '#addnewquote_quoteduedate',
      'quoteduedatesel': '#addnewquote_quoteduedatesel',
      'expdate': '#addnewquote_expdate',
      'stdmessage': '#addnewquote_stdmessage',
      'stdmessagesel': '#addnewquote_stdmessagesel',
      'closed': '#addnewquote_closed',
      'closeddate': '#addnewquote_closeddate',
      'quoterid': '#addnewquote_quoterid',
      'quoteparts': '#addnewquote_quoteparts',
      'headertxt': '#addnewquote_header',
      'footertxt': '#addnewquote_footer',
      'quotefollowups': '#addnewquote_quotefollowups',
      'quotecalls': '#addnewquote_quotecalls',
      'emailquote': '#addnewquote_emailquotebtn'
    },
    control: {
      form: {
        initialize: 'initializeForm',
        show: 'showForm',
        hide: function () {
          this.scrollFormToTop();
        }
      },
      customer: {
        clearicontap: function () {
          this.btnCancelTap();
        }
      },
      shipcustomer: {
        change: function () {
          if (this.getShipcustomerid().getValue() == '' && this.getShipcustomer().getValue() != '') {
            this.verifyCustomerID('shipcust');
          }
        },
        clearicontap: function () {
          this.getShipcustomerid().setValue('');
          this.getShipcustomerid().isDirty = true;
          this.getShiplocationid().setValue('');
          this.getShiplocation().setValue('');
          this.loadShipContact(null);
        }
      },
      shiplocation: {
        change: function () {
          if (this.getShiplocationid().getValue() == '' && this.getShiplocation().getValue() != '') {
            this.verifyCustomerID('shiploc');
          }
        },
        clearicontap: function () {
          this.getShiplocationid().setValue('');
          this.getShiplocationid().isDirty = true;
          this.loadShipContact(null);
        }
      },
      quotelocation: {
        clearicontap: function () {
          this.getQuotelocationid().setValue('');
          this.getQuotelocationid().isDirty = true;
          this.loadQuoteContact(null);
        }
      },
      invlocation: {
        clearicontap: function () {
          this.getInvlocationid().setValue('');
          this.getInvlocationid().isDirty = true;
          this.loadAcctContact(null);
        }
      },
      shiplocation: {
        clearicontap: function () {
          this.getShiplocationid().setValue('');
          this.getShiplocationid().isDirty = true;
          this.loadShipContact(null);
        }
      },
      cancelbtn: {
        tap: 'btnCancelTap'
      },
      customerlookup: {
        tap: 'custLookupTap'
      },
      quoteinvlocationlookup: {
        tap: 'invlocationlookupTap'
      },
      quotelocationlookup: {
        tap: 'quotelocationlookupTap'
      },
      shipcustlookup: {
        tap: 'shipcustlookupTap'
      },
      shiplocationlookup: {
        tap: 'shiplocationlookupTap'
      },
      addnewbtn: {
        tap: 'btnAddNewTap'
      },
      quoteparts: {
        tap: 'quotepartsTap'
      },
      quotefollowups: {
        tap: 'quotefollowupsTap'
      },
      quotecalls: {
        tap: 'quotecallsTap'
      },
      emailquote: {
        tap: 'emailQuoteTap'
      },
      quotedatesel: {
        change: 'quotedateselChange'
      },
      quotersel: {
        change: 'quoterselChange'
      }
    }
  },
  initializeForm: function () {
    this.formInitilize();
    this.createOptionsMenu();
    this.getClosed().parent.parent.hide();
    this.loadFormProperties();
  },
  loadFormProperties: function () {
    Ext.create('M1CRM.model.crmAddEditQuotation', {}).getData({
      RequestID: 'prop',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          this.getStdmessagesel().setStore(result.ResultObject.StandardMessagesList);
          this.getQuotersel().setStore(this.getStoreFromListForDropDown(result.ResultObject.EmpAsQuoterList, 'crmEmployee', true));

          result.ResultObject.EmpAsQuoterList = null;
          this.setQuoterInfo();
          this.config._quotationPropeties = result.ResultObject;
          this.setExpDate();
          this.quotedateselChange();
          this.setDefaultHeaderFooterText();
        } else {

          Ext.Msg.alert('Error', 'Unable to access quotation default information, cannot proceed.', function (btn) {
            if (btn == 'ok') {
              this.toBackTap(this.getForm().id);
            }
          }, this);

        }
      }
    });
  },
  setExpDate: function () {
    if (this.config._quotationPropeties != null) {
      var dt1 = new Date();
      var dt2 = new Date();
      var dt1 = new Date(dt2.setDate(dt1.getDate() + parseInt(this.config._quotationPropeties.ExpirationDays)));
      this.config._quoteexpdate = new Date(dt1);
      this.getExpdate().setValue(formatDate(new Date(dt1), SessionObj.getSimpleDateFormat()));
    }
  },
  quotedateselChange: function () {
    if (this.config._quotationPropeties != null) {
      var dt1 = this.getQuotedatesel().getValue() != null ? new Date(this.getQuotedatesel().getValue().toString()) : new Date();
      dt1.setDate(dt1.getDate() + parseInt(this.config._quotationPropeties.ExpirationDays));
      this.config._quoteexpdate = new Date(dt1);
      this.getExpdate().setValue(formatDate(dt1, SessionObj.getSimpleDateFormat()));
    }
  },
  custLookupTap: function () {
    AddEditQuote = null;
    this.config._shiporglookup = false;
    var view = M1CRM.util.crmViews.getCustomers();
    view.config._parentformid = this.getForm().id;
    this.slideLeft(view);
  },
  invlocationlookupTap: function () {
    var view = M1CRM.util.crmViews.getQuoteIQSLocations();
    view.config._parentformid = 'addnewquote';
    view.config._custid = this.getCustomerid().getValue();
    view.config._loctype = 'ARINV';
    M1CRM.util.crmViews.slideLeft(view);
  },
  quotelocationlookupTap: function () {
    var view = M1CRM.util.crmViews.getQuoteIQSLocations();
    view.config._parentformid = 'addnewquote';
    view.config._custid = this.getCustomerid().getValue();
    view.config._custname = this.getCustomer().getValue();

    view.config._loctype = 'QTLOC';
    M1CRM.util.crmViews.slideLeft(view);
  },
  shipcustlookupTap: function () {
    this.config._shiporglookup = true;
    var view = M1CRM.util.crmViews.getCustomers();
    view.config._parentformid = 'addnewquote';
    M1CRM.util.crmViews.slideLeft(view);
  },
  shiplocationlookupTap: function () {
    var view = M1CRM.util.crmViews.getQuoteIQSLocations();
    view.config._parentformid = 'addnewquote';
    view.config._custid = this.getShipcustomerid().getValue() != '' ? this.getShipcustomerid().getValue() : this.getCustomerid().getValue();
    view.config._loctype = 'SHIPLOC';
    M1CRM.util.crmViews.slideLeft(view);
  },
  setFormAsNewEntry: function () {
    this.setEntryFormMode('addnew');
    this.makeFormEditable(true);
    this.getSubtitle().setHtml('');
    this.config._totalquotelines = 0;
    this.showControls([this.getQuoteactcontactsel().id, this.getQuotecontactsel().id, this.getShipcontactsel().id, this.getStdmessagesel().id,
      this.getQuotedatesel().id, this.getQuoteduedatesel().id, this.getQuotersel().id]);
    this.hideControls([this.getQuoteactcontact().id, this.getQuotecontact().id, this.getShipcontact().id, this.getStdmessage().id, this.getQuotedate().id, this.getQuoteduedate().id, this.getQuoter().id]);


    this.getQuotedatesel().setValue(new Date());
    this.getQuoteduedatesel().setValue(new Date());
    this.getClosed().parent.parent.hide();
    this.setQuoterInfo();
    this.setExpDate();
    this.setDefaultHeaderFooterText();
    this.resetEditableForms(this.getEditableFormFields());
    this.enableControls(this.getAllLookupBtns());
    this.disableControls(this.getAllInfoBtns());

    //Ext.getCmp('addnewquote_morelocinfofieldset').show();
  },
  setQuoterInfo: function () {
    this.getQuoterid().setValue(SessionObj.getEmpId());
    this.getQuotersel().setValue(SessionObj.getEmpId());
    this.getQuoter().setValue(SessionObj.getEmpName());
  },
  loadQuoteInformation: function () {
    Ext.create('M1CRM.model.crmAddEditQuotation', {
      QuoteID: Ext.getCmp('addnewquote_quoteid').getValue()
    }).getData({
      RequestID: 'data',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          var rec = result.ResultObject;

          if (rec.QuoteLocationID != null && rec.QuoteLocationID != '') {
            this.getQuotelocationid().setValue(rec.QuoteLocationID);
            this.getQuotelocation().setValue(rec.QuoteLocationName);
            this.getQuotelocationinfo().enable();
          } else {
            this.getQuotelocationid().setValue('');
            this.getQuotelocation().setValue('');
            this.getQuotelocationinfo().disable();
          }

          this.loadQuoteContact(rec.QuoteContactID);
          this.getQuotecontact().setValue(rec.QuoteContactName);
          this.getQuotecontactid().setValue(rec.QuoteContactID);
          if (rec.QuoteContactID == null || rec.QuoteContactID == '') {
            this.getQuotecontactinfo().disable();
          } else {
            this.getQuotecontactinfo().enable();
          }

          if (rec.ARInvoiceLocationID != null && rec.ARInvoiceLocationID != '') {
            this.getInvlocationid().setValue(rec.ARInvoiceLocationID);
            this.getInvlocation().setValue(rec.InvLocationName);
            this.getInvlocationinfo().enable();
          } else {
            this.getInvlocationid().setValue('');
            this.getInvlocation().setValue('');
            this.getInvlocationinfo().disable();
          }

          this.loadAcctContact(rec.ARInvoiceContactID);
          this.getQuoteactcontact().setValue(rec.ARContactName);
          this.getActcontactid().setValue(rec.ARInvoiceContactID);
          if (rec.ARInvoiceContactID == null || rec.ARInvoiceContactID == '') {
            this.getContactinfo().disable();
          } else {
            this.getContactinfo().enable();
          }

          if (rec.ShipLocationID != null && rec.ShipLocationID != '') {
            this.getShiplocationid().setValue(rec.ShipLocationID);
            this.getShiplocation().setValue(rec.ShipLocationName);
            this.getShiplocationinfo().enable();
          } else {
            this.getShiplocationid().setValue('');
            this.getShiplocation().setValue('');
            this.getShiplocationinfo().disable();
          }

          if (rec.ShipOrganizationID != null) {
            this.getShipcustomerid().setValue(rec.ShipOrganizationID);
            this.getShipcustomer().setValue(rec.ShipOrganization);
          } else {
            this.getShipcustomerid().setValue(rec.getCustomerid);
            this.getShipcustomer().setValue(this.getCustomer().getValue());
          }

          this.loadShipContact(rec.ShipContactID);
          this.getShipcontact().setValue(rec.ShipContactName);
          this.getShipcontactid().setValue(rec.ShipContactID);
          if (rec.ShipContactID == null || rec.ShipContactID == '') {
            this.getShipcontactinfo().disable();
          } else {
            this.getShipcontactinfo().enable();
          }


          this.getStdmessagesel().setValue(rec.StandardMessageID);
          this.setStandardMsg();

          this.getHeadertxt().setValue(rec.QuoteHeaderMessageText);
          this.getFootertxt().setValue(rec.QuoteFooterMessageText);

          var tdate = this.getCorrectDate(rec.QuoteDate);
          this.getQuotedate().setValue(formatDate(tdate, SessionObj.getSimpleDateFormat()));
          this.getQuotedatesel().setValue(new Date(tdate));

          tdate = this.getCorrectDate(rec.DueDate);
          this.getQuoteduedate().setValue(formatDate(tdate, SessionObj.getSimpleDateFormat()));
          this.getQuoteduedatesel().setValue(new Date(tdate));

          var tdate = this.getCorrectDate(rec.ExpirationDate);
          this.getExpdate().setValue(formatDate(tdate, SessionObj.getSimpleDateFormat()));

          if (rec.QuoterEmployeeID != null) {
            this.getQuotersel().setValue(rec.QuoterEmployeeID);
          }
          
          this.setButtonBadgeText(this.getQuoteparts().id, parseInt(rec.Summary.QuoteLines));
          this.setButtonBadgeText(this.getQuotefollowups().id, parseInt(rec.Summary.QuoteFollowUps));
          this.setButtonBadgeText(this.getQuotecalls().id, parseInt(rec.Summary.QuoteCalls));

          this.getClosed().setValue(rec.Closed);
          if (rec.Closed == true) {
            this.getEditbtn().hide(); // disable();
          } else {
            this.getEditbtn().show(); // enable()
          }

        }
      }
    });
  },
  setStandardMsg: function () {
    var key = this.getStdmessagesel().getValue();
    var stdMsg = "";
    if (this.getStdmessagesel().getStore() != null) {
      var arrObj = this.getStdmessagesel().getStore().getData().items;
      for (var i = 0; i < arrObj.length; i++) {
        if (arrObj[i].data.StandardMessageID == key) {
          stdMsg = arrObj[i].data.ShortDescription;
          break;
        }
      }
    }
    this.getStdmessage().setValue(stdMsg);
  },
  getFormButtons: function () {
    return [this.getSavebtn().id, this.getCancelbtn().id];
  },
  getEditableFormFields: function () {
    return [this.getCustomerid().id, this.getInvlocationid().id, this.getInvlocation().id, this.getQuotelocationid().id, this.getQuotelocation().id,
      this.getShipcustomerid().id, this.getShipcustomer().id, this.getShiplocationid().id, this.getShiplocation().id, this.getQuoteactcontactsel().id,
      this.getQuotecontactsel().id, this.getShipcontactsel().id, this.getQuotedatesel().id,
      this.getQuoteduedatesel().id, this.getStdmessagesel().id,
      this.getClosed().id
    ]
  },
  showForm: function () {
    if (SessionObj.getQuoteAddEditTSecurity() == null || SessionObj.getQuoteAddEditTSecurity() == '') {
      Ext.create('M1CRM.model.crmUserSecurity', {}).getUserSecurityLevel({
        ID: 'CRMADDEDITQUOTATION',
        scope: this,
        onReturn: function (rec) {
          if (rec != null) {
            SessionObj.setQuoteAddEditTSecurity(rec.PAccessLevel + ',' + rec.SAccessLevel);
            if (this.applyUserSecuritySettings(this.getForm().id, parseInt(rec.PAccessLevel), parseInt(rec.SAccessLevel),
                this.getFormButtons()
              )) {
              this.processShowForm();
            }
          } else {
            this.toBackTap(this.getForm().id);
          }
        }
      });
    } else {
      if (this.applyUserSecuritySettings(this.getForm().id,
          parseInt(this.getPKeyFromSessionObj(SessionObj.getQuoteAddEditTSecurity())), parseInt(this.getSKeyFromSessionObj(SessionObj.getQuoteAddEditTSecurity())), this.getFormButtons())) {
        this.processShowForm();
      } else {
        this.toBackTap(this.getForm().id);
      }
    }

    this.createOptionsMenu();
  },
  populateQuoteData: function (rec) {
    if (rec.QuoteID != null || (Ext.getCmp('addnewquote_quoteid').getValue() != null && Ext.getCmp('addnewquote_quoteid').getValue() != '')) {
      if (rec.QuoteID != null)
        Ext.getCmp('addnewquote_quoteid').setValue(rec.QuoteID);

      this.getSubtitle().setHtml('ID : ' + Ext.getCmp('addnewquote_quoteid').getValue());
      this.getCustomer().setValue(rec.OrganizationName);
      this.getCustomerid().setValue(rec.OrganizationID);
      this.loadQuoteInformation();
    } else {
      if (rec.OrganizationID) {
        if (this.config._shiporglookup == false) {
          var orgid = rec.OrganizationID;
          var name = rec.OrganizationName;
          this.getCustomerid().setValue(orgid);
          this.getCustomer().setValue(name);
          this.getCustomerid().isDirty = true;
          this.getShipcustomerid().setValue(orgid);
          this.getShipcustomerid().isDirty = true;
          this.getShipcustomer().setValue(this.getCustomer().getValue());
          if (this.getQuotecontactsel().getValue() == null || this.getQuotecontactsel().getValue() == '') {
            this.loadQuoteContact(null, null);
          }
        } else {
          this.getShipcustomerid().setValue(rec.OrganizationID);
          this.getShipcustomer().setValue(rec.OrganizationName);
          this.loadShipContact(null);
        }
      }

      if (rec.QuoteLocationID != null) {
        this.getQuotelocationid().setValue(rec.QuoteLocationID);
        this.getQuotelocationid().isDirty = true;
        this.getQuotelocation().setValue(rec.LocationName);
        this.getQuotelocation().isDirty = true;
        if (rec.ContactID == null)
          this.loadQuoteContact(null, null);
        else
          this.loadQuoteContact(rec.ContactID, rec.ContactName);

      }
      if (rec.InvLocationID != null) {
        this.getInvlocationid().setValue(rec.InvLocationID);
        this.getInvlocationid().isDirty = true;
        this.getInvlocation().setValue(rec.LocationName);
        this.loadAcctContact(null);
      }
      if (rec.ShipLocationID != null) {
        this.getShiplocationid().setValue(rec.ShipLocationID);
        this.getShiplocationid().isDirty = true;
        this.getShiplocation().setValue(rec.LocationName);
        this.getShiplocation().isDirty = true;
        this.loadShipContact(null);
      }

    }
  },
  processShowForm: function () {
    this.createOptionsMenu();
    var mode = Ext.getCmp(this.getForm().id).config._mode;
    if (this.getForm().getRecord() != null) {
      var rec = this.getForm().getRecord().data;
      if (mode == 'view') {
        this.setEntryFormMode('view');
        this.makeFormEditable(false);
        this.populateQuoteData(rec);
      } else if (mode == 'edit') {
        this.setEntryFormMode('edit');
        this.makeFormEditable(true);
        this.populateQuoteData(rec);
      } else if (mode == 'addnew') {
        this.setEntryFormMode('addnew');
        this.makeFormEditable(true);
        if (rec != null) {
          if (rec.OrganizationID != null) {
            rec = Ext.create('M1CRM.model.crmAddEditQuotation', {
              QuoteID: null,
              OrganizationID: rec.OrganizationID,
              OrganizationName: rec.OrganizationName != null && rec.OrganizationName != '' ? rec.OrganizationName : Ext.getCmp('customeraddedit_customer').getValue(),
              QuoteLocationID: rec.QuoteLocationID != null ? rec.QuoteLocationID : rec.LocationID,
              ShipLocationID: rec.ShipLocationID,
              InvLocationID: rec.InvLocationID,
              LocationName: rec.LocationName,
              ContactID: rec.ContactID != null ? rec.ContactID : null,
              ContactName: rec.ContactID != null ? Ext.getCmp('customercontactaddedit_contactname').getValue() : ''
            }).data;
          }
          this.populateQuoteData(rec);
        } else {
          this.setFormAsNewEntry();
        }
      }
    } else {
      this.clearFormFields();
      this.setFormAsNewEntry();
      this.getQuoteparts().setBadgeText(0);
    }
  },
  loadDefaultCustomerLocation: function (ContactID) {
    Ext.create('M1CRM.model.crmAddEditQuotation', {
      OrganizationID: this.getCustomerid().getValue()
    }).getData({
      RequestID: 'custdefaults',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          this.getInvlocationid().setValue(result.ResultObject.ARInvoiceLocationID);
          this.getQuotelocationid().setValue(result.ResultObject.QuoteLocationID);
          this.getShiplocationid().setValue(result.ResultObject.ShipLocationID);
          this.loadAcctContact(null);
          this.loadQuoteContact(ContactID);
          this.loadShipContact(null);
        }
      }
    });
  },
  loadAcctContact: function (sellid) {
    this.loadContacts(this.getQuoteactcontactsel(), this.getCustomerid().getValue(), this.getInvlocationid().getValue(), sellid);
  },
  loadQuoteContact: function (sellid, name) {
    this.loadContacts(this.getQuotecontactsel(), this.getCustomerid().getValue(), this.getQuotelocationid().getValue(), sellid);
    if (sellid != null) {
      this.getQuotecontactsel().setValue(sellid);
      if (Ext.getCmp(this.getForm().id).config._mode != 'view')
        this.getQuoteactcontactsel().isDirty = false;
    }
  },
  loadShipContact: function (sellid) {
    var custid = '';
    if (this.getCustomerid().getValue() != this.getShipcustomerid().getValue() && this.getShipcustomerid().getValue() != '') {
      custid = this.getShipcustomerid().getValue();
    } else {
      custid = this.getCustomerid().getValue();
    }
    this.loadContacts(this.getShipcontactsel(), custid, this.getShiplocationid().getValue(), sellid);
  },
  formOptionsMenuTap: function (button) {
    this.getMenuOptions().showBy(button);
  },
  getMenuOptions: function () {
    return M1CRM.util.crmViews.getOptionsMenu(this);
  },
  getSaveObject: function () {
    return Ext.create('M1CRM.model.crmAddEditQuotation', {
      QuoteID: this.getQuoteid().getValue(),
      OrganizationID: this.getCustomerid().getValue(),
      QuoteLocationID: this.getQuotelocationid().getValue(),
      QuoteContactID: this.getQuotecontactsel().getValue(),
      ARInvoiceLocationID: this.getInvlocationid().getValue(),
      ARInvoiceContactID: this.getQuoteactcontactsel().getValue(),
      ShipOrganizationID: this.getShipcustomerid().getValue(),
      ShipLocationID: this.getShiplocationid().getValue(),
      ShipContactID: this.getShipcontactsel().getValue(),
      StandardMessageID: this.getStdmessagesel().getValue(),
      Quoter: this.getQuotersel().getValue(), //this.getQuoterid().getValue(),
      QuoteDate: new Date(this.getQuotedatesel().getValue().toString()),
      DueDate: new Date(this.getQuoteduedatesel().getValue().toString()),
      ExpirationDate: this.config._quoteexpdate,
      QuoteHeaderMessageText: this.getHeadertxt().getValue(),
      QuoteFooterMessageText: this.getFootertxt().getValue(),
      QuoterEmployeeID: SessionObj.getEmpId()
    });
  },
  btnSaveTap: function () {
    if (this.getQuotedatesel().getValue() == null) {
      Ext.Msg.alert('Warning', 'Quote date cannot be blank');
    } else if (this.getQuoteduedatesel().getValue() == null) {
      Ext.Msg.alert('Warning', 'Quote due date cannot be blank');
    } else {
      var saveObj = this.getSaveObject();
      var error = saveObj.validate();
      if (saveObj.isValid()) {
        saveObj.saveData({
          scope: this,
          onReturn: function (result) {
            if (result.SaveStatus == true) {

              this.getSubtitle().setHtml('ID : ' + result.ResultObject.QuoteID);
              Ext.getCmp('addnewquote_quoteid').setValue(result.ResultObject.QuoteID);

              if (this.getQuotecontactsel().getStore() != null) {
                this.getQuotecontact().setValue(this.getContactName(this.getQuotecontactsel().getStore().getData().items, this.getQuotecontactsel().getValue()));
              }
              this.pairControlsShowAndHide(true, this.getQuotecontact().id);

              if (this.getQuoteactcontactsel().getStore() != null) {
                this.getQuoteactcontact().setValue(this.getContactName(this.getQuoteactcontactsel().getStore().getData().items, this.getQuoteactcontactsel().getValue()));
              }
              this.pairControlsShowAndHide(true, this.getQuoteactcontact().id);

              if (this.getShipcontactsel().getStore() != null) {
                this.getShipcontact().setValue(this.getContactName(this.getShipcontactsel().getStore().getData().items, this.getShipcontactsel().getValue()));
              }
              this.pairControlsShowAndHide(true, this.getShipcontact().id);

              this.pairControlsShowAndHide(true, this.getQuoter().id);


              this.getQuotedate().setValue(formatDate(new Date(this.getQuotedatesel().getValue()), SessionObj.statics.SimpleDateFormat));
              this.pairControlsShowAndHide(true, this.getQuotedate().id);

              this.getQuoteduedate().setValue(formatDate(new Date(this.getQuoteduedatesel().getValue()), SessionObj.statics.SimpleDateFormat));
              this.pairControlsShowAndHide(true, this.getQuoteduedate().id);

              var key = this.getStdmessagesel().getValue();
              var arrObj = this.getStdmessagesel().getStore().getData().items;
              var stdMsg = "";
              for (var i = 0; i < arrObj.length; i++) {
                if (arrObj[i].data.StandardMessageID == key) {
                  stdMsg = arrObj[i].data.ShortDescription;
                  break;
                }
              }
              this.getStdmessage().setValue(stdMsg);
              this.pairControlsShowAndHide(true, this.getStdmessage().id);
              this.setEntryFormMode('view');
              Ext.getCmp(this.getForm().id).config._mode = 'view';
              this.makeFormEditable(false);
              this.resetEditableForms(this.getEditableFormFields());

              this.setButtonBadgeText(this.getQuoteparts().id, parseInt(result.ResultObject.Summary.QuoteLines));
              this.setButtonBadgeText(this.getQuotefollowups().id, parseInt(result.ResultObject.Summary.QuoteFollowUps));
              this.setButtonBadgeText(this.getQuotecalls().id, parseInt(result.ResultObject.Summary.QuoteCalls));
            } else {
              this.setEntryFormMode('addnew');
              Ext.Msg.alert('Error', 'Error saving quotation');
            }
          }

        });

      } else {
        M1CRM.util.crmViews.displayValidationMessage(error);
      }
    }
  },
  processAutoSave: function (type) {
    if (Ext.getCmp(this.getForm().id).config._mode != 'view' && this.isFormDirty(this.getEditableFormFields())) {
      var isnewRec = false;
      if ((Ext.getCmp('addnewquote_quoteid').getValue() == null || Ext.getCmp('addnewquote_quoteid').getValue() == '') && this.getCustomerid().getValue() != '') {
        var msg = Ext.String.format("This quote has no lines/parts. Do you wish to {0} anyway? ", Ext.getCmp(this.getForm().id).config._mode == 'view' ? "exit" : "save");

        Ext.Msg.confirm("Save Data", msg, function (btn, type) {
          if (btn == 'yes') {
            this.invokeprocessAutoSave(type);
          } else {
            this.processAfterAutoSave(type, this.getForm().id);
          }
        }, this);
      } else if (this.getQuoteparts().getBadgeText() == null || this.getQuoteparts().getBadgeText() == 0) {
        var msg = "This quote has no lines/parts.  Do you wish to exit anyway?";
        Ext.Msg.confirm("Save Data", msg, function (btn, type) {
          if (btn == 'yes') {
            this.invokeprocessAutoSave(type);
          }
        }, this);
      } else {
        this.processAfterAutoSave(type, this.getForm().id);
      }
    } else if (Ext.getCmp('addnewquote_quoteid').getValue() != null && Ext.getCmp('addnewquote_quoteid').getValue() != '' &&
      (this.getQuoteparts().getBadgeText() == null || this.getQuoteparts().getBadgeText() == 0 )) {
      var msg = "This quote has no lines/parts. Do you wish to exit anyway?"
      Ext.Msg.confirm("Warning", msg, function (btn, type) {
        if (btn == 'yes') {
          this.processAfterAutoSave(type, this.getForm().id);
        }
      }, this);
    } else {
      this.processAfterAutoSave(type, this.getForm().id);
    }
  },

  invokeprocessAutoSave: function (type) {
    var saveObj = this.getSaveObject();
    saveObj.validate();
    if (saveObj.isValid()) {
      saveObj.saveData({
        scope: this,
        onReturn: function (result) {
          if (result.SaveStatus == true) {
            this.makeFormEditable(false);
            this.processAfterAutoSave(type, this.getForm().id);
          } else {
            Ext.Msg.alert('Error', 'Error saving customer quotation');
          }
        }
      });
    } else {
      Ext.Msg.confirm("", this.getRecordInCompleteMessage(), function (btn, type) {
        if (btn == 'yes') {
          this.processAfterAutoSave(type, this.getForm().id);
        }
      }, this);
    }
  },
  getContactName: function (arrayObj, key) {
    for (var i = 0; i < arrayObj.length; i++) {
      if (arrayObj[i].data.ContactID == key)
        return arrayObj[i].data.ContactName;
    }
    return "";
  },
  editQuoteTap: function () {
    this.makeFormEditable(true);
  },
  canceleditQuoteTap: function () {
    this.makeFormEditable(false);
    this.resetEditableForms(this.getEditableFormFields());
    this.scrollFormToTop();
  },
  getAllInfoBtns: function () {
    return [this.getCustinfo().id, this.getQuotelocationinfo().id, this.getQuotecontactinfo().id, this.getInvlocationinfo().id,
      this.getContactinfo().id, this.getShipcustinfo().id, this.getShiplocationinfo().id, this.getShipcontactinfo().id
    ];
  },
  getAllLookupBtns: function () {
    return [this.getCustomerlookup().id, this.getQuotelocationlookup().id, this.getQuoteinvlocationlookup().id, this.getShipcustlookup().id, this.getShiplocationlookup().id];
  },
  makeFormEditable: function (flag) {
    this.getInvlocation().setReadOnly(!flag);
    this.getQuotelocation().setReadOnly(!flag);
    this.getShipcustomer().setReadOnly(!flag);
    Ext.getCmp('addnewquote_shiplocation').setReadOnly(!flag);
    if (flag) {
      this.enableControls(this.getAllLookupBtns());
      this.disableControls(this.getAllInfoBtns());
      Ext.getCmp('addnewquote_btnop1').hide();
      if (this.getQuotecontactid().getValue() != null && this.getQuotecontactid().getValue() != '') {
        this.getQuotecontactsel().setValue(this.getQuotecontactid().getValue());
        this.getQuotecontactid().setValue('');
      }

      if (this.getShipcontactid().getValue() != null && this.getShipcontactid().getValue() != '') {
        this.getShipcontactsel().setValue(this.getShipcontactid().getValue());
        this.getShipcontactid().setValue('');
      }

      if (this.getActcontactid().getValue() != null && this.getActcontactid().getValue() != '') {
        this.getQuoteactcontactsel().setValue(this.getActcontactid().getValue());
        this.getActcontactid().setValue('');
      }

    } else {
      this.disableControls(this.getAllLookupBtns());
      this.enableControls(this.getAllInfoBtns());
      Ext.getCmp('addnewquote_btnop1').show();
    }

    this.hideorshowTextFieldLookupInfo(this.getCustomer());
    this.hideorshowTextFieldLookupInfo(this.getQuotelocation());
    this.hideorshowTextFieldLookupInfo(this.getInvlocation());
    this.hideorshowTextFieldLookupInfo(this.getShipcustomer());
    this.hideorshowTextFieldLookupInfo(this.getShiplocation());


    this.pairControlsShowAndHide(!flag, this.getQuoteactcontact().id);
    this.pairControlsShowAndHide(!flag, this.getQuotecontact().id);
    this.pairControlsShowAndHide(!flag, this.getShipcontact().id);
    this.pairControlsShowAndHide(!flag, this.getQuoter().id);
    this.pairControlsShowAndHide(!flag, this.getQuotedate().id);
    this.pairControlsShowAndHide(!flag, this.getQuoteduedate().id);
    this.pairControlsShowAndHide(!flag, this.getStdmessage().id);
    this.getShipcustomer().setReadOnly(!flag);
    this.getHeadertxt().setReadOnly(!flag);
    this.getFootertxt().setReadOnly(!flag);
  },
  btnEditTap: function () {
    this.setEntryFormMode('edit');
    Ext.getCmp(this.getForm().id).config._mode = 'edit';
    this.makeFormEditable(true);
  },
  btnCancelTap: function () {
    if (this.getForm().getRecord() == null) {
      Ext.getCmp(this.getForm().id).config._mode = 'addnew';
      this.clearFormFields();
      this.loadQuoteContact(null, null);
      this.loadAcctContact(null);
      this.loadShipContact(null);
      this.setFormAsNewEntry();
    } else if (Ext.getCmp(this.getForm().id).config._mode == 'addnew') {
      this.getForm().setRecord(null);
      this.clearFormFields();
      this.loadQuoteContact(null, null);
      this.loadAcctContact(null);
      this.loadShipContact(null);
      this.setFormAsNewEntry();
    } else {
      Ext.getCmp(this.getForm().id).config._mode = 'view';
      this.setEntryFormMode('view');
      this.makeFormEditable(false);
      this.populateQuoteData(this.getForm().getRecord().getData());
    }
  },
  verifyCustomerID: function (type) {
    var msg = '';
    var custid = this.getCustomerid().getValue();
    if (type == 'shipcust') {
      msg = this.getShipcustomer().getValue() + ' is not a valid Ship ' + SessionObj.getOrgLabel();
      custid = this.getShipcustomer().getValue();
    } else if (type == 'shiploc') {
      custid = this.getShiplocationid().getValue();
      msg = Ext.getCmp('addnewquote_shiplocation').getValue() + ' is not a valid Ship Location';
    }

    Ext.create('M1CRM.model.crmAddEditQuotation', {
      OrganizationID: custid
    }).getData({
      RequestID: 'custdata',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == false) {
          Ext.Msg.alert('', msg, function (btn) {
            if (btn == 'ok') {
              if (type == 'shipcust') {
                this.getShipcustomer().setValue('');
                this.getShipcustomer().isDirty = true;
              } else {
                Ext.getCmp('addnewquote_shiplocation').setValue('');
                Ext.getCmp('addnewquote_shiplocation').isDirty = true;
              }
            }
          }, this);
        } else if (result != null && result.RecordFound == true) {
          if (type == 'shipcust') {
            this.getShipcustomerid().setValue(result.ResultObject.OrganizationID);
            this.getShipcustomerid().isDirty = true;
            this.getShipcustomer().setValue(result.ResultObject.OrganizationName);
            this.getShipcustomer().isDirty = true;
          } else {
            this.getShiplocationid().setValue(result.ResultObject.OrganizationID);
            this.getShiplocationid().isDirty = true;
            this.getShiplocationid().setValue(result.ResultObject.OrganizationName);
            this.getShiplocation().isDirty = true;
          }
          this.loadShipContact(null);
        }
      }
    });
  },
  btnAddNewTap: function () {
    Ext.getCmp(this.getForm().id).config._mode = 'addnew';
    if (this.getQuoteid().getValue() == '') {
      this.getForm().setRecord(null);
      this.setFormAsNewEntry();
      this.getExpdate().setValue(this.getFormatDateFromJSonDate(this.config._quoteexpdate, SessionObj.getSimpleDateFormat()));
    } else {
      var tempcustid = this.getCustomerid().getValue();
      var tempcustomer = this.getCustomer().getValue();
      this.setFormAsNewEntry();
      this.getQuoteid().setValue('');
      this.getCustomerid().setValue(tempcustid);
      this.getCustomerid().isDirty = true;
      this.getCustomer().setValue(tempcustomer);
      this.getShipcustomerid().setValue(this.getCustomerid().getValue());
      this.getShipcustomerid().isDirty = true;
      this.getShipcustomer().setValue(this.getCustomer().getValue());
      this.getExpdate().setValue(this.getFormatDateFromJSonDate(this.config._quoteexpdate, SessionObj.getSimpleDateFormat()));
    }
  },
  quotepartsTap: function () {
    var view = M1CRM.util.crmViews.getQuoteParts();
    if (parseInt(this.getQuoteparts().getBadgeText()) == 0) {
      view = M1CRM.util.crmViews.getQuotePartAddEdit();
      Ext.getCmp('addeditquotepart').setRecord(null);
      Ext.getCmp(view.id).config._mode = 'addnew';
    }
    AddEditQuote = Ext.create('M1CRM.model.crmQuotations', {
      QuoteID: Ext.getCmp('addnewquote_quoteid').getValue(),
      OrganizationID: this.getCustomerid().getValue(),
      LocationID: this.getInvlocationid().getValue(),
      ContactID: this.getQuoteactcontactsel().getValue(),
      Quoter: this.getQuoterid().getValue(),
      ExpirationDate: new Date(this.getExpdate().getValue().toString()),
      ShipOrganizationID: this.getShipcustomerid().getValue(),
      ShipLocationID: this.getShiplocationid().getValue(),
      ShipContactID: this.getShipcontactid().getValue(),
      ARInvoiceLocationID: this.getInvlocationid().getValue(),
      ARInvoiceContactID: this.getActcontactid().getValue(),
      QuoteLocationID: this.getQuotelocationid().getValue(),
      QuoteContactID: this.getQuotecontactid().getValue()
    });
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addeditquotepart';
    this.slideLeft(view);
  },
  quotefollowupsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerFollowups();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'followupaddedit';
    this.slideLeft(view);
  },
  quotecallsTap: function () {
    var view = M1CRM.util.crmViews.getCustomerOpenCalls();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'addeditcall';
    this.slideLeft(view);
  },
  setDefaultHeaderFooterText: function () {
    if (this.config._quotationPropeties != null) {
      this.getHeadertxt().setValue(this.config._quotationPropeties.MobileQuoteHeaderMessage);
      this.getFootertxt().setValue(this.config._quotationPropeties.MobileQuoteFooterMessage);
    }
  },
  createOptionsMenu: function () {
    var items = this.getOptionMenuItems();
    Ext.Viewport.setMenu(this.createMenu(items), {
      side: 'right',
      reveal: true
    });
  },
  getOptionMenuItems: function(scope) {
    var items = [{
      xtype: 'm1menubutton',
      itemId: this.getForm().id + '_emailquotebtn',
      id: this.getForm().id + '_emailquotebtn',
      text: 'Email Quote',
      listners: {
        scope: this,
        tap: function () {
          this.emailQuoteTap();
        }
      }
    }];

    return items;
  },
  quoterselChange: function () {
    this.getQuoter().setValue(this.getSelectFieldDisplayValue(this.getQuotersel()));
  },
  emailQuoteTap: function () {
    this.hideOptionsMenu();
    if (this.getQuoteparts().getBadgeText() == null || parseInt(this.getQuoteparts().getBadgeText()) <= 0) {
      Ext.Msg.alert('Warning', 'You must have at least one quote line entry to email this quote');
    } else if (this.getQuotecontactid().getValue() == null || this.getQuotecontactid().getValue() == '') {
      Ext.Msg.alert('Warning', 'Quote Contact not defined cannot generate email.');
    } else {
      Ext.create('M1CRM.model.crmAddEditQuotation', {
        QuoteID: Ext.getCmp('addnewquote_quoteid').getValue(),
        OrganizationID: this.getCustomerid().getValue(),
        LocationID: this.getQuotelocationid().getValue(),
        ContactID: this.getQuotecontactid().getValue(),
        ContactName: this.getQuotecontact().getValue()
      }).emailIt({
        RequestID: 'email',
        scope: this,
        onReturn: function (result) {
          if (result != null && result.EmailProcessed == false) {
            Ext.Msg.alert('Warning', result.Message, Ext.emptyFn, this);
          } else if (result != null && result.EmailProcessed == true) {
            Ext.Msg.alert('Info', result.Message, Ext.emptyFn, this);
          }
        }
      });
    }
  }
});
