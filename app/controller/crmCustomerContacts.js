Ext.define('M1CRM.controller.crmCustomerContacts', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmCustomerContacts',
    'M1CRM.store.crmCustomerContacts'
  ],
  config: {
    _listid: 'customercontactlist',
    _seardfieldid: 'customercontact_search',
    control: {
      customercontacts: {
        initialize: 'initializeList',
        show: 'onCustomerContactsInitialize',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.onCustomerContactTap(item);
        },
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },

  init: function () {
    this.control({
      '#customercontactlist_addnew': {
        scope: this,
        tap: this.addNewCustContactTap
      },

      '#customercontactlist_options': {
        scope: this,
        tap: this.optionsMenuTap
      }
    });
  },
  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labCustContactsSummary';
    Ext.getCmp('customercontactlist_toolbarid').setTitle(SessionObj.statics.OrgLabel + ' Contacts');
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmCustomerContacts', {}).getListLayout());
  },
  onCustomerContactsInitialize: function (obj, e) {
    this.initilize();
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  getDataObject: function () {
    var rec = null;
    var dataObj = null;
    var parentid = Ext.getCmp(this.config._listid).config._parentformid;
    if (parentid == 'addeditcall' || parentid == 'followupaddedit' || parentid == 'customerlocationaddedit') {
      var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
        OrganizationID: Ext.getCmp(parentid + '_custid').getValue(),
        OrganizationName: Ext.getCmp(parentid + '_customer').getValue(),
        LocationID: Ext.getCmp(parentid + '_locationid').getValue() != null ? Ext.getCmp(parentid + '_locationid').getValue() : '',
        LocationName: Ext.getCmp(parentid + '_location').getValue() != null ? Ext.getCmp(parentid + '_location').getValue() : '',
        ContactID: null
      });

      this.setSummary('Org:' + dataObj.data.OrganizationID + ' Loc:' + dataObj.data.LocationID);
    } else {
      rec = Ext.getCmp('customeraddedit').getRecord();
      if (rec != null) {
        var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
          OrganizationID: rec.OrganizationID,
          OrganizationName: rec.OrganizationName,
          LocationID: ''
        });
        this.setSummary('Org:' + rec.OrganizationID);
      }
    }
    return dataObj;
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings('customercontactlist', parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmCustomerContacts', this.getDataObject());
  },
  addNewCustContactTap: function () {
    if (Ext.getCmp('customercontactaddedit') != null && Ext.getCmp('customercontactaddedit').getRecord() != null)
      Ext.getCmp('customercontactaddedit').setRecord(null);
    var view = M1CRM.util.crmViews.getCustomerContactAddEdit();
    view.config._parentformid = this.config._listid; //   'customercontactlist';
    view.setRecord(this.getDataObject());
    Ext.getCmp(view.id).config._mode = 'addnew';
    this.slideLeft(view);
  },
  onCustomerContactTap: function (item) {
    var parentid = Ext.getCmp(this.config._listid).config._parentformid;
    var view = null;
    var searchfield = Ext.ComponentQuery.query('#' + this.config._listid + ' searchfield')[0].id;
    Ext.getCmp(searchfield).setValue('');

    if (parentid == 'addeditcall') {
      var view = M1CRM.util.crmViews.getAddEditCall();
    } else if (parentid == 'followupaddedit') {
      view = M1CRM.util.crmViews.getFollowupAddedit();
    }
    if (view != null) {
      view.setRecord(item);
      Ext.getCmp(this.config._listid).config._parentformid = null;
      this.slideRight(view);
    } else {

      if (parentid != null) {
        item.data.OrganizationName = Ext.getCmp(parentid + '_customer').getValue();
        if (Ext.isDefined(Ext.getCmp(parentid + '_locationid'))) {
          item.data.LocationID = Ext.getCmp(parentid + '_locationid').getValue() != null ? Ext.getCmp(parentid + '_locationid').getValue() : '';
        }
        if (Ext.isDefined(Ext.getCmp(parentid + '_location'))) {
          item.data.LocationName = Ext.getCmp(parentid + '_location').getValue() != null ? Ext.getCmp(parentid + '_location').getValue() : '';
        }
      }
      this.viewDetail(this.config._listid, item);
    }
  }

});
