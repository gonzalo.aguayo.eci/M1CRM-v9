Ext.define('M1CRM.controller.crmAllOpenQuotes', {
  extend: 'M1CRM.util.crmQuotesListControllerBase',
  config: {
    _listid: 'allopenquoteslist',
    _seardfieldid: 'allopenquotes_search',
    control: {
      allopenquotes: {
        initialize: 'initializeList',
        show: 'onShow',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.viewDetail('allopenquoteslist', item);
        },

        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },

  init: function () {
    this.control({
      '#allopenquoteslist_addnew': {
        scope: this,
        tap: this.addNewQuoteTap
      }
    });
  },

  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labAllOpenQuotesSummary';
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmQuotations', {}).getAllOpenQuoteLayout());

  },
  onShow: function (obj, e) {
    this.initilize();
    this.createOptionsMenu();
    this.loadData(obj);

  },
  createOptionsMenu: function () {
    Ext.Viewport.setMenu(this.createMenu(this.getOptionMenuItems(this)), {
      side: 'right',
      reveal: true
    });
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings('allopenquoteslist', parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  getDataObject: function () {
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {});
    return dataObj;
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmQuotations', this.getDataObject());
  },
  addNewQuoteTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getAddNewQuote();
    view.config._parentformid = 'allopenquoteslist';
    Ext.getCmp('addnewquote').setRecord(null);
    M1CRM.util.crmViews.slideLeft(view);
  }
});
