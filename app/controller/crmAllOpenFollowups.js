Ext.define('M1CRM.controller.crmAllOpenFollowups', {
  extend: 'M1CRM.util.crmFollowupListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmFollowups',
    'M1CRM.store.crmFollowups'
  ],
  config: {
    _listid: 'allopenfollowuplist',
    _seardfieldid: 'allopenfollowup_search',
    control: {
      allopenfollowups: {
        initialize: 'initializeList',
        show: 'onShow',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.viewDetail('allopenfollowuplist', item);
        },
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },

  init: function () {
    this.control({
      '#allopenfollowuplist_addnew': {
        scope: this,
        tap: function () {
          Ext.Viewport.getActiveItem().setRecord(null);
          this.addNewFollowupTap();
        }
      }
    });
  },

  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labAllOpenFollowupsSummary';
    this.createOptionMenu();
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmFollowups', {}).getMyFollowupListLayout());
  },
  createOptionMenu: function () {
    Ext.Viewport.setMenu(this.createMenu(this.getOptionMenuItems(this)), {
      side: 'right',
      reveal: true
    });
  },

  onShow: function (obj, e) {
    this.initilize();
    //this.createOptionMenu();
    this.doUserSecurityCheck(this, obj);
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
      UserID: SessionObj.statics.EmpId
    });
    this.loadDatafromDB('crmFollowups', dataObj);
  }

});
