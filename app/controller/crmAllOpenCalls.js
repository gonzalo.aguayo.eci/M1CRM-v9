Ext.define('M1CRM.controller.crmAllOpenCalls', {
  extend: 'M1CRM.util.crmCallsListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmCalls',
    'M1CRM.store.crmCalls'
  ],
  config: {
    _listid: 'allopencallslist',
    _seardfieldid: 'allopencalls_search',
    control: {
      allopencalls: {
        initialize: 'initializeList',
        show: 'onShow',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.viewDetail('allopencallslist', item);
        },
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },

  init: function () {
    this.control({
      '#allopencallslist_addnew': {
        scope: this,
        tap: function () {
          Ext.Viewport.getActiveItem().setRecord(null);
          this.addNewCallTap();
        }
      }
    });
  },

  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labAllOpenCallSummary';

    Ext.getCmp('allopencallslist').config._parentformid = 'myopencallslist';
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmCalls', {}).getMyOpenCallsListLayout());
  },
  createOptionMenu: function () {
    var items = this.getOptionMenuItems(this);
    Ext.Viewport.setMenu(this.createMenu(items), {
      side: 'right',
      reveal: true
    });
  },
  onShow: function (obj, e) {
    this.initilize();
    this.createOptionMenu();
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmCalls', Ext.create('M1CRM.model.crmDataFilterModel', {
      AssignedToEmployeeID: SessionObj.statics.EmpId
    }));
  },
  optionsAddNewTap: function () {
    this.hideOptionsMenu();
    var view = M1CRM.util.crmViews.getAddEditCall();
    view.config._parentformid = this.config._listid;
    M1CRM.util.crmViews.slideLeft(view);
  }

});
