Ext.define('M1CRM.controller.crmLeadsCloseThisMonth', {
  extend: 'M1CRM.util.crmLeadsListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmLeads',
    'M1CRM.store.crmLeads',
    'M1CRM.store.crmLeadsCloseByMonth'
  ],
  config: {
    _listid: 'leadsclosethismonthlist',
    _seardfieldid: 'leadsclosethismonth_search',
    control: {
      leadsclosethismonth: {
        initialize: 'initializeList',
        show: 'onShow',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.viewDetail('leadsclosethismonthlist', item);
        },

        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },

  init: function () {
    this.control({


      '#leadsclosethismonthlist_addnew': {
        cope: this,
        tap: function () {
          Ext.Viewport.getActiveItem().setRecord(null);
          this.addNewLeadsTap();
        }
      }

    });
  },

  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labLeadsCloseThisMonthSummary';
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmLeads', {}).getOpenLeadsLayout());

  },
  onShow: function (obj, e) {
    this.initilize();
    this.createOptionMenu();
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  createOptionMenu: function () {
    Ext.Viewport.setMenu(this.createMenu(this.getOptionMenuItems(this)), {
      side: 'right',
      reveal: true
    });
  },
  getDataObject: function () {
    Ext.getCmp(this.config._listid).setGrouped(true);
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {});
    return dataObj;
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmLeads', this.getDataObject(), 'crmLeadsCloseByMonth');
  }
});
