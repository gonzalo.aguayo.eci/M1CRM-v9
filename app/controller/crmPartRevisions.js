Ext.define('M1CRM.controller.crmPartRevisions', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmPartRevision',
    'M1CRM.store.crmPartRevisions'
  ],
  config: {
    _listid: 'partrevisionlist',
    _seardfieldid: 'partrevision_search',
    control: {
      partrevision: {
        show: 'onShow',
        itemtap: 'onPartsTap',
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },
  onShow: function (obj, e) {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labPartRevisionSummary';
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmPartRevision', {}).getListLayout());
    this.initilize();
    this.loadData(obj);

  },
  loadData: function (obj) {
    //this.doUserSecurityCheck(this, obj);
    this.loadFromDB();
  },
  getDataObject: function () {
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
      PartID: this.getParentFormFieldValue('_partid'),
      ParentView: this.getParetViewId()
    });
    return dataObj;
  },
  loadDataAfterSecurityCheck: function (obj, pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmPartRevisions', this.getDataObject());
  },
  onPartsTap: function (nestedlist, list, index, item, rec, e) {
    var view = null;
    if (Ext.getCmp(this.config._listid).config._parentformid != null) {
      if (Ext.getCmp(this.config._listid).config._parentformid == 'crmhome') {
        view = M1CRM.util.crmViews.getPartDetail();
        view.config._parentformid = this.config._listid;
        view.setRecord(item);
        M1CRM.util.crmViews.slideLeft(view);
      } else {
        view = M1CRM.util.crmViews.getViewByParentFormId(Ext.getCmp(this.config._listid).config._parentformid);
        view.setRecord(item);
        M1CRM.util.crmViews.slideRight(view);
      }
    }
    else {
      view = M1CRM.util.crmViews.getPartDetail();
      view.setRecord(item);
      M1CRM.util.crmViews.slideLeft(view);
    }
  }

});
