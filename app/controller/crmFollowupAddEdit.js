Ext.define('Ext.overrides.field.Select', {
  override: 'Ext.field.Select',

  updateStore: function (newStore) {
    if (newStore) {
      this.onStoreDataChanged(newStore);
    }

    if (this.getUsePicker() && this.picker) {
      this.picker.down('pickerslot').setStore(newStore);
    } else if (this.listPanel) {
      this.listPanel.down('dataview').setStore(newStore);
    }
  }
});


Ext.define('M1CRM.controller.crmFollowupAddEdit', {
  extend: 'M1CRM.util.crmFormControllerBase', //crmControllerBase',
  requirs: [
    'M1CRM.model.crmFollowupTask',
    'M1CRM.store.crmFollowupTask',
    'M1CRM.model.crmPriority',
    'M1CRM.store.crmPriority',
    'M1CRM.model.crmFollowupStatus',
    'M1CRM.store.crmFollowupStatus',
    'M1CRM.model.crmEmployee',
    'M1CRM.store.crmEmployee',
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmCustomerFollowupAddEdit'
  ],
  config: {
    _followupid: '',
    _viewmodeTitle: 'Follow-up',
    _addmodeTitle: 'Add Follow-up',
    _editmodeTitle: 'Edit Follow-up',
    refs: {
      'form': '#followupaddedit',
      'toolbarid': '#followupaddedit_toolbarid',
      'addnewbtn': '#followupaddedit_addnew',
      'cancelbtn': '#followupaddedit_cancelbtn',
      'savebtn': '#followupaddedit_savebtn',
      'editbtn': '#followupaddedit_editbtn',
      'subtitle': '#followupaddedittitleid',
      'lookupcustomer': '#followupaddedit_customer_lookup',
      'lookuplocation': '#followupaddedit_location_lookup',
      'tohome': '#followupaddedit_home',
      'btntoAllFollowups': '#btnFollowupAddEdit_ToAllFollowups',
      'followuptypesel': '#followup_typesel',
      'followuptype': '#followup_type',
      'followupcontact': '#followupaddedit_contact',
      'followupprioritysel': '#followup_prioritysel',
      'followuppriority': '#followup_priority',
      'followupstatussel': '#followup_statussel',
      'followupstatus': '#followup_status',
      'followupassigntosel': '#followup_assigntosel',
      'followupassignto': '#followup_assignto',
      'followupstartdatesel': '#followup_startdatesel',
      'followupstartdate': '#followup_startdate',
      'followupduedatesel': '#followup_duedatesel',
      'followupduedate': '#followup_duedate',
      'formOptionMenu': '#followupaddedit_options',
      'callinfo': '#followup_callinfo',
      'quoteinfo': '#followup_quoteinfo',
      'leadinfo': '#followup_leadinfo',
      'followupcustomerid': '#followupaddedit_custid',
      'followupcustomerlocationid': '#followupaddedit_locationid',
      'followupcontactid': '#followupaddedit_contactid',
      'customer': '#followupaddedit_customer',
      'location': '#followupaddedit_location',
      'notes': '#followupaddedit_notes',
      'followupdesc': '#followupaddedit_desc',
      'meetinglocation': '#followupaddedit_meetinglocation'
    },
    control: {
      form: {
        initialize: 'initializeForm',
        show: 'showForm',
        hide: function () {
          this.scrollFormToTop();
        }
      },
      lookuplocation: {
        tap: 'lookuplocationTap'
      },
      followupstatussel: {
        change: 'statusSelChange'
      },
      cancelbtn: {
        tap: function () {
          this.btnCancelTap();
          this.scrollFormToTop();
        }
      },
      btntoAllFollowups: {
        tap: function () {
          this.toAllFollowupsTap();
        }
      },
      followuptypesel: {
        change: 'followuptypeChange'
      },
      followupprioritysel: {
        change: 'followuppriorityChange'
      },
      followupassigntosel: {
        change: 'followupassigntoChange'
      },
      followupstartdatesel: {
        change: 'followupstartdateChange'
      },
      followupduedatesel: {
        change: 'followupduedateChange'
      },
      callinfo: {
        tap: 'callinfoTap'
      },
      quoteinfo: {
        tap: 'quoteinfoTap'
      },
      leadinfo: {
        tap: 'leadinfoTap'
      },
      addnewbtn: {
        tap: 'btnAddNewTap'
      },
      formOptionMenu: {
        tap: 'formOptionMenuTap'
      },
      lookupcustomer: {
        tap: 'lookupcustomerTap'
      }
    }
  },

  initializeForm: function () {
    this.formInitilize();
    this.initFields();
    this.loadFormProperties();
  },
  initFields: function () {
    this.getFollowupdesc().setMaxLength(50);
    this.getMeetinglocation().setMaxLength(50);
  },
  loadFormProperties: function () {
    Ext.create('M1CRM.model.crmCustomerFollowupAddEdit', {}).getData({
      RequestID: 'prop',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {

          this.getFollowuptypesel().suspendEvents();
          this.getFollowuptypesel().setStore(Ext.create('M1CRM.model.crmFollowupTask').getStoreFromValueList(result.ResultObject.FollowupTask));
          this.getFollowuptypesel().setValue(2);
          this.getFollowuptypesel().isDirty = false;
          this.getFollowuptypesel().resumeEvents(true);

          Ext.getCmp('followup_assigntosel').suspendEvents();
          Ext.getCmp('followup_assigntosel').setStore(this.getStoreFromListForDropDown(result.ResultObject.EmployeeList, 'crmEmployee', true));
          Ext.getCmp('followup_assigntosel').setValue(SessionObj.getEmpId());
          Ext.getCmp('followup_assigntosel').resumeEvents(true);

          this.getFollowupprioritysel().suspendEvents();
          this.getFollowupprioritysel().setStore(this.getStoreFromListForDropDown(result.ResultObject.PriorityList, 'crmPriority', true));
          this.getFollowupprioritysel().setValue("");
          this.getFollowupprioritysel().isDirty = false;
          this.getFollowupprioritysel().resumeEvents(true);

          this.getFollowupstatussel().suspendEvents();
          this.getFollowupstatussel().setStore(Ext.create('M1CRM.model.crmFollowupStatus').getStoreFromValueList(result.ResultObject.FollowupStatus));
          this.getFollowupstatussel().resumeEvents(true);
        }
      }
    });
  },

  getFormButtons: function () {
    return [this.getCancelbtn().getId(), this.getSavebtn().getId()];
  },
  getEditableFormFields: function () {
    return ['followup_typesel', this.getCustomer().id, this.getLocation().id, this.getFollowupcontact().id,
      this.getFollowupprioritysel().id, this.getFollowupstatussel().id, 'followup_assigntosel',
      'followupaddedit_desc', 'followupaddedit_meetinglocation', this.getNotes().id
    ]
  },
  showForm: function () {
    if (SessionObj.getFollowupAddEditTSecurity() == null || SessionObj.getFollowupAddEditTSecurity() == '') {
      Ext.create('M1CRM.model.crmUserSecurity', {}).getUserSecurityLevel({
        ID: 'CRMFOLLOWUPADDEDIT',
        scope: this,
        onReturn: function (rec) {
          if (rec != null) {
            SessionObj.setFollowupAddEditTSecurity(rec.PAccessLevel + ',' + rec.SAccessLevel);
            if (this.applyUserSecuritySettings(this.getForm().id, parseInt(rec.PAccessLevel), parseInt(rec.SAccessLevel),
                this.getFormButtons())) {
              this.processShowForm();
            }
          } else {
            this.toBackTap(this.getForm().id);
          }
        }
      });
    } else {
      if (this.applyUserSecuritySettings(this.getForm().id,
          parseInt(this.getPKeyFromSessionObj(SessionObj.getFollowupAddEditTSecurity())), parseInt(this.getSKeyFromSessionObj(SessionObj.getFollowupAddEditTSecurity())), this.getFormButtons())) {
        this.processShowForm();
      } else {
        this.toBackTap(this.getForm().id);
      }

    }
  },
  processShowForm: function () {
    if (Ext.getCmp('followupaddedit').config._parentformid != 'myfollowuplist' && Ext.getCmp('followupaddedit').config._parentformid != 'customerfollowupslist' &&
      Ext.getCmp('followupaddedit').config._parentformid != 'allfollowuplist' && Ext.getCmp('followupaddedit').config._parentformid != 'crmhome') {
      this.makeFormReadOnly(false);
      this.makeFormAsNewEntry();
    } else {
      this.makeFormReadOnly(true);
    }
    this.resetEditableForms(this.getEditableFormFields());
    this.loadData();
  },
  loadData: function () {
    var mode = Ext.getCmp('followupaddedit').config._mode;
    if (mode == 'view') {
      this.setEntryFormMode('view');
      if (Ext.getCmp('followupaddedit').getRecord() != null) {
        var recObj = this.config._orgrec == null ? Ext.getCmp('followupaddedit').getRecord().getData() : this.config._orgrec;
        if (recObj.FollowupID != null) {
          this.config._followupid = recObj.FollowupID;
          this.config._orgrec = recObj;
          this.getSubtitle().setHtml('ID:' + recObj.FollowupID);
        } else {
          if (this.config._followupid != null) {
            this.getSubtitle().setHtml('ID:' + this.config._followupid);
          }
        }
        Ext.getCmp('followupid').setValue(recObj.FollowupID);
        this.populateFields(recObj);
        this.makeFormReadOnly(true);
        this.hideorshowContactField(this.getFollowupcontact(), false);
      }
    } else if (mode == 'edit') {
      this.setEntryFormMode('edit');
      if (Ext.getCmp('followupaddedit').getRecord() != null) {
        var recObj = Ext.getCmp('followupaddedit').getRecord().getData();
        var orgid = recObj.OrganizationID;
        var orgname = recObj.OrganizationName;

        if (orgid != null && orgid != this.getFollowupcustomerid().getValue()) {
          this.getFollowupcustomerid().setValue(orgid);
          this.getCustomer().setValue(orgname);
          this.getCustomer().isDirty = true;
        }
        if (this.getCustomer().isDirty) {
          this.getFollowupcustomerlocationid().setValue('');
          this.getLocation().setValue('');
          this.getLocation().isDirty = true;
        }
        var locid = recObj.LocationID
        var locname = recObj.LocationName;

        if (locid != null && locid != '' && this.getFollowupcustomerlocationid().getValue() != locid) {
          this.getFollowupcustomerlocationid().setValue(locid);
          this.getLocation().setValue(locname);
          this.getLocation().isDirty = true;
        }
        if (this.getCustomer().isDirty || this.getLocation().isDirty) {
          this.getFollowupcontact().setValue('');
          this.getFollowupcontactid().setValue('');
          this.getFollowupcontact().isDirty = true;
        }
        if (recObj.ContactID != null) {
          this.getFollowupcontact().setValue(recObj.ContactName);
          this.getFollowupcontactid().setValue(recObj.ContactID);
          this.getFollowupcontact().isDirty = true;
        }
        this.makeFormReadOnly(false);
        this.hideorshowContactField(this.getFollowupcontact(), true);
      }
    } else if (mode == 'addnew') {
      this.setEntryFormMode('addnew');
      this.clearFormForNewEntry();
      if (Ext.getCmp(this.getForm().id).getRecord() != null) {
        this.makeFormReadOnly(false);
        var recObj = Ext.getCmp('followupaddedit').getRecord().getData();

        this.getFollowupcustomerid().setValue(recObj.OrganizationID);
        this.getCustomer().setValue(recObj.OrganizationName);
        this.getCustomer().isDirty = true;
        this.manageLocationAndContactLookup();

        if (recObj.LocationID != null) {
          this.getFollowupcustomerlocationid().setValue(recObj.LocationID);
          this.getLocation().setValue(recObj.LocationName);
          this.getLocation().isDirty = true;
        }
        Ext.getCmp('followupaddedit_callid').setValue(recObj.CallID);

        this.getLookuplocation().show();
        Ext.getCmp('followupaddedit_location_info').hide();
        if (recObj.ContactID != null) {
          this.getFollowupcontact().setValue(recObj.Name);
          this.getFollowupcontactid().setValue(recObj.ContactID);
          this.getFollowupcontact().isDirty = true;
        } else {
          this.getFollowupcontactid().setValue('');
          this.getFollowupcontact().setValue('');
        }
        this.hideorshowContactField(this.getFollowupcontact(), true);
        this.getLookupcustomer().enable();

        if (recObj.CallID != null && recObj.CallID != '') {
          Ext.getCmp('followup_callid').setValue(recObj.CallID);
        } else {
          Ext.getCmp('followup_callid').setValue('');
        }

        if (recObj.QuoteID != null && recObj.QuoteID != '') {
          Ext.getCmp('followup_quoteid').setValue(recObj.QuoteID);
        } else {
          Ext.getCmp('followup_quoteid').setValue('');
        }

        if (recObj.LeadID != null && recObj.LeadID != '') {
          Ext.getCmp('followup_leadid').setValue(recObj.LeadID);
          Ext.getCmp('followup_leadinfo').enable();
          this.getLookupcustomer().disable();
          this.getLookuplocation().disable();
          Ext.getCmp(this.getFollowupcontact().id + '_lookupbtn').disable();
        } else {
          Ext.getCmp('followup_leadid').setValue('');
          Ext.getCmp('followup_leadinfo').disable();
          this.getLookupcustomer().enable();
          this.getLookuplocation().enable();
          Ext.getCmp(this.getFollowupcontact().id + '_lookupbtn').enable();
        }
      } else {
        this.setEntryFormMode('addnew');
        this.makeFormAsNewEntry(true);
        Ext.getCmp('followupaddedit_callid').setValue('');
        this.hideorshowContactField(this.getFollowupcontact(), true);
      }
    }
  },
  manageLocationAndContactLookup: function () {
    if (this.getFollowupcustomerid().getValue() != null || this.getFollowupcustomerid().getValue() != '') {
      this.getLookuplocation().enable();
      this.disableorenableCallLookup(this.getFollowupcontact(), false);
    } else {
      this.getLookuplocation().disable();
      this.disableorenableCallLookup(this.getFollowupcontact(), true);
    }
  },
  makeFormAsNewEntry: function () {
    this.makeFormReadOnly(false);
    this.getFollowuptypesel().enable();
    this.getFollowuptypesel().suspendEvents();
    this.getFollowuptypesel().setValue(2);
    this.getFollowuptypesel().resumeEvents(true);
    this.followuptypeChange();
    this.getFollowupcustomerid().setValue('');
    this.getCustomer().setValue('');
    this.getFollowupcustomerlocationid().setValue('');
    this.getLocation().setValue('');
    this.getLookupcustomer().show();
    Ext.getCmp('followupaddedit_customer_info').hide();
    this.getLookuplocation().show();
    Ext.getCmp('followupaddedit_location_info').hide();
    this.getFollowupstartdatesel().setValue(new Date());
    this.getFollowupduedatesel().setValue(new Date());
    this.getFollowupprioritysel().suspendEvents();
    this.getFollowupprioritysel().setValue("");
    this.getFollowupprioritysel().resumeEvents(true);
    this.getFollowupstatussel().suspendEvents();
    this.getFollowupstatussel().setValue(1);
    this.getFollowupstatussel().resumeEvents(true);
    this.statusSelChange();
    this.getFollowupcontactid().setValue('');
    this.getFollowupcontact().setValue('');
    this.getLookuplocation().disable();
    this.disableorenableCallLookup(this.getFollowupcontact(), true);
    this.resetEditableForms(this.getEditableFormFields());
  },
  populateFields: function (recObj) {
    this.getFollowupcustomerid().setValue(recObj.OrganizationID);
    this.getCustomer().setValue(recObj.OrganizationName);
    this.getFollowupcustomerlocationid().setValue(recObj.LocationID);
    if (this.getFollowupcustomerlocationid().getValue() != null && this.getFollowupcustomerlocationid().getValue() != '') {
      Ext.getCmp('followupaddedit_location_info').enable();
      this.getLocation().setValue(recObj.LocationName);
    } else {
      Ext.getCmp('followupaddedit_location_info').disable();
      this.getLocation().setValue('');
    }
    this.getFollowupcontactid().setValue(recObj.ContactID);
    this.getFollowupcontact().setValue(recObj.ContactName);
    var taskid = recObj.FollowupType != null ? parseInt(recObj.FollowupType) : 1;
    if (taskid == 1) {
      var startDate = recObj.StartDate != null ? new Date(recObj.StartDate) : new Date()
      Ext.getCmp('followup_startdatesel').setValue(startDate);
      Ext.getCmp('followup_startdate').setValue(formatDate(startDate, SessionObj.getDisplayDateTime()));
    } else {
      Ext.getCmp('followup_startdatesel').setValue(new Date());
      Ext.getCmp('followup_startdate').setValue(formatDate(new Date(), SessionObj.getDisplayDateTime()));
    }
    var dueDate = recObj.DueDate != null ? new Date(recObj.DueDate) : new Date();
    this.getFollowupduedatesel().setValue(dueDate);
    Ext.getCmp('followup_duedate').setValue(formatDate(dueDate, SessionObj.getDisplayDateTime()));
    var tempEmpid = recObj.AssignedToEmployeeID != null ? recObj.AssignedToEmployeeID : SessionObj.getEmpId();
    this.setAssignToInfo(tempEmpid);
    this.getFollowupdesc().setValue(recObj.ShortDescription);
    if (recObj.FollowupID != null)
      this.getFollowupDetails(recObj);
  },
  setAssignToInfo: function (tempEmpid) {
    this.getFollowupassigntosel().setValue(tempEmpid);
    if (this.getFollowupassigntosel().getStore() != null && this.getFollowupassigntosel().getStore().getData() != null &&
      this.getFollowupassigntosel().getStore().getData().items != null && this.getFollowupassigntosel().getStore().getData().items.length > 0) {
      this.getFollowupassignto().setValue(this.getSelectFieldDisplayValue(this.getFollowupassigntosel()));
    } else {
      Ext.create('M1CRM.model.crmEmployee', {}).getActiveEmployees({
        scope: this,
        onReturn: function (result) {
          if (result != null) {
            this.getFollowupassigntosel().setStore(result);
            this.getFollowupassigntosel().setValue(SessionObj.getEmpId());
            this.getFollowupassignto().setValue(this.getSelectFieldDisplayValue(this.getFollowupassigntosel()));
          }
        }
      });
    }
    this.getFollowupassigntosel().setValue(tempEmpid);
  },
  getFollowupDetails: function (recObj) {

    this.getFollowuptypesel().setValue(recObj.FollowupType);
    if (this.getFollowuptypesel().getStore() != null && this.getFollowuptypesel().getStore().getData() != null &&
      this.getFollowuptypesel().getStore().getData().items != null && this.getFollowuptypesel().getStore().getData().items.length > 0) {
      this.getFollowuptype().setValue(this.getSelectFieldDisplayValue(this.getFollowuptypesel()));
    }
    Ext.create('M1CRM.model.crmCustomerFollowupAddEdit', {
      FollowupID: recObj.FollowupID
    }).getData({
      RequestID: 'data',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          this.getFollowupprioritysel().setValue(result.ResultObject.Priority);
          if (this.getFollowupprioritysel().getStore() != null && this.getFollowupprioritysel().getStore().getData() != null &&
            this.getFollowupprioritysel().getStore().getData().items != null && this.getFollowupprioritysel().getStore().getData().items.length > 0) {
            this.getFollowuppriority().setValue(this.getSelectFieldDisplayValue(this.getFollowupprioritysel()));
          }

          this.getFollowupstatussel().setValue(result.ResultObject.Status);
          if (this.getFollowupstatussel().getStore() != null && this.getFollowupstatussel().getStore().getData() != null &&
            this.getFollowupstatussel().getStore().getData().items != null && this.getFollowupstatussel().getStore().getData().items.length > 0) {
            this.getFollowupstatus().setValue(this.getSelectFieldDisplayValue(this.getFollowupstatussel()));
          }
          if (parseInt(result.ResultObject.Status) == 3) {
            Ext.getCmp('followup_completeddatesel').setValue(this.changeDisplayDateToCorrectDateTime(result.ResultObject.CompletedDate));
            Ext.getCmp('followup_completeddate').setValue(formatDate(this.changeDisplayDateToCorrectDateTime(result.ResultObject.CompletedDate), SessionObj.getSimpleDateFormat()));
          } else {
            Ext.getCmp('followup_completeddatesel').setValue(new Date());
            Ext.getCmp('followup_completeddate').setValue(formatDate(new Date(), SessionObj.getSimpleDateFormat()));
          }

          this.getNotes().setValue(result.ResultObject.LongDescriptionText != null ? result.ResultObject.LongDescriptionText : '');

          this.getMeetinglocation().setValue(result.ResultObject.MeetingLocation);
          Ext.getCmp('followup_callid').setValue(result.ResultObject.CallID);
          if (result.ResultObject.CallID != null && result.ResultObject.CallID != '') {
            Ext.getCmp('followup_callinfo').enable();
          } else {
            Ext.getCmp('followup_callinfo').disable();
          }

          Ext.getCmp('followup_leadid').setValue(result.ResultObject.LeadID);
          if (result.ResultObject.LeadID != null && result.ResultObject.LeadID != '') {
            Ext.getCmp('followup_leadinfo').enable();
          } else {
            Ext.getCmp('followup_leadinfo').disable();
          }

          Ext.getCmp('followup_quoteid').setValue(result.ResultObject.QuoteID);
          if (result.ResultObject.QuoteID != null && result.ResultObject.QuoteID != '') {
            Ext.getCmp('followup_quoteinfo').enable();
          } else {
            Ext.getCmp('followup_quoteinfo').disable();
          }
          Ext.getCmp('followup_orderid').setValue(result.ResultObject.SalesOrderID);
        }
      }
    });
  },
  followuptypeChange: function () {
    if (Ext.getCmp('followup_typesel').getValue() == '1') {
      Ext.getCmp('followup_startdatesel').parent.show();
    } else {
      Ext.getCmp('followup_startdate').parent.hide();
    }
    this.getFollowuptype().setValue(this.getSelectFieldDisplayValue(this.getFollowuptypesel()));
  },
  followuppriorityChange: function () {
    this.getFollowuppriority().setValue(this.getSelectFieldDisplayValue(this.getFollowupprioritysel()));
  },
  followupassigntoChange: function () {
    this.getFollowupassignto().setValue(this.getSelectFieldDisplayValue(this.getFollowupassigntosel()));
  },
  followupstartdateChange: function () {
    this.getFollowupstartdate().setValue(formatDate(this.getFollowupstartdatesel().getValue(), SessionObj.getDisplayDateTime()));
  },
  followupduedateChange: function () {
    this.getFollowupduedate().setValue(formatDate(this.getFollowupduedatesel().getValue(), SessionObj.getDisplayDateTime()));
  },
  makeFormReadOnly: function (flag) {
    this.resetEditableForms(this.getEditableFormFields());
    this.pairControlsShowAndHide(flag, 'followup_type');
    this.pairControlsShowAndHide(flag, 'followup_startdate');
    this.pairControlsShowAndHide(flag, 'followup_duedate');
    this.pairControlsShowAndHide(flag, 'followup_priority');
    this.pairControlsShowAndHide(flag, 'followup_status');
    this.pairControlsShowAndHide(flag, 'followup_assignto');
    this.pairControlsShowAndHide(flag, 'followup_completeddate');
    this.getFollowupdesc().setReadOnly(flag);
    this.getMeetinglocation().setReadOnly(flag);
    this.getNotes().setReadOnly(flag);
    this.getLookupcustomer().setHidden(flag);
    ;
    Ext.getCmp('followupaddedit_customer_info').setHidden(!flag);
    this.getLookuplocation().setHidden(flag);
    Ext.getCmp('followupaddedit_location_info').setHidden(!flag);
    Ext.getCmp(this.getSavebtn().getId()).setHidden(flag);
    Ext.getCmp(this.getCancelbtn().getId()).setHidden(flag);
    this.getLookupcustomer().disable();
    if (!flag) {
      this.getLookupcustomer().enable();
      this.getLookuplocation().enable();
      Ext.getCmp('followupaddedit_contact_lookupbtn').enable();
      Ext.getCmp('followup_callinfo').disable();
      Ext.getCmp('followup_leadinfo').disable();
      Ext.getCmp('followup_quoteinfo').disable();
      Ext.getCmp('followup_orderinfo').disable();

    } else {
      Ext.getCmp('followup_callinfo').enable();
      Ext.getCmp('followup_leadinfo').enable();
      Ext.getCmp('followup_quoteinfo').enable();
      Ext.getCmp('followup_orderinfo').enable();
    }
  },
  getCustomerID: function () {
    var custId = null;
    var parentid = Ext.getCmp('followupaddedit').config._parentformid;
    if (parentid != null && ( parentid == 'customerfollowupslist' || parentid == 'myfollowuplist' || parentid == 'crmhome' || parentid == 'customercontactaddedit' ))
      custId = this.getFollowupcustomerid().getValue();
    else
      custId = Ext.getCmp('followupaddedit').getRecord().getData().cmfOrganizationID != null ? Ext.getCmp('followupaddedit').getRecord().getData().cmfOrganizationID : Ext.getCmp('followupaddedit').getRecord().getData().CustomerID;
    return custId;
  },
  getLocationID: function () {
    if (Ext.getCmp('followupaddedit').config._parentformid != null)
      return this.getLocation().getValue() != null ? this.getLocation().getValue() : '';
    else
      return Ext.getCmp('followupaddedit').getRecord().getData().cmfLocationID != null ? Ext.getCmp('followupaddedit').getRecord().getData().cmfLocationID : '';
  },
  getContactID: function () {
    if (Ext.getCmp('followupaddedit').config._parentformid != null)
      return this.getFollowupcontact().getValue() != null ? this.getFollowupcontact().getValue() : '';
    return Ext.getCmp('followupaddedit').getRecord().getData().cmcContactID != null ?
      Ext.getCmp('followupaddedit').getRecord().getData().cmcContactID : '';
  },
  getSaveObject: function () {
    return Ext.create('M1CRM.model.crmCustomerFollowupAddEdit', {
      FollowupID: Ext.getCmp('followupid').getValue(),
      FollowupType: parseInt(Ext.getCmp('followup_typesel').getValue()),
      OrganizationID: this.getFollowupcustomerid().getValue(),
      LocationID: this.getFollowupcustomerlocationid().getValue(),
      ContactID: this.getFollowupcontactid().getValue(),
      MeetingLocation: this.getMeetinglocation().getValue(),
      ShortDescription: this.getFollowupdesc().getValue(),
      LongDescriptionText: this.getNotes().getValue(),
      StartDate: Ext.getCmp('followup_startdatesel').getValue(),
      Duedate: this.getFollowupduedatesel().getValue(),
      AssignedToEmployeeID: Ext.getCmp('followup_assigntosel').getValue(),
      Status: parseInt(this.getFollowupstatussel().getValue()),
      CompletedDate: Ext.getCmp('followup_completeddatesel').getValue(),
      Priority: this.getFollowupprioritysel().getValue() != '' ? parseInt(this.getFollowupprioritysel().getValue()) : 0,
      CallID: Ext.getCmp('followupaddedit_callid').getValue() != null ? Ext.getCmp('followupaddedit_callid').getValue() : '',
      QuoteID: Ext.getCmp('followup_quoteid').getValue() != null ? Ext.getCmp('followup_quoteid').getValue() : '',
      LeadID: Ext.getCmp('followup_leadid').getValue() != null ? Ext.getCmp('followup_leadid').getValue() : ''

    });
  },
  btnSaveTap: function () {
    var saveObj = this.getSaveObject();
    var error = saveObj.validate();
    if (saveObj.isValid()) {

      saveObj.saveData({
        scope: this,
        onReturn: function (result) {
          if (result != null && result.SaveStatus == true) {
            this.makeFormReadOnly(true);
            this.setEntryFormMode('view');
            Ext.getCmp('followupaddedit').config._mode = 'view';
            this.hideorshowContactField(this.getFollowupcontact(), false);
            if (Ext.getCmp('followupid').getValue() != result.ResultObject.FollowupID) {
              Ext.getCmp('followupid').setValue(result.ResultObject.FollowupID);
              this.getSubtitle().setHtml('ID:' + result.ResultObject.FollowupID);
              this.config._followupid = result.ResultObject.FollowupID;
              this.hideorshowContactField(this.getFollowupcontact(), true);
            }
          } else {
            this.setEntryFormMode('addnew');
            Ext.Msg.alert('Error', 'Error saving followup');
          }
        }
      });
    } else {
      this.displayValidationMessage(error);
    }
  },
  processAutoSave: function (type) {
    if (Ext.getCmp(this.getForm().id).config._mode != 'view' && this.isFormDirty(this.getEditableFormFields())) {
      var saveObj = this.getSaveObject();
      saveObj.validate();
      if (saveObj.isValid()) {
        saveObj.saveData({
          scope: this,
          onReturn: function (result) {
            if (result.SaveStatus == true) {
              this.btnCancelTap();
              this.config._followupid = '';
              this.config._orgrec = null;
              this.processAfterAutoSave(type, this.getForm().id);
            } else {
              this.displaySaveErrorMessage('follow up');
            }
          }
        });
      } else {
        Ext.Msg.confirm("", this.getRecordInCompleteMessage(), function (btn, type) {
          if (btn == 'yes') {
            this.btnCancelTap();
            this.config._followupid = '';
            this.config._orgrec = null;
            this.processAfterAutoSave(type, this.getForm().id);
          }
        }, this);
      }
    }
    else {
      Ext.getCmp('followupaddedit').config._mode = 'view';
      this.btnCancelTap();
      this.config._orgrec = null;
      this.processAfterAutoSave(type, this.getForm().id)
    }
  },
  btnEditTap: function () {
    this.makeFormReadOnly(false);
    Ext.getCmp(this.getForm().id).config._mode = 'edit';
    this.hideorshowContactField(this.getFollowupcontact(), true);

    if (Ext.getCmp('followup_leadid') != null && Ext.getCmp('followup_leadid') != '') {
      this.getLookupcustomer().disable();
      this.getLookuplocation().disable();
      Ext.getCmp(this.getFollowupcontact().id + '_lookupbtn').disable();
    } else {
      this.getLookupcustomer().enable();
      this.getLookuplocation().enable();
      Ext.getCmp(this.getFollowupcontact().id + '_lookupbtn').enable();
    }

  },
  btnCancelTap: function () {
    if (Ext.getCmp('followupid').getValue() != '') {
      Ext.getCmp('followupaddedit').config._mode = 'view';
      this.setEntryFormMode('view');
      this.makeFormReadOnly(true);
      this.hideorshowContactField(this.getFollowupcontact(), false);
      this.loadData();
    } else {
      this.makeFormAsNewEntry();
    }
  },
  getLocationID: function () {
    return this.getFollowupcustomerlocationid().getValue();
  },
  getLoacationOrContactValue: function (fieldid) {
    if (!Ext.getCmp(fieldid + 'sel').isHidden() && Ext.getCmp(fieldid + 'sel') != '')
      return Ext.getCmp(fieldid + 'sel').getValue() != null ? Ext.getCmp(fieldid + 'sel').getValue() : '';
    else if (!Ext.getCmp(fieldid).isHidden())
      return fieldid == 'followup_location' ? Ext.getCmp('followupaddedit').getRecord().getData().cmfLocationID : Ext.getCmp('followupaddedit').getRecord().getData().cmcContactID;
    else
      return null;
  },
  btnAddNewTap: function () {
    this.getForm().setRecord(null);
    this.makeFormReadOnly(false);
    this.resetEditableForms([this.getCustomer().id]);
    this.clearFormForNewEntry();
    this.getLookupcustomer().enable();
    Ext.getCmp('followupaddedit').config._mode = 'addnew';
    this.setEntryFormMode('addnew');
    this.hideorshowContactField(this.getFollowupcontact(), true);
    this.manageLocationAndContactLookup();
    this.getFollowupstartdatesel().setValue(new Date());
    this.getFollowupduedatesel().setValue(new Date());
    this.getFollowupprioritysel().setValue("");
    this.getFollowupstatussel().setValue(1);
    this.scrollFormToTop();
  },
  clearFormForNewEntry: function () {
    this.getSubtitle().setHtml('');
    this.getLookupcustomer().show();
    Ext.getCmp('followupid').setValue('');
    this.getFollowupdesc().setValue('');
    this.getMeetinglocation().setValue('');
    this.getNotes().setValue('');
    Ext.getCmp('followup_callid').setValue('');
    Ext.getCmp('followup_leadid').setValue('');
    Ext.getCmp('followup_quoteid').setValue('');
    Ext.getCmp('followup_orderid').setValue('');
  },
  statusSelChange: function () {
    if (this.getFollowupstatussel().getValue() == 3 && !this.getFollowupstatussel().isHidden()) {
      Ext.getCmp('followup_completeddatesel').parent.show();
      Ext.getCmp('followup_completeddatesel').show();
      Ext.getCmp('followup_completeddate').hide();
    } else if (this.getFollowupstatussel().getValue() != 3) {
      Ext.getCmp('followup_completeddatesel').parent.hide();
      Ext.getCmp('followup_completeddate').hide();
    } else {
      Ext.getCmp('followup_completeddatesel').parent.show();
      Ext.getCmp('followup_completeddate').show();
    }
    this.getFollowupstatus().setValue(this.getSelectFieldDisplayValue(this.getFollowupstatussel()));
  },
  lookuplocationTap: function () {
    var view = M1CRM.util.crmViews.getCustomerLocations();
    view.config._parentformid = this.getForm().id;
    this.slideLeft(view);
  },
  lookupcustomerTap: function () {
    var view = M1CRM.util.crmViews.getCustomers();
    view.config._parentformid = this.getForm().id;
    this.slideLeft(view);
  },
  callinfoTap: function () {
    var view = M1CRM.util.crmViews.getAddEditCall();
    view.config._parentformid = this.getForm().id;
    view.setRecord(Ext.create('M1CRM.model.crmCalls', {
      kbpCallID: Ext.getCmp('followup_callid').getValue()
    }));
    this.slideLeft(view);
  },
  toAllFollowupsTap: function () {
    var view = M1CRM.util.crmViews.getAllFollowups();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = 'followupdetail';
    M1CRM.util.crmViews.slideLeft(view);
  },
  quoteinfoTap: function () {
    var view = M1CRM.util.crmViews.getAddNewQuote();
    view.config._parentformid = this.getForm().id;
    view.setRecord(Ext.create('M1CRM.model.crmQuotations', {
      QuoteID: Ext.getCmp('followup_quoteid').getValue(),
      OrganizationID: this.getFollowupcustomerid().getValue(),
      OrganizationName: this.getCustomer().getValue()
    }));
    this.slideLeft(view);
  },
  leadinfoTap: function () {
    var view = M1CRM.util.crmViews.getAddEditLead();
    view.config._parentformid = this.getForm().id;
    view.setRecord(Ext.create('M1CRM.model.crmLeads', {
      LeadID: Ext.getCmp('followup_quoteid').getValue(),
      OrganizationID: this.getFollowupcustomerid().getValue(),
      OrganizationName: this.getCustomer().getValue()
    }));
    this.slideLeft(view);
  }

});
