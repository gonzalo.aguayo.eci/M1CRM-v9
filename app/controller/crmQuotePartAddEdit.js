Ext.define('M1CRM.controller.crmQuotePartAddEdit', {
  extend: 'M1CRM.util.crmFormControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmQuotePartQty',
    'M1CRM.store.crmQuotePartQty',
    'M1CRM.model.crmPartRevision',
    'M1CRM.model.crmAddEditQuotation',
    'M1CRM.model.crmQuotePartAddEdit'
  ],
  config: {
    _viewmodeTitle: 'Quote Line',
    _addmodeTitle: 'Add Quote Line',
    _editmodeTitle: 'Edit Quote Line',
    _taxrate: 0,
    _secondtaxrate: 0,
    _propNonTaxReasonID: '',
    _propPartsMustExist: false,
    _tmpitemCount: 0,
    refs: {
      'form': '#addeditquotepart',
      'toolbarid': '#addeditquotepart_toolbarid',
      'addnewbtn': '#addeditquotepart_addnew',
      'cancelbtn': '#addeditquotepart_cancelbtn',
      'savebtn': '#addeditquotepart_savebtn',
      'editbtn': '#addeditquotepart_editbtn',
      'subtitle': '#addeditquotepartsubtitle',
      'quoteid': '#addeditquotepart_quoteid',
      'quotelineid': '#addeditquotepart_quotelineid',
      'partid': '#addeditquotepart_partid',
      'partlookup': '#addeditquotepart_partid_lookup',
      'partrevision': '#addeditquotepart_partrevision',
      'partinfo': '#addeditquotepart_partinfo',
      'partdesc': '#addeditquotepart_partdesc',
      'uom': '#addeditquotepart_uom',
      'firm': '#addeditquotepart_firm',
      'taxid': '#addeditquotepart_taxid',
      'taxidsel': '#addeditquotepart_taxidsel',
      'taxid2': '#addeditquotepart_taxid2',
      'taxid2sel': '#addeditquotepart_taxid2sel',
      'nontaxreasonid': '#addeditquotepart_nontaxreasonid',
      'nontaxreason': '#addeditquotepart_nontaxreasonidsel',
      'taxinfofieldset': '#addeditquotepart_taxinfofieldset',
      'quoteqty1': '#addeditquotepartqty_quoteqty1',
      'quoteqty2': '#addeditquotepartqty_quoteqty2',
      'quoteqty3': '#addeditquotepartqty_quoteqty3',
      'quoteqty4': '#addeditquotepartqty_quoteqty4',
      'quoteqty5': '#addeditquotepartqty_quoteqty5',
      'quoteqty6': '#addeditquotepartqty_quoteqty6',
      'quoteqty7': '#addeditquotepartqty_quoteqty7',
      'quoteqty8': '#addeditquotepartqty_quoteqty8',
      'quoteqty9': '#addeditquotepartqty_quoteqty9',
      'discount1': '#addeditquotepartqty_discount1',
      'discount2': '#addeditquotepartqty_discount2',
      'discount3': '#addeditquotepartqty_discount3',
      'discount4': '#addeditquotepartqty_discount4',
      'discount5': '#addeditquotepartqty_discount5',
      'discount6': '#addeditquotepartqty_discount6',
      'discount7': '#addeditquotepartqty_discount7',
      'discount8': '#addeditquotepartqty_discount8',
      'discount9': '#addeditquotepartqty_discount9',
      'unitdiscount1': '#addeditquotepartqty_unitdiscount1',
      'unitdiscount2': '#addeditquotepartqty_unitdiscount2',
      'unitdiscount3': '#addeditquotepartqty_unitdiscount3',
      'unitdiscount4': '#addeditquotepartqty_unitdiscount4',
      'unitdiscount5': '#addeditquotepartqty_unitdiscount5',
      'unitdiscount6': '#addeditquotepartqty_unitdiscount6',
      'unitdiscount7': '#addeditquotepartqty_unitdiscount7',
      'unitdiscount8': '#addeditquotepartqty_unitdiscount8',
      'unitdiscount9': '#addeditquotepartqty_unitdiscount9',
      'revisedunitprice1': '#addeditquotepart_revisedunitprice1',
      'revisedunitprice2': '#addeditquotepart_revisedunitprice2',
      'revisedunitprice3': '#addeditquotepart_revisedunitprice3',
      'revisedunitprice4': '#addeditquotepart_revisedunitprice4',
      'revisedunitprice5': '#addeditquotepart_revisedunitprice5',
      'revisedunitprice6': '#addeditquotepart_revisedunitprice6',
      'revisedunitprice7': '#addeditquotepart_revisedunitprice7',
      'revisedunitprice8': '#addeditquotepart_revisedunitprice8',
      'revisedunitprice9': '#addeditquotepart_revisedunitprice9',
      'unitprice1': '#addeditquotepartqty_unitprice1',
      'unitprice2': '#addeditquotepartqty_unitprice2',
      'unitprice3': '#addeditquotepartqty_unitprice3',
      'unitprice4': '#addeditquotepartqty_unitprice4',
      'unitprice5': '#addeditquotepartqty_unitprice5',
      'unitprice6': '#addeditquotepartqty_unitprice6',
      'unitprice7': '#addeditquotepartqty_unitprice7',
      'unitprice8': '#addeditquotepartqty_unitprice8',
      'unitprice9': '#addeditquotepartqty_unitprice9'
    },
    control: {
      form: {
        initialize: 'initializeForm',
        show: 'showForm',
        hide: function () {
          this.scrollFormToTop();
        }
      },
      partlookup: {
        tap: 'partLookupTap'
      },
      formOptionsMenu: {
        tap: 'formOptionsMenuTap'
      },

      partid: {
        change: 'checkPartID',
        blur: 'checkPartID',
        clearicontap: 'newQuoteLineTap'
      },
      newQuoteLine: {
        tap: 'newQuoteLineTap'
      },
      editQuoteLine: {
        tap: 'editQuoteLineTap'
      },
      cancelbtn: {
        tap: 'btnCancelTap'
      },
      partinfo: {
        tap: 'partinfoTap'
      },
      taxidsel: {
        change: 'taxidselChange'
      },
      taxid2sel: {
        change: 'taxid2selChange'
      },
      nontaxreason: {
        change: 'nontaxreasonChange'
      },

      addnewbtn: {
        tap: 'btnAddnewTap'
      },
      quoteqty1: {
        keyup: function (key) {
          this.quoteQtyKeyIn(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.quoteQtyChange(parseInt(key.id.slice(-1)));
        }
      },
      quoteqty2: {
        keyup: function (key) {
          this.quoteQtyKeyIn(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.quoteQtyChange(parseInt(key.id.slice(-1)));
        }
      },
      quoteqty3: {
        keyup: function (key) {
          this.quoteQtyKeyIn(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.quoteQtyChange(parseInt(key.id.slice(-1)));
        }
      },
      quoteqty4: {
        keyup: function (key) {
          this.quoteQtyKeyIn(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.quoteQtyChange(parseInt(key.id.slice(-1)));
        }
      },
      quoteqty5: {
        keyup: function (key) {
          this.quoteQtyKeyIn(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.quoteQtyChange(parseInt(key.id.slice(-1)));
        }
      },
      quoteqty6: {
        keyup: function (key) {
          this.quoteQtyKeyIn(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.quoteQtyChange(parseInt(key.id.slice(-1)));
        }
      },
      quoteqty7: {
        keyup: function (key) {
          this.quoteQtyKeyIn(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.quoteQtyChange(parseInt(key.id.slice(-1)));
        }
      },
      quoteqty8: {
        keyup: function (key) {
          this.quoteQtyKeyIn(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.quoteQtyChange(parseInt(key.id.slice(-1)));
        }
      },
      quoteqty9: {
        keyup: function (key) {
          this.quoteQtyKeyIn(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.quoteQtyChange(parseInt(key.id.slice(-1)));
        }
      },
      discount1: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          var idx = parseInt(key.id.slice(-1));
          this.discountChange(idx, parseFloat(Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue()), parseFloat(Ext.getCmp('addeditquotepartqty_discount' + idx).getValue()));
        }
      },
      discount2: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          var idx = parseInt(key.id.slice(-1));
          this.discountChange(idx, parseFloat(Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue()), parseFloat(Ext.getCmp('addeditquotepartqty_discount' + idx).getValue()));
        }
      },
      discount3: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          var idx = parseInt(key.id.slice(-1));
          this.discountChange(idx, parseFloat(Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue()), parseFloat(Ext.getCmp('addeditquotepartqty_discount' + idx).getValue()));
        }
      },
      discount4: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          var idx = parseInt(key.id.slice(-1));
          this.discountChange(idx, parseFloat(Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue()), parseFloat(Ext.getCmp('addeditquotepartqty_discount' + idx).getValue()));
        }
      },
      discount5: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          var idx = parseInt(key.id.slice(-1));
          this.discountChange(idx, parseFloat(Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue()), parseFloat(Ext.getCmp('addeditquotepartqty_discount' + idx).getValue()));
        }
      },
      discount6: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          var idx = parseInt(key.id.slice(-1));
          this.discountChange(idx, parseFloat(Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue()), parseFloat(Ext.getCmp('addeditquotepartqty_discount' + idx).getValue()));
        }
      },
      discount7: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          var idx = parseInt(key.id.slice(-1));
          this.discountChange(idx, parseFloat(Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue()), parseFloat(Ext.getCmp('addeditquotepartqty_discount' + idx).getValue()));
        }
      },
      discount8: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          var idx = parseInt(key.id.slice(-1));
          this.discountChange(idx, parseFloat(Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue()), parseFloat(Ext.getCmp('addeditquotepartqty_discount' + idx).getValue()));
        }
      },
      discount9: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          var idx = parseInt(key.id.slice(-1));
          this.discountChange(idx, parseFloat(Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue()), parseFloat(Ext.getCmp('addeditquotepartqty_discount' + idx).getValue()));
        }
      },
      unitdiscount1: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.unitdiscountChange(parseInt(key.id.slice(-1)));
        }
      },
      unitdiscount2: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.unitdiscountChange(parseInt(key.id.slice(-1)));
        }
      },
      unitdiscount3: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.unitdiscountChange(parseInt(key.id.slice(-1)));
        }
      },
      unitdiscount4: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.unitdiscountChange(parseInt(key.id.slice(-1)));
        }
      },
      unitdiscount5: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.unitdiscountChange(parseInt(key.id.slice(-1)));
        }
      },
      unitdiscount6: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.unitdiscountChange(parseInt(key.id.slice(-1)));
        }
      },
      unitdiscount7: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.unitdiscountChange(parseInt(key.id.slice(-1)));
        }
      },
      unitdiscount8: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.unitdiscountChange(parseInt(key.id.slice(-1)));
        }
      },
      unitdiscount9: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.unitdiscountChange(parseInt(key.id.slice(-1)));
        }
      },
      revisedunitprice1: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.getRevisedunitprice1().suspendEvents();
          this.revisedunitpriceChange(parseInt(key.id.slice(-1)));
          this.getRevisedunitprice1().resumeEvents(true);
        }
      },
      revisedunitprice2: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.getRevisedunitprice2().suspendEvents();
          this.revisedunitpriceChange(parseInt(key.id.slice(-1)));
          this.getRevisedunitprice2().resumeEvents(true);
        }
      },
      revisedunitprice3: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.getRevisedunitprice3().suspendEvents();
          this.revisedunitpriceChange(parseInt(key.id.slice(-1)));
          this.getRevisedunitprice3().resumeEvents(true);
        }
      },
      revisedunitprice4: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.getRevisedunitprice4().suspendEvents();
          this.revisedunitpriceChange(parseInt(key.id.slice(-1)));
          this.getRevisedunitprice4().resumeEvents(true);
        }
      },
      revisedunitprice5: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.getRevisedunitprice5().suspendEvents();
          this.revisedunitpriceChange(parseInt(key.id.slice(-1)));
          this.getRevisedunitprice5().resumeEvents(true);
        }
      },
      revisedunitprice6: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.getRevisedunitprice6().suspendEvents();
          this.revisedunitpriceChange(parseInt(key.id.slice(-1)));
          this.getRevisedunitprice6().resumeEvents(true);
        }
      },
      revisedunitprice7: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.getRevisedunitprice7().suspendEvents();
          this.revisedunitpriceChange(parseInt(key.id.slice(-1)));
          this.getRevisedunitprice7().resumeEvents(true);
        }
      },
      revisedunitprice8: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.getRevisedunitprice8().suspendEvents();
          this.revisedunitpriceChange(parseInt(key.id.slice(-1)));
          this.getRevisedunitprice8().resumeEvents(true);
        }
      },
      revisedunitprice9: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.getRevisedunitprice9().suspendEvents();
          this.revisedunitpriceChange(parseInt(key.id.slice(-1)));
          this.getRevisedunitprice9().resumeEvents(true);
        }
      },
      partrevision: {
        blur: 'partRevisionBlur',
        change: 'partRevisionChange',
        clearicontap: function () {
          this.getPartrevision().focus();
          this.clearPartInfo(false);
        }
      },
      unitprice1: {
        focus: function (key) {
          this.setFocusOnQty(parseInt(key.id.slice(-1)));
        },
        change: function (key) {
          this.unitpriceChange(parseInt(key.id.slice(-1)));
        }
      },
      unitprice2: {
        change: function (key) {
          this.unitpriceChange(parseInt(key.id.slice(-1)));
        }
      },
      unitprice3: {
        change: function (key) {
          this.unitpriceChange(parseInt(key.id.slice(-1)));
        }
      },
      unitprice4: {
        change: function (key) {
          this.unitpriceChange(parseInt(key.id.slice(-1)));
        }
      },
      unitprice5: {
        change: function (key) {
          this.unitpriceChange(parseInt(key.id.slice(-1)));
        }
      },
      unitprice6: {
        change: function (key) {
          this.unitpriceChange(parseInt(key.id.slice(-1)));
        }
      },
      unitprice7: {
        change: function (key) {
          this.unitpriceChange(parseInt(key.id.slice(-1)));
        }
      },
      unitprice8: {
        change: function (key) {
          this.unitpriceChange(parseInt(key.id.slice(-1)));
        }
      },
      unitprice9: {
        change: function (key) {
          this.unitpriceChange(parseInt(key.id.slice(-1)));
        }
      }
    }
  },
  initializeForm: function () {
    this.formInitilize();
    this.qtyFieldSetShowHide(false);
    this.loadFormProperties();
    if (SessionObj.getShowSecondTax() == true) {
      this.getTaxid2().getParent().show();
    } else {
      this.getTaxid2().getParent().hide();
    }

  },
  qtyFieldSetShowHide: function (flag) {
    var fieldid = 'addeditquotepart_qty';
    if (flag) {
      for (var i = 2; i <= 9; i++) {
        Ext.getCmp(fieldid + i + 'fieldset').show();
      }
    } else {
      var j = 0;
      Ext.getCmp(fieldid + '2').show();
      Ext.getCmp(fieldid + '2' + '_add').show();
      Ext.getCmp(fieldid + '2' + '_clear').hide();
      for (var i = 2; i <= 9; i++) {
        j = i + 1;
        Ext.getCmp(fieldid + i + 'fieldset').hide();
        if (i < 9) {
          Ext.getCmp(fieldid + j).hide();
        }
      }
    }
  },

  loadFormProperties: function () {
    Ext.create('M1CRM.model.crmQuotePartAddEdit', {}).getIt({
      RequestID: 'prop',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          this.getNontaxreason().suspendEvents();
          this.getTaxidsel().suspendEvents();
          this.getTaxidsel().setStore(this.getStoreFromListForDropDown(result.ResultObject.TaxCodeList, 'crmTaxCodes', true));
          this.getTaxidsel().isDirty = false;
          this.getTaxidsel().resumeEvents(true);

          this.getTaxid2sel().suspendEvents();
          this.getTaxid2sel().setStore(this.getStoreFromListForDropDown(result.ResultObject.SecondTaxCodeList, 'crmTaxCodes', true));
          this.getTaxid2sel().isDirty = false;
          this.getTaxid2sel().resumeEvents(true);

          this.getNontaxreason().suspendEvents();
          this.getNontaxreason().setStore(this.getStoreFromListForDropDown(result.ResultObject.ReasonList, 'crmReason', true));
          this.getNontaxreason().isDirty = false;
          this.config._propNonTaxReasonID = result.ResultObject.NonTaxReasonID;
          this.getNontaxreason().setValue(this.config._propNonTaxReasonID);
          this.config._propPartsMustExist = result.ResultObject.PartsMustExist;
          this.getNontaxreason().resumeEvents(true);
        }
      }
    });
  },

  getFormButtons: function () {
    return [this.getSavebtn()];
  },

  getEditableFormFields: function () {
    return [this.getQuoteid().id, this.getQuotelineid().id, this.getPartid().id, this.getFirm().id, this.getTaxidsel().id, this.getTaxid2sel().id, this.getNontaxreason().id,
      this.getQuoteqty1().id, this.getQuoteqty2().id, this.getQuoteqty3().id, this.getQuoteqty4().id, this.getQuoteqty5().id, this.getQuoteqty6().id, this.getQuoteqty7().id, this.getQuoteqty8().id,
      this.getQuoteqty9().id,
      this.getUnitprice1().id, this.getUnitprice2().id, this.getUnitprice3().id, this.getUnitprice4().id, this.getUnitprice5().id, this.getUnitprice6().id,
      this.getUnitprice7().id, this.getUnitprice8().id, this.getUnitprice9().id,
      this.getDiscount1().id, this.getDiscount2().id, this.getDiscount3().id, this.getDiscount4().id, this.getDiscount5().id, this.getDiscount6().id, this.getDiscount7().id, this.getDiscount8().id,
      this.getDiscount9().id,
      this.getUnitdiscount1().id, this.getUnitdiscount2().id, this.getUnitdiscount3().id, this.getUnitdiscount4().id, this.getUnitdiscount5().id, this.getUnitdiscount6().id, this.getUnitdiscount7().id,
      this.getUnitdiscount8().id, this.getUnitdiscount9().id,
      this.getRevisedunitprice1().id, this.getRevisedunitprice2().id, this.getRevisedunitprice3().id, this.getRevisedunitprice4().id, this.getRevisedunitprice5().id, this.getRevisedunitprice6().id,
      this.getRevisedunitprice7().id, this.getRevisedunitprice8().id, this.getRevisedunitprice9().id
    ];
  },

  showForm: function () {
    if (SessionObj.getQuoteQtyAddEditTSecurity() == null || SessionObj.getQuoteQtyAddEditTSecurity() == '') {
      Ext.create('M1CRM.model.crmUserSecurity', {}).getUserSecurityLevel({
        ID: 'CRMADDEDITQUOTATPART',
        scope: this,
        onReturn: function (rec) {
          if (rec != null) {
            SessionObj.setQuoteQtyAddEditTSecurity(rec.PAccessLevel + ',' + rec.SAccessLevel);
            if (this.applyUserSecuritySettings(this.getForm().id, parseInt(rec.PAccessLevel), parseInt(rec.SAccessLevel),
                this.getFormButtons())) {
              this.processShowForm();
            }
          } else {
            this.toBackTap(this.getForm().id);
          }
        }
      });
    } else {
      if (this.applyUserSecuritySettings(this.getForm().id,
          parseInt(this.getPKeyFromSessionObj(SessionObj.getQuoteQtyAddEditTSecurity())), parseInt(this.getSKeyFromSessionObj(SessionObj.getQuoteQtyAddEditTSecurity())), this.getFormButtons())) {
        this.processShowForm();
      } else {
        this.toBackTap(this.getForm().id);
      }
    }

  },

  processShowForm: function () {
    this.qtyFieldSetShowHide(false);
    this.showHideAdditionalQtyCtrls(false);
    // this.getNontaxreason().setValue(this.config._propNonTaxReasonID);
    this.getNontaxreason().isDirty = false;
    var mode = Ext.getCmp(this.getForm().id).config._mode;
    if (Ext.getCmp('addnewquote').getRecord() != null && Ext.getCmp('addnewquote').getRecord().getData() != null && Ext.getCmp('addeditquotepart').getRecord() == null) {
      this.setEntryFormMode('addnew');
      this.clearFormFields();
      this.getQuoteid().setValue(Ext.getCmp('addnewquote').getRecord().getData().QuoteID != null ? Ext.getCmp('addnewquote').getRecord().getData().QuoteID : AddEditQuote.getData().QuoteID);
      this.getSubtitle().setHtml('ID :' + this.getQuoteid().getValue());
      this.makeformNew();
    } else if (Ext.getCmp('addeditquotepart').getRecord() != null && Ext.getCmp('addeditquotepart').getRecord().getData() != null) {
      if (mode == 'addnew' || mode == 'edit') {
        this.setEntryFormMode(mode);
        this.makeFormReadOnly(false);
        if (mode == 'edit') {
          if (this.getPartid().getValue() != '' && this.getPartid().getValue() != this.getForm().getRecord().getData().PartID) {
            Ext.Msg.confirm("Warning", "Changing the Part ID will clear all existing Quote Quantities.  Do you wish to proceed?", function (btn) {
              if (btn == 'yes') {
                this.loadData();
                this.getFirm().uncheck();
                this.showHideAdditionalQtyCtrls(true);
              }
            }, this);

          } else {
            this.loadData();
          }
        } else {
          this.loadData();
          //this.getFirm().check();
        }
        this.getPartid().setReadOnly(false);
      } else if (mode == 'view') {
        this.setEntryFormMode('view');
        this.resetEditableForms(this.getEditableFormFields());
        this.loadData();
        this.makeFormReadOnly(true);
        if (Ext.isDefined(Ext.getCmp('addnewquote_closed')) && Ext.getCmp('addnewquote_closed').getValue() == true) {
          this.getEditbtn().hide(); // disable();
          this.getAddnewbtn().hide();
        } else {
          this.getEditbtn().show(); // enable()
          this.getAddnewbtn().show();
        }
        Ext.getCmp('addeditquotepart_nontaxreasonidsel').hide();
      }
      this.resetEditableForms(this.getEditableFormFields());
    }
    this.getPartid().focus();
  },

  loadData: function () {
    var rec = this.getForm().getRecord().getData();
    if (rec.QuoteID != null) {
      this.getQuoteid().setValue(rec.QuoteID);
      this.getSubtitle().setHtml('ID :' + rec.QuoteID);
    }
    if (rec.QuoteLineID != null) {
      Ext.getCmp('addeditquotepart_tempquotlineid').setValue(rec.QuoteLineID);
      this.getQuotelineid().setValue(rec.QuoteLineID);
      this.resetEditableForms(this.getEditableFormFields());
    }

    this.getPartid().suspendEvents();
    this.getPartid().setValue(rec.PartID);
    this.getPartid().resumeEvents(true);


    Ext.getCmp(this.getForm().id).config._mode != 'view' ? this.getPartid().isDirty = true : this.getPartid().isDirty = false;
    var rev = '';
    if (rec.RevisionID != null)
      rev = rec.RevisionID;
    else if (rec.PartRevisionID != '')
      rev = rec.PartRevisionID;
    this.getPartrevision().setValue(rev);

    Ext.getCmp('addeditquotepart_partgroupid').setValue(rec.PartGroupID);

    if (rec.PartDesc != null)
      this.getPartdesc().setValue(rec.PartDesc);
    else if (rec.ShortDescription != null)
      this.getPartdesc().setValue(rec.ShortDescription);
    else
      this.getPartdesc().setValue(rec.Description);
    this.getPartid().setReadOnly(true);
    this.getUom().setValue(rec.IUoM != null ? rec.IUoM : rec.InventoryUnitOfMeasure != null ? rec.InventoryUnitOfMeasure : rec.UOM);
    if (rec.Firm == null || rec.Firm == false) {
      this.getFirm().uncheck();
    } else {
      this.getFirm().check();
    }

    if (rec.TaxCodeID != null) {
      this.getTaxidsel().setValue(rec.TaxCodeID);
      if (rec.TaxCodeID == '') {
        this.getTaxid().setValue('<None>');
      } else {
        var store = this.getTaxidsel().getStore();
        var items = store && store.getData() ? store.getData().items : [];
        this.getTaxid().setValue(this.getTaxCodeDescriptionFromList(this.getTaxidsel().getValue(), items));
      }
      this.getTaxid2sel().setValue(rec.SecondTaxCodeID);
      if (rec.SecondTaxCodeID == '') {
        this.getTaxid2().setValue('<None>');
      } else {
        this.getTaxid2().setValue(this.getTaxCodeDescriptionFromList(this.getTaxid2sel().getValue(), this.getTaxid2sel().getStore().getData().items));
      }

    } else {
      this.loadPartTaxInfo();
    }
    this.getNontaxreason().setValue(rec.NonTaxReasonID);
    if (this.getNontaxreason().getStore() != null && this.getNontaxreason().getStore().getData() != null) {
      var strDec = "";
      if (this.getNontaxreason().getValue() != "") {
        var key = this.getNontaxreason().getValue();
        var arrObj = this.getNontaxreason().getStore().getData().items;
        for (var i = 0; i < arrObj.length; i++) {
          if (arrObj[i].data.ID == key) {
            strDec = arrObj[i].data.Description;
            break;
          }
        }
        this.getNontaxreasonid().setValue(strDec);
      }
    }

    if (rec.QuoteID != null && rec.QuoteLineID != null) {
      this.loadQuoteQuantityInfo(rec);
    } else {
      Ext.getCmp('addeditquotepartqty_quoteqty1').focus();
    }
  },

  loadPartTaxInfo: function () {
    Ext.create('M1CRM.model.crmQuotePartAddEdit', {
      PartID: this.getPartid().getValue()
    }).getIt({
      RequestID: 'parttaxcodes',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          if (result.ResultObject.TaxCodeID != '') {
            this.getTaxidsel().setValue(result.ResultObject.TaxCodeID);
            this.getTaxid2sel().setValue(result.ResultObject.SecondTaxCodeID);
            this.getTaxidsel().isDirty = false;
            this.getTaxid2sel().isDirty = false;
          } else {
            this.getCustomerTaxCodes();
          }
        } else {
          this.getCustomerTaxCodes();
        }
      }
    });


  },
  loadQuoteQuantityInfo: function (recObj) {
    var flag = Ext.getCmp(this.getForm().id).config._mode == 'view' ? true : false;
    if (recObj.QtQuantityList != null && recObj.QtQuantityList.length > 0) {
      var sortQuoteQuantityArr = recObj.QtQuantityList;
      var ctrlid = 0;

      var fieldid = '';
      var fieldidbase = 'addeditquotepartqty_';
      for (var i = 0; i < sortQuoteQuantityArr.length; i++) {
        ctrlid++;
        if (i > 0) {
          fieldid = 'addeditquotepart_qty';
          Ext.getCmp(fieldid + ctrlid + 'fieldset').show();
          Ext.getCmp(fieldid + ctrlid + '_add').hide();
        }


        fieldid = 'addeditquotepart_qtyid';
        Ext.getCmp(fieldid + ctrlid).setValue(sortQuoteQuantityArr[i].QuoteQuantityID);

        fieldid = fieldidbase + 'quoteqty';
        Ext.getCmp(fieldid + ctrlid).suspendEvents();
        Ext.getCmp(fieldid + ctrlid).setValue(sortQuoteQuantityArr[i].QuoteQuantity);
        Ext.getCmp(fieldid + ctrlid).setReadOnly(flag);
        Ext.getCmp(fieldid + ctrlid).resumeEvents(true);
        fieldid = fieldidbase + 'unitprice';
        Ext.getCmp(fieldid + ctrlid).suspendEvents();
        Ext.getCmp(fieldid + ctrlid).setValue(this.roundWithDecimal(sortQuoteQuantityArr[i].FullRevisedUnitPriceBase, 2).toFixed(2));
        Ext.getCmp(fieldid + ctrlid).setReadOnly(flag);
        Ext.getCmp(fieldid + ctrlid).resumeEvents(true);
        fieldid = fieldidbase + 'discount';
        Ext.getCmp(fieldid + ctrlid).suspendEvents();
        Ext.getCmp(fieldid + ctrlid).setValue(sortQuoteQuantityArr[i].DiscountPercent)
        Ext.getCmp(fieldid + ctrlid).setReadOnly(flag);
        Ext.getCmp(fieldid + ctrlid).resumeEvents(true);
        fieldid = fieldidbase + 'unitdiscount';
        Ext.getCmp(fieldid + ctrlid).suspendEvents();
        Ext.getCmp(fieldid + ctrlid).setValue(this.roundWithDecimal(sortQuoteQuantityArr[i].UnitDiscountBase, 2).toFixed(2));
        Ext.getCmp(fieldid + ctrlid).setReadOnly(flag);
        Ext.getCmp(fieldid + ctrlid).resumeEvents(true);
        fieldid = 'addeditquotepart_revisedunitprice';
        Ext.getCmp(fieldid + ctrlid).suspendEvents();
        Ext.getCmp(fieldid + ctrlid).setValue(this.roundWithDecimal(sortQuoteQuantityArr[i].RevisedUnitPriceBase, 2).toFixed(2));
        Ext.getCmp(fieldid + ctrlid).setReadOnly(flag);
        Ext.getCmp(fieldid + ctrlid).suspendEvents();
        fieldid = 'addeditquotepart_unitfirsttax';
        Ext.getCmp(fieldid + ctrlid).setValue(this.roundWithDecimal(sortQuoteQuantityArr[i].UnitTaxAmountBase, 2).toFixed(2));
        Ext.getCmp(fieldid + ctrlid).setReadOnly(true);
        fieldid = 'addeditquotepart_unitsecondtax';
        Ext.getCmp(fieldid + ctrlid).setValue(this.roundWithDecimal(sortQuoteQuantityArr[i].UnitSecondTaxAmountBase, 2).toFixed(2));
        Ext.getCmp(fieldid + ctrlid).setReadOnly(true);
      }
    }
  },

  getTaxCodeDescriptionFromList: function (key, arrObj) {
    var strDec = "";
    if (arrObj != null) {
      for (var i = 0; i < arrObj.length; i++) {
        if (arrObj[i].data.TaxCodeID == key) {
          strDec = arrObj[i].data.Description;
          break;
        }
      }
    }
    return strDec;
  },

  partLookupTap: function () {
    this.openPartsList();
  },

  openPartsList: function () {
    var view = M1CRM.util.crmViews.getParts();
    view.config._parentformid = 'addeditquotepart';
    M1CRM.util.crmViews.slideLeft(view);
  },

  getTaxRateOnTaxCode: function (taxcode, type) {
    Ext.create('M1CRM.model.crmQuotePartAddEdit', {
      TaxCodeID: taxcode
    }).getIt({
      RequestID: 'taxrates',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          if (type == 1) {
            this.config._taxrate = result.ResultObject.TaxRate;
          } else {
            this.config._secondtaxrate = result.ResultObject.TaxRate;
          }
        }
        this.recalculateTax();
      }
    });
  },
  taxidselChange: function () {
    if (Ext.getCmp(this.getForm().id).config._mode != 'view') {
      if (this.getTaxidsel().getStore() != null && this.getTaxidsel().getStore().getData() != null) {
        this.getTaxid().setValue(this.getTaxCodeDescriptionFromList(this.getTaxidsel().getValue(), this.getTaxidsel().getStore().getData().items));
        if (this.getTaxidsel().getValue() == '') {
          this.config._taxrate = 0;
          this.recalculateTax();
          this.enableControls([this.getNontaxreason().id]);
          this.disableControls([this.getTaxidsel().id /*, this.getTaxid2sel().id*/]);
        } else {
          this.getTaxRateOnTaxCode(this.getTaxidsel().getValue(), 1);
          this.enableControls([this.getTaxidsel().id /*, this.getTaxid2sel().id*/]);
          this.disableControls([this.getNontaxreason().id]);
        }
      }
    }
  },
  recalculateTax: function () {
    if (parseFloat(Ext.getCmp('addeditquotepartqty_quoteqty1').getValue()) > 0) {
      this.calculateFirstTax(1);
    }
    for (var i = 2; i <= 9; i++) {
      if (!Ext.getCmp('addeditquotepart_qty' + i + 'fieldset').isHidden() && parseFloat(Ext.getCmp('addeditquotepartqty_quoteqty' + i).getValue()) > 0) {
        this.calculateFirstTax(i);
      }
    }
  },
  recalculateSecondTax: function () {
    if (parseFloat(Ext.getCmp('addeditquotepartqty_quoteqty1').getValue()) > 0) {
      this.calculateSecondTax(1);
    }
    for (var i = 2; i <= 9; i++) {
      if (!Ext.getCmp('addeditquotepart_qty' + i + 'fieldset').isHidden() && parseFloat(Ext.getCmp('addeditquotepartqty_quoteqty' + i).getValue()) > 0) {
        this.calculateSecondTax(i);
      }
    }
  },
  taxid2selChange: function () {
    if (Ext.getCmp(this.getForm().id).config._mode != 'view') {
      if (this.getTaxid2sel().getStore() != null && this.getTaxid2sel().getStore().getData() != null) {
        this.getTaxid2().setValue(this.getTaxCodeDescriptionFromList(this.getTaxid2sel().getValue(), this.getTaxid2sel().getStore().getData().items));
        if (this.getTaxid2sel().getValue() == '') {
          this.config._secondtaxrate = 0;
          this.recalculateSecondTax();
        } else {
          this.getTaxRateOnTaxCode(this.getTaxid2sel().getValue(), 2);
        }
      }
    }
  },
  nontaxreasonChange: function (control, newvalue, oldvalue, opt) {
    if (Ext.getCmp(this.getForm().id).config._mode != 'view') {
      if (this.getNontaxreason().getStore() != null && this.getNontaxreason().getStore().getData() != null) {
        var strDec = "";
        if (this.getNontaxreason().getValue() != "") {
          var key = this.getNontaxreason().getValue();
          var arrObj = this.getNontaxreason().getStore().getData().items;

          for (var i = 0; i < arrObj.length; i++) {
            if (arrObj[i].data.ID == key) {
              strDec = arrObj[i].data.Description;
              break;
            }
          }
          this.getNontaxreasonid().suspendEvents();
          this.getNontaxreasonid().setValue(strDec);
          this.getNontaxreasonid().resumeEvents(true);

          this.enableControls([this.getNontaxreason().id]);
          this.disableControls([this.getTaxidsel().id, this.getTaxid2sel().id]);

          this.getTaxidsel().suspendEvents();
          this.getTaxidsel().setValue('');
          this.getTaxidsel().resumeEvents(true);

          this.getTaxid2sel().suspendEvents();
          this.getTaxid2sel().setValue('');
          this.getTaxid2sel().resumeEvents(true);


        } else {
          this.getNontaxreasonid().setValue(strDec);
          this.disableControls([this.getNontaxreason().id]);
          this.enableControls([this.getTaxidsel().id, this.getTaxid2sel().id]);
        }
      }
    } else {
      this.hideControls([this.getNontaxreason().id]);
    }

  },
  getSaveObject: function () {
    var emptyRecord = {raw: {}};
    var quoteRecord = (Ext.getCmp('addnewquote').getRecord() || emptyRecord).raw;

    return Ext.create('M1CRM.model.crmQuotePartAddEdit', {
      QuoteID: Ext.getCmp('addnewquote_quoteid').getValue(),
      QuoteLineID: this.getQuotelineid().getValue() == null || this.getQuotelineid().getValue() == "" ? 0 : parseInt(this.getQuotelineid().getValue()),
      PartID: this.getPartid().getValue(),
      PartRevisionID: this.getPartrevision().getValue(),
      UOM: this.getUom().getValue(),
      PartDesc: this.getPartdesc().getValue(),
      Firm: this.getFirm().isChecked(),
      TaxCodeID: this.getTaxidsel().getValue() != null ? this.getTaxidsel().getValue() : '',
      SecondTaxCodeID: this.getTaxid2sel().getValue() != null ? this.getTaxid2sel().getValue() : '',
      NonTaxReasonID: this.getNontaxreason().getValue() != null ? this.getNontaxreason().getValue() : '',
      QtQuantityList: this.getQuoteQuantity(),
      EmployeeID: SessionObj.getEmpId(),
      OrganizationID: quoteRecord.OrganizationID,
      LocationID: quoteRecord.QuoteLocationID
    });
  },
  btnSaveTap: function () {
    var quotePart = this.getSaveObject();
    var error = quotePart.validate();
    if (quotePart.isValid()) {
      quotePart.saveData({
        scope: this,
        onReturn: function (result) {
          if (result != null && result.SaveStatus == true) {
            if (this.getQuotelineid().getValue() == "") {
              this.getQuotelineid().setValue(result.ResultObject.QuoteLineID);
              this.setEntryFormMode('view');
              this.makeFormReadOnly(true);
              this.resetEditableForms(this.getEditableFormFields());
              this.showHideAdditionalQtyCtrls(false);
            } else {
              this.setEntryFormMode('view');
              this.makeFormReadOnly(true);
              this.resetEditableForms(this.getEditableFormFields());
              this.showHideAdditionalQtyCtrls(false);
            }
          } else {
            Ext.Msg.alert('Error', 'Error saving quotation part info');
          }
        }
      });
    } else {
      this.displayValidationMessage(error);
    }
  },
  processAutoSave: function (type) {

    if (this.isFormDirty(this.getEditableFormFields())) {
      var quotePart = this.getSaveObject();
      quotePart.validate();
      if (quotePart.isValid()) {
        quotePart.saveData({
          scope: this,
          onReturn: function (result) {
            if (result != null && result.SaveStatus == true) {
              this.clearFormFields();
              this.processAfterAutoSave(type, this.getForm().id);
            } else {
              Ext.Msg.alert('Error', 'Error saving quotation part info');
            }
          }
        });
      } else {
        Ext.Msg.confirm("", this.getRecordInCompleteMessage(), function (btn, type) {
          if (btn == 'yes') {
            this.clearFormFields();
            this.processAfterAutoSave(type, this.getForm().id);
          }
        }, this);
      }

    }
    else {
      this.clearFormFields();
      this.processAfterAutoSave(type, this.getForm().id)
    }
  },
  editQuoteLineTap: function () {
    if (Ext.getCmp('addeditquotepart').getRecord() != null && Ext.getCmp('addeditquotepart').getRecord().getData().Firm != null && Ext.getCmp('addeditquotepart').getRecord().getData().Firm == false) {
      this.enableControls([this.getFirm().id]);
      this.hideControls([this.getTaxid().id, this.getTaxid2().id, this.getNontaxreasonid().id]);
      this.showControls([this.getTaxidsel().id, this.getTaxid2sel().id, this.getNontaxreason().id]);
    } else if (Ext.getCmp('addeditquotepart').getRecord() != null && Ext.getCmp('addeditquotepart').getRecord().getData().Firm != null && Ext.getCmp('addeditquotepart').getRecord().getData().Firm != false) {
      Ext.Msg.alert('Info', 'Quote Item cannot be changed');
    }
  },
  btnCancelTap: function () {
    if (Ext.getCmp('addeditquotepart').getRecord() == null || Ext.getCmp('addeditquotepart').getRecord().getData().QuoteLineID == null) {
      this.setEntryFormMode('addnew');
      Ext.getCmp(this.getForm().id).config._mode = 'addnew';
      this.makeformNew();
      this.resetEditableForms(this.getEditableFormFields());
    } else {
      this.setEntryFormMode('view');
      Ext.getCmp(this.getForm().id).config._mode = 'view';
      this.disableControls([this.getFirm().id]);
      this.clearAllQtyBreaks();
      this.hideAllQtyBreaks();
      this.loadData();
      this.makeFormReadOnly(true);
      this.showHideAdditionalQtyCtrls(false);
    }
  },
  newQuoteLineTap: function () {
    if (!this.isDirty()) {
      this.makeformNew();
    }
    else {
      Ext.Msg.confirm("Exit Quote Line Entry", "Would you like to save changes?", function (btn) {
        if (btn == 'no') {
          this.makeformNew();
        }
      }, this);
    }
  },
  makeformNew: function () {
    this.hideorshowTextFieldLookupInfo(this.getPartid());
    this.enableControls([this.getFirm().id]);
    this.getQuotelineid().setValue('');
    this.getPartid().setReadOnly(false);
    Ext.getCmp('addeditquotepart_tempquotlineid').setValue("");
    this.getPartid().focus();
    this.showControls([this.getTaxidsel().id, this.getTaxid2sel().id, this.getNontaxreason().id]);
    this.hideControls([this.getTaxid().id, this.getTaxid2().id, this.getNontaxreasonid().id]);
    this.resetEditableForms(this.getEditableFormFields());
    this.getCustomerTaxCodes();
    this.getNontaxreason().setValue(this.config._propNonTaxReasonID);
    this.getNontaxreason().isDirty = false;
    this.clearPartInfo(true);
  },
  getCustomerTaxCodes: function () {
    var shipLocationId = '';
    if (Ext.getCmp('addnewquote').getRecord() != null && Ext.getCmp('addnewquote').getRecord().getData() != null && AddEditQuote != null && AddEditQuote.data != null) {
      shipLocationId = AddEditQuote.data.ShipLocationID != null ? AddEditQuote.data.ShipLocationID : AddEditQuote.data.LocationID;
    }
    Ext.create('M1CRM.model.crmQuotePartAddEdit', {
      OrganizationID: AddEditQuote.data.OrganizationID,
      LocationID: shipLocationId
    }).getIt({
      RequestID: 'custtaxcodes',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          this.getTaxidsel().setValue(result.ResultObject.CustomerTaxCodeID);
          this.getTaxid().setValue(this.getSelectFieldDisplayValue(this.getTaxidsel()));
          this.getTaxid2sel().setValue(result.ResultObject.CustomerSecondTaxCodeID);
          this.getTaxid2().setValue(this.getSelectFieldDisplayValue(this.getTaxid2sel()));
          if (result.ResultObject.CustomerTaxCodeID != '' || result.ResultObject.CustomerSecondTaxCodeID != '') {
            this.getNontaxreason().setValue('');
          }
        } else {
          this.getTaxidsel().setValue('');
          this.getTaxid().setValue(this.getSelectFieldDisplayValue(this.getTaxidsel()));
          this.getTaxid2sel().setValue('');
          this.getTaxid2().setValue(this.getSelectFieldDisplayValue(this.getTaxid2sel()));
        }
        this.getTaxidsel().isDirty = false;
        this.getTaxid2sel().isDirty = false;
      }
    });
  },
  makeFormReadOnly: function (flag) {
    if (flag) {
      this.hideControls([this.getTaxidsel().id, this.getTaxid2sel().id, this.getNontaxreason().id]);
      this.showControls([this.getTaxid().id, this.getTaxid2().id, this.getNontaxreasonid().id]);
      this.disableControls([this.getFirm().id]);
    } else {
      this.showControls([this.getTaxidsel().id, this.getTaxid2sel().id, this.getNontaxreason().id]);
      this.hideControls([this.getTaxid().id, this.getTaxid2().id, this.getNontaxreasonid().id]);
      this.enableControls([this.getFirm().id]);
      if (this.getNontaxreason().getValue() != '') {
        this.getNontaxreason().enable();
        this.getTaxidsel().disable();
        this.getTaxid2sel().disable();
      } else if (this.getTaxidsel().getValue() != '') {
        this.getNontaxreason().disable();
        this.getTaxidsel().enable();
        this.getTaxid2sel().enable();
      } else {
        this.getNontaxreason().enable();
        this.getTaxidsel().enable();
        this.getTaxid2sel().enable();
      }

    }
    this.getPartid().setReadOnly(flag);
    this.getPartrevision().setReadOnly(flag);
    for (ctrlid = 1; ctrlid <= 9; ctrlid++) {
      if (!Ext.getCmp('addeditquotepartqty_quoteqty' + ctrlid).isHidden()) {
        Ext.getCmp('addeditquotepartqty_quoteqty' + ctrlid).setReadOnly(flag);
        Ext.getCmp('addeditquotepartqty_unitprice' + ctrlid).setReadOnly(flag);
        Ext.getCmp('addeditquotepartqty_discount' + ctrlid).setReadOnly(flag);
        Ext.getCmp('addeditquotepartqty_unitdiscount' + ctrlid).setReadOnly(flag);
        Ext.getCmp('addeditquotepart_revisedunitprice' + ctrlid).setReadOnly(flag);
        Ext.getCmp('addeditquotepart_unitfirsttax' + ctrlid).setReadOnly(true);
        Ext.getCmp('addeditquotepart_unitsecondtax' + ctrlid).setReadOnly(true);
      }
    }
    this.hideorshowTextFieldLookupInfo(this.getPartid());
  },
  btnAddnewTap: function () {
    Ext.getCmp(this.getForm().id).config._mode = 'addnew';
    this.setEntryFormMode('addnew');
    this.clearFormFields();
    this.makeformNew();
    this.getSubtitle().show();
  },
  btnEditTap: function () {
    Ext.getCmp(this.getForm().id).config._mode = 'edit';
    this.setEntryFormMode('edit');
    this.makeFormReadOnly(false);
    this.showHideAdditionalQtyCtrls(true);
  },
  showHideAdditionalQtyCtrls: function (flag) {
    var rec = this.getForm().getRecord() != null && this.getForm().getRecord().getData() != null ? this.getForm().getRecord().getData() : null;
    var ctrlid = 0;
    var fieldid = 'addeditquotepart_qty';
    if (rec != null && rec.QuoteID != null && rec.QuoteLineID != null && rec.QtQuantityList != null && rec.QtQuantityList.length > 0) {
      var sortQuoteQuantityArr = rec.QtQuantityList.sort(function (a, b) {
        return a.QuoteQuantityID - b.QuoteQuantityID
      });
      for (var i = 0; i < sortQuoteQuantityArr.length; i++) {
        ctrlid++;
        if (i > 0) {
          Ext.getCmp(fieldid + ctrlid + 'fieldset').show();
          Ext.getCmp(fieldid + ctrlid).show();
          if (flag) {
            Ext.getCmp(fieldid + ctrlid + '_clear').show();
            Ext.getCmp(fieldid + ctrlid + '_add').hide();
          } else {
            Ext.getCmp(fieldid + ctrlid + '_clear').hide();
            Ext.getCmp(fieldid + ctrlid + '_add').hide();
          }
        }
      }
      ctrlid++;
      Ext.getCmp(fieldid + ctrlid).show();
      if (flag) {
        Ext.getCmp(fieldid + ctrlid + '_clear').hide();
        Ext.getCmp(fieldid + ctrlid + '_add').show();
      } else {
        Ext.getCmp(fieldid + ctrlid + '_clear').hide();
        Ext.getCmp(fieldid + ctrlid + '_add').hide();
      }
    } else {
      if (flag) {
        this.clearAllQtyBreaks();
      }
      this.hideAllQtyBreaks();

    }
  },
  hideAllQtyBreaks: function () {
    var id = 'addeditquotepart_qty';
    Ext.getCmp(id + 2).show();
    Ext.getCmp(id + 2 + 'fieldset').hide();
    Ext.getCmp(id + 2 + '_clear').hide();
    Ext.getCmp(id + 2 + '_add').show();

    for (ctrlid = 3; ctrlid <= 9; ctrlid++) {
      Ext.getCmp(id + ctrlid).show();
      Ext.getCmp(id + ctrlid + 'fieldset').hide();
      Ext.getCmp(id + ctrlid + '_clear').hide();
      Ext.getCmp(id + ctrlid + '_add').hide();
    }
  },
  clearAllQtyBreaks: function () {
    var fieldid = '';
    for (var ctrlid = 1; ctrlid <= 9; ctrlid++) {

      Ext.getCmp('addeditquotepart_qtyid' + ctrlid).setValue(0);

      fieldid = 'addeditquotepartqty';
      Ext.getCmp(fieldid + '_quoteqty' + ctrlid).suspendEvents();
      Ext.getCmp(fieldid + '_quoteqty' + ctrlid).setValue(0);
      Ext.getCmp(fieldid + '_quoteqty' + ctrlid).resumeEvents(true);

      Ext.getCmp(fieldid + '_unitprice' + ctrlid).suspendEvents();
      Ext.getCmp(fieldid + '_unitprice' + ctrlid).setValue(0);
      Ext.getCmp(fieldid + '_unitprice' + ctrlid).resumeEvents(true);

      Ext.getCmp(fieldid + '_discount' + ctrlid).suspendEvents();
      Ext.getCmp(fieldid + '_discount' + ctrlid).setValue(0);
      Ext.getCmp(fieldid + '_discount' + ctrlid).resumeEvents(true);

      Ext.getCmp(fieldid + '_unitdiscount' + ctrlid).suspendEvents();
      Ext.getCmp(fieldid + '_unitdiscount' + ctrlid).setValue(0);
      Ext.getCmp(fieldid + '_unitdiscount' + ctrlid).resumeEvents(true);

      fieldid = 'addeditquotepart';
      Ext.getCmp(fieldid + '_revisedunitprice' + ctrlid).suspendEvents();
      Ext.getCmp(fieldid + '_revisedunitprice' + ctrlid).setValue(0);
      Ext.getCmp(fieldid + '_revisedunitprice' + ctrlid).resumeEvents(true);

      Ext.getCmp(fieldid + '_unitfirsttax' + ctrlid).suspendEvents();
      Ext.getCmp(fieldid + '_unitfirsttax' + ctrlid).setValue(0);
      Ext.getCmp(fieldid + '_unitfirsttax' + ctrlid).resumeEvents(true);

      Ext.getCmp(fieldid + '_unitsecondtax' + ctrlid).suspendEvents();
      Ext.getCmp(fieldid + '_unitsecondtax' + ctrlid).setValue(0);
      Ext.getCmp(fieldid + '_unitsecondtax' + ctrlid).resumeEvents(true);
    }
  },
  quoteQtyKeyIn: function (idx) {
    Ext.getCmp('addeditquotepartqty_unitprice' + idx).setValue(0);
    Ext.getCmp('addeditquotepart_revisedunitprice' + idx).setValue(0);
  },
  quoteQtyChange: function (idx) {
    var qty = Ext.getCmp('addeditquotepartqty_quoteqty' + idx).getValue();
    if (qty > 0) {
      this.getPartUnitPrice(idx, qty);
    } else if (qty < 0) {
      Ext.Msg.alert('Warning', 'Quantity must be greater than 0 to get Unit Price .');
    } else if (qty == 0) {
      Ext.getCmp('addeditquotepartqty_unitprice' + idx).setValue(0);
      Ext.getCmp('addeditquotepartqty_discount' + idx).setValue(0);
      Ext.getCmp('addeditquotepartqty_unitdiscount' + idx).setValue(0);
      this.unitpriceChange(idx);
    }
  },
  getPartUnitPrice: function (idx, qty) {
    Ext.create('M1CRM.model.crmQuotePartAddEdit', {
      PartID: this.getPartid().getValue(),
      PartRevisionID: this.getPartrevision().getValue(),
      PartGroupID: Ext.getCmp('addeditquotepart_partgroupid').getValue(), //'',
      OrganizationID: Ext.getCmp('addnewquote').getRecord().getData().OrganizationID,
      LocationID: Ext.getCmp('addnewquote').getRecord().getData().QuoteLocationID,
      Qty: qty
    }).getIt({
      RequestID: 'partprice',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          Ext.getCmp('addeditquotepartqty_unitprice' + idx).setValue(this.roundWithDecimal(result.ResultObject.PartPrice.DiscountedPrice, 2).toFixed(2));
          Ext.getCmp('addeditquotepartqty_discount' + idx).setValue(result.ResultObject.PartPrice.Discount);
          this.discountChange(idx, this.roundWithDecimal(result.ResultObject.PartPrice.DiscountedPrice, 2).toFixed(2), result.ResultObject.PartPrice.Discount);
        } else {
          var fieldid = 'addeditquotepartqty';
          Ext.getCmp(fieldid + '_unitprice' + idx).setValue(0);
          Ext.getCmp(fieldid + '_discount' + idx).setValue(0);
          Ext.getCmp(fieldid + '_unitdiscount' + idx).setValue(0);
        }
      }
    });
  },
  discountChange: function (idx, unitprice, discount) {
    var calObj = Ext.create('M1CRM.model.crmQuotePartAddEdit', {
      Discount: discount,
      UnitPrice: unitprice
    });
    calObj.calcRevisedUnitPrice();
    Ext.getCmp('addeditquotepartqty_unitdiscount' + idx).setValue(this.roundWithDecimal(calObj.getData().UnitDiscountBase, 2));
    Ext.getCmp('addeditquotepart_revisedunitprice' + idx).suspendEvents();
    Ext.getCmp('addeditquotepart_revisedunitprice' + idx).setValue(this.roundWithDecimal(calObj.getData().FullRevisedUnitPriceBase, 2));
    Ext.getCmp('addeditquotepart_revisedunitprice' + idx).suspendEvents();
    this.calculateTax(idx);
  },
  calculateFirstTax: function (idx) {
    Ext.getCmp('addeditquotepart_unitfirsttax' + idx).setValue(this.roundWithDecimal(Ext.getCmp('addeditquotepart_revisedunitprice' + idx).getValue() * (parseFloat(this.config._taxrate) / 100), 2));
  },
  calculateSecondTax: function (idx) {
    Ext.getCmp('addeditquotepart_unitsecondtax' + idx).setValue(this.roundWithDecimal(Ext.getCmp('addeditquotepart_revisedunitprice' + idx).getValue() * (parseFloat(this.config._secondtaxrate) / 100), 2));
  },
  calculateTax: function (idx) {
    this.calculateFirstTax(idx);
    this.calculateSecondTax(idx);
  },
  unitdiscountChange: function (idx) {
    Ext.getCmp('addeditquotepart_revisedunitprice' + idx).setValue(Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue() - Ext.getCmp('addeditquotepartqty_unitdiscount' + idx).getValue());
    var discount = 100 - ((Ext.getCmp('addeditquotepart_revisedunitprice' + idx).getValue() * 100) / Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue());
    Ext.getCmp('addeditquotepartqty_discount' + idx).setValue(this.roundWithDecimal(discount, 1));
    this.calculateTax(idx);
  },
  revisedunitpriceChange: function (idx) {
    var unitdis = Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue() - Ext.getCmp('addeditquotepart_revisedunitprice' + idx).getValue();
    Ext.getCmp('addeditquotepartqty_unitdiscount' + idx).setValue(this.roundWithDecimal(unitdis, 2));
    this.unitdiscountChange(idx);
  },
  getQuoteQuantity: function () {
    var quoteQuantityArr = [];
    if (parseFloat(Ext.getCmp('addeditquotepartqty_quoteqty1').getValue()) > 0)
      quoteQuantityArr.push(this.getQuoteQuantityObj(1));

    for (var i = 2; i <= 9; i++) {
      if (!Ext.getCmp('addeditquotepart_qty' + i + 'fieldset').isHidden() && parseFloat(Ext.getCmp('addeditquotepartqty_quoteqty' + i).getValue()) > 0) {
        quoteQuantityArr.push(this.getQuoteQuantityObj(i));
      }
    }
    return quoteQuantityArr;
  },
  getQuoteQuantityObj: function (idx) {
    return Ext.create('M1CRM.model.crmQuotePartQty', {
      QuoteID: Ext.getCmp('addnewquote_quoteid').getValue(),
      QuoteLineID: this.getQuotelineid().getValue() == null || this.getQuotelineid().getValue() == "" ? 1 : parseInt(this.getQuotelineid().getValue()),
      QuoteQuantityID: Ext.getCmp('addeditquotepart_qtyid' + idx).getValue() == null || Ext.getCmp('addeditquotepart_qtyid' + idx).getValue() == '' ? 0 : Ext.getCmp('addeditquotepart_qtyid' + idx).getValue(),
      QuoteQuantity: Ext.getCmp('addeditquotepartqty_quoteqty' + idx).getValue(),
      //RevisedUnitPriceBase

      FullRevisedUnitPriceBase: Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue() != null ? Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue() : 0,
      DiscountPercent: Ext.getCmp('addeditquotepartqty_discount' + idx).getValue() != null ? Ext.getCmp('addeditquotepartqty_discount' + idx).getValue() : 0,
      UnitDiscountBase: Ext.getCmp('addeditquotepartqty_unitdiscount' + idx).getValue() != null ? Ext.getCmp('addeditquotepartqty_unitdiscount' + idx).getValue() : 0,
      //FullRevisedUnitPriceBase
      RevisedUnitPriceBase: Ext.getCmp('addeditquotepart_revisedunitprice' + idx).getValue() != null ? Ext.getCmp('addeditquotepart_revisedunitprice' + idx).getValue() : 0,
      UnitTaxAmountBase: Ext.getCmp('addeditquotepart_unitfirsttax' + idx).getValue() != null ? Ext.getCmp('addeditquotepart_unitfirsttax' + idx).getValue() : 0,
      UnitSecondTaxAmountBase: Ext.getCmp('addeditquotepart_unitsecondtax' + idx).getValue() != null ? Ext.getCmp('addeditquotepart_unitsecondtax' + idx).getValue() : 0
    });
  },

  checkPartID: function () {
    this.config._tmpitemCount = 0;
    var $part = this.getPartid();
    var $form = this.getForm();
    
    if (!$part.isFocused && $part.getValue() && Ext.getCmp($form.id).config._mode !== 'view') {
      Ext.create('M1CRM.model.crmQuotePartAddEdit', {
        PartID: this.getPartid().getValue()
      }).getIt({
        RequestID: 'checkrevisions',
        scope: this,
        onReturn: function (result) {
          if (result != null) {
            if (this.config._propPartsMustExist == true && result.RecordFound == false) {
              Ext.Msg.alert("Warning", "Part does not exist in the database",
                function (btn) {
                  if (btn == 'ok') {
                    this.getPartid().setValue('');
                    this.getPartid().focus();
                  }
                }, this);
            } else if (this.config._propPartsMustExist == false && result.RecordFound == false && Ext.getCmp(this.getForm().id).config._mode != 'view') {
              this.getPartrevision().setReadOnly(false);
              this.getPartdesc().setReadOnly(false);
              this.getUom().setReadOnly(false);
              this.getPartrevision().focus();
            } else if (parseInt(result.RecCount) > 1) {
              this.config._tmpitemCount = parseInt(result.ResultObject.PartRevision.PartRevisionCount);
              this.getPartrevision().setReadOnly(false);
              this.displayPartRevisions();
            } else if (parseInt(result.RecCount) == 1) {
              this.config._tmpitemCount = parseInt(result.ResultObject.PartRevision.PartRevisionCount);
              if (result.ResultObject != null) {
                this.getPartrevision().setValue(result.ResultObject.PartRevision.RevisionID);
                this.getPartrevision().setReadOnly(false);
                this.getPartdesc().setValue(result.ResultObject.PartRevision.ShortDescription);
                this.getUom().setValue(result.ResultObject.PartRevision.IUoM);
                this.setTaxIDInfo(result.ResultObject.PartRevision);
              }
            }
          }
        }
      });
    }
  },
  partRevisionBlur: function () {
    if (this.getPartid().getValue() != '' && !this.getPartrevision().getReadOnly() && this.getPartrevision().getValue() === '' && this.config._tmpitemCount > 0 && this.config._propPartsMustExist) {
      Ext.Msg.alert('', 'There are multiple revisions for this part, enter required revision id.',
        function (btn) {
          if (btn === 'ok') {
            this.getPartrevision().focus();
          }
        }, this);
    } else {
      this.partRevisionChange();
    }
  },
  clearPartInfo: function (flag) {
    if (flag) {
      this.getPartid().setValue('');
      this.getPartid().focus();
    }
    this.getPartrevision().setValue('');
    this.getPartrevision().setReadOnly(flag);
    this.getPartdesc().setValue('');
    this.getUom().setValue('');
    this.clearQtyBreaks();
    this.getCustomerTaxCodes();
    this.getNontaxreason().setValue(this.config._propNonTaxReasonID);
    this.getNontaxreason().isDirty = false;
    this.getFirm().uncheck();
    this.clearAllQtyBreaks();
    this.hideAllQtyBreaks();
  },
  partRevisionChange: function () {
    var $partRevision = this.getPartrevision();
    if (!$partRevision.isFocused && this.getPartid().getValue() && !$partRevision.getReadOnly() && this.config._tmpitemCount > 0) {
      this.getPartInfo(true);
    }
  },
  getPartInfo: function (flag) {
    Ext.create('M1CRM.model.crmQuotePartAddEdit', {
      PartID: this.getPartid().getValue(),
      PartRevisionID: this.getPartrevision().getValue()
    }).getIt({
      RequestID: 'partinfo',
      scope: this,
      onReturn: function (result) {
        if (result != null && result.RecordFound == true) {
          if (flag) {

            if (result.ResultObject.PartRevision.EffectiveEndDate != null && parseDate(this.getFormatDateFromJSonDate(result.ResultObject.PartRevision.EffectiveEndDate)) < new Date()) {
              msg = 'Part Revision has expired';
              Ext.Msg.alert('', msg,
                function (btn) {
                  if (btn == 'ok') {
                    if (flag) {
                      this.getPartrevision().setValue('');
                      this.getPartrevision().focus();
                    }
                  }
                }, this);
            } else {
              this.getPartdesc().setValue(result.ResultObject.PartRevision.ShortDescription);
              this.getUom().setValue(result.ResultObject.PartRevision.IUoM);
              this.setTaxIDInfo(result.ResultObject.PartRevision);
            }
          } else {
            this.viewInfo('part', 'addeditquotepart', null, null, null, null, null, null,
              this.getPartid().getValue(), this.getPartrevision().getValue(), null);

          }
        } else if (result != null && result.RecordFound == false && this.config._propPartsMustExist == true) {
          msg = flag ? 'Part Revision does not exist in the database.' : 'Part does not exist in the database.';
          Ext.Msg.alert('', msg, function (btn) {
            if (btn === 'ok') {
              if (flag) {
                
                this.clearPartInfo(false);
              }
            }
          }, this);
        }
      }
    });
  },
  displayPartRevisions: function () {
    var view = M1CRM.util.crmViews.getPartRevisions();
    view.config._parentformid = this.getForm().id;
    view.config._detailformid = null;
    this.getForm().setRecord(this.getPartRevisionDataObject());
    this.slideLeft(view);
  },
  getPartRevisionDataObject: function () {
    var dataObj = Ext.create('M1CRM.model.crmPartRevision', {});
    dataObj.data.user = SessionObj.statics.EmpId;
    dataObj.data.PartID = this.getPartid().getValue();
    dataObj.data.ParentId = this.getForm().id;
    return dataObj;
  },
  clearQtyBreaks: function () {
    if (Ext.getCmp(this.getForm().id).config._mode == 'addnew') {
      this.showHideAdditionalQtyCtrls(true);
    }
  },
  setTaxIDInfo: function (rec) {
    //this.getTaxidsel().suspendEvents();
    this.getTaxidsel().setValue(rec.TaxCodeID);
    this.getTaxid2sel().setValue(rec.SecondTaxCodeID);
    this.getTaxidsel().isDirty = true;
    this.getTaxid2sel().isDirty = true;
    this.getNontaxreason().setValue(rec.NonTaxReasonID);
    //this.getTaxidsel().resumeEvents(true);

  },
  partinfoTap: function () {
    if (this.getPartid().getValue() != '') {
      this.getPartInfo(false);
    }
  },
  unitpriceChange: function (idx) {
    var value = Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue() != null ? Ext.getCmp('addeditquotepartqty_unitprice' + idx).getValue() : 0;
    var discount = Ext.getCmp('addeditquotepartqty_discount' + idx).getValue() != null ? Ext.getCmp('addeditquotepartqty_discount' + idx).getValue() : 0;
    Ext.getCmp('addeditquotepart_revisedunitprice' + idx).setValue(this.roundWithDecimal(value, 2).toFixed(2));
    this.discountChange(idx, this.roundWithDecimal(value, 2).toFixed(2), discount);
  },
  setFocusOnQty: function (idx) {
    if (Ext.getCmp('addeditquotepartqty_quoteqty' + idx).getValue() == 0 || Ext.getCmp('addeditquotepartqty_quoteqty' + idx).getValue() == null) {
      Ext.getCmp('addeditquotepartqty_quoteqty' + idx).focus();
    }
  }
});
