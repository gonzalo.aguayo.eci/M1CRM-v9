Ext.define('M1CRM.controller.crmContacts', {
  extend: 'M1CRM.util.crmListControllerBase',  //'Ext.app.Controller',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmContacts',
    'M1CRM.store.crmContacts'
  ],
  config: {
    _listid: 'contactlist',
    _seardfieldid: 'contact_search',

    control: {
      contacts: {
        initialize: 'initializeList',
        show: 'onShowContacts',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.viewDetail(this.config._listid, item);
        },
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },
  init: function () {
    this.control({
      '#contactlist_addnew': {
        scope: this,
        tap: this.addNewContactTap
      }
    });
  },
  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labContactsSummary';
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmContacts', {}).getListLayout());
  },
  onShowContacts: function (obj, e) {
    this.initilize();
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {});
    this.loadDatafromDB('crmContacts', dataObj);
  },
  addNewContactTap: function () {
    var view = M1CRM.util.crmViews.getCustomerContactAddEdit();  //getAddNewContact();
    Ext.getCmp(view.id).config._mode = 'addnew';
    view.config._parentformid = this.config._listid;
    M1CRM.util.crmViews.slideLeft(view);
  }

});
