Ext.define('M1CRM.controller.crmCallLines', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmCallLines',
    'M1CRM.store.crmCallLines'
  ],

  config: {
    _listid: 'calllineslist',
    _seardfieldid: 'callline_search',
    control: {
      calllines: {
        initialize: 'initializeForm',
        show: 'onShowList',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.viewDetail('calllineslist', item);
        },
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },

  init: function () {
    this.control({
      '#calllineslist_addnew': {
        scope: this,
        tap: this.addNewCallLineTap
      }
    });
  },
  initializeForm: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labCallLinesSummary';
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmCallLines', {}).getListLayout());
  },
  onShowList: function (obj, e) {
    this.initilize();
    //Ext.getCmp('callline_search').setValue('');
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.loadFromDB();
    //this.doUserSecurityCheck(this, obj);
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    var rec = Ext.getCmp('addeditcall').getRecord().getData();

    //var dataObj = Ext.create('M1CRM.model.crmCallLines', {});
    //dataObj.data.callid = rec.CallID != null ? rec.CallID  : rec.kbpCallID
    //dataObj.data.user =  SessionObj.getEmpId();

    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
      CallID: rec.CallID,
      AssignedToEmployeeID: SessionObj.getEmpId()
    });


    this.loadDatafromDB('crmCallLines', dataObj);
  },
  addNewCallLineTap: function () {
    if (Ext.getCmp('calllineaddedit') != null && Ext.getCmp('calllineaddedit').getRecord() != null)
      Ext.getCmp('calllineaddedit').setRecord(null);
    var view = M1CRM.util.crmViews.getCallLineAddEdit();
    view.config._parentformid = this.config._listid;
    Ext.getCmp(view.id).config._mode = 'addnew';
    this.slideLeft(view);
  }
});
