Ext.define('M1CRM.controller.crmLeadsCloseNextMonth', {
  extend: 'M1CRM.util.crmLeadsListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmLeads',
    'M1CRM.store.crmLeads'
  ],
  config: {
    _listid: 'leadsclosenextmonthlist',
    _seardfieldid: 'leadsclosenextmonth_search',
    control: {
      leadsclosenextmonth: {
        initialize: 'initializeList',
        show: 'onShow',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.viewDetail('leadsclosenextmonthlist', item);
        },
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },

  init: function () {
    this.control({


      '#leadsclosenextmonthlist_addnew': {
        cope: this,
        tap: function () {
          Ext.Viewport.getActiveItem().setRecord(null);
          this.addNewLeadsTap();
        }
      }
    });
  },

  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labLeadsCloseNextMonthSummary';
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmLeads', {}).getOpenLeadsLayout());


  },

  createOptionMenu: function () {
    Ext.Viewport.setMenu(this.createMenu(this.getOptionMenuItems(this)), {
      side: 'right',
      reveal: true
    });
  },

  onShow: function (obj, e) {
    this.initilize();
    this.createOptionMenu();
    this.loadData(obj);

  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  getDataObject: function () {
    Ext.getCmp(this.config._listid).setGrouped(true);
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {});
    return dataObj;
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmLeads', this.getDataObject(), 'crmLeadsCloseByMonth');
  }
});
