Ext.define('M1CRM.controller.crmParts', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'M1CRM.model.crmParts',
    'M1CRM.store.crmParts',
    'M1CRM.model.crmDataFilterModel'
  ],
  config: {
    _listid: 'partslist',
    _seardfieldid: 'parts_search',
    control: {
      parts: {
        show: 'onShow',
        itemtap: 'onPartsTap',
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },
  onShow: function (obj, e) {    
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labPartsSummary';
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmParts', {}).getListLayout());
    this.initilize();
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj, true);
  },
  loadDataAfterSecurityCheck: function (obj, pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
      EmployeeID: SessionObj.getEmpId()
    });
    var tparentfmid = Ext.getCmp(this.config._listid).config._parentformid;
    if (tparentfmid != 'crmhome' && Ext.isDefined(Ext.getCmp(tparentfmid + '_partid')) && Ext.getCmp(tparentfmid + '_partid').getValue() != '') {
      this.setSearchFieldValue(Ext.getCmp(tparentfmid + '_partid').getValue());
      Ext.getCmp(tparentfmid + '_partid').setValue();
    }
    this.loadDatafromDB('crmParts', dataObj);
  },
  onPartsTap: function (nestedlist, list, index, item, rec, e) {
    var view = null;
    var searchfield = Ext.ComponentQuery.query('#' + this.config._listid + ' searchfield')[0].id;
    Ext.getCmp(searchfield).setValue('');

    if (Ext.isDefined(Ext.getCmp(this.config._listid).config._haslistchange)) {
      Ext.getCmp(this.config._listid).config._haslistchange = false;
    }

    if (Ext.getCmp(this.config._listid).config._parentformid != null) {
      if (Ext.getCmp(this.config._listid).config._parentformid == 'crmhome') {
        view = crmViews.self.getPartDetail();
        view.config._parentformid = this.config._listid;
        view.setRecord(item);
        M1CRM.util.crmViews.slideLeft(view);
      } else {
        view = crmViews.self.getViewByParentFormId(Ext.getCmp(this.config._listid).config._parentformid);
        view.setRecord(item);
        M1CRM.util.crmViews.slideRight(view);
      }

    }
    else {
      view = M1CRM.util.crmViews.getPartDetail();
      view.setRecord(item);
      M1CRM.util.crmViews.slideLeft(view);
    }
  }


});
