Ext.define('M1CRM.controller.crmCustomers', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmCustomers',
    'M1CRM.store.crmCustomers'
  ],
  config: {
    _listid: 'customerlist',
    _seardfieldid: 'customer_search',
    control: {
      customers: {
        initialize: 'initializeList',
        show: 'showForm',
        itemtap: 'onCustomerTap',
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },
  init: function () {
    this.control({
      '#customerlist_addnew': {
        scope: this,
        tap: 'addNewCustomerTap'
      }
    });
  },
  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labCustSummary';
    Ext.getCmp(this.config._listid + '_toolbarid').setTitle(SessionObj.statics.OrgLabel + 's');
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmCustomers', {}).getListLayout());
  },
  showForm: function (obj, e) {
    this.initilize();
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj, true);
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
      EmployeeID: SessionObj.getEmpId()
    });

    if (Ext.getCmp(this.config._listid).config._parentformid == 'addnewquote' || Ext.getCmp(this.config._listid).config._parentformid == 'addeditlead') {
      dataObj.data.ListType = 'active_prospective';
    } else {
      dataObj.data.ListType = 'all';
    }
    this.loadDatafromDB('crmCustomers', dataObj);
  },
  getPreviousView: function () {
    var view = null;
    if (Ext.getCmp(this.config._listid).config._parentformid != null) {
      switch (Ext.getCmp(this.config._listid).config._parentformid) {
        case  'addnewmyfollowup' : {
          view = M1CRM.util.crmViews.getAddNewFollowup();
          break;
        }
        case 'followupaddedit' : {
          view = M1CRM.util.crmViews.getFollowupAddedit();
          break;
        }
        case 'addeditcall': {
          view = M1CRM.util.crmViews.getAddEditCall();
          break;
        }
        case 'customercontactaddedit' : {
          view = M1CRM.util.crmViews.getCustomerContactAddEdit();
          break;
        }
        case 'addnewquote' : {
          view = M1CRM.util.crmViews.getAddNewQuote();
          break;
        }
        case 'addeditlead' : {
          view = M1CRM.util.crmViews.getAddEditLead();
          break;
        }

        default : {
          view = M1CRM.util.crmViews.getHome();
          break;
        }
      }
      Ext.getCmp(this.config._listid).config._parentformid = null;
    }
    else {
      view = M1CRM.util.crmViews.getHome();
    }
    return view;
  },
  onCustomerTap: function (nestedlist, list, index, item, rec, e) {


    var view = null;
    var searchfield = Ext.ComponentQuery.query('#' + this.config._listid + ' searchfield')[0].id;
    Ext.getCmp(searchfield).setValue('');

    if (Ext.getCmp(this.config._listid).config._parentformid != null) {
      if (Ext.getCmp(this.config._listid).config._parentformid == 'crmhome') {
        view = M1CRM.util.crmViews.getCustomerAddEdit();
        view.setRecord(item);
        view.config._parentformid = this.config._listid;
        Ext.getCmp(this.config._listid).config._parentformid = null;
        M1CRM.util.crmViews.slideLeft(view);
      } else {
        if (Ext.isDefined(Ext.getCmp(this.config._listid).config._haslistchange)) {
          Ext.getCmp(this.config._listid).config._haslistchange = true;
          Ext.getCmp(this.config._listid).config._pageid = 1;
        }
        view = this.getPreviousView();
        view.setRecord(item);
        this.slideRight(view);
      }
    }
    else {
      if (Ext.isDefined(Ext.getCmp(this.config._listid).config._haslistchange)) {
        Ext.getCmp(this.config._listid).config._haslistchange = false;
      }
      view = M1CRM.util.crmViews.getCustomerAddEdit();
      view.setRecord(item);
      this.slideLeft(view);
    }
  },
  addNewCustomerTap: function () {
    if (Ext.isDefined(Ext.getCmp(this.config._listid).config._haslistchange)) {
      Ext.getCmp(this.config._listid).config._haslistchange = true;
      Ext.getCmp(this.config._listid).config._pageid = 1;
    }

    var view = M1CRM.util.crmViews.getCustomerAddEdit();
    view.config._parentformid = this.config._listid;
    if (Ext.getCmp('customeraddedit').getRecord() != null)
      Ext.getCmp('customeraddedit').setRecord(null);
    this.slideLeft(view);
  }

});
