Ext.define('M1CRM.controller.crmCustomerFollowups', {
  extend: 'M1CRM.util.crmListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmFollowups',
    'M1CRM.store.crmFollowups',
    'M1CRM.model.crmFollowupTask',
    'M1CRM.store.crmFollowupTask',
    'M1CRM.model.crmFollowupStatus',
    'M1CRM.store.crmFollowupStatus',
    'M1CRM.model.crmCustomerFollowupAddEdit'
  ],
  config: {
    _listid: 'customerfollowupslist',
    _seardfieldid: 'custfollowup_search',
    control: {
      customerfollowups: {
        initialize: 'initializeList',
        show: 'onShow',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.viewDetail('customerfollowupslist', item);
        },
        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }
      }
    }
  },
  init: function () {
    this.control({
      '#customerfollowupslist_addnew': {
        scope: this,
        tap: this.optionsNewCustFollowupTap
      }
    });
  },
  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labCustomerFollowupSummary';
    Ext.getCmp('customerfollowupslist_toolbarid').setTitle('Linked Follow-ups');
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmFollowups', {}).getMyFollowupListLayout());
  },
  onShow: function (obj, e) {
    this.initilize();
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  loadDataAfterSecurityCheck: function (pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  getDataObject: function () {
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
      AssignedToEmployeeID: SessionObj.statics.EmpId
    });

    var parentviewid = this.getParetViewId();
    var tStr = '';

    tStr = '#' + parentviewid + ' #' + parentviewid + '_custid';
    dataObj.data.OrganizationID = Ext.ComponentQuery.query(tStr)[0].getValue();
    tStr = '#' + parentviewid + ' #' + parentviewid + '_customer';
    dataObj.data.OrganizationName = Ext.ComponentQuery.query(tStr)[0].getValue();
    dataObj.data.LocationID = '';

    if (parentviewid == 'customeraddedit') {
      dataObj.data.ListType = 'CUSTOMERFOLLOWUPS';
      this.setSummary('Org:' + dataObj.data.OrganizationID);
    } else if (parentviewid == 'customerlocationaddedit') {
      tStr = '#' + parentviewid + ' #' + parentviewid + '_locationid';
      dataObj.data.LocationID = Ext.ComponentQuery.query(tStr)[0].getValue();
      tStr = '#' + parentviewid + ' #' + parentviewid + '_location';
      dataObj.data.LocationName = Ext.ComponentQuery.query(tStr)[0].getValue();
      dataObj.data.ListType = 'CUSTOMERLOCFOLLOWUPS';
      this.setSummary('Org:' + dataObj.data.OrganizationID + ' Loc : ' + dataObj.data.LocationID);
    } else if (parentviewid == 'customercontactaddedit' || parentviewid == 'addeditcall') {
      tStr = '#' + parentviewid + ' #' + parentviewid + '_locationid';
      dataObj.data.LocationID = Ext.ComponentQuery.query(tStr)[0].getValue();
      tStr = '#' + parentviewid + ' #' + parentviewid + '_location';
      dataObj.data.LocationName = Ext.ComponentQuery.query(tStr)[0].getValue();
      tStr = '#' + parentviewid + ' #' + parentviewid + '_contactid';
      dataObj.data.ContactID = Ext.ComponentQuery.query(tStr)[0].getValue();
      if (dataObj.data.LocationID != null && dataObj.data.LocationID != '') {
        this.setSummary('Org:' + dataObj.data.OrganizationID + ' Loc:' + dataObj.data.LocationID + ' Contact:' + dataObj.data.ContactID);
      } else {
        this.setSummary('Org:' + dataObj.data.OrganizationID + ' Contact:' + dataObj.data.ContactID);
      }
      if (parentviewid == 'customercontactaddedit') {
        dataObj.data.ListType = 'CUSTOMERCONTACTFOLLOWUPS';
        tStr = '#' + parentviewid + ' #' + parentviewid + '_contactname';
        dataObj.data.ContactName = Ext.ComponentQuery.query(tStr)[0].getValue();
      } else {
        dataObj.data.ListType = 'CALLFOLLOWUPS';
        tStr = '#' + parentviewid + ' #' + parentviewid + '_callid';
        dataObj.data.CallID = Ext.ComponentQuery.query(tStr)[0].getValue();
        dataObj.data.ContactName = Ext.getCmp('addeditcall_contactname').getValue();
        this.setSummary('Call:' + dataObj.data.CallID);
      }
      tStr = '#' + parentviewid + ' #' + parentviewid + '_leadid';
      if (Ext.ComponentQuery.query(tStr).length > 0)
        dataObj.data.LeadID = Ext.ComponentQuery.query(tStr)[0].getValue();

    } else if (parentviewid == 'addnewquote') {
      dataObj.data.ListType = 'QUOTEFOLLOWUPS';
      tStr = '#' + parentviewid + ' #' + parentviewid + '_quoteid';
      dataObj.data.QuoteID = Ext.ComponentQuery.query(tStr)[0].getValue();
      tStr = '#' + parentviewid + ' #' + parentviewid + '_quotelocationid';
      dataObj.data.LocationID = Ext.ComponentQuery.query(tStr)[0].getValue();
      tStr = '#' + parentviewid + ' #' + parentviewid + '_quotelocation';
      dataObj.data.LocationName = Ext.ComponentQuery.query(tStr)[0].getValue();
      tStr = '#' + parentviewid + ' #' + parentviewid + '_quotecontactid';
      dataObj.data.ContactID = Ext.ComponentQuery.query(tStr)[0].getValue();
      tStr = '#' + parentviewid + ' #' + parentviewid + '_quotecontact';
      dataObj.data.ContactName = Ext.ComponentQuery.query(tStr)[0].getValue();
      this.setSummary('Quote :' + dataObj.data.QuoteID + ' Org:' + dataObj.data.OrganizationID);
    } else if (parentviewid = 'addeditlead') {
      dataObj.data.ListType = 'LEADFOLLOWUPS';
      tStr = '#' + parentviewid + ' #' + parentviewid + '_leadid';
      dataObj.data.LeadID = Ext.ComponentQuery.query(tStr)[0].getValue();
      tStr = '#' + parentviewid + ' #' + parentviewid + '_invlocationid';
      dataObj.data.LocationID = Ext.ComponentQuery.query(tStr)[0].getValue();
      tStr = '#' + parentviewid + ' #' + parentviewid + '_invlocation';
      dataObj.data.LeadLocation = Ext.ComponentQuery.query(tStr)[0].getValue();
      tStr = '#' + parentviewid + ' #' + parentviewid + '_leadcontactid';
      dataObj.data.ContactID = Ext.ComponentQuery.query(tStr)[0].getValue();
      tStr = '#' + parentviewid + ' #' + parentviewid + '_leadcontact';
      dataObj.data.ContactName = Ext.ComponentQuery.query(tStr)[0].getValue();
      this.setSummary('Lead :' + dataObj.data.LeadID + ' Org:' + dataObj.data.OrganizationID);
    }
    return dataObj;
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmFollowups', this.getDataObject());
  },
  optionsNewCustFollowupTap: function () {
    var view = M1CRM.util.crmViews.getFollowupAddedit();
    view.config._parentformid = this.config._listid;
    view.setRecord(this.getDataObject());
    Ext.getCmp(view.id).config._mode = 'addnew';
    this.slideLeft(view);
  }

});
