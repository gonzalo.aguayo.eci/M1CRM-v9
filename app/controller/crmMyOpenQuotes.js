Ext.define('M1CRM.controller.crmMyOpenQuotes', {
  extend: 'M1CRM.util.crmQuotesListControllerBase',
  requires: [
    'M1CRM.model.crmUserSecurity',
    'M1CRM.model.crmQuotations',
    'M1CRM.store.crmQuotations'
  ],
  config: {
    _listid: 'myopenquoteslist',
    _seardfieldid: 'myopenquotes_search',
    control: {
      myopenquotes: {
        initialize: 'initializeList',
        show: 'onShow',
        itemtap: function (nestedlist, list, index, item, rec, e) {
          this.hideOptionsMenu();
          this.viewDetail('myopenquoteslist', item);
        },

        pop: function (objnevView, objview) {
          this.onNewContainerPop(objnevView, objview);
        }

      }
    }
  },

  init: function () {
    this.control({
      '#myopenquoteslist_addnew': {
        cope: this,
        tap: function () {
          Ext.Viewport.getActiveItem().setRecord(null);
          this.addNewQuoteTap();
        }
      }
    });
  },

  initializeList: function () {
    this.config._nextPageId = this.config._listid + '_ToNextPage';
    this.config._prevPageId = this.config._listid + '_ToPrevPage';
    this.config._navigateSummaryId = 'labMyOpenQuotesSummary';
    Ext.getCmp(this.config._listid).setItemTpl(Ext.create('M1CRM.model.crmQuotations', {}).getMyOpenQuoteLayout());

  },
  createOptionMenu: function () {
    Ext.Viewport.setMenu(this.createMenu(this.getOptionMenuItems(this)), {
      side: 'right',
      reveal: true
    });
  },
  onShow: function (obj, e) {
    this.initilize();
    this.createOptionMenu();
    this.loadData(obj);
  },
  loadData: function (obj) {
    this.doUserSecurityCheck(this, obj);
  },
  getDataObject: function () {
    var dataObj = Ext.create('M1CRM.model.crmDataFilterModel', {
      AssignedToEmployeeID: SessionObj.getEmpId()
    });

    if (Ext.getCmp(this.config._listid).config._parentformid == 'crmcustomerdateil') {
      dataObj.data.OrganizationID = Ext.getCmp('crmcustomerdateil').getRecord().getData().OrganizationID;
    }
    return dataObj;

  },
  loadDataAfterSecurityCheck: function (obj, pAccess, sAccess) {
    if (this.applyUserSecuritySettings(this.config._listid, parseInt(pAccess), parseInt(sAccess))) {
      this.loadFromDB();
    }
  },
  loadFromDB: function () {
    this.loadDatafromDB('crmQuotations', this.getDataObject());
  }
});
